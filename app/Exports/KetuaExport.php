<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class KetuaExport implements FromCollection,WithHeadings
{
    public function headings(): array {
        return [
          "Kode Register", "No KTP / Passport","No NPWP","No. NRKA / No. STRA","SKA Sub","SKA Kualifikasi","Nama","ID Pengguna","No HP","Email","Provinsi","Kota / Kabupaten","Alamat","Kode Pos","Tanggal Berakhir SKA"
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::select('register_code','ktp_no','npwp_no','nrka_no','ska_sub_id','ska_kualifikasi_id','name','username','phone','email','province_id','city_id','address','postcode','tanggal_ska')->where('role',4)->where('user_id',0)->get();
    }
}
