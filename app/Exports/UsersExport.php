<?php

namespace App\Exports;

// use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
// use Maatwebsite\Excel\Concerns\WithHeadings;
// use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


// class UsersExport implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize, WithStyles
class UsersExport implements FromView, ShouldAutoSize, WithStyles, WithColumnFormatting
{
    
    use Exportable;
    private $jumlah_row = 0;
    public function query()
    {
        // return User::all();
        $jenis_say =  DB::table(DB::raw("
                            (SELECT
                                (@cnt := @cnt + 1) AS rowNumber,
                               *
                            FROM
                                `users`
                            CROSS JOIN (SELECT @cnt := 0) AS dummy
                            WHERE NOT
                                (
                                    jenis_sayembara IS NULL OR jenis_sayembara = '' OR jenis_sayembara = '[]'
                                ) AND user_id = 0 AND role = 4) as k
                        "))->orderBy('id', 'ASC');
        $this->jumlah = $jenis_say->count(); 
        return $jenis_say;
    }

    public function view(): View
    {
            $data =    DB::select(DB::raw("
                                            SELECT 
                                                a.id,
                                                a.name,
                                                a.email,
                                                a.phone,
                                                a.nrka_no,
                                                a.tanggal_ska,
                                                a.address,
                                                a.postcode as ketua_kode_pos,
                                                a.ktp_no,
                                                a.ska_kualifikasi_id,
                                                IFNULL(ff.name, '-') as nama_ska_ketua,
                                                d.name as ketua_provinsi,
                                                g.name as ketua_negara,
                                                i.name as ketua_kota,

                                                (SELECT COUNT(id) FROM users n WHERE n.user_id = a.id ) as jumlah_anggota,
                                                IF((SELECT COUNT(id) FROM users t WHERE jenis_sayembara LIKE '%1%' AND t.id = a.id) > 0, 1, 0) as sayembara_1,
                                                IF((SELECT COUNT(id) FROM users t WHERE jenis_sayembara LIKE '%2%' AND t.id = a.id) > 0, 1, 0) as sayembara_2,
                                                IF((SELECT COUNT(id) FROM users t WHERE jenis_sayembara LIKE '%3%' AND t.id = a.id) > 0, 1, 0) as sayembara_3,
                                                IF((SELECT COUNT(id) FROM users t WHERE jenis_sayembara LIKE '%4%' AND t.id = a.id) > 0, 1, 0) as sayembara_4,
                                                
                                                
                                                c.name as nama_perusahaan,
                                                c.siujk_date as tagl_siujk,
                                                c.sbu as sbu,
                                                c.sbu_no as nomor_sbu,

                                                b.user_id,
                                                b.id as id_anggota, 
                                                b.name as name_anggota,
                                                b.email as email_anggota,
                                                b.ktp_no as ktp_anggota,
                                                b.phone as telepon_anggota,
                                                b.ska_kualifikasi_id as ska_anggota,
                                                b.nrka_no as nrka_anggota,
                                                b.tanggal_ska as tgl_ska_anggota,
                                                b.address as alamat_anggota,
                                                b.postcode as anggota_kode_pos,
                                                
                                                IFNULL(f.name, '-') as nama_ska_anggota,
                                                e.name as provinsi_anggota,
                                                h.name as negara_anggota,
                                                j.name as kota_anggota,
                                                c.npwp_no as npwp_perusahaan
                                            FROM `users` a 
                                            LEFT JOIN users b ON a.id = b.user_id
                                            LEFT JOIN user_companies c ON a.id = c.user_id
                                            LEFT JOIN provinces d ON a.province_id = d.id
                                            LEFT JOIN provinces e ON b.province_id = e.id
                                            LEFT JOIN qualifications f ON b.ska_kualifikasi_id = f.id
                                            LEFT JOIN qualifications ff ON a.ska_kualifikasi_id = ff.id
                                            LEFT JOIN countries g ON a.country_id = g.id
                                            LEFT JOIN countries h ON b.country_id = h.id
                                            LEFT JOIN cities i ON a.city_id = i.id
                                            LEFT JOIN cities j ON b.city_id = j.id
                                            WHERE a.user_id = 0 AND a.role = 4
                                            ORDER BY a.id ASC
                            "));
        $this->jumlah_row = count($data);
        return view('pages.statistik.user_export', [
            'data' => $data
        ]);
    }
    
    public function styles(Worksheet $sheet)
    {
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];
         $block_field = 'A2:AI'.($this->jumlah_row + 2);
         $sheet->getStyle($block_field)->applyFromArray($styleArray);
         $sheet->getStyle($block_field)->getAlignment()->applyFromArray(
            array('vertical' => 'center') //left,right,center & vertical
        );


    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function map($user): array
    {

        return [
            $user->rowNumber,
            $user->name,
            $user->ktp_no,
            $user->npwp_no,
            $user->nrka_no,
            $user->jenis_sayembara
        ];
    }

    public function headings(): array
    {
        return [
            ['Rekapitulasi Statistik Peserta'], 
            [
                'No',
                'Nama',
                'Nomor Ktp',
                'Nomor NPWP',
                'Nomor RKA',
                'Jenis Sayembara'
            ]
        ];
    }
}
