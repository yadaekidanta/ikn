<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\UserCompany as Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $collection = Company::where('name','LIKE','%'.$keywords.'%')->paginate(10);
            return view('pages.admin.company.list',compact('collection'));
        }
        return view('pages.admin.company.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Company $company)
    {
        //
    }
    public function edit(Company $company)
    {
        //
    }
    public function update(Request $request, Company $company)
    {
        //
    }
    public function destroy(Company $company)
    {
        //
    }
}
