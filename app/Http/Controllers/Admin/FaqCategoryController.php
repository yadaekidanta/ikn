<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FaqCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class FaqCategoryController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax() ) {
            $keywords = $request->keywords;
            $collection = FaqCategory::where('name','LIKE','%'.$keywords.'%')->paginate(10);
            return view('pages.admin.faq-category.list',compact('collection'));
        }
        return view('pages.admin.faq-category.main');
    }
    public function create()
    {
        return view('pages.admin.faq-category.input',['data'=> new FaqCategory]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:faq_category',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $faqCategory = New FaqCategory;
        $faqCategory->name = Str::title($request->name);
        $faqCategory->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori tersimpan',
        ]);
    }
    public function show(FaqCategory $faqCategory)
    {
        // 
    }
    public function edit(FaqCategory $faqCategory)
    {
        return view('pages.admin.faq-category.input',['data'=> $faqCategory]);
    }
    public function update(Request $request, FaqCategory $faqCategory)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:faq_category',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $faqCategory->name = Str::title($request->name);
        $faqCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori terubah',
        ]);
    }
    public function destroy(FaqCategory $faqCategory)
    {
        $faqCategory->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori '. $faqCategory->name . ' terhapus',
        ]);
    }
}
