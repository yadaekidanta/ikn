<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\FaqCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax() ) {
            $keywords = $request->keywords;
            $collection = Faq::where('question','LIKE','%'.$keywords.'%')->orWhere('answer','LIKE','%'.$keywords.'%')->paginate(10);
            return view('pages.admin.faq.list',compact('collection'));
        }
        return view('pages.admin.faq.main');
    }
    public function create()
    {
        $category = FaqCategory::get();
        return view('pages.admin.faq.input', ['data' => new Faq, 'category' => $category]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'faq_category_id' => 'required',
            'question' => 'required',
            'answer' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('faq_category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('faq_category_id'),
                ]);
            }else if ($errors->has('question')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('question'),
                ]);
            }else if ($errors->has('answer')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('answer'),
                ]);
            }
        }
        $data = New Faq;
        $data->faq_category_id = $request->faq_category_id;
        $data->question = $request->question;
        $data->answer = $request->answer;
        $data->created_at = date('Y-m-d H:i:s');
        $data->created_by = Auth::user()->id;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'FAQ tersimpan',
        ]);
    }
    public function show(Faq $faq)
    {
        //
    }
    public function edit(Faq $faq)
    {
        $category = FaqCategory::get();
        return view('pages.admin.faq.input', ['data' => $faq, 'category' => $category]);
    }
    public function update(Request $request, Faq $faq)
    {
        $validator = Validator::make($request->all(), [
            'faq_category_id' => 'required',
            'question' => 'required',
            'answer' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('faq_category_id')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('faq_category_id'),
                ]);
            }else if ($errors->has('question')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('question'),
                ]);
            }else if ($errors->has('answer')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('answer'),
                ]);
            }
        }
        $faq->faq_category_id = $request->faq_category_id;
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->created_at = date('Y-m-d H:i:s');
        $faq->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'FAQ tersimpan',
        ]);
    }
    public function destroy(Faq $faq)
    {
        $faq->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'FAQ terhapus',
        ]);
    }
}
