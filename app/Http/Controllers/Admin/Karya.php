<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Karya as Transaksi;
use App\Models\KaryaFile as FileTransaksi;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class Karya extends Controller
{
    //
    private $paging = 10;
    private $kategori_data_name = array(
        1 => 'Data Karya Istana Wakil Presiden',
        2 => 'Data Karya Perkantoran Legislatif',
        3 => 'Data Karya Perkantoran Yudikatif',
        4 => 'Data Karya Peribadatan'   
    );

    private $kategoriToInt = array(
        'wapres' => 1,
        'legislatif' => 2,
        'yudikatif' => 3,
        'peribadatan' => 4,

    );

    public function index(Request $request, $kategori='wapres'){

     
        if(Auth::guard('web')->user()->role >= 4){
            return abort(404);
        }
        $jenis = $this->kategoriToInt[$kategori];
        $jenis = (empty($jenis)) ? 1 : $jenis; 

        // --------------------- Start Rekap Verifikasi Jenis Sayembara ------------//
            $array_for_loop = array(
                1 => array('name' => 'Istana Wakil Presiden', 'color'=>'#ab6c6c'),
                2 => array('name' => 'Perkantoran Legislatif', 'color'=>'#961a2c'),
                3 => array('name' => 'Perkantoran Yudikatif', 'color'=>'#1c3144'),
                4 => array('name' => 'Peribadatan', 'color'=>'#a7a5a5'),
            );
            $data_chart = array();

            foreach($array_for_loop as $key_loop => $array_data){
                $query = "
                            SELECT 
                                (SELECT 
                                    COUNT(id) 
                                FROM `users` 
                                where user_id = 0
                                AND role = 4
                                AND jenis_sayembara LIKE '%$key_loop%') as jumlah_cat_1,

                                (SELECT 
                                    COUNT(a.id)
                                FROM `users` a
                                LEFT JOIN user_verification b ON a.id = b.user_id
                                where a.user_id = 0
                                AND a.role = 4
                                AND b.st IS NOT NULL
                                AND a.jenis_sayembara LIKE '%$key_loop%'
                                AND LOWER(b.st) = 'lulus verifikasi') as lolos_cat_1,

                                (SELECT 
                                    SUM(IF(jumlah_karya = 7, 1, 0)) as jumlah_karya
                                    FROM (SELECT 
                                        karya_id, 
                                        COUNT(karya_id) as jumlah_karya 
                                    FROM `karya_file` a 
                                    RIGHT JOIN karya  b ON a.karya_id = b.id
                                    WHERE a.file IS NOT NULL
                                    AND b.jenis_sayembara = $key_loop
                                    GROUP BY karya_id) as n) as jumlah_karya,
                                
                                (SELECT 
                                    COUNT(jenis_sayembara) as jumlah_lanjut 
                                FROM karya 
                                WHERE LOWER(TRIM(st)) = 'lulus verifikasi'
                                AND jenis_sayembara = $key_loop
                                GROUP BY jenis_sayembara) as jumlah_lanjut
                        ";

                $sayembara =  DB::select(DB::raw($query));

                $jumlah_rekap = array(
                    'kategori' => $array_data['name'], 
                    'jumlah'=> intval(@$sayembara[0]->jumlah_cat_1), // jumlah pemilih
                    'lolos'=> intval(@$sayembara[0]->lolos_cat_1), // jumlah lolos
                    'jumlah_karya'=> intval(@$sayembara[0]->jumlah_karya), // jumlah file yang lengkap 
                    'jumlah_lanjut'=> intval(@$sayembara[0]->jumlah_lanjut), // jumlah lanjut penilaian
                    // 'tidak_lolos'=>intval(@$sayembara[0]->tidak_lolos_cat_1), // jumlah tidak lolos
                    // 'karya' => 0,
                    // 'color'=> $array_data['color']
                );

                $data_chart[] = $jumlah_rekap;
            }
            
            $data_chart = json_encode($data_chart);
        // --------------------- End Rekap Verifikasi Jenis Sayembara ------------//

        if($request->ajax())
        {
            $title_table = $this->kategori_data_name[$jenis];
            $paging = $this->paging;
            $jumlah_sayembara = array();
            $data_sayembara = array();
            for($i=1; $i<= 4; $i++){
                $data_sayembara[$i] = User::select('users.*', 
                                                    'b.id as id_karya', 
                                                    // 'b.urut', 
                                                    'b.no_reg', 
                                                    'b.jenis_sayembara as js', 
                                                    'b.surat_pakta_integritas',
                                                    'b.surat_tanggung_jawab',
                                                    'b.st as status_verifikasi',
                                                    'b.ket as ket_verifikasi',
                                                    'b.verified_by as user_verifikasi',
                                                    'b.updated_at as waktu_verifikasi',
                                                    'c.name as verifikator',
                                                    'd.file_karya as file_karya')
                            ->leftJoin('karya as b', function($join) {
                                $join->on('users.id', '=', 'b.user_id');
                            })
                            ->leftJoin('users as c', function($join){
                                $join->on('b.verified_by', '=', 'c.id')
                                     ->where('c.role', '<=', 3);
                            })
                            ->leftJoin(
                                DB::raw("
                                (SELECT karya_id, GROUP_CONCAT(CONCAT('[',urut,'::',file,'::',id,']')) as file_karya FROM (SELECT v.karya_id, v.urut, MAX(file) as file, id FROM karya_file v WHERE v.urut IS NOT NULL AND file IS NOT NULL GROUP BY v.karya_id, v.urut, id ORDER BY v.karya_id ASC, v.urut ASC) l GROUP BY l.karya_id) d 
                                "), 
                                'b.id', '=', 'd.karya_id'
                            )
                            ->where('users.role', 4)
                            ->where('users.user_id',0)
                            ->where('users.jenis_sayembara','like', '%'.$i.'%')
                            ->where('b.jenis_sayembara',$i);
                           
                $jumlah_sayembara[$i] = $data_sayembara[$i]
                                        ->get()
                                        ->count();
            }
            // DB::enableQueryLog();
            $data_view = $data_sayembara[$jenis]
                        ->paginate($paging)
                        ->withQueryString();
            // $data_view->setBaseUrl('karya/wapres');
            // $data_view->appends(['jenis' => $jenis]);
            // dd(DB::getQueryLog());
            // return;
            
            

            return view('pages.admin.karya.list', compact('jumlah_sayembara','paging','data_view', 'title_table','kategori'));
        }
        return view('pages.admin.karya.main', compact('kategori', 'data_chart'));
    }

    public function verifikasi(Request $request, $jenis, $penilaian, $id){
        $jenis_nilai = array(
            'reset' => NULL,
            'lulus' => 'Lulus Verifikasi',
            'tidak_lulus' => 'Tidak Lulus Verifikasi'
        );

        if(Auth::user()->role == 2){
            unset($jenis_nilai['reset']);
        }

        $jenis = $this->kategoriToInt[$jenis];
        $keterangan = $request->post('keterangan');
        $keterangan = (empty($keterangan)) ? NULL : $keterangan;


        $update_data = array();
        $update_data['st'] = $jenis_nilai[$penilaian];
        $update_data['ket'] = $keterangan;
        $update_data['verified_by'] = Auth::user()->id;
        $update_data['updated_at'] = date('Y-m-d H:i:s');

        try{
            DB::table('karya')
            ->where('user_id', $id)
            ->where('jenis_sayembara', $jenis)
            ->update($update_data);

            return response()->json([
                    'status' => 'success',
                    'message' => 'Data Berhasil Di Update',
            ]);
        }catch(\Illuminate\Database\QueryException $ex){
            return response()->json([
                'status' => 'error',
                'message' => $ex->getMessage(),
            ]);
        }
        

    }
    public function edit_karya(FileTransaksi $fileTransaksi){
        return view('pages.admin.karya.input_karya',['data' => $fileTransaksi]);
    }
    public function update_karya(Request $request, FileTransaksi $fileTransaksi){
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:pdf|max:20000',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('file')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('file'),
                ]);
            }
        }
        if(request()->file('file')){
            Storage::delete($fileTransaksi->file);
            $karya = request()->file('file')->store("karya");
            $fileTransaksi->file = $karya;
        }
        $fileTransaksi->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Karya tersimpan',
        ]);
    }
    public function edit_urut(Transaksi $transaksi){
        return view('pages.admin.karya.input_urut',['data' => $transaksi]);
        // 
    }
    public function update_urut(Request $request, Transaksi $transaksi){
        $cek = Transaksi::where('urut',$request->urut)->where('jenis_sayembara',$transaksi->jenis_sayembara)->get()->count();
        if($cek > 0){
            return response()->json([
                'alert' => 'info',
                'message' => 'No Urut sudah terpakai',
            ]);
        }
        $validator = Validator::make($request->all(), [
            'urut' => 'required|unique:karya,urut,'.$transaksi->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('urut')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('urut'),
                ]);
            }
        }
        $transaksi->urut = $request->urut;
        $transaksi->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'No Urut tersimpan',
        ]);
    }


}
