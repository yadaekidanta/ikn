<?php

namespace App\Http\Controllers\Admin;

use App\Exports\KetuaExport;
use Illuminate\Http\Request;
use App\Models\User as Ketua;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class KetuaController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $madya = Ketua::where('role',4)->where('user_id',0)->where('ska_sub_id',2)->get()->count();
            $utama = Ketua::where('role',4)->where('user_id',0)->where('ska_sub_id',3)->get()->count();
            $collection = Ketua::where('name','LIKE','%'.$keywords.'%')->where('role',4)->where('user_id',0)->paginate(10);
            return view('pages.admin.ketua.list',compact('collection','madya','utama'));
        }
        return view('pages.admin.ketua.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Ketua $ketua)
    {
        //
    }
    public function edit(Ketua $ketua)
    {
        //
    }
    public function update(Request $request, Ketua $ketua)
    {
        //
    }
    public function destroy(Ketua $ketua)
    {
        //
    }
    public function export(){
        return Excel::download(new KetuaExport, 'ketua_'.date('Y-m-d_H-i-s').'.xlsx');
    }
}
