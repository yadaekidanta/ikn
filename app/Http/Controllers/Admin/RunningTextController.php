<?php

namespace App\Http\Controllers\Admin;

use App\Models\RunningText;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class RunningTextController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax() ){
            $keywords = $request->keywords;
            $collection = RunningText::where('name','LIKE','%'.$keywords.'%')->paginate(5);
            return view('pages.admin.running-text.list',compact('collection'));
        }
        return view('pages.admin.running-text.main');
    }
   public function create()
    {
        return view('pages.admin.running-text.input', ['data'=> new RunningText]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $runningText = New RunningText;
        $runningText->name = Str::title($request->name);
        $runningText->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Text Berjalan tersimpan',
        ]);
    }
    public function show(RunningText $runningText)
    {
        //
    }
    public function edit(RunningText $runningText)
    {
        return view('pages.admin.running-text.input',['data'=> $runningText]);
    }
    public function update(Request $request, RunningText $runningText)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $runningText->name = Str::title($request->name);
        $runningText->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Text Berjalan Terupdate',
        ]);
    }
    public function destroy(RunningText $runningText)
    {
        $runningText->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Text Berjalan Terhapus',
        ]);
    }
}
