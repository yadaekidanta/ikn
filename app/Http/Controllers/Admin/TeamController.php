<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User as Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keywords;
            $muda = Team::where('role',4)->where('user_id','!=',0)->where('ska_sub_id',1)->get()->count();
            $madya = Team::where('role',4)->where('user_id','!=',0)->where('ska_sub_id',2)->get()->count();
            $utama = Team::where('role',4)->where('user_id','!=',0)->where('ska_sub_id',3)->get()->count();
            $collection = Team::where('name','LIKE','%'.$keywords.'%')->where('role',4)->where('user_id','!=',0)->paginate(10);
            return view('pages.admin.team.list',compact('collection','muda','madya','utama'));
        }
        return view('pages.admin.team.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Team $team)
    {
        //
    }
    public function edit(Team $team)
    {
        //
    }
    public function update(Request $request, Team $team)
    {
        //
    }
    public function destroy(Team $team)
    {
        //
    }
}
