<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()) {
            $keywords = $request->keywords;
            $admin = User::where('name','LIKE','%'.$keywords.'%')->where('role',1)->get()->count();
            $panitia = User::where('name','LIKE','%'.$keywords.'%')->where('role',2)->get()->count();
            $penilai = User::where('name','LIKE','%'.$keywords.'%')->where('role',3)->get()->count();
            $collection = User::where('name','LIKE','%'.$keywords.'%')->where('role','<',4)->paginate(10);
            return view('pages.admin.user.list',compact('collection','admin','panitia','penilai'));
        }
        return view('pages.admin.user.main');
    }
    public function create()
    {
        $data = new User();
        return view('pages.admin.user.input',compact('data'));
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'username' => 'required|unique:users,username',
            'role' => 'required',
            'no_handphone' => 'required|unique:users,phone',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama_lengkap')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_lengkap'),
                ]);
            }elseif($errors->has('username')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('username'),
                ]);
            }elseif($errors->has('role')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('role'),
                ]);
            }elseif($errors->has('no_handphone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_handphone'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        $data = new User();
        $data->username = $request->username;
        $pass = $request->password;
        $data->password = Hash::make($pass);
        $data->name = $request->nama_lengkap;
        $data->phone = $request->no_handphone;
        $data->email = $request->email;
        $data->role = $request->role;
        $data->st = 'a';
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'User tersimpan',
        ]);
    }
    public function show(User $user)
    {
        //
    }
    public function edit(User $user)
    {
        $data = $user;
        return view('pages.admin.user.input',compact('data'));
    }
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'username' => 'required|unique:users,username,'.$user->id,
            'role' => 'required',
            'no_handphone' => 'required|unique:users,phone,'.$user->id,
            'email' => 'required|email|unique:users,email,'.$user->id,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama_lengkap')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_lengkap'),
                ]);
            }elseif($errors->has('username')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('username'),
                ]);
            }elseif($errors->has('role')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('role'),
                ]);
            }elseif($errors->has('no_handphone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_handphone'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('password')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('password'),
                ]);
            }
        }
        $user->username = $request->username;
        if($request->password){
            $pass = $request->password;
            $user->password = Hash::make($pass);
        }
        $user->name = $request->nama_lengkap;
        $user->phone = $request->no_handphone;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'User tersimpan',
        ]);
    }
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'User terhapus',
        ]);
    }
}
