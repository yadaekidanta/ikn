<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use setasign\Fpdi\Fpdi;
use Illuminate\Http\Request;
use App\Mail\VerificationMail;
use App\Models\UserVerification;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Notification;
use App\Exports\UserVerification AS UserExport;
use App\Notifications\VerificationNotification;

class UserVerificationController extends Controller
{
    private $link_download_kartu = array(
        1=>'wapres',
        2=>'legislatif',
        3=>'yudikatif',
        4=>'peribadatan'
    );
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $link_download_kartu = $this->link_download_kartu;
            $lengkap = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('users.jenis_sayembara','!=','[]')->get()->count();
            $belum = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('users.jenis_sayembara','!=','[]')->get()->count();
            $lulus = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('uv.st','Lulus Verifikasi')->get()->count();
            $tidak = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('uv.st','Tidak Lulus Verifikasi')->get()->count();
            $administrasi = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('users.jenis_sayembara','[]')->get()->count();
            $total = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->get()->count();
            $st = $request->st;
            if($st == "Belum"){
                $collection = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('users.jenis_sayembara','!=','[]')->where('uv.st','!=','Lulus Verifikasi')->where('uv.st','!=','Tidak Lulus Verifikasi')->orderBy('users.id','ASC')->paginate(10);
            }elseif($st == "Lulus"){
                $collection = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('uv.st','Lulus Verifikasi')->orderBy('users.id','ASC')->paginate(10);
            }elseif($st == "Tidak"){
                $collection = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('uv.st','Tidak Lulus Verifikasi')->orderBy('users.id','ASC')->paginate(10);
            }else{
                $collection = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->orderBy('users.id','ASC')->paginate(10);
            }

            // --------------------- Start Rekap Verifikasi Jenis Sayembara ------------//
                $array_for_loop = array(
                    1 => array('name' => 'Istana Wakil Presiden', 'color'=>'#ab6c6c'),
                    2 => array('name' => 'Perkantoran Legislatif', 'color'=>'#961a2c'),
                    3 => array('name' => 'Perkantoran Yudikatif', 'color'=>'#1c3144'),
                    4 => array('name' => 'Peribadatan', 'color'=>'#a7a5a5'),
                );
                $data_chart = array();

                foreach($array_for_loop as $key_loop => $array_data){
                    $query = "
                                SELECT 
                                    (SELECT 
                                        COUNT(id) 
                                    FROM `users` 
                                    where user_id = 0
                                    AND role = 4
                                    AND jenis_sayembara LIKE '%$key_loop%') as jumlah_cat_1,

                                    (SELECT 
                                        COUNT(a.id)
                                    FROM `users` a
                                    LEFT JOIN user_verification b ON a.id = b.user_id
                                    where a.user_id = 0
                                    AND a.role = 4
                                    AND b.st IS NOT NULL
                                    AND a.jenis_sayembara LIKE '%$key_loop%'
                                    AND LOWER(b.st) = 'lulus verifikasi') as lolos_cat_1,

                                    (SELECT 
                                        COUNT(a.id)
                                    FROM `users` a
                                    LEFT JOIN user_verification b ON a.id = b.user_id
                                    where a.user_id = 0
                                    AND a.role = 4
                                    AND b.st IS NOT NULL
                                    AND a.jenis_sayembara LIKE '%$key_loop%'
                                    AND LOWER(b.st) = 'tidak lulus verifikasi') as tidak_lolos_cat_1
                            ";

                    $sayembara =  DB::select(DB::raw($query));

                    $jumlah_rekap = array(
                        'kategori' => $array_data['name'], 
                        'jumlah'=> intval(@$sayembara[0]->jumlah_cat_1), // jumlah pemilih
                        'lolos'=> intval(@$sayembara[0]->lolos_cat_1), // jumlah lolos
                        'tidak_lolos'=>intval(@$sayembara[0]->tidak_lolos_cat_1), // jumlah tidak lolos
                        // 'karya' => 0,
                        // 'color'=> $array_data['color']
                    );

                    $data_chart[] = $jumlah_rekap;
                }
                
                $data_chart = json_encode($data_chart);
            // --------------------- End Rekap Verifikasi Jenis Sayembara ------------//

            return view('pages.admin.verifikasi.list',compact('collection','belum','lulus','tidak','administrasi','total','lengkap', 'data_chart','link_download_kartu'));
        }
        return view('pages.admin.verifikasi.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(UserVerification $userVerification)
    {
        $verif = $userVerification;
        $data = $userVerification->user;
        $company = $userVerification->user->company;
        $anggota = User::where('user_id',$data->id)->get();
        return view('pages.admin.verifikasi.show',compact('data','verif','company','anggota'));
    }
    public function edit(UserVerification $userVerification)
    {
        //
    }
    public function update(Request $request, UserVerification $userVerification)
    {
        //
    }
    public function destroy(UserVerification $userVerification)
    {
        //
    }
    public function cancel(UserVerification $userVerification)
    {
        $userVerification->ket = '';
        $userVerification->st = 'Belum Verifikasi';
        $userVerification->created_by = 0;
        $userVerification->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Verifikasi dibatalkan',
        ]);
    }
    public function export_pdf(){
        return Excel::download(new UserExport, 'verifikasi_'.date('Y-m-d_H-i-s').'.xlsx');
    }
    public function send_mail(){
        // $data = UserVerification::get();
        // $user = array(
        //     [
        //         'email' => 'officialrizkyr@gmail.com',
        //     ],
        // );
        $data = UserVerification::where('st','Lulus Verifikasi')->orWhere('st','Tidak Lulus Verifikasi')->take(5)->get();
        foreach ($data as $key => $value) {
            // Mail::to($user)->send(new VerificationMail($value->user,$value->st));
            Notification::send($value->user, new VerificationNotification($value->user,$value->st));
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengunguman berhasil dikirim',
        ]);
    }
    public function unduh(UserVerification $userVerification,$sayembara_name)
    {
        if(strtolower($userVerification->st) != 'lulus verifikasi'){
            abort('404');
            return;
        }
        
        if(!in_array( $sayembara_name, $this->link_download_kartu)){
            abort('404');
            return;
        }
        $value_int = array_keys($this->link_download_kartu, $sayembara_name);
      
        $user_profile = $userVerification->user;
        $sayembara_pilihan = json_decode($user_profile->jenis_sayembara);
        // if(!in_array(@$value_int[0], $sayembara_pilihan)){
        //     abort('404');
        //     return;
        // }

        $data_karya = DB::table('karya')
                        ->where('user_id', $userVerification->user_id)
                        ->where('jenis_sayembara', @$value_int[0])
                        ->first();

        $nomor_urut = @$data_karya->no_reg;
        // if(!$nomor_urut){
        //     abort('404');
        //     return;
        // }

        $sayembara_check = array(
                                    'wapres' => array(
                                                    'template' => 'Iswapres_v2.pdf',
                                                    'text' => 'KOMPLEKS ISTANA WAKIL PRESIDEN',
                                                    'prefix' => NULL,
                                                    'color' => array('r' => 0, 'g' => 0, 'b'=> 0),    
                                                ),
                                    'legislatif' => array(
                                                        'template' => 'Legislatif.pdf',
                                                        'text' => 'KOMPLEKS PERKANTORAN LEGISLATIF',
                                                        'prefix' => NULL,
                                                        'color' => array('r' => 0, 'g' => 0, 'b'=> 0),    
                                                    ),
                                    'yudikatif'=>   array(
                                                        'template' => 'Yudikatif.pdf',
                                                        'text' => 'KOMPLEKS PERKANTORAN YUDIKATIF',
                                                        'prefix' => NULL,
                                                        'color' =>  array('r' => 255, 'g' => 255, 'b'=> 255),    
                                                    ),
                                    'peribadatan'=> array(
                                                        'template' => 'Peribadatan.pdf',
                                                        'text' => 'KOMPLEKS PERIBADATAN',
                                                        'prefix' => NULL,
                                                        'color' => array('r' => 255, 'g' => 255, 'b'=> 255),    
                                                    ),
                                );
        
         

        $pdf = new Fpdi();
        $pdf->AddFont('bebas','','BebasNeue-Regular.php');

        $r = $sayembara_check[$sayembara_name]['color']['r'];
        $g = $sayembara_check[$sayembara_name]['color']['g'];
        $b = $sayembara_check[$sayembara_name]['color']['b'];

        $pdf->SetTextColor($r,$g,$b);
        // get the page count
        $template_name = $sayembara_check[$sayembara_name]['template'];
        $pageCount = $pdf->setSourceFile('public/dokumen/template_kartu/'.$template_name);
        // iterate through all pages
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            // import a page
            $templateId = $pdf->importPage($pageNo);
        
            $pdf->AddPage();
            // use the imported page and adjust the page size

            $pdf->useTemplate($templateId, ['adjustPageSize' => true]);
            $pdf->SetFont('bebas','',30);
            $pdf->SetXY(37, 55);
            $pdf->Write(5, 'PESERTA');

            $pdf->SetFont('bebas','',100);
            $pdf->SetXY(17, 80);
            $nomor_peserta = $sayembara_check[$sayembara_name]['prefix'].$nomor_urut;
            $pdf->Write(5, $nomor_peserta);

            $pdf->SetFont('bebas','',25);
            $pdf->SetXY(0, 100);
            
            $pdf->MultiCell(105,10,$sayembara_check[$sayembara_name]['text'],0,'C');

        }
        
        // Output the new PDF
        $pdf->Output();
        // $pdf->Output('D', 'kartu_peserta_'.$sayembara_name.'.pdf');
    }
}
