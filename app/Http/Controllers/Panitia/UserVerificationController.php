<?php

namespace App\Http\Controllers\Panitia;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\UserVerification;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserVerificationController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $lengkap = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('users.jenis_sayembara','!=','[]')->get()->count();
            $belum = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('users.jenis_sayembara','!=','[]')->get()->count();
            $lulus = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('uv.st','Lulus Verifikasi')->get()->count();
            $tidak = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('uv.st','Tidak Lulus Verifikasi')->get()->count();
            $administrasi = User::select('users.id','uv.created_by','uv.st')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('users.jenis_sayembara','[]')->get()->count();
            $total = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->get()->count();
            $st = $request->st;
            if($st == "Belum"){
                $collection = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('users.jenis_sayembara','!=','[]')->where('uv.st','!=','Lulus Verifikasi')->where('uv.st','!=','Tidak Lulus Verifikasi')->orderBy('users.id','ASC')->paginate(10);
            }elseif($st == "Lulus"){
                $collection = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('uv.st','Lulus Verifikasi')->orderBy('users.id','ASC')->paginate(10);
            }elseif($st == "Tidak"){
                $collection = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->where('uv.st','Tidak Lulus Verifikasi')->orderBy('users.id','ASC')->paginate(10);
            }else{
                $collection = User::select('users.id','users.name as nama_ketua','users.email','users.phone','uv.id as id_uv','uv.created_by as id_verif','users.jenis_sayembara','uv.st','uv.ket')->leftJoin('user_verification as uv','users.id','=','uv.user_id')->where('users.role',4)->where('users.user_id',0)->orderBy('users.id','ASC')->paginate(10);
            }

            

            return view('pages.panitia.verifikasi.list',compact('collection','belum','lulus','tidak','administrasi','total','lengkap'));
        }

        // --------------------- Start Rekap Verifikasi Jenis Sayembara ------------//
            $array_for_loop = array(
                1 => array('name' => 'Istana Wakil Presiden', 'color'=>'#ab6c6c'),
                2 => array('name' => 'Perkantoran Legislatif', 'color'=>'#961a2c'),
                3 => array('name' => 'Perkantoran Yudikatif', 'color'=>'#1c3144'),
                4 => array('name' => 'Peribadatan', 'color'=>'#a7a5a5'),
            );
            $data_chart = array();

            foreach($array_for_loop as $key_loop => $array_data){
                $query = "
                            SELECT 
                                (SELECT 
                                    COUNT(id) 
                                FROM `users` 
                                where user_id = 0
                                AND role = 4
                                AND jenis_sayembara LIKE '%$key_loop%') as jumlah_cat_1,

                                (SELECT 
                                    COUNT(a.id)
                                FROM `users` a
                                LEFT JOIN user_verification b ON a.id = b.user_id
                                where a.user_id = 0
                                AND a.role = 4
                                AND b.st IS NOT NULL
                                AND a.jenis_sayembara LIKE '%$key_loop%'
                                AND LOWER(b.st) = 'lulus verifikasi') as lolos_cat_1,

                                (SELECT 
                                    COUNT(a.id)
                                FROM `users` a
                                LEFT JOIN user_verification b ON a.id = b.user_id
                                where a.user_id = 0
                                AND a.role = 4
                                AND b.st IS NOT NULL
                                AND a.jenis_sayembara LIKE '%$key_loop%'
                                AND LOWER(b.st) = 'tidak lulus verifikasi') as tidak_lolos_cat_1
                        ";

                $sayembara =  DB::select(DB::raw($query));

                $jumlah_rekap = array(
                    'kategori' => $array_data['name'], 
                    'jumlah'=> intval(@$sayembara[0]->jumlah_cat_1), // jumlah pemilih
                    'lolos'=> intval(@$sayembara[0]->lolos_cat_1), // jumlah lolos
                    'tidak_lolos'=>intval(@$sayembara[0]->tidak_lolos_cat_1), // jumlah tidak lolos
                    'karya' => 0,
                    // 'color'=> $array_data['color']
                );

                $data_chart[] = $jumlah_rekap;
            }
            
            $data_chart = json_encode($data_chart);
        // --------------------- End Rekap Verifikasi Jenis Sayembara ------------//

        return view('pages.panitia.verifikasi.main', compact('data_chart'));
    }
    public function create(User $user)
    {
        $data = new UserVerification;
        $data->user_id = $user->id;
        if($user->jenis_sayembara == "[]" || $user->jenis_sayembara == null){
            $data->st = 'Tidak Lengkap Administrasi';
        }else{
            $data->st = 'Data Ketua sedang di verifikasi';
        }
        $data->ket = '';
        $data->created_by = Auth::guard('web')->user()->id;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Sukses',
            'redirect' => 'input',
            'route' => route('web.user-verification.show',$data->id),
        ]);
    }
    public function store(Request $request)
    {
        //
    }
    public function show(UserVerification $userVerification)
    {
        if($userVerification->created_by > 0 || $userVerification->created_by != null){
            if($userVerification->created_by != Auth::guard('web')->user()->id){
                return response()->json([
                    'alert' => 'info',
                    'message' => 'Tim ini sedang di verifikasi oleh '.$userVerification->verif->name,
                ]);
            }else{
                if($userVerification->st == "Belum Verifikasi"){
                    if($userVerification->user->jenis_sayembara == "[]" || $userVerification->user->jenis_sayembara == null){
                        $userVerification->st = 'Tidak Lengkap Administrasi';
                    }else{
                        $userVerification->st = 'Data Ketua sedang di verifikasi';
                    }
                    $userVerification->created_by = Auth::guard('web')->user()->id;
                    $userVerification->update();
                }
                $verif = $userVerification;
                // if($userVerification->st == "Data Ketua sedang di verifikasi"){
                $data = $userVerification->user;
                $company = $userVerification->user->company;
                $anggota = User::where('user_id',$data->id)->get();
                return view('pages.panitia.verifikasi.show',compact('data','verif','company','anggota'));
                // }elseif($userVerification->st == "Data Perusahaan sedang di verifikasi"){
                //     $data = $userVerification->user->company;
                //     return view('pages.panitia.verifikasi.company',compact('data','verif'));
                // }
            }
        }else{
            if($userVerification->user->jenis_sayembara == "[]" || $userVerification->user->jenis_sayembara == null){
                $userVerification->st = 'Tidak Lengkap Administrasi';
            }else{
                $userVerification->st = 'Data Ketua sedang di verifikasi';
            }
            $userVerification->created_by = Auth::guard('web')->user()->id;
            $userVerification->update();
            $verif = $userVerification;
            $data = $userVerification->user;
            $company = $userVerification->user->company;
            $anggota = User::where('user_id',$data->id)->get();
            return view('pages.panitia.verifikasi.show',compact('data','verif','company','anggota'));
        }
    }
    public function edit(UserVerification $userVerification)
    {
        $verif = $userVerification;
        $data = $userVerification->user;
        $company = $userVerification->user->company;
        $anggota = User::where('user_id',$data->id)->get();
        return view('pages.panitia.verifikasi.show',compact('data','verif','company','anggota'));
    }
    public function update(Request $request, UserVerification $userVerification)
    {
        $step = $request->step;
        if($userVerification->user->jenis_sayembara == "[]" || $userVerification->user->jenis_sayembara == null){
            $userVerification->st = "Tidak Lengkap Administrasi";
        }else{
            if($step == 1){
                $userVerification->st = 'Data Ketua sedang di verifikasi';
            }elseif($step == 2){
                $userVerification->st = 'Data Perusahaan sedang di verifikasi';
            }elseif($step == 3){
                $userVerification->st = 'Data Anggota SKA sedang di verifikasi';
            }elseif($step == 4){
                $userVerification->st = 'Data Anggota Non SKA sedang di verifikasi';
            }
        }
        $userVerification->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Sukses',
        ]);
    }
    public function destroy(UserVerification $userVerification)
    {
        //
    }
    public function cancel(UserVerification $userVerification)
    {
        $userVerification->ket = '';
        // if($userVerification->user->jenis_sayembara == "[]" || $userVerification->user->jenis_sayembara == null){
        //     $userVerification->st = "Tidak Lengkap Administrasi";
        // }else{
        // }
        $userVerification->st = 'Belum Verifikasi';
        $userVerification->created_by = 0;
        $userVerification->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Verifikasi dibatalkan',
        ]);
    }
    public function verify(UserVerification $userVerification)
    {
        $userVerification->ket = '';
        $userVerification->st = 'Lulus Verifikasi';
        $userVerification->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Verifikasi selesai',
        ]);
    }
    public function nverify(UserVerification $userVerification, Request $request)
    {
        $userVerification->ket = $request->ket;
        $userVerification->st = 'Tidak Lulus Verifikasi';
        $userVerification->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Verifikasi selesai',
        ]);
    }
}
