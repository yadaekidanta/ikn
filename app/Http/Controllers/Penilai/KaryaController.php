<?php

namespace App\Http\Controllers\Penilai;

use App\Models\User;
use App\Models\Karya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class KaryaController extends Controller
{
    private $paging = 10;
    private $kategori_data_name = array(
        1 => 'Data Karya Istana Wakil Presiden',
        2 => 'Data Karya Perkantoran Legislatif',
        3 => 'Data Karya Perkantoran Yudikatif',
        4 => 'Data Karya Peribadatan'   
    );
    private $kategoriToInt = array(
        'wapres' => 1,
        'legislatif' => 2,
        'yudikatif' => 3,
        'peribadatan' => 4,

    );
    public function index(Request $request, $kategori='wapres')
    {
        if(Auth::guard('web')->user()->role != 3){
            return abort(404);
        }
        $jenis = $this->kategoriToInt[$kategori];
        $jenis = (empty($jenis)) ? 1 : $jenis; 
        $array_for_loop = array(
            1 => array('name' => 'Istana Wakil Presiden', 'color'=>'#ab6c6c'),
            2 => array('name' => 'Perkantoran Legislatif', 'color'=>'#961a2c'),
            3 => array('name' => 'Perkantoran Yudikatif', 'color'=>'#1c3144'),
            4 => array('name' => 'Peribadatan', 'color'=>'#a7a5a5'),
        );
        $data_chart = array();

        foreach($array_for_loop as $key_loop => $array_data){
            $query = "
                        SELECT 
                            (SELECT 
                                COUNT(id) 
                            FROM `users` 
                            where user_id = 0
                            AND role = 4
                            AND jenis_sayembara LIKE '%$key_loop%') as jumlah_cat_1,

                            (SELECT 
                                COUNT(a.id)
                            FROM `users` a
                            LEFT JOIN user_verification b ON a.id = b.user_id
                            where a.user_id = 0
                            AND a.role = 4
                            AND b.st IS NOT NULL
                            AND a.jenis_sayembara LIKE '%$key_loop%'
                            AND LOWER(b.st) = 'lulus verifikasi') as lolos_cat_1,

                            (SELECT 
                                SUM(IF(jumlah_karya = 7, 1, 0)) as jumlah_karya
                                FROM (SELECT 
                                    karya_id, 
                                    COUNT(karya_id) as jumlah_karya 
                                FROM `karya_file` a 
                                RIGHT JOIN karya  b ON a.karya_id = b.id
                                WHERE a.file IS NOT NULL
                                AND b.jenis_sayembara = $key_loop
                                GROUP BY karya_id) as n) as jumlah_karya,
                            
                            (SELECT 
                                COUNT(jenis_sayembara) as jumlah_lanjut 
                            FROM karya 
                            WHERE LOWER(TRIM(st)) = 'lulus verifikasi'
                            AND jenis_sayembara = $key_loop
                            GROUP BY jenis_sayembara) as jumlah_lanjut
                    ";

            $sayembara =  DB::select(DB::raw($query));

            $jumlah_rekap = array(
                'kategori' => $array_data['name'], 
                'jumlah'=> intval(@$sayembara[0]->jumlah_cat_1), // jumlah pemilih
                'lolos'=> intval(@$sayembara[0]->lolos_cat_1), // jumlah lolos
                'jumlah_karya'=> intval(@$sayembara[0]->jumlah_karya), // jumlah file yang lengkap 
                'jumlah_lanjut'=> intval(@$sayembara[0]->jumlah_lanjut), // jumlah lanjut penilaian
                // 'tidak_lolos'=>intval(@$sayembara[0]->tidak_lolos_cat_1), // jumlah tidak lolos
                // 'karya' => 0,
                // 'color'=> $array_data['color']
            );

            $data_chart[] = $jumlah_rekap;
        }
        
        $data_chart = json_encode($data_chart);
        // --------------------- End Rekap Verifikasi Jenis Sayembara ------------//
        if($request->ajax())
        {
            $title_table = $this->kategori_data_name[$jenis];
            $paging = $this->paging;
            $jumlah_sayembara = array();
            $data_sayembara = array();
            for($i=1; $i<= 4; $i++){
                $data_sayembara[$i] = User::select('users.*', 
                                                    'b.id as id_karya', 
                                                    'b.urut', 
                                                    'b.no_reg', 
                                                    'b.jenis_sayembara as js', 
                                                    'b.surat_pakta_integritas',
                                                    'b.surat_tanggung_jawab',
                                                    'b.st as status_verifikasi',
                                                    'b.ket as ket_verifikasi',
                                                    'b.verified_by as user_verifikasi',
                                                    'b.updated_at as waktu_verifikasi',
                                                    'c.name as verifikator',
                                                    'd.file_karya as file_karya')
                            ->leftJoin('karya as b', function($join) {
                                $join->on('users.id', '=', 'b.user_id');
                            })
                            ->leftJoin('users as c', function($join){
                                $join->on('b.verified_by', '=', 'c.id')
                                     ->where('c.role', '<=', 3);
                            })
                            ->leftJoin(
                                DB::raw("
                                (SELECT karya_id, GROUP_CONCAT(CONCAT('[',urut,'::',file,'::',id,']')) as file_karya FROM (SELECT v.karya_id, v.urut, MAX(file) as file, id FROM karya_file v WHERE v.urut IS NOT NULL AND file IS NOT NULL GROUP BY v.karya_id, v.urut, id ORDER BY v.karya_id ASC, v.urut ASC) l GROUP BY l.karya_id) d 
                                "), 
                                'b.id', '=', 'd.karya_id'
                            )
                            ->where('users.role', 4)
                            ->where('users.user_id',0)
                            ->where('users.jenis_sayembara','like', '%'.$i.'%')
                            ->where('b.jenis_sayembara',$i);
                           
                $jumlah_sayembara[$i] = $data_sayembara[$i]
                                        ->get()
                                        ->count();
            }
            // DB::enableQueryLog();
            $data_view = $data_sayembara[$jenis]
                        ->paginate($paging)
                        ->withQueryString();
            // $data_view->setBaseUrl('karya/wapres');
            // $data_view->appends(['jenis' => $jenis]);
            // dd(DB::getQueryLog());
            // return;
            
            

            return view('pages.penilai.karya.list', compact('jumlah_sayembara','paging','data_view', 'title_table','kategori'));
        }
        return view('pages.penilai.karya.main', compact('kategori', 'data_chart'));
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Karya $karya)
    {
        return view('pages.penilai.karya.show',compact('karya'));
    }
    public function edit(Karya $karya)
    {
        //
    }
    public function update(Request $request, Karya $karya)
    {
        //
    }
    public function destroy(Karya $karya)
    {
        //
    }
}
