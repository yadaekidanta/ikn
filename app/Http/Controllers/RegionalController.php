<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Province;
use Illuminate\Http\Request;

class RegionalController extends Controller
{
    public function get_province(Request $request)
    {
        $collection = Province::where('country_id',$request->country)->get();
        $list = "<option>Pilih Propinsi</option>";
        foreach($collection as $row){
            $list.="<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
    public function get_city(Request $request)
    {
        $collection = City::where('province_id',$request->prov)->get();
        $list = "<option>Pilih Kota / Kabupaten</option>";
        foreach($collection as $row){
            $list.="<option value='$row->id' $row->id==$request->prov?selected:''>$row->name</option>";
        }
        return $list;
    }
}
