<?php

namespace App\Http\Controllers\Web;

use App\Models\Sub;
use App\Models\User;
use App\Models\Session;
use App\Models\Province;
use App\Models\UserVerify;
use App\Models\UserCompany;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Qualification;
use App\Models\TrackerSession;
use App\Models\User as Anggota;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Notification;
use App\Notifications\RegistrationNotification;

class AnggotaController extends Controller
{
    public function index(Request $request)
    {
        $data = Auth::guard('web')->user();
        if ($request->ajax()) {
            $user = Auth::guard('web')->user();
            $data = User::where('user_id',$user->id)->get();
            $complete = $user->calculate_profile($data);
            $company = UserCompany::where('user_id',$user->id)->get()->count() > 0 ? UserCompany::where('user_id',$user->id)->first() : new UserCompany();
            $completep = $company->calculate_company($company);
            $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
            return view('pages.peserta.anggota.list',compact('user','data','last','complete','completep'));
        }
        return view('pages.peserta.anggota.main',compact('data'));
    }
    public function create()
    {
        $data = new User();
        $user = Auth::guard('web')->user();
        $collection = User::where('user_id',$user->id)->get();
        $sub = Sub::get();
        $complete = $user->calculate_profile($collection);
        $company = UserCompany::where('user_id',$user->id)->get()->count() > 0 ? UserCompany::where('user_id',$user->id)->first() : new UserCompany();
        $completep = $company->calculate_company($company);
        $kualifikasi = Qualification::get();
        $province = Province::get();
        $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
        return view('pages.peserta.anggota.input',compact('user','province','kualifikasi','sub','last','data','complete','completep','collection'));
    }
    public function store(Request $request)
    {
        $user = Auth::guard('web')->user();
        $collection = User::where('user_id',$user->id)->get();
        if($collection->count() > 9){
            return response()->json([
                'alert' => 'info',
                'message' => 'Jumlah anggota anda sudah melebihi batas maksimal',
            ]);
        }
        if($collection->count() < 4){
            $vhp = 'required|unique:users,phone';
            $vemail = 'required|email|unique:users,email';
            $vktp = 'required|unique:users,ktp_no';
            $vnpwp = 'required|unique:users,npwp_no';
            $vnrka = 'required|unique:users,nrka_no';
            $vfktp = 'required|max:2000';
            $vfnpwp = 'required|max:2000';
            $vfska = 'required|max:2000';
            $vfijazah = 'required|max:2000';
            $sub = 'required';
            $kolom = 'kualifikasi';
            $vkualifikasi ='required';
            $alamat = 'required';
            $kode_pos = 'required';
            $provinsi = $request->provinsi == 'Pilih Provinsi' ? 'required' : '';
            $kota = $request->kota == 'Harap pilih Provinsi dahulu' || $request->kota == 'Pilih Kota / Kabupaten' ? 'required' : '';
        }else{
            $vhp = '';
            $vemail = '';
            $vktp = 'required|unique:users,ktp_no';
            $vnpwp = '';
            $vnrka = '';
            $vfktp = 'required|max:2000';
            $vfnpwp = '';
            $vfska = '';
            $vfijazah = 'required|max:2000';
            $sub = '';
            $kolom = '';
            $vkualifikasi = '';
            $alamat = '';
            $kode_pos = '';
            $provinsi = '';
            $kota = '';
        }
        $check = User::where('user_id',$user->id)->where('ska_kualifikasi_id',$request->kualifikasi)->get();
        if($check->count() > 0){
            return response()->json([
                'alert' => 'info',
                'message' => 'Anggota dengan kualifikasi ini sudah ada',
            ]);
        }
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'no_handphone' => $vhp,
            'email' => $vemail,
            'no_ktp' => $vktp,
            'no_npwp' => $vnpwp,
            'no_nrka' => $vnrka,
            'ktp' => $vfktp,
            'npwp' => $vfnpwp,
            'ska' => $vfska,
            'ijazah' => $vfijazah,
            'sub' => $sub,
            $kolom => $vkualifikasi,
            'alamat' => $alamat,
            'kode_pos' => $kode_pos,
            'provinsi' => $provinsi,
            'kota' => $kota,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama_lengkap')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_lengkap'),
                ]);
            }elseif($errors->has('no_handphone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_handphone'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('no_ktp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_ktp'),
                ]);
            }elseif($errors->has('no_npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_npwp'),
                ]);
            }elseif($errors->has('no_nrka')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_nrka'),
                ]);
            }elseif($errors->has('ktp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ktp'),
                ]);
            }elseif($errors->has('npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('npwp'),
                ]);
            }elseif($errors->has('ska')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ska'),
                ]);
            }elseif($errors->has('ijazah')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ijazah'),
                ]);
            }elseif($errors->has($kolom)){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first($kolom),
                ]);
            }elseif($errors->has('sub')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sub'),
                ]);
            }elseif($errors->has('alamat')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alamat'),
                ]);
            }elseif($errors->has('kode_pos')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode_pos'),
                ]);
            }elseif($errors->has('provinsi')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('provinsi'),
                ]);
            }elseif($errors->has('kota')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kota'),
                ]);
            }
        }
        $data = new User();
        // $data->username = Str::random(8);
        // $pass = Str::random(8);
        // $data->password = Hash::make($pass);
        $data->name = $request->nama_lengkap;
        $data->phone = $request->no_handphone;
        $data->email = $request->email;
        $data->tanggal_ska = $request->tanggal_ska;
        $data->user_id = $user->id;
        $data->country_id = 102;
        $data->ktp_no = $request->no_ktp;
        $data->npwp_no = $request->no_npwp;
        $data->nrka_no = $request->no_nrka;
        $data->ska_kualifikasi = $request->ska_kualifikasi;
        $data->ska_sub_id = $request->sub ? $request->sub : 0;
        $data->ska_kualifikasi_id = $request->kualifikasi != '' ? $request->kualifikasi : 0;
        $data->province_id = $request->provinsi != 'Pilih Provinsi' ? $request->provinsi : 0;
        if($request->kota == 'Harap pilih Provinsi dahulu' || $request->kota == 'Pilih Kota / Kabupaten'){
            $data->city_id = 0;
        }else{
            $data->city_id = $request->kota;
        }
        $data->address = $request->alamat;
        $data->postcode = $request->kode_pos;
        if(request()->file('ktp')){
            Storage::delete($data->ktp);
            $ktp = request()->file('ktp')->store("ktp");
            $data->ktp = $ktp;
        }
        if(request()->file('npwp')){
            Storage::delete($data->npwp);
            $npwp = request()->file('npwp')->store("npwp");
            $data->npwp = $npwp;
        }
        if(request()->file('ska')){
            Storage::delete($data->ska);
            $ska = request()->file('ska')->store("ska");
            $data->ska = $ska;
        }
        if(request()->file('ijazah')){
            Storage::delete($data->ijazah);
            $ijazah = request()->file('ijazah')->store("ijazah");
            $data->ijazah = $ijazah;
        }
        $data->save();
        // $token = Str::random(64);

        // UserVerify::create([
        //     'user_id' => $data->id,
        //     'token' => $token
        // ]);
        // Notification::send($data, new RegistrationNotification($data,$token,$pass));
        return response()->json([
            'alert' => 'success',
            'message' => 'Anggota Anda tersimpan',
        ]);
    }
    public function show(Anggota $anggota)
    {
        $data = $anggota;
        $user = Auth::guard('web')->user();
        $complete = $user->calculate_profile($data);
        $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
        return view('pages.peserta.anggota.show',compact('user','last','data','complete'));
    }
    public function edit(Anggota $anggota)
    {
        $data = $anggota;

        $user = Auth::guard('web')->user();
        $collection = User::where('user_id',$user->id)->get();
        // $id_user_list = $collection->id;
        $col_num = 0;
        $id_array = array();
        foreach($collection as $col_item){
            if($col_num >= 4){
                break;
            }
            $id_array[] = $col_item->id;
            $col_num++;
        }
      
        // print_r($data->id);
        // print_r($id_array);
        // return;

        // if($collection->count() < 4 || in_array($data->id, $id_array) ){
        $sub = Sub::get();
        // }
        // else{
        //     $sub = Sub::where('id', '>', 1)->get();
        // }
        // dd(DB::getQueryLog());
        // return;
        $kualifikasi = Qualification::get();
        $province = Province::get();
        $complete = $user->calculate_profile($user);
        $company = UserCompany::where('user_id',$user->id)->get()->count() > 0 ? UserCompany::where('user_id',$user->id)->first() : new UserCompany();
        $completep = $company->calculate_company($company);
        $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
        return view('pages.peserta.anggota.input',compact('user','province','kualifikasi','sub','last','data','complete','completep','collection'));
    }
    public function update(Request $request, Anggota $anggota)
    {
        $user = Auth::guard('web')->user();
        $collection = User::where('user_id',$user->id)->get();
        $col_num = 0;
        $id_array = array();
        foreach($collection as $col_item){
            if($col_num >= 4){
                break;
            }
            $id_array[] = $col_item->id;
            $col_num++;
        }
        if($collection->count() < 4 || in_array($anggota->id, $id_array) ){
            $vhp = 'required|unique:users,phone,'.$anggota->id;
            $vemail = 'required|email|unique:users,email,'.$anggota->id;
            $vktp = 'required|unique:users,ktp_no,'.$anggota->id;
            $vnpwp = 'required|unique:users,npwp_no,'.$anggota->id;
            $vnrka = 'required|unique:users,nrka_no,'.$anggota->id;
            $vfktp = 'max:2000';
            $vfnpwp = 'max:2000';
            $vtska = 'required';
            $vfska = 'max:2000';
            $vfijazah = 'max:2000';
            $sub = 'required';
            $kolom = 'kualifikasi';
            $vkualifikasi ='required';
            $alamat = 'required';
            $kode_pos = 'required';
            $provinsi = $request->provinsi == 'Pilih Provinsi' ? 'required' : '';
            $kota = $request->kota == 'Harap pilih Provinsi dahulu' || $request->kota == 'Pilih Kota / Kabupaten' ? 'required' : '';
        }else{
            $vhp = '';
            $vemail = '';
            $vktp = 'required|unique:users,ktp_no,'.$anggota->id;
            $vnpwp = '';
            $vnrka = '';
            $vfktp = $anggota->ktp ? 'max:2000' : 'required|max:2000';
            $vfnpwp = '';
            $vtska = '';
            $vfska = '';
            $vfijazah = $anggota->ijazah ? 'max:2000' : 'required|max:2000';
            $sub = '';
            $kolom = '';
            $vkualifikasi = '';
            $alamat = '';
            $kode_pos = '';
            $provinsi = '';
            $kota = '';
        }
        if($request->kualifikasi != $anggota->ska_kualifikasi_id){
            $check = User::where('user_id',$user->id)->where('ska_kualifikasi_id',$request->kualifikasi)->get();
            if($check->count() > 0){
                return response()->json([
                    'alert' => 'info',
                    'message' => 'Anggota dengan kualifikasi ini sudah ada',
                ]);
            }
        }
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'no_handphone' => $vhp,
            'email' => $vemail,
            'no_ktp' => $vktp,
            'no_npwp' => $vnpwp,
            'no_nrka' => $vnrka,
            'ktp' => $vfktp,
            'npwp' => $vfnpwp,
            'ska' => $vfska,
            'tanggal_ska' => $vtska,
            'ijazah' => $vfijazah,
            'sub' => $sub,
            $kolom => $vkualifikasi,
            'alamat' => $alamat,
            'kode_pos' => $kode_pos,
            'provinsi' => $provinsi,
            'kota' => $kota,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama_lengkap')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_lengkap'),
                ]);
            }elseif($errors->has('no_handphone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_handphone'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('no_ktp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_ktp'),
                ]);
            }elseif($errors->has('no_npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_npwp'),
                ]);
            }elseif($errors->has('no_nrka')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_nrka'),
                ]);
            }elseif($errors->has('ktp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ktp'),
                ]);
            }elseif($errors->has('npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('npwp'),
                ]);
            }elseif($errors->has('ska')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ska'),
                ]);
            }elseif($errors->has('tanggal_ska')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_ska'),
                ]);
            }elseif($errors->has('ijazah')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ijazah'),
                ]);
            }elseif($errors->has('kualifikasi')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kualifikasi'),
                ]);
            }elseif($errors->has('sub')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sub'),
                ]);
            }elseif($errors->has('alamat')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alamat'),
                ]);
            }elseif($errors->has('kode_pos')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode_pos'),
                ]);
            }elseif($errors->has('provinsi')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('provinsi'),
                ]);
            }elseif($errors->has('kota')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kota'),
                ]);
            }
        }
        $anggota->name = $request->nama_lengkap;
        $anggota->phone = $request->no_handphone;
        $anggota->email = $request->email;
        $anggota->ktp_no = $request->no_ktp;
        $anggota->tanggal_ska = $request->tanggal_ska;
        $anggota->npwp_no = $request->no_npwp;
        $anggota->nrka_no = $request->no_nrka;
        $anggota->ska_kualifikasi = $request->ska_kualifikasi;
        $anggota->ska_sub_id = $request->sub ? $request->sub : 0;
        $anggota->ska_kualifikasi_id = $request->kualifikasi != '' ? $request->kualifikasi : 0;
        $anggota->province_id = $request->provinsi != 'Pilih Provinsi' ? $request->provinsi : 0;
        if($request->kota == 'Harap pilih Provinsi dahulu' || $request->kota == 'Pilih Kota / Kabupaten'){
            $anggota->city_id = 0;
        }else{
            $anggota->city_id = $request->kota;
        }
        $anggota->address = $request->alamat;
        $anggota->postcode = $request->kode_pos;
        if(request()->file('ktp')){
            Storage::delete($anggota->ktp);
            $ktp = request()->file('ktp')->store("ktp");
            $anggota->ktp = $ktp;
        }
        if(request()->file('npwp')){
            Storage::delete($anggota->npwp);
            $npwp = request()->file('npwp')->store("npwp");
            $anggota->npwp = $npwp;
        }
        if(request()->file('ska')){
            Storage::delete($anggota->ska);
            $ska = request()->file('ska')->store("ska");
            $anggota->ska = $ska;
        }
        if(request()->file('ijazah')){
            Storage::delete($anggota->ijazah);
            $ijazah = request()->file('ijazah')->store("ijazah");
            $anggota->ijazah = $ijazah;
        }
        $anggota->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Anggota berhasil diperbarui',
        ]);
    }
    public function destroy()
    {
        //
    }
}
