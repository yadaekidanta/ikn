<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DocController extends Controller
{
    public function index(Request $request)
    {
        $data = Auth::guard('web')->user();
        if ($request->ajax()) {
            $data = Auth::guard('web')->user();
            return view('page.web.doc.list',compact('data'));
        }
        return view('page.web.doc.main',compact('data'));
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function edit()
    {
        $data = Auth::guard('web')->user();
        return view('page.web.doc.input',compact('data'));
    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ktp' => 'required',
            'npwp' => 'required',
            'ska' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('ktp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ktp'),
                ]);
            }elseif($errors->has('npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('npwp'),
                ]);
            }elseif($errors->has('ska')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ska'),
                ]);
            }
        }
        $data = User::where('id',Auth::guard('web')->user()->id)->first();
        if(request()->file('ktp')){
            Storage::delete($data->ktp);
            $ktp = request()->file('ktp')->store("ktp");
            $data->ktp = $ktp;
        }
        if(request()->file('npwp')){
            Storage::delete($data->npwp);
            $npwp = request()->file('npwp')->store("npwp");
            $data->npwp = $npwp;
        }
        if(request()->file('ska')){
            Storage::delete($data->ska);
            $ska = request()->file('ska')->store("ska");
            $data->ska = $ska;
        }
        $data->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Dokumen terlengkapi, harap menunggu verifikasi',
        ]);
    }
    public function destroy()
    {
        //
    }
}
