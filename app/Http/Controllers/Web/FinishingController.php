<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Models\Session;

use App\Models\UserVerify;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\TrackerSession;
use App\Models\UserCompany as Perusahaan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\RegistrationNotification;

class FinishingController extends Controller
{
	private $kategori_list = array(
        'Sayembara Kompleks Istana Wakil Presiden',
        'Sayembara Kompleks Perkantoran Legislatif',
        'Sayembara Kompleks Perkantoran Yudikatif',
        'Sayembara Kompleks Peribadatan'   
    );
    public function index(Request $request){
    	$data = Auth::guard('web')->user();
        if ($request->ajax()) {
       		
            $user = Auth::guard('web')->user();
            $data = User::where('user_id',$user->id)->get();
            $complete = User::calculate_profile($data);
            $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
            $jns_sayembara = $user->jenis_sayembara;
            $sayembara_array = json_decode($jns_sayembara);
            $data_perusahaan = Perusahaan::where('user_id',$user->id)->get()->toArray();
            $anggota_group = User::where('user_id',$user->id)->get();

            $sayembara_name = NULL;
            $sayembara_pilihan  = array();
            foreach ($sayembara_array as $symb){
                $sayembara_name .= $this->kategori_list[$symb - 1];
                $sayembara_pilihan[] = $this->kategori_list[$symb - 1];
                if(end($sayembara_array) == $symb){
                    break;
                }

                if(count($sayembara_array) > 1){
                    $sayembara_name .= ' dan ';
                }
            }
            // echo $sayembara_name;


            return view('pages.peserta.finishing.list', compact('last','complete','data', 'user', 'sayembara_name','data_perusahaan','anggota_group', 'sayembara_pilihan'));
        }
        return view('pages.peserta.finishing.main');
    }

    public function end_request(Request $request){
        
        if ($request->ajax()) {
            $user = Auth::guard('web')->user();
            DB::table('users')
            ->where('id', $user->id)
            ->update(['is_done' => 1]);
            return response()->json([
                'alert' => 'success',
                'message' => 'Data berhasil disimpan',
            ]);
        }
        return abort(404, 'Page not found.');
    }
}
