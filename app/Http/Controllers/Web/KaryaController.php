<?php

namespace App\Http\Controllers\Web;

use App\Models\Karya;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class KaryaController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()){
            $user = Auth::guard('web')->user();
            $collection = Karya::where('user_id',$user->id)->paginate(2);
            return view('pages.peserta.karya.list',compact('collection'));
        }
        return view('pages.peserta.karya.main');
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Karya $karya)
    {
        return view('pages.peserta.karya.show',compact('karya'));
    }
    public function edit(Karya $karya)
    {
        return view('pages.peserta.karya.input',compact('karya'));
    }
    public function update(Request $request, Karya $karya)
    {
        if($request->tipe == 1){
            $validator = Validator::make($request->all(), [
                'surat_pakta_integritas' => 'required',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                if ($errors->has('surat_pakta_integritas')) {
                    return response()->json([
                        'alert' => 'error',
                        'message' => $errors->first('surat_pakta_integritas'),
                    ]);
                }
            }
        }else{
            $validator = Validator::make($request->all(), [
                'surat_tanggung_jawab' => 'required',
            ]);
            if ($validator->fails()) {
                $errors = $validator->errors();
                if($errors->has('surat_tanggung_jawab')){
                    return response()->json([
                        'alert' => 'error',
                        'message' => $errors->first('surat_tanggung_jawab'),
                    ]);
                }
            }
        }
        if(request()->file('surat_pakta_integritas')){
            Storage::delete($karya->surat_pakta_integritas);
            $surat_pakta_integritas = request()->file('surat_pakta_integritas')->store("surat_pakta_integritas");
            $karya->surat_pakta_integritas = $surat_pakta_integritas;
        }
        if(request()->file('surat_tanggung_jawab')){
            Storage::delete($karya->surat_tanggung_jawab);
            $surat_tanggung_jawab = request()->file('surat_tanggung_jawab')->store("surat_tanggung_jawab");
            $karya->surat_tanggung_jawab = $surat_tanggung_jawab;
        }
        // if($karya->created_at){}
        $karya->created_at = date('Y-m-d H:i:s');
        $karya->st = "Menunggu Verifikasi";
        $karya->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Dokumen terlengkapi, harap menunggu verifikasi',
        ]);
    }
    public function destroy(Karya $karya)
    {
        //
    }
    public function edits(Karya $karya)
    {
        return view('pages.peserta.karya.edit',compact('karya'));
    }
}
