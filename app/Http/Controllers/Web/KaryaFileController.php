<?php

namespace App\Http\Controllers\Web;

use App\Models\KaryaFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class KaryaFileController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'karya'.$request->urut => 'required|mimes:pdf|max:20000',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('karya'.$request->urut)) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('karya'.$request->urut),
                ]);
            }
        }
        $cek = KaryaFile::where('karya_id',$request->id_karya)->get()->count();
        if($cek > 6){
            return response()->json([
                'alert' => 'error',
                'message' => 'Jumlah karya sudah mencapai batas maksimum',
            ]);
        }
        // $data[]='';
        // if($request->hasfile('filename')){
        //     foreach($request->file('filename') as $file){
        //         $name = $file->getClientOriginalName();
        //         $file->move(public_path().'/karya/', $name);  
        //         $data[] = $name;
        //     }
        // }
        // $file = new KaryaFile;
        // $file->karya_id = $request->karya_id;
        // $file->file = json_encode($data);
        // $file->save();
        // foreach ($request->karya as $karya) {
            // $data = new KaryaFile;
            // $data->karya_id = $request->karya_id;
            // $file = $karya->store("karya");
            // $data->file = $file;
            // $data->save();
        // }
        if($request->tipe == "update"){
            $data = KaryaFile::where('karya_id',$request->id_karya)->where('urut',$request->urut)->first();
            Storage::delete($data->file);
        }else{
            $data = new KaryaFile;
        }
        if($request->hasfile('karya1')){
            $karya = request()->file('karya1')->store("karya");
        }elseif($request->hasfile('karya2')){
            $karya = request()->file('karya2')->store("karya");
        }elseif($request->hasfile('karya3')){
            $karya = request()->file('karya3')->store("karya");
        }elseif($request->hasfile('karya4')){
            $karya = request()->file('karya4')->store("karya");
        }elseif($request->hasfile('karya5')){
            $karya = request()->file('karya5')->store("karya");
        }elseif($request->hasfile('karya6')){
            $karya = request()->file('karya6')->store("karya");
        }elseif($request->hasfile('karya7')){
            $karya = request()->file('karya7')->store("karya");
        }
        $data->karya_id = $request->id_karya;
        $data->urut = $request->urut;
        $data->file = $karya;
        if($request->tipe == "update"){
            $data->update();
        }else{
            $data->save();
        }
        $cek = KaryaFile::where('karya_id',$request->id_karya)->get()->count();
        return response()->json([
            'alert' => 'success',
            'message' => 'Dokumen sudah terunggah',
            'id_file' => $data->id,
            'total' => $cek,
            'redirect' => 'tabulasi',
            'urut' => $request->urut,
            'file' => $data ? asset('storage/' .$data->file) : '',
            'data' => $data ? $data : '',
        ]);
    }
    public function show(KaryaFile $karyaFile)
    {
        //
    }
    public function edit(KaryaFile $karyaFile)
    {
        //
    }
    public function update(Request $request, KaryaFile $karyaFile)
    {
        //
    }
    public function destroy(KaryaFile $karyaFile)
    {
        $id = $karyaFile->karya_id;
        Storage::delete($karyaFile->file);
        $karyaFile->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Karya terhapus',
            'redirect' => 'input',
            'route' => route('web.karya.edit',$id),
        ]);
    }
    public function ubah(request $request){
        $karya = KaryaFile::where('id',$request->id)->first();
        if(request()->file('karya')){
            Storage::delete($karya->file);
            $file = request()->file('karya')->store("karya");
            $karya->file = $file;
        }
        $karya->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Karya diperbarui',
            'redirect' => 'input',
            'route' => route('web.karya.show',$karya->karya_id),
        ]);
    }
    public function get_file(Request $request){
        $data = KaryaFile::where('karya_id',$request->id_karya)->where('urut',$request->urut)->first();
        return response()->json([
            'file' => asset('storage/' .$data->file),
        ]);
    }
    public function check_step(Request $request){
        $count = KaryaFile::where('karya_id',$request->id_karya)->get()->count();
        $data = KaryaFile::where('karya_id',$request->id_karya)->where('urut',$request->urut)->first();
        // dd($data);
        return response()->json([
            'count' => $count,
            'file' => $data ? asset('storage/' .$data->file) : '',
            'data' => $data ? $data : '',
        ]);
    }
}
