<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Models\Session;

use App\Models\UserVerify;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\TrackerSession;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\RegistrationNotification;

class KategoriSayembaraController extends Controller
{

    private $kategori_list = array(
        'Sayembara Kompleks Istana Wakil Presiden',
        'Sayembara Kompleks Perkantoran Legislatif',
        'Sayembara Kompleks Perkantoran Yudikatif',
        'Sayembara Kompleks Peribadatan'   
    );
   
    public function index(Request $request){
    	$data = Auth::guard('web')->user();
        // if($data->is_done){
        //     return abort(404, 'Page not found.');
        // }
        if ($request->ajax()) {
       		
            $user = Auth::guard('web')->user();
            $data = User::where('user_id',$user->id)->get();
            $complete = $user->calculate_profile($data);
            $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
            return view('pages.peserta.kategori.list', compact('last','complete','data', 'user'));
        }
        return view('pages.peserta.kategori.main');
    }

    public function store(Request $request){
    	$user = Auth::guard('web')->user();
        $validator = Validator::make($request->all(), [
            'kategori' => 'present|array',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'alert' => 'error',
                'message' => $errors->first('kategori'),
            ]);
        }
        $user_id = $user->id;
        $kategori = $request->kategori;
        $kategori = json_encode($kategori, JSON_NUMERIC_CHECK);
        // print_r($kategori);

        DB::table('users')
            ->where('id', $user_id)
            ->update(array(
                            'jenis_sayembara' => $kategori,
                            'is_done' => 1,
                        )
                    );
            
    	// return;
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori Sudah Dipilih',
            'redirect' => 'href',
            'href' => route('web.finishing.index'),
        ]);
    }
}
