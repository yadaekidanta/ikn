<?php

namespace App\Http\Controllers\Web;

use File;
use Tracker;
use ZipArchive;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Session;
use setasign\Fpdi\Fpdi;
use App\Models\FaqCategory;
use Illuminate\Support\Str;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\Models\TrackerSession;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use App\Models\UserCompany as Perusahaan;


class MainController extends Controller
{
    private $kategori_list = array(
        'Sayembara Kompleks Istana Wakil Presiden',
        'Sayembara Kompleks Perkantoran Legislatif',
        'Sayembara Kompleks Perkantoran Yudikatif',
        'Sayembara Kompleks Peribadatan'   
    );
    private $link_download_kartu = array(
                                    1=>'wapres',
                                    2=>'legislatif',
                                    3=>'yudikatif',
                                    4=>'peribadatan'
                                );
    private $file_data_dukung = array(
        1 => '1-Kontur-Kompleks-Istana-Wakil-Presiden.dwg',
        2 => '2-Kontur-Kompleks-Legislatif.dwg',
        3 => '3-Kontur-Kompleks-Yudikatif.dwg',
        4 => '4-Kontur-Kompleks-Peribadatan.dwg',
    );

    private function info_peserta($with_header=TRUE){
        return view('pages.dashboard.dashboard_peserta',compact($with_header));
    }

    private function view_info($with_header = TRUE){
        $user = Auth::guard('web')->user();
            $data = User::where('user_id',$user->id)->get();
            $complete = User::calculate_profile($data);
            $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
            $jns_sayembara = $user->jenis_sayembara;
            $sayembara_array = json_decode($jns_sayembara);
            $data_perusahaan = Perusahaan::where('user_id',$user->id)->get()->toArray();
            $anggota_group = User::where('user_id',$user->id)->get();

            $sayembara_name = NULL;
            $sayembara_pilihan  = array();
            foreach ($sayembara_array as $symb){
                $sayembara_name .= $this->kategori_list[$symb - 1];
                $sayembara_pilihan[] = $this->kategori_list[$symb - 1];
                if(end($sayembara_array) == $symb){
                    break;
                }

                if(count($sayembara_array) > 1){
                    $sayembara_name .= ' dan ';
                }
            }

            return view('pages.peserta.finishing.list-info', compact('last','complete','data', 'user', 'sayembara_name','data_perusahaan','anggota_group', 'sayembara_pilihan','with_header'));
    }

    private function MateriAanwijzing(){
        return view('pages.peserta.kartu.materi-aanwijzing');
    }

    private function BeritaAcaraAanwizjing(){
        return view('pages.peserta.kartu.berita-acara');
    }

    private function VideoLapangan(){
        return view('pages.peserta.kartu.video-lapangan');
    }

    public function __construct()
    {
        $this->middleware('guest:web')->except(
                                                'dashboard',
                                                'download',
                                                'statistik',
                                                'export_sayembara',
                                                'information',
                                                'StepUp',
                                                'kartu_peserta',
                                                'agreement',
                                                'ZipDownload',
                                                'download_berita'
                                            );
    }
    public function index()
    {
        $kategori = FaqCategory::get();
        return view('pages.web.home.main',compact('kategori'));
    }

    public function pemenang(){
        return view('pages.web.home.pemenang');
    }

    public function captcha_login()
    {
        return response()->json(['captcha'=> captcha_img('flat')]);
    }
    public function captcha_register()
    {
        return response()->json(['captcha'=> captcha_img('flat')]);
    }
    

    public function dashboard(Request $request)
    {

        
        DB::enableQueryLog();

        if(Auth::guard('web')->user()->role >= 4 && (date('Y-m-d') >= date('2022-04-08'))){
            return $this->StepUp($request);
            // return redirect('/info');
        }
            if ($request->ajax()) {
                // $login = Session::selectRaw("user_id, client_ip, count( client_ip ) AS total,DATE_FORMAT( FROM_UNIXTIME( last_activity ), '%Y-%m-%d' ) AS date")->where('user_id','!=',null)->groupBy('date','client_ip','user_id')->orderBy('date','desc')->get();
                $login = TrackerSession::selectRaw("
                            user_id, 
                            client_ip, 
                            count( client_ip ) AS total,
                            DATE(created_at) AS date
                        ")
                        ->where('user_id','!=',null)
                        ->whereDate('created_at', date('Y-m-d'))
                        ->groupBy('date','client_ip','user_id')
                        ->orderBy('date','desc')
                        ->get();
                $visitor =  TrackerSession::selectRaw("client_ip, count( client_ip ) AS total,DATE(created_at) AS date")
                            ->where('user_id', '!=', NULL)
                            ->whereDate('created_at', date('Y-m-d'))
                            ->groupBy('client_ip', 'date')
                            ->orderBy('date','desc')
                            ->get();
                $pendaftar = User::selectRaw('count(id) as total, DATE(created_at) as created_at')
                            ->where('role','=',4)
                            ->where('user_id',0)
                            ->whereNull('verified_at')
                            ->groupByRaw('DATE(created_at)')
                            ->orderByRaw('DATE(created_at) DESC')
                            ->get();
                // dd($pendaftar);
                // $visitor = Tracker::currentSession();

                // dd(DB::getQueryLog());
                // return;
                $tahun = $request->year;
                $tahun = (empty(@$tahun)) ? date('Y') : $tahun;
                $bulan = $request->month;
                $bulan = (empty(@$bulan)) ? date('m') : $bulan;

                $date1 = date($tahun."-".Str::replace("0","",$bulan)."-d");
                // if(empty(@$tahun) || empty($bulan)){
                // 	$date1 = date("Y-m-d");
                // } 

                $date2 = date("Y-m-d");
    
                $diff = abs(strtotime($date2) - strtotime($date1));
    
                $years = floor($diff/(365*60*60*24));
                $months = floor(($diff-$years*365*60*60*24)/(30*60*60*24));
                $total_month = number_format($months,0)+1;
                // $arrc = array();
                // $arrp = array();
                $arrv = array();
              

                $cvisitor =  DB::select(DB::raw(" 
                                SELECT 
                                    IFNULL(jumlah, 0) as total_visitor,
                                    IFNULL(jumlah_pes, 0) as total_peserta,
                                    IFNULL(jumlah_finish, 0) as total_selesai,
                                    IFNULL(date, CONCAT(DATE_FORMAT('$date1', '%Y-%m'), '-', d)) as date_field,

                                    d
                                FROM (
                                    SELECT 
                                        tb_2.jumlah as jumlah_pes,
                                        tb_3.jumlah as jumlah_finish,
                                        tb_1.*

                                    FROM 
                                    (SELECT 
                                            count(client_ip) as jumlah,
                                            date, 
                                            TRIM( LEADING 0 FROM DATE_FORMAT(date, '%d')) as number_date
                                        FROM 
                                        (SELECT 
                                            client_ip, 
                                            DATE(created_at) as date
                                        FROM `tracker_sessions`
                                        WHERE user_id IS NULL
                                        AND MONTH(created_at) = $bulan AND YEAR(created_at) = $tahun
                                        GROUP BY client_ip, DATE(created_at)
                                        ) as n
                                    GROUP BY n.date ) as tb_1

                                    LEFT JOIN (
                                        SELECT 
                                                count(client_ip) as jumlah,
                                                date, 
                                                TRIM( LEADING 0 FROM DATE_FORMAT(date, '%d')) as number_date
                                            FROM 
                                            (SELECT 
                                                client_ip, 
                                                DATE(a.created_at) as date
                                            FROM `tracker_sessions` a
                                            JOIN users b ON a.user_id = b.id
                                            WHERE a.user_id IS NOT NULL AND (b.is_done IS NULL OR b.is_done = 0) AND b.role = 4
                                            AND MONTH(a.created_at) = $bulan AND YEAR(a.created_at) = $tahun
                                            GROUP BY client_ip, DATE(a.created_at)
                                            ) as n
                                        GROUP BY n.date
                                    ) as tb_2 ON tb_1.date = tb_2.date
                                    LEFT JOIN (
                                        SELECT 
                                                count(client_ip) as jumlah,
                                                date, 
                                                TRIM( LEADING 0 FROM DATE_FORMAT(date, '%d')) as number_date
                                            FROM 
                                            (SELECT 
                                                client_ip, 
                                                DATE(a.created_at) as date
                                            FROM `tracker_sessions` a
                                            JOIN users b ON a.user_id = b.id
                                            WHERE a.user_id IS NOT NULL AND b.is_done = 1 AND b.role = 4
                                            AND MONTH(a.created_at) = $bulan AND YEAR(a.created_at) = $tahun
                                            GROUP BY client_ip, DATE(a.created_at)
                                            ) as n
                                        GROUP BY n.date
                                    ) as tb_3 ON tb_2.date = tb_3.date
                                ) as x
                                RIGHT JOIN (SELECT d FROM `days` WHERE d BETWEEN 1 AND DATE_FORMAT(LAST_DAY('$date1'), '%d') ORDER BY d ASC) y ON x.number_date = y.d  
                                ORDER BY  CAST(d as UNSIGNED) ASC 
                "));
            

                foreach($cvisitor as $item){
                    $temp=array(
                        "date_field"=>$item->date_field,
                        "total_visitor"=>$item->total_visitor,
                        "total_peserta"=>$item->total_peserta,
                        "total_selesai"=>$item->total_selesai
                    );
                    array_push($arrv,$temp);
                }
                // $cpeserta = json_encode($arrc);
                // $peserta = json_encode($arrp);
                $cvisitor = json_encode($arrv);
                $info_peserta = $this->info_peserta();
                return view('pages.dashboard.list',compact('visitor','login','pendaftar','cvisitor','info_peserta'));
            }
        // }
        // if(Auth::guard('web')->user()->register_code != null || Auth::guard('web')->user()->register_code != ''){
            return view('pages.dashboard.main');
        // }else{
        //     return redirect()->route('web.profile.index');
        // }
    }
    public function download($id)
    {
        $data = User::where('id',Auth::guard('web')->user()->id)->first();
        $data->is_unduh = 1;
        $data->update();
        // $file= public_path(). "/file.pdf";

        // $headers = array(
        //         'Content-Type: application/pdf',
        //         );
        // echo $id;
        $kak_array = array(
        						1=> '1_IstanaWapres.pdf',
        						2=> '2_Legislatif.pdf',
        						3=> '3_Yudikatif.pdf',
        						4=> '4_KomplekPeribadatan.pdf',
                                5=> '1280-Hasil-Sayembara-Istana-Wakil-Presiden.pdf',
                                6=> '1280.1-Hasil-Sayembara-Kompleks-Perkantoran-Legislatif.pdf',
                                7=> '1280.2-Hasil-Sayembara-Kompleks-Perkantoran-yudikatif.pdf',
                                8=> '1280.3-Hasil-Sayembara-Kompleks-Peribadatan.pdf',
    						);
        // return;
        // return Response::download($file, 'file.pdf', $headers);
        return response()->json([
            'alert' => 'success',
            'message' => 'Unduh file selesai',
            'url' => asset('dokumen/'.$kak_array[$id])
        ]);
    }

    public function statistik(Request $request)
    {
        // DB::enableQueryLog();

        // if(Auth::guard('web')->user()->role >= 4){
        //     return abort(404);
        // }

        if ($request->ajax()) {
            // $to = Carbon::createFromFormat('2022-04-16 H:s:i', '2015-5-5 3:30:34');
            // $from = Carbon::createFromFormat('Y-m-d H:s:i', '2015-5-6 9:30:34');
            // $diff_in_days = $to->diffInDays($from);
            // print_r($diff_in_days); // Output: 1
           
            // ------------ Jumlah kelengkapan ketua -----------------//
                $cvisitor =  DB::select(DB::raw("SELECT COUNT(*) as jumlah FROM users b WHERE b.user_id = 0 AND b.role = 4 "));
                $jumlah_pendaftar = intval(@$cvisitor[0]->jumlah);

                $lengkap_ketua =  DB::select(DB::raw("
                    SELECT COUNT(*) as jumlah 
                    FROM users b WHERE b.user_id = 0 AND b.role = 4 AND b.name IS NOT NULL AND b.ktp IS NOT NULL AND b.ska IS NOT NULL AND b.npwp IS NOT NULL AND b.ijazah IS NOT NULL
                "));
                $jumlah_ketua = intval(@$lengkap_ketua[0]->jumlah);
                $jumlah_ketua = array(
                                        'kategori' => 'Ketua', 
                                        'jumlah1'=> $jumlah_ketua,
                                        'jumlah2'=>intval($jumlah_pendaftar - $jumlah_ketua), // yang belum lengkap
                                        'jumlah3'=>$jumlah_pendaftar, // jumlah total
                                        'color'=> '#961a2c'
                                    );

                $total_pendaftar = array(
                    'kategori' => 'Pendaftar', 
                    'jumlah1'=> intval(@$jumlah_pendaftar),
                    'jumlah2'=> intval(0), // yang belum lengkap
                    'jumlah3'=> intval(0), // jumlah total
                    'color'=> '#a7a5a5'
                );
            // ------------ end Jumlah kelengkapan ketua -----------------//
            
            // ------------ start Jumlah kelengkapan badan usaha -----------------//
                $lengkap_bu =  DB::select(DB::raw("
                                SELECT 
                                    *, 
                                total - lengkap as belum
                                FROM 
                                (SELECT 
                                    (SELECT COUNT(a.user_id) FROM users a, user_companies c WHERE a.id = c.user_id AND c.name IS NOT NULL AND c.akta IS NOT NULL AND c.sbu IS NOT NULL AND c.siujk IS NOT NULL AND c.npwp IS NOT NULL AND c.file_dukungan_perusahaan IS NOT NULL AND a.user_id = 0 AND a.role = 4) as lengkap, 
                                    (SELECT COUNT(*)  FROM users a, user_companies c WHERE a.id = c.user_id AND a.user_id = 0 AND a.role = 4) as total
                                ) as n
                "));
                $jumlah_perusahaan = array(
                    'kategori' => 'Perusahaan', 
                    'jumlah1'=> intval(@$lengkap_bu[0]->lengkap),
                    'jumlah2'=> intval(@$lengkap_bu[0]->belum), // yang belum lengkap
                    'jumlah3'=> intval(@$lengkap_bu[0]->total), // jumlah total
                    'color'=> '#1c3144'
                );
            // ------------ end Jumlah kelengkapan badan usaha -----------------//

            // ------------ Start Jumlah kelengkapan dokumen anggota -----------------//
                /*
                    $query_backup= "
                                SELECT 
                                    *, 
                                    total - lengkap as belum 
                                FROM 
                                    (SELECT 
                                        (SELECT SUM(IF((SELECT COUNT(user_id) FROM users x WHERE n.user_id = x.id LIMIT 1 ) = 1, 1, 0)) as jumlah FROM (SELECT user_id FROM users n WHERE user_id != 0 AND name IS NOT NULL AND ijazah IS NOT NULL GROUP BY user_id) as n) as lengkap, 
                                        (SELECT SUM(IF((SELECT COUNT(user_id) FROM users x WHERE n.user_id = x.id LIMIT 1 ) = 1, 1, 0)) as jumlah FROM (SELECT user_id FROM users n WHERE user_id != 0 GROUP BY user_id) as n) as total 
                                    ) as n";
                */
                $lengkap_anggota =  DB::select(DB::raw("
                                SELECT 
                                    *, 
                                    total - lengkap as belum 
                                FROM 
                                    (SELECT 
                                        (SELECT COUNT(*) as jumlah FROM (SELECT user_id, urutan_anggota FROM (SELECT a.id, a.name, a.ijazah, a.ska_kualifikasi_id, a.ska, a.user_id, (@cnt := if(@grp = a.user_id, @cnt + 1, 1)) AS urutan_anggota, (@grp := a.user_id) as temp_group FROM users a CROSS JOIN (SELECT @cnt := 0, @grp := null) AS dummy JOIN users b ON a.user_id = b.id WHERE a.user_id != 0 AND a.role = 4 AND a.name IS NOT NULL AND a.ijazah IS NOT NULL ORDER BY a.user_id ASC, a.id ASC) n WHERE (CASE WHEN urutan_anggota <= 4 THEN ska IS NOT NULL ELSE TRUE END) GROUP BY user_id ASC , urutan_anggota ASC having urutan_anggota = 4) as o) as lengkap, 

                                        (SELECT SUM(IF((SELECT COUNT(user_id) FROM users x WHERE n.user_id = x.id LIMIT 1 ) = 1, 1, 0)) as jumlah FROM (SELECT user_id FROM users n WHERE user_id != 0 AND role = 4 GROUP BY user_id) as n) as total
                                    ) as n
                "));
            
                $jumlah_anggota = array(
                    'kategori' => 'Anggota', 
                    'jumlah1'=> intval(@$lengkap_anggota[0]->lengkap),
                    'jumlah2'=> intval(@$lengkap_anggota[0]->belum), // yang belum lengkap
                    'jumlah3'=> intval(@$lengkap_anggota[0]->total), // jumlah total
                    'color'=> '#a7a5a5'
                );
            // ------------ End Jumlah kelengkapan dokumen anggota -----------------//
            // ------------ Start Jumlah Sudah Memilih Sayembara (Minat) -----------------//
                $sudah_pilih =  DB::select(DB::raw("
                                    SELECT
                                        COUNT(id) as jumlah
                                    FROM
                                        `users`
                                    WHERE NOT
                                        (
                                            jenis_sayembara IS NULL OR jenis_sayembara = '' OR jenis_sayembara = '[]'
                                        ) AND user_id = 0 AND role = 4
                "));

                $jumlah_minat = array(
                    'kategori' => 'Pilih Sayembara', 
                    'jumlah1'=> intval(@$sudah_pilih[0]->jumlah),
                    'jumlah2'=> intval(0), // yang belum lengkap
                    'jumlah3'=> intval(0), // jumlah total
                    'color'=> '#a7a5a5'
                );
            // ------------ End Jumlah Sudah Memilih Sayembara (Minat) -----------------//

            $total_array = array();
            $total_array[] = $total_pendaftar;
            $total_array[] = $jumlah_ketua;
            $total_array[] = $jumlah_perusahaan;
            $total_array[] = $jumlah_anggota;
            $total_array[] = $jumlah_minat;
                
            $total_array = json_encode($total_array);
            

            // --------------------- Start Statistik Jenis Sayembara -----------//
                $jenis_say =  DB::select(DB::raw("
                                SELECT
                                    (SELECT
                                        COUNT(id)
                                    FROM
                                        `users`
                                    WHERE
                                        jenis_sayembara IS NOT NULL AND jenis_sayembara != '' AND user_id = 0 AND role = 4 AND jenis_sayembara LIKE '%1%' ) AS jns_1,
                                    (SELECT
                                        COUNT(id)
                                    FROM
                                        `users`
                                    WHERE
                                        jenis_sayembara IS NOT NULL AND jenis_sayembara != '' AND user_id = 0 AND role = 4 AND jenis_sayembara LIKE '%2%') AS jns_2,
                                    (SELECT
                                        COUNT(id)
                                    FROM
                                        `users`
                                    WHERE
                                        jenis_sayembara IS NOT NULL AND jenis_sayembara != '' AND user_id = 0 AND role = 4 AND jenis_sayembara LIKE '%3%') AS jns_3,
                                    (SELECT
                                        COUNT(id)
                                    FROM
                                        `users`
                                    WHERE
                                        jenis_sayembara IS NOT NULL AND jenis_sayembara != '' AND user_id = 0 AND role = 4 AND jenis_sayembara LIKE '%4%') AS jns_4,
                                    
                                    (SELECT
                                        COUNT(id)
                                    FROM
                                        `users`
                                    WHERE
                                        user_id = 0 AND role = 4) AS total,
                                    (SELECT
                                        COUNT(id)
                                    FROM
                                        `users`
                                    WHERE
                                        (
                                            jenis_sayembara IS NULL OR jenis_sayembara = '' OR jenis_sayembara = '[]'
                                        ) AND user_id = 0 AND role = 4) AS belum_pilih,
                                    (SELECT
                                        COUNT(id)
                                    FROM
                                        `users`
                                    WHERE NOT
                                        (
                                            jenis_sayembara IS NULL OR jenis_sayembara = '' OR jenis_sayembara = '[]'
                                        ) AND user_id = 0 AND role = 4) AS sudah_pilih
                "));

                $array_jns = array();
                $array_jns[] = array(
                                        'jenis'=> 'Istana Wakil Presiden' ,
                                        'jumlah'=>intval(@$jenis_say[0]->jns_1),
                                        'color'=> '#ab6c6c'
                                    );
                $array_jns[] = array(
                                        'jenis'=> 'Perkantoran Legislatif' ,
                                        'jumlah'=> intval(@$jenis_say[0]->jns_2),
                                        'color'=> '#961a2c'
                                    );
                $array_jns[] = array(
                                        'jenis'=> 'Perkantoran Yudikatif',
                                        'jumlah'=> intval(@$jenis_say[0]->jns_3),
                                        'color'=> '#1c3144'
                                    );
                $array_jns[] = array(
                                        'jenis'=> 'Peribadatan' , 
                                        'jumlah'=> intval(@$jenis_say[0]->jns_4),
                                        'color'=> '#a7a5a5'
                                    );
                // $array_jns[] = array(
                //                         'jenis'=> 'Belum Memilih Sayembara', 
                //                         'jumlah'=> intval(@$jenis_say[0]->belum_pilih),
                //                         'color'=> '#ffba08'
                //                     );
                    
                $array_jns = json_encode($array_jns);

            // --------------------- End Statistik Jenis Sayembara -----------//

            // --------------------- Start Statistik Ketua Per Provinsi -----------//
                $query_ketua_prov =  DB::select(DB::raw("
                                    SELECT 
                                        IFNULL(jumlah, 0) as jumlah_ketua,
                                        id, 
                                        name
                                    FROM 
                                    (SELECT 
                                        COUNT(b.id) as jumlah, b.id as provinsi_id
                                        -- a.id, a.name, b.name
                                    FROM `users` a
                                    JOIN provinces b ON a.province_id = b.id
                                    WHERE a.user_id = 0 AND a.role = 4
                                    GROUP BY b.id ORDER BY b.id ASC) as n
                                    RIGHT JOIN provinces c ON n.provinsi_id = c.id
                "));
                $ketua_prov = array();
                foreach($query_ketua_prov as $ketua){
                    $ketua_prov[] = array(
                                        'provinsi' => $ketua->name,
                                        'jumlah' => $ketua->jumlah_ketua
                    );
                }
                $ketua_prov = json_encode($ketua_prov, JSON_PRETTY_PRINT);
            // --------------------- End Statistik Ketua Per Provinsi -----------//
            return view('pages.statistik.list',compact('total_array', 'array_jns', 'ketua_prov'));
        }
    
        return view('pages.statistik.main');
    
    }

    public function export_sayembara(Request $request){
        return Excel::download(new UsersExport, 'rekapitulasi_peserta.xlsx');

    }
    
    public function information(Request $request){
        $data = Auth::guard('web')->user();
        if ($request->ajax()) {
       		$view_info = $this->view_info();
            return $view_info;

        }
        return view('pages.peserta.finishing.main-info');
    }

    public function StepUp(Request $request){
        if(Auth::guard('web')->user()->role != 4){
            abort('404');
            return;
        }
        $user_session = Auth::user();
        $user_detail = User::with('verifikator')->where('id',$user_session->id)->first();
        
        if ($request->ajax()) {
            $kategori_list = $this->kategori_list;
            $link_download_kartu = $this->link_download_kartu;
            $data_dukung = $this->file_data_dukung;
            
            // --------------- Data For Info View ---------//
                $header = FALSE;
                $view_info = $this->view_info($header);
            // --------------- End Data For Info View ---------//

            // --------------- Data For dashboard KAK sayembara ---------//
                $kak_peserta = $this->info_peserta($header);
            // --------------- End Data For dashboard KAK sayembara ---------//

            // -------------- start view untuk materi aanwijzing ------------//
                $materi_aanwijzing = NULL;
                $berita_acara = NULL;
                $video_lapangan = NULL;

                if(strtolower(trim(Auth::user()->verifikator->st)) == 'lulus verifikasi'){
                    $materi_aanwijzing = $this->MateriAanwijzing();
                    $berita_acara = $this->BeritaAcaraAanwizjing();
                    
                    // if(Auth::user()->id == 397){
                    $video_lapangan = $this->VideoLapangan();
                    // }
                    
                }
            // -------------- end view untuk materi aanwijzing ------------//
            return view('pages.peserta.kartu.list', compact('user_detail','kategori_list','link_download_kartu','data_dukung','view_info','kak_peserta', 'materi_aanwijzing','berita_acara','video_lapangan'));
        }
        return view('pages.peserta.kartu.main',compact('user_detail'));
    }

    
    public function download_berita(Request $request, $jenis){
        if (! $request->hasValidSignature()) {
            return abort(401);
        }
        if(strtolower(trim(Auth::user()->verifikator->st)) != 'lulus verifikasi'){
            return abort(401);
        }
        if($jenis == 'berita'){
            return $this->ZipBA();
        }

        $jenis_file = array(
                        'wapres'=>'dokumen/addendum/1.2022-04-23-Addendum-KAK-Sayembara-Istana-Wapres.pdf',
                        'legislatif'=>'dokumen/addendum/2.2022.04.23_Addendum-KAK-Kompleks-Lembaga-Legislatif.pdf',
                        'yudikatif'=>'dokumen/addendum/3.2022-04-23-Addendum-KAK-Sayembara-Yudikatif.pdf',
                        'peribadatan'=>'dokumen/addendum/4.2022-04-23-Addendum-KAK-Sayembara-Komplek-Peribadatan.pdf',
        );


        $filepath = public_path($jenis_file[$jenis]);
        return response()->download($filepath);

    }
    private function ZipBA(){

        $zip = new ZipArchive();
        $fileName = 'Berita_Acara.zip';
        if ($zip->open(public_path($fileName), ZipArchive::CREATE)== TRUE)
        {
            $files = File::files(public_path('dokumen/zip_berita_acara'));
            foreach ($files as $key => $value){
                $relativeName = basename($value);
                $zip->addFile($value, $relativeName);
            }
            $zip->close();
        }

        return response()->download(public_path($fileName))->deleteFileAfterSend(true);
    }
    public function ZipDownload(Request $request, $user){
        if (! $request->hasValidSignature()) {
            return abort(401);
        }
        $limit_download = 1;

        if(strtolower(trim(Auth::user()->verifikator->st)) != 'lulus verifikasi'){
            return abort(401);
        }
        
        DB::table('user_verification')
                ->where('user_id', Auth::user()->id)
                ->update(['count_download' => Auth::user()->verifikator->count_download + 1]);

        $zip = new ZipArchive();
        $fileName = 'Data_dukung_tambahan.zip';
        if ($zip->open(public_path($fileName), ZipArchive::CREATE)== TRUE)
        {
            $files = File::files(public_path('dokumen/zip_folder'));
            foreach ($files as $key => $value){
                $relativeName = basename($value);
                $zip->addFile($value, $relativeName);
            }
            $zip->close();
        }

        return response()->download(public_path($fileName))->deleteFileAfterSend(true);
    }
    public function agreement(Request $request){
        $kondisi_lulus = Auth::user()->verifikator->st;
        $kondisi_data = Auth::user()->verifikator->agree;
        
        if(strtolower($kondisi_lulus) != 'lulus verifikasi'){
            abort(404, 'Not found');

             return;
        }
        // if($kondisi_data !== null){
        //      abort(404, 'Not found');
        //      return;
        // }

        DB::table('user_verification')
                ->where('user_id', Auth::user()->id)
                ->update(['agree' => 1]);
        
        return URL::temporarySignedRoute('web.StepUp.zip-download',now()->addMinutes(5),['user' => Auth::user()->id]);

    }

    public function kartu_peserta(Request $request, $sayembara_name){
        // $pdf = App::make('dompdf.wrapper');
        // $pdf->loadHTML('<h1>Test</h1>');
        // return $pdf->stream();
        if(strtolower(Auth::user()->verifikator->st) != 'lulus verifikasi'){
            abort('404');
            return;
        }
        
        if(!in_array( $sayembara_name, $this->link_download_kartu)){
            abort('404');
            return;
        }
        $value_int = array_keys($this->link_download_kartu, $sayembara_name);
      
        $user_profile = Auth::guard('web')->user();
        $sayembara_pilihan = json_decode($user_profile->jenis_sayembara);
        if(!in_array(@$value_int[0], $sayembara_pilihan)){
            abort('404');
            return;
        }

        $data_karya = DB::table('karya')
                        ->where('user_id', Auth::user()->id)
                        ->where('jenis_sayembara', @$value_int[0])
                        ->first();

        $nomor_urut = @$data_karya->no_reg;
        if(!$nomor_urut){
            abort('404');
            return;
        }

        $sayembara_check = array(
                                    'wapres' => array(
                                                    'template' => 'Iswapres_v2.pdf',
                                                    'text' => 'KOMPLEKS ISTANA WAKIL PRESIDEN',
                                                    'prefix' => NULL,
                                                    'color' => array('r' => 0, 'g' => 0, 'b'=> 0),    
                                                ),
                                    'legislatif' => array(
                                                        'template' => 'Legislatif.pdf',
                                                        'text' => 'KOMPLEKS PERKANTORAN LEGISLATIF',
                                                        'prefix' => NULL,
                                                        'color' => array('r' => 0, 'g' => 0, 'b'=> 0),    
                                                    ),
                                    'yudikatif'=>   array(
                                                        'template' => 'Yudikatif.pdf',
                                                        'text' => 'KOMPLEKS PERKANTORAN YUDIKATIF',
                                                        'prefix' => NULL,
                                                        'color' =>  array('r' => 255, 'g' => 255, 'b'=> 255),    
                                                    ),
                                    'peribadatan'=> array(
                                                        'template' => 'Peribadatan.pdf',
                                                        'text' => 'KOMPLEKS PERIBADATAN',
                                                        'prefix' => NULL,
                                                        'color' => array('r' => 255, 'g' => 255, 'b'=> 255),    
                                                    ),
                                );
        
         

        $pdf = new Fpdi();
        $pdf->AddFont('bebas','','BebasNeue-Regular.php');

        $r = $sayembara_check[$sayembara_name]['color']['r'];
        $g = $sayembara_check[$sayembara_name]['color']['g'];
        $b = $sayembara_check[$sayembara_name]['color']['b'];

        $pdf->SetTextColor($r,$g,$b);
        // get the page count
        $template_name = $sayembara_check[$sayembara_name]['template'];
        $pageCount = $pdf->setSourceFile('public/dokumen/template_kartu/'.$template_name);
        // iterate through all pages
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            // import a page
            $templateId = $pdf->importPage($pageNo);
        
            $pdf->AddPage();
            // use the imported page and adjust the page size

            $pdf->useTemplate($templateId, ['adjustPageSize' => true]);
            $pdf->SetFont('bebas','',30);
            $pdf->SetXY(37, 55);
            $pdf->Write(5, 'PESERTA');

            $pdf->SetFont('bebas','',100);
            $pdf->SetXY(17, 80);
            $nomor_peserta = $sayembara_check[$sayembara_name]['prefix'].$nomor_urut;
            $pdf->Write(5, $nomor_peserta);

            $pdf->SetFont('bebas','',25);
            $pdf->SetXY(0, 100);
            
            $pdf->MultiCell(105,10,$sayembara_check[$sayembara_name]['text'],0,'C');

        }
        
        // Output the new PDF
        $pdf->Output();
        // $pdf->Output('D', 'kartu_peserta_'.$sayembara_name.'.pdf');
    }
}
