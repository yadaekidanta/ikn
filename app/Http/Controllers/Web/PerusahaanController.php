<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Models\Session;
use Illuminate\Http\Request;
use App\Models\TrackerSession;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\UserCompany as Perusahaan;
use Illuminate\Support\Facades\Validator;

class PerusahaanController extends Controller
{
    public function index(Request $request)
    {
        $data = Auth::guard('web')->user();
        if ($request->ajax()) {
            $user = Auth::guard('web')->user();
            // $perusahaan = new Perusahaan;
            // print_r($perusahaan->getTable());
            // return;
            $data = Perusahaan::where('user_id',$user->id)->get()->count() > 0 ? Perusahaan::where('user_id',$user->id)->first() : new Perusahaan();

            $complete = User::calculate_profile($user);
            $completep = $data->calculate_company($data);
            $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
            return view('pages.peserta.perusahaan.list',compact('user','last','data','complete','completep'));
        }
        return view('pages.peserta.perusahaan.main',compact('data'));
    }
    public function create(){
        $data = new Perusahaan();
        $user = Auth::guard('web')->user();
        $complete = $user->calculate_profile($data);
        $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
        return view('pages.peserta.perusahaan.input',compact('user','last','data','complete'));
    }
    public function store(Request $request)
    {
        $user = Auth::guard('web')->user();
        $validator = Validator::make($request->all(), [
            'nama_perusahaan' => 'required',
            'alamat' => 'required',
            'no_npwp' => 'required',
            'npwp' => 'required|max:2000',
            'akta' => 'required|max:2000',
            'akta_perubahan' => 'max:2000',
            'siujk' => 'required|max:2000',
            'siujk_date' => 'required',
            'sbu' => 'required',
            // 'sbu_no' => 'required',
            'sbu_file' => 'max:2000',
            'file_dukungan_perusahaan' => 'required|max:2000',
            
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama_perusahaan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_perusahaan'),
                ]);
            }
            elseif($errors->has('alamat')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alamat'),
                ]);
            }elseif($errors->has('no_npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_npwp'),
                ]);
            }elseif($errors->has('npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('npwp'),
                ]);
            }elseif($errors->has('akta')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('akta'),
                ]);
            }elseif($errors->has('akta_perubahan')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('akta_perubahan'),
                ]);
            }elseif($errors->has('siujk')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('siujk'),
                ]);
            }elseif($errors->has('siujk_date')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('siujk_date'),
                ]);
            }
            elseif($errors->has('sbu')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sbu'),
                ]);
            }
            elseif($errors->has('sbu_file')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sbu_file'),
                ]);
            }
            elseif($errors->has('sbu_no')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sbu_no'),
                ]);
            }
            elseif($errors->has('file_dukungan_perusahaan')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('file_dukungan_perusahaan'),
                ]);
            }
        }
        $data = new Perusahaan();
        $data->user_id = $user->id;
        $data->name = $request->nama_perusahaan;
        $data->address = $request->alamat;
        $data->npwp_no = $request->no_npwp;
        $data->sbu = $request->sbu;
        $data->sbu_no = $request->sbu_no;
        $data->siujk_date = $request->siujk_date;
        // print_r($data);
        // return;
        if(request()->file('npwp')){
            Storage::delete($data->npwp);
            $npwp = request()->file('npwp')->store("npwp");
            $data->npwp = $npwp;
        }
        if(request()->file('akta')){
            Storage::delete($data->akta);
            $akta = request()->file('akta')->store("akta");
            $data->akta = $akta;
        }
        if(request()->file('akta_perubahan')){
            Storage::delete($data->akta_perubahan);
            $akta_perubahan = request()->file('akta_perubahan')->store("akta");
            $data->akta_perubahan = $akta_perubahan;
        }
        if(request()->file('siujk')){
            Storage::delete($data->siujk);
            $siujk = request()->file('siujk')->store("siujk");
            $data->siujk = $siujk;
        }
        if(request()->file('file_dukungan_perusahaan')){
            Storage::delete($data->file_dukungan_perusahaan);
            $file_dukungan_perusahaan = request()->file('file_dukungan_perusahaan')->store("surat_perusahaan");
            $data->file_dukungan_perusahaan = $file_dukungan_perusahaan;
        }
        if(request()->file('sbu_file')){
            Storage::delete($data->sbu_file);
            $sbu_file = request()->file('sbu_file')->store("sbu");
            $data->sbu_file = $sbu_file;
        }

        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Perusahaan berhasil terdaftar',
            'redirect' => 'href',
            'href' => route('web.anggota.index'),
        ]);
    }
    public function show(Perusahaan $perusahaan)
    {
        $data = $perusahaan;
        $user = Auth::guard('web')->user();
        $complete = $user->calculate_profile($data);
        $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
        return view('pages.peserta.perusahaan.show',compact('user','last','data','complete'));
    }
    public function edit(Perusahaan $perusahaan)
    {
        $data = $perusahaan;
        $user = Auth::guard('web')->user();
        $complete = $user->calculate_profile($data);
        $last = Session::where('user_id',$user->id)->latest('last_activity')->first();
        return view('pages.peserta.perusahaan.input',compact('user','last','data','complete'));
    }
    public function update(Request $request, Perusahaan $perusahaan)
    {
        $vnpwp = $request->no_ktp ? 'required|unique:users,npwp_no,'.$perusahaan->id : '';
        $vakta = $request->akta ? 'max:2000' : '';
        if($perusahaan->npwp){
            $vfnpwp = $request->npwp ? 'max:2000' : '';
        }else{
            $vfnpwp = $request->npwp ? 'max:2000' : 'required';
        }
        if($perusahaan->akta){
            $vfakta = $request->akta ? 'max:2000' : '';
        }else{
            $vfakta = $request->akta ? 'max:2000' : 'required';
        }
        if($perusahaan->siujk){
            $vfsiujk = $request->siujk ? 'max:2000' : '';
        }else{
            $vfsiujk = $request->siujk ? 'max:2000' : 'required';
        }
        if($perusahaan->file_dukungan_perusahaan){
            $vffile_dukungan_perusahaan = $request->file_dukungan_perusahaan ? 'max:2000' : '';
        }else{
            $vffile_dukungan_perusahaan = $request->file_dukungan_perusahaan ? 'max:2000' : 'required';
        }

        if($perusahaan->sbu_file){
            $file_sbu_check = $request->sbu_file ? 'max:2000' : '';
        }else{
            $file_sbu_check = $request->sbu_file ? 'max:2000' : 'required';
        }

        $validator = Validator::make($request->all(), [
            'nama_perusahaan' => 'required',
            'alamat' => 'required',
            'no_npwp' => 'required',
            'npwp' => $vfnpwp,
            'akta' => $vfakta,
            'akta_perubahan' => 'max:2000',
            'siujk' => $vfsiujk,
            'siujk_date' => 'required',
            'sbu' => 'required',
            // 'sbu_no' => 'required',
            'sbu_file' => 'max:2000',
            'file_dukungan_perusahaan' => $vffile_dukungan_perusahaan,
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama_perusahaan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_perusahaan'),
                ]);
            }
            elseif($errors->has('alamat')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alamat'),
                ]);
            }elseif($errors->has('no_npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_npwp'),
                ]);
            }elseif($errors->has('npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('npwp'),
                ]);
            }elseif($errors->has('akta')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('akta'),
                ]);
            }elseif($errors->has('akta_perubahan')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('akta_perubahan'),
                ]);
            }elseif($errors->has('siujk')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('siujk'),
                ]);
            }elseif($errors->has('siujk_date')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('siujk_date'),
                ]);
            }
            elseif($errors->has('sbu')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sbu'),
                ]);
            }
            elseif($errors->has('sbu_no')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sbu_no'),
                ]);
            }
            elseif($errors->has('sbu_file')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sbu_file'),
                ]);
            }
            elseif($errors->has('file_dukungan_perusahaan')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('file_dukungan_perusahaan'),
                ]);
            }
        }
        $perusahaan->name = $request->nama_perusahaan;
        $perusahaan->address = $request->alamat;
        $perusahaan->npwp_no = $request->no_npwp;
        $perusahaan->sbu = $request->sbu;
        $perusahaan->siujk_date = $request->siujk_date;
        $perusahaan->sbu_no = $request->sbu_no;
        if(request()->file('npwp')){
            Storage::delete($perusahaan->npwp);
            $npwp = request()->file('npwp')->store("npwp");
            $perusahaan->npwp = $npwp;
        }
        if(request()->file('akta')){
            Storage::delete($perusahaan->akta);
            $akta = request()->file('akta')->store("akta");
            $perusahaan->akta = $akta;
        }
        if(request()->file('akta_perubahan')){
            Storage::delete($perusahaan->akta_perubahan);
            $akta_perubahan = request()->file('akta_perubahan')->store("akta");
            $perusahaan->akta_perubahan = $akta_perubahan;
        }
        if(request()->file('siujk')){
            Storage::delete($perusahaan->siujk);
            $siujk = request()->file('siujk')->store("siujk");
            $perusahaan->siujk = $siujk;
        }
        if(request()->file('file_dukungan_perusahaan')){
            Storage::delete($perusahaan->file_dukungan_perusahaan);
            $file_dukungan_perusahaan = request()->file('file_dukungan_perusahaan')->store("surat_perusahaan");
            $perusahaan->file_dukungan_perusahaan = $file_dukungan_perusahaan;
        }
        if(request()->file('sbu_file')){
            Storage::delete($perusahaan->sbu_file);
            $sbu_file = request()->file('sbu_file')->store("sbu");
            $perusahaan->sbu_file = $sbu_file;
        }

        $perusahaan->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Perusahaan berhasil diperbarui',
            'redirect' => 'href',
            'href' => route('web.anggota.index'),
        ]);
    }
}
