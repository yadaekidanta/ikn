<?php

namespace App\Http\Controllers\Web;

use App\Models\Sub;
use App\Models\City;
use App\Models\User;
use App\Models\Session;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Models\Qualification;
use App\Models\TrackerSession;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $data = Auth::guard('web')->user();
        if ($request->ajax()) {
            $data = Auth::guard('web')->user();
            $complete = $data->calculate_profile($data);
            $company = $data->company;
            $last = TrackerSession::where('user_id',$data->id)->latest('created_at')->first();
            $collection = User::where('user_id',$data->id)->get();
            $sub = Sub::where('id','>','1')->get();
            $kualifikasi = Qualification::get();
            $province = Province::get();
            return view('pages.peserta.profile.list',compact('data','last','collection','company','complete','sub','kualifikasi','province'));
        }
        return view('pages.peserta.profile.main',compact('data'));
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show()
    {
        $data = Auth::guard('web')->user();
        $complete = $data->calculate_profile($data);
        $last = TrackerSession::where('user_id',$data->id)->latest('created_at')->first();
        return view('pages.peserta.profile.show',compact('data','last','complete'));
    }
    public function edit()
    {
        $sub = Sub::where('id','>','1')->get();
        $kualifikasi = Qualification::get();
        $province = Province::get();
        $data = Auth::guard('web')->user();
        $last = TrackerSession::where('user_id',$data->id)->latest('created_at')->first();
        $complete = $data->calculate_profile($data);
        return view('pages.peserta.profile.input',compact('data','province','kualifikasi','sub','complete','last'));
    }
    public function update(Request $request)
    {
        $data = User::where('id',Auth::guard('web')->user()->id)->first();
        if($data->ktp){
            $vfktp = $request->ktp ? 'max:2000' : '';
        }else{
            $vfktp = $request->ktp ? 'max:2000' : 'required';
        }
        if($data->npwp){
            $vfnpwp = $request->npwp ? 'max:2000' : '';
        }else{
            $vfnpwp = $request->npwp ? 'max:2000' : 'required';
        }
        if($data->ska){
            $vfska = $request->ska ? 'max:2000' : '';
        }else{
            $vfska = $request->ska ? 'max:2000' : 'required';
        }
        if($data->ijazah){
            $vfijazah = $request->ijazah ? 'max:2000' : '';
        }else{
            $vfijazah = $request->ijazah ? 'max:2000' : 'required';
        }

        $vktp = $request->no_ktp ? 'unique:users,ktp_no,'.$data->id : 'required';
        $vnpwp = $request->no_npwp ? 'unique:users,npwp_no,'.$data->id : 'required';
        $vnrka = $request->no_nrka ? 'unique:users,nrka_no,'.$data->id : 'required';
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'no_handphone' => 'required|unique:users,phone,'.$data->id,
            'email' => 'required|email|unique:users,email,'.$data->id,
            'no_ktp' => $vktp,
            'no_npwp' => $vnpwp,
            'no_nrka' => $vnrka,
            'ktp' => $vfktp,
            'npwp' => $vfnpwp,
            'sub' => 'required',
            'ska' => $vfska,
            'tanggal_ska' => 'required',
            'ijazah' => $vfijazah,
            'alamat' => 'required',
            'kode_pos' => 'required',
            'provinsi' => 'required',
            'kota' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if($errors->has('nama_lengkap')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama_lengkap'),
                ]);
            }elseif($errors->has('no_handphone')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_handphone'),
                ]);
            }elseif($errors->has('email')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }elseif ($errors->has('sub')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('sub'),
                ]);
            }elseif ($errors->has('tanggal_ska')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_ska'),
                ]);
            }elseif ($errors->has('no_ktp')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_ktp'),
                ]);
            }elseif($errors->has('no_npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_npwp'),
                ]);
            }elseif($errors->has('no_nrka')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('no_nrka'),
                ]);
            }elseif($errors->has('ktp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ktp'),
                ]);
            }elseif($errors->has('npwp')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('npwp'),
                ]);
            }elseif($errors->has('ska')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ska'),
                ]);
            }elseif($errors->has('ijazah')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('ijazah'),
                ]);
            }elseif($errors->has('tanggal_ska')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_ska'),
                ]);
            }elseif($errors->has('alamat')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('alamat'),
                ]);
            }elseif($errors->has('kode_pos')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode_pos'),
                ]);
            }elseif($errors->has('provinsi')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('provinsi'),
                ]);
            }elseif($errors->has('kota')){
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kota'),
                ]);
            }
        }
        $data->name = $request->nama_lengkap;
        $data->phone = $request->no_handphone;
        $data->email = $request->email;
        $data->ktp_no = $request->no_ktp;
        $data->npwp_no = $request->no_npwp;
        $data->nrka_no = $request->no_nrka;
        $data->ska_sub_id = $request->sub ? $request->sub : 0;
        $data->ska_kualifikasi_id = $request->kualifikasi != 'Pilih SKA Kualifikasi' ? $request->kualifikasi : 0;
        $data->province_id = $request->provinsi != 'Pilih Provinsi' ? $request->provinsi : 0;
        $data->province_id = $request->provinsi != 'Pilih Provinsi' ? $request->provinsi : 0;
        if($request->kota == 'Harap pilih Provinsi dahulu' || $request->kota == 'Pilih Kota / Kabupaten'){
            $data->city_id = 0;
        }else{
            $data->city_id = $request->kota;
        }
        $data->address = $request->alamat;
        $data->postcode = $request->kode_pos;
        $data->tanggal_ska = $request->tanggal_ska;
        if(request()->file('ktp')){
            Storage::delete($data->ktp);
            $ktp = request()->file('ktp')->store("ktp");
            $data->ktp = $ktp;
        }
        if(request()->file('npwp')){
            Storage::delete($data->npwp);
            $npwp = request()->file('npwp')->store("npwp");
            $data->npwp = $npwp;
        }
        if(request()->file('ska')){
            Storage::delete($data->ska);
            $ska = request()->file('ska')->store("ska");
            $data->ska = $ska;
        }
        if(request()->file('ijazah')){
            Storage::delete($data->ijazah);
            $ijazah = request()->file('ijazah')->store("ijazah");
            $data->ijazah = $ijazah;
        }
        $data->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Profil tersimpan',
            'redirect' => 'href',
            'href' => route('web.perusahaan.index'),
        ]);
    }
    public function destroy()
    {
        //
    }
    public function deactivate()
    {
        //
    }
}