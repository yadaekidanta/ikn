<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerificationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $st;
    public function __construct($user,$st)
    {
        $this->user = $user;
        $this->st = $st;
    }
    public function build()
    {
        $notifiable = $this->user;
        $st = $this->st;
        return $this->view('pages.email.announcement',compact('notifiable','st'));
    }
}
