<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use HasFactory;
    public $table = "faq";
    public function faqCategory(){
        return $this->belongsTo(FaqCategory::class,'faq_category_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class,'created_by','id');
    }
}
