<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    use HasFactory;
    public $table = "faq_category";
    public function faq(){
        return $this->hasMany(Faq::class,'faq_category_id','id');
    }
}
