<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karya extends Model
{
    use HasFactory;
    public $table = "karya";
    public function karya_file(){
        return $this->hasMany(KaryaFile::class,'karya_id','id');
    }
}
