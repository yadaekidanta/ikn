<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KaryaFile extends Model
{
    use HasFactory;
    public $table = "karya_file";
}
