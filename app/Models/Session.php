<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    use HasFactory;
    public $table = "sessions";
    public $timestamps = false;
    protected $dates = ['date'];
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
