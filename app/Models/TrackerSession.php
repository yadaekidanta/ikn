<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackerSession extends Model
{
    use HasFactory;
    protected $dates = ["date","created_at","updated_at"];
}
