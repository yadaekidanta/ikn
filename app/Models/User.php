<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $dates = ["verified_at"];
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public static function calculate_profile($profile)
    {
        if ( ! $profile) {
            return 0;
        }
        $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($profile->makeHidden(['id','register_code','name','username','phone','email','email_verified_at','password','country_id','ktp','npwp','ska','tanggal_ska','role','user_id','st','is_unduh','is_cetak','is_unggah','is_done','jenis_sayembara','remember_token','created_at','updated_at','verified_at','verified_by','karya_verified_at','karya_verified_by'])->toArray()), PREG_GREP_INVERT);
        $per_column = count($columns) > 1 ? 100 / count($columns) : 0;
        $total      = 0;
    
        foreach ($profile->toArray() as $key => $value) {
            if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
                $total += $per_column;
            }
        }
        return round($total);
    }
    public static function calculate_anggota($profile)
    {
        if ( ! $profile) {
            return 0;
        }
        $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($profile->makeHidden(['id','register_code','username','email_verified_at','ska_kualifikasi','password','country_id','st','is_unduh','is_cetak','is_unggah','is_done','jenis_sayembara','remember_token','created_at','updated_at','verified_at','verified_by','karya_verified_at','karya_verified_by'])->toArray()), PREG_GREP_INVERT);
        $per_column = count($columns) > 1 ? 100 / count($columns) : 0;
        $total      = 0;
    
        foreach ($profile->toArray() as $key => $value) {
            if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
                $total += $per_column;
            }
        }
        return round($total);
    }
    public static function calculate_anggota_non($profile)
    {
        if ( ! $profile) {
            return 0;
        }
        $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($profile->makeHidden(['id','register_code','npwp_no','nrka_no','ska_sub_id','ska_kualifikasi_id','ska_kualifikasi','username','phone','email','email_verified_at','password','country_id','province_id','city_id','address','postcode','npwp','ska','tanggal_ska','role','is_unduh','is_cetak','is_unggah','is_done','user_id','jenis_sayembara','st','remember_token','created_at','updated_at','verified_at','verified_by','karya_verified_at','karya_verified_by'])->toArray()), PREG_GREP_INVERT);
        $per_column = count($columns) > 1 ? 100 / count($columns) : 0;
        $total      = 0;
    
        foreach ($profile->toArray() as $key => $value) {
            if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
                $total += $per_column;
            }
        }
        return round($total);
    }
    public static function calculate_doc($profile)
    {
        if ( ! $profile) {
            return 0;
        }
        $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($profile->makeHidden(['id','register_code','ktp_no','npwp_no','nrka_no','ska_sub_id','ska_kualifikasi_id','name','username','phone','email','email_verified_at','password','country_id','province_id','city_id','address','postcode','role','user_id','st','remember_token','created_at','updated_at','verified_at'])->toArray()), PREG_GREP_INVERT);
        $per_column = count($columns) > 1 ? 100 / count($columns) : 0;
        $total      = 0;
    
        foreach ($profile->toArray() as $key => $value) {
            if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
                $total += $per_column;
            }
        }
        return round($total);
    }
    public function ketua(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function company(){
        return $this->belongsTo(UserCompany::class,'id','user_id');
    }
    public function qualification(){
        return $this->belongsTo(Qualification::class,'ska_kualifikasi_id','id');
    }
    public function sub(){
        return $this->belongsTo(Sub::class,'ska_sub_id','id');
    }
    public function country(){
        return $this->belongsTo(Country::class,'country_id','id');
    }
    public function province(){
        return $this->belongsTo(Province::class,'province_id','id');
    }
    public function city(){
        return $this->belongsTo(City::class,'city_id','id');
    }
    public function verifikator(){
        return $this->belongsTo(UserVerification::class,'id','user_id');
    }
    public function anggota(){
        return $this->hasMany(User::class,'user_id','id');
    }
    public function karya(){
        return $this->belongsTo(Karya::class,'user_id','id');
    }
}
