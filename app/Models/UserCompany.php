<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model
{
    use HasFactory;
    public static function calculate_company($profile)
    {
        if ( ! $profile) {
            return 0;
        }
        $columns    = preg_grep('/(.+ed_at)|(.*id)/', array_keys($profile->makeHidden(['id','akta_perubahan','user_id','created_at','updated_at','verified_at','verified_by'])->toArray()), PREG_GREP_INVERT);
        $per_column = count($columns) > 1 ? 100 / count($columns) : 0;
        $total      = 0;
    
        foreach ($profile->toArray() as $key => $value) {
            if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
                $total += $per_column;
            }
        }
        return round($total);
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function sbup(){
        return $this->belongsTo(Sbu::class,'sbu','id');
    }
}
