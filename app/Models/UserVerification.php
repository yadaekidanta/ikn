<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model
{
    use HasFactory;
    public $table = "user_verification";
    
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function verif(){
        return $this->belongsTo(User::class,'created_by','id');
    }
}
