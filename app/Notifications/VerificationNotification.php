<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VerificationNotification extends Notification
{
    use Queueable;
    public $user;
    public $st;
    public function __construct($user,$st)
    {
        $this->user = $user;
        $this->st = $st;
        //
    }
    public function via($notifiable)
    {
        return ['mail','database'];
    }
    public function toMail($notifiable)
    {
        $st = $this->st;
        return (new MailMessage)->subject('Announcement Email')->from('panitia.sayembaraikn@pu.go.id')->view('pages.email.announcement',compact('notifiable','st'));
    }
    public function toArray($notifiable)
    {
        return [
            'tipe' => 2,
            'nama' => $this->user->name,
            'pesan' => "Peserta dengan nomor Tim ".$this->user->id." dinyatakan “Lulus Verifikasi”. Anda dapat mengunduh kartu peserta dan mengunduh data dukung (dalam format Autocad) sebagai bahan pembuatan karya."
        ];
    }
}
