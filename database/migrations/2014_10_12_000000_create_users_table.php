<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('register_code',20)->unique()->nullable();
            $table->string('ktp_no',16)->unique()->nullable();
            $table->string('npwp_no',20)->unique()->nullable();
            $table->string('nrka_no',20)->unique()->nullable();
            $table->integer('ska_sub_id')->nullable();
            $table->integer('ska_kualifikasi_id')->nullable();
            $table->string('name');
            $table->string('username',20)->unique();
            $table->string('phone',13)->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->smallInteger('country_id')->nullable();
            $table->smallInteger('province_id')->nullable();
            $table->integer('city_id')->default(0);
            $table->longText('address')->nullable();
            $table->string('postcode',5)->nullable();
            $table->string('ktp')->nullable();
            $table->string('npwp')->nullable();
            $table->string('ska')->nullable();
            $table->date('tanggal_ska')->nullable();
            $table->smallInteger('role')->default(4);
            $table->smallInteger('is_unduh')->nullable();
            $table->smallInteger('is_cetak')->nullable();
            $table->smallInteger('is_unggah')->nullable();
            $table->smallInteger('is_done')->nullable();
            $table->integer('user_id')->default(0);
            $table->string('jenis_sayembara',1)->nullable();
            $table->string('st',1)->default('n');
            $table->rememberToken();
            $table->timestamps();
            $table->timestamp('verified_at')->nullable();
            $table->integer('verified_by')->nullable();
            $table->timestamp('karya_verified_at')->nullable();
            $table->integer('karya_verified_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
