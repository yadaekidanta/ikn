<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_companies', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('name')->nullable();
            $table->longText('address')->nullable();
            $table->string('npwp_no',20)->nullable();
            $table->string('npwp')->nullable();
            $table->string('akta')->nullable();
            $table->string('akta_perubahan')->nullable();
            $table->string('surat_perjanjian_dukungan')->nullable();
            $table->string('siujk')->nullable();
            $table->string('sbu')->nullable();
            $table->timestamps();
            $table->timestamp('verified_at')->nullable();
            $table->integer('verified_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_companies');
    }
}
