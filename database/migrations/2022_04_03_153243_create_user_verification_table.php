<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserVerificationTable extends Migration
{
    public function up()
    {
        Schema::create('user_verification', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('st');
            $table->longText('ket');
            $table->integer('created_by');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('user_verification');
    }
}
