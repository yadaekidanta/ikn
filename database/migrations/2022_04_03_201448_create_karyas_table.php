<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKaryasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karya', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('no_reg')->nullable();
            $table->string('jenis_sayembara')->nullable();
            $table->string('karya')->nullable();
            $table->string('st')->nullable();
            $table->integer('verified_by')->nullable();
            $table->longText('ket')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karya');
    }
}
