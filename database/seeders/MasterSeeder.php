<?php

namespace Database\Seeders;

use App\Models\Day;
use App\Models\Sub;
use App\Models\Qualification;
use Illuminate\Database\Seeder;

class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qualification = array(
            [
                'code' => 'AA101',
                'name' => 'Arsitek',
            ],
            [
                'code' => 'AA102',
                'name' => 'Ahli Desain Interior',
            ],
            [
                'code' => 'AA103',
                'name' => 'Ahli Arsitektur Lansekap',
            ],
            [
                'code' => 'AA104',
                'name' => 'Ahli Iluminasi',
            ],
            [
                'code' => 'AE401',
                'name' => 'Ahli Teknik Tenaga Listrik',
            ],
            [
                'code' => 'AE405',
                'name' => 'Ahli Teknik Elektronika dan Telekomunikasi Dalam G',
            ],
            [
                'code' => 'AE406',
                'name' => 'Ahli Teknik Sistem Sinyal Telekomunikasi Kereta Ap',
            ],
            [
                'code' => 'AL601',
                'name' => 'Ahli Manajemen Konstruksi',
            ],
            [
                'code' => 'AL602',
                'name' => 'Ahli Manajemen Proyek',
            ],
            [
                'code' => 'AL603',
                'name' => 'Ahli K3 Konstruksi',
            ],
            [
                'code' => 'AL604',
                'name' => 'Ahli Sistem Manajemen Mutu',
            ],
            [
                'code' => 'AM301',
                'name' => 'Ahli Teknik Mekanikal',
            ],
            [
                'code' => 'AM302',
                'name' => 'Ahli Teknik Sistem Tata Udara dan Refrigerasi',
            ],
            [
                'code' => 'AM303',
                'name' => 'Ahli Teknik Plambing dan Pompa Mekanik',
            ],
            [
                'code' => 'AM304',
                'name' => 'Ahli Teknik Proteksi Kebakaran',
            ],
            [
                'code' => 'AM305',
                'name' => 'Ahli Teknik Transportasi Dalam Gedung',
            ],
            [
                'code' => 'AS201',
                'name' => 'Ahli Teknik Bangunan Gedung',
            ],
            [
                'code' => 'AS202',
                'name' => 'Ahli Teknik Jalan',
            ],
            [
                'code' => 'AS203',
                'name' => 'Ahli Teknik Jembatan',
            ],
            [
                'code' => 'AS204',
                'name' => 'Ahli Keselamatan Jalan',
            ],
            [
                'code' => 'AS205',
                'name' => 'Ahli Teknik Terowongan',
            ],
            [
                'code' => 'AS206',
                'name' => 'Ahli Teknik Landasan Terbang',
            ],
            [
                'code' => 'AS207',
                'name' => 'Ahli Teknik Jalan Rel',
            ],
            [
                'code' => 'AS208',
                'name' => 'Ahli Teknik Dermaga',
            ],
            [
                'code' => 'AS209',
                'name' => 'Ahli Teknik Bangunan Lepas Pantai',
            ],
            [
                'code' => 'AS210',
                'name' => 'Ahli Teknik Bendungan Besar',
            ],
            [
                'code' => 'AS211',
                'name' => 'Ahli Teknik Sungai dan Drainase',
            ],
            [
                'code' => 'AS214',
                'name' => 'Ahli Teknik Pembongkaran Bangunan',
            ],
            [
                'code' => 'AS215',
                'name' => 'Ahli Pemeliharaan dan Perawatan Bangunan',
            ],
            [
                'code' => 'AS216',
                'name' => 'Ahli Geoteknik',
            ],
            [
                'code' => 'AS217',
                'name' => 'Ahli Geodesi',
            ],
            [
                'code' => 'AT501',
                'name' => 'Ahli Teknik Lingkungan',
            ],
            [
                'code' => 'AT502',
                'name' => 'Ahli Perencanaan Wilayah dan Kota',
            ],
            [
                'code' => 'AT503',
                'name' => 'Ahli Teknik Sanitasi dan Limbah',
            ],
            [
                'code' => 'AT504',
                'name' => 'Ahli Teknik Air Minum',
            ],
        );
        $sub = array(
            [
                'name' => 'Muda',
            ],
            [
                'name' => 'Madya',
            ],
            [
                'name' => 'Utama',
            ],
        );
        $day = array(
            [
                'd' => '0'
            ],
            [
                'd' => '1'
            ],
            [
                'd' => '2'
            ],
            [
                'd' => '3'
            ],
            [
                'd' => '4'
            ],
            [
                'd' => '5'
            ],
            [
                'd' => '6'
            ],
            [
                'd' => '7'
            ],
            [
                'd' => '8'
            ],
            [
                'd' => '9'
            ],
            [
                'd' => '10'
            ],
            [
                'd' => '11'
            ],
            [
                'd' => '12'
            ],
            [
                'd' => '13'
            ],
            [
                'd' => '14'
            ],
            [
                'd' => '15'
            ],
            [
                'd' => '16'
            ],
            [
                'd' => '17'
            ],
            [
                'd' => '18'
            ],
            [
                'd' => '19'
            ],
            [
                'd' => '20'
            ],
            [
                'd' => '21'
            ],
            [
                'd' => '22'
            ],
            [
                'd' => '23'
            ],
            [
                'd' => '24'
            ],
            [
                'd' => '25'
            ],
            [
                'd' => '26'
            ],
            [
                'd' => '27'
            ],
            [
                'd' => '28'
            ],
            [
                'd' => '29'
            ],
            [
                'd' => '30'
            ],
            [
                'd' => '31'
            ],
        );
        foreach($qualification AS $q){
            Qualification::create([
                'code' => $q['code'],
                'name' => $q['name']
            ]);
        }
        foreach($sub AS $s){
            Sub::create([
                'name' => $q['name']
            ]);
        }
        foreach($day AS $d){
            Day::create([
                'd' => $d['d']
            ]);
        }
    }
}
