<?php

namespace Database\Seeders;

use App\Models\User;
use App\Helpers\Helper;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = array(
            [
                'ktp_no' => 1111111111111111,
                'npwp_no' => '11.111.111.1-111.111',
                'nrka_no' => 1111111111,
                'name' => 'Administrator',
                'username' => 'admin',
                'phone' => '1',
                'email' => 'admin@pu.go.id',
                'email_verified_at' => date("Y-m-d H:i:s"),
                'password' => Hash::make('password'),
                'country_id' => 102,
                'province_id' => 11,
                'city_id' => 159,
                'address' => 'Jl. Pattimura No. 20 Kebayoran Baru Jakarta Selatan',
                'postcode' => '12110',
                'role' => 1,
                'st' => 'a',

            ],
            [
                'ktp_no' => 2222222222222222,
                'npwp_no' => '22.222.222.2-222.222',
                'nrka_no' => 2222222222,
                'name' => 'Panitia',
                'username' => 'panitia',
                'phone' => '2',
                'email' => 'panitia@pu.go.id',
                'email_verified_at' => date("Y-m-d H:i:s"),
                'password' => Hash::make('password'),
                'country_id' => 102,
                'province_id' => 11,
                'city_id' => 159,
                'address' => 'Jl. Pattimura No. 20 Kebayoran Baru Jakarta Selatan',
                'postcode' => '12110',
                'role' => 2,
                'st' => 'a',

            ],
            [
                'ktp_no' => 3333333333333333,
                'npwp_no' => '33.333.333.3-333.333',
                'nrka_no' => 3333333333,
                'name' => 'Penilai',
                'username' => 'penilai',
                'phone' => '3',
                'email' => 'penilai@pu.go.id',
                'email_verified_at' => date("Y-m-d H:i:s"),
                'password' => Hash::make('password'),
                'country_id' => 102,
                'province_id' => 11,
                'city_id' => 159,
                'address' => 'Jl. Pattimura No. 20 Kebayoran Baru Jakarta Selatan',
                'postcode' => '12110',
                'role' => 3,
                'st' => 'a',
            ],
        );
        foreach($user AS $u){
            User::create([
                'register_code' => Helper::IDGenerator(new User, 'IKN-'),
                'ktp_no' => $u['ktp_no'],
                'npwp_no' => $u['npwp_no'],
                'nrka_no' => $u['nrka_no'],
                'name' => $u['name'],
                'username' => $u['username'],
                'phone' => $u['phone'],
                'email' => $u['email'],
                'email_verified_at' => $u['email_verified_at'],
                'password' => $u['password'],
                'country_id' => $u['country_id'],
                'city_id' => $u['city_id'],
                'address' => $u['address'],
                'postcode' => $u['postcode'],
                'role' => $u['role'],
                'st' => $u['st'],
            ]);
        }
    }
}
