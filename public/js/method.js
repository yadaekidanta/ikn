$("body").on("contextmenu", "img", function(e) {
    return false;
});
var audio = document.getElementById("audio");
$('img').attr('draggable', false);
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function() {
    $(window).keydown(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            // load_list(1);
        }
    });
});
let page;
$(window).on('hashchange', function() {
    if (window.location.hash) {
        page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            load_list(page);
        }
    }
});
$(document).ready(function() {
    $(document).on('click', '.paginasi', function(event) {
        event.preventDefault();
        $('.paginasi').removeClass('active');
        $(this).parent('.paginasi').addClass('active');
        // var myurl = $(this).attr('href');
        page = $(this).attr('halaman').split('page=')[1];
        load_list(page);
    });
});
function main_content(obj){
    $("#content_list").hide();
    $("#content_input").hide();
    $("#" + obj).show();
}
function load_list(page){
    $.get('?page=' + page, $('#content_filter').serialize(), function(result) {
        $('#list_result').html(result);
        main_content('content_list');
    }, "html");
}
function load_input(url){
    $.get(url, {}, function(result) {
        try {
            data = JSON.parse(result);
            Swal.fire(data.message, '', data.alert)
        } catch(e) {
            $('#content_input').html(result);
            main_content('content_input');
            return;
        }
    }, "html");
}
function handle_open_modal(url,modal,content){
    $.ajax({
        type: "POST",
        url: url,
        success: function (html) {
            $(modal).modal('show');
            $(content).html(html);
        },
        error: function () {
            $(content).html('<h3>Ajax Bermasalah !!!</h3>')
        },
    });
}
function handle_save(tombol, form, url, method){
    $(tombol).submit(function() {
        return false;
    });
    let data = $(form).serialize();
    $(tombol).prop("disabled", true);
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        beforeSend: function() {

        },
        success: function(response) {
            if (response.alert == "success") {
                success_toastr(response.message);
                $(form)[0].reset();
                $(tombol).prop("disabled", false);
                if(response.redirect == "input"){
                    load_input(response.route);
                }else if(response.redirect == "reload"){
                    location.reload();
                }else{
                    setTimeout(function() {
                        load_list(1);
                    }, 2000);
                }
            } else {
                error_toastr(response.message);
                setTimeout(function() {
                    $(tombol).prop("disabled", false);
                }, 2000);
            }
        },
    });
}
function handle_confirm(title, confirm_title, deny_title, method, route){
    Swal.fire({
        title: title,
        showDenyButton: true,
        showCancelButton: false,
        confirmButtonText: confirm_title,
        denyButtonText: deny_title,
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: method,
                url: route,
                dataType: 'json',
                success: function(response) {
                    if(response.alert == "success"){
                        if(response.redirect == "cart"){
                            load_cart(localStorage.getItem("route_cart"));
                        }else if(response.redirect == "input"){
                            load_input(response.route);
                        }else if(response.redirect == "reload"){
                            location.reload();
                        }else{
                            load_list(1);
                        }
                    }else{
                        Swal.fire(response.message, '', response.alert)
                    }
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Konfirmasi dibatalkan', '', 'info')
        }
    });
}
function handle_confirm_input(title, confirm_title, deny_title, method, route){
    Swal.fire({
        title: title,
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        confirmButtonText: confirm_title,
        // denyButtonText: deny_title,
        showCancelButton: deny_title,
        showLoaderOnConfirm: true,
        preConfirm: (ket) => {
            $.ajax({
                type: method,
                data: "ket="+ket,
                url: route,
                dataType: 'json',
                success: function(response) {
                    if(response.alert == "success"){
                        load_list(1);
                    }else{
                        Swal.showValidationMessage(response.message)
                    }
                }
            });
            // return fetch(`//api.github.com/users/${login}`).then(response => {
            //     if (!response.ok) {
            //         throw new Error(response.statusText)
            //     }
            //     return response.json()
            // }).catch(error => {
            //     Swal.showValidationMessage(
            //         `Request failed: ${error}`
            //     )
            // });
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.isConfirmed) {
            Swal.showValidationMessage(result.message)
            // Swal.fire({
            //     title: `${result.value.login}'s avatar`,
            //     imageUrl: result.value.avatar_url
            // })
        }
    });
}
function handle_upload(tombol, form, url, method){
    $(document).one('submit', form, function(e) {
        loading();
        let data = new FormData(this);
        data.append('_method', method);
        $(tombol).prop("disabled", true);
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            enctype: 'multipart/form-data',
            cache: false,
            contentType: false,
            resetForm: true,
            processData: false,
            dataType: 'json',
            beforeSend: function() {

            },
            success: function(response) {
                $(".progress").hide();
                loaded();
                $(tombol).prop("disabled", false);
                if (response.alert == "success") {
                    success_toastr(response.message);
                    $(form)[0].reset();
                    if(response.redirect == "href"){
                        location.href = response.href;
                    }else if(response.redirect == "input"){
                        load_input(response.route);
                    }else if(response.redirect == "tabulasi"){
                        $("#total").html(response.total);
                        $('#tipe').val('update');
                        $("#f0"+response.urut).addClass('custom-file');
                        $("#file"+response.urut).show();
                        $("#file"+response.urut).attr("href", response.file);
                    }else{
                        load_list(1);
                    }
                } else {
                    error_toastr(response.message);
                    setTimeout(function() {
                        $(tombol).prop("disabled", false);
                    }, 2000);
                }
            },
            error: function (a) {
                $("#lblMessage").html(a.responseText);
            },
            failure: function (a) {
                $("#lblMessage").html(a.responseText);
            },
            xhr: function () {
                var fileXhr = $.ajaxSettings.xhr();
                if (fileXhr.upload) {
                    $(".progress").show();
                    fileXhr.upload.addEventListener("progress", function (e) {
                        if (e.lengthComputable) {
                            var percentage = Math.ceil(((e.loaded / e.total) * 100));
                            $('.progress-bar').text(percentage + '%');
                            $('.progress-bar').width(percentage + '%');
                            if (percentage == 100) {
                                $('.progress-bar').text('100%');
                            }
                        }
                    }, false);
                }
                return fileXhr;
            }
        });
        return false;
    });
}
function handle_download(tombol,url){
    $(tombol).prop("disabled", true);
    $(tombol).attr("data-kt-indicator", "on");
    $.get(url, function(result) {
        if (result.alert == "success") {
            // $(download).attr({target: '_blank', href  : result.url, 'download' : result.url});
            // var href = $('.cssbuttongo').attr('href');
            window.open(result.url, '_blank');
            window.open(result.url1, '_blank');
            window.open(result.url2, '_blank');
            Swal.fire({ text: result.message, icon: "success", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } }).then(function() {
                load_list(1);
            });
        }else{
            Swal.fire({ text: result.message, icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
        }
        $(tombol).prop("disabled", false);
        $(tombol).removeAttr("data-kt-indicator");
    }, "json");
}