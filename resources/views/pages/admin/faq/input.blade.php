<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data FAQ
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <form id="form_input">
                <div class="card-body pt-0">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label class="required fs-6 fw-bold mb-2">FAQ Katergori</label>
                            <select class="form-control" name="faq_category_id" id="faq_category_id">
                                <option value="">Pilih FAQ Kategori</option>
                                    @foreach($category as $category)
                                        <option value="{{$category->id}}" {{$data->faq_category_id == $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Pertanyaan</label>
                            <textarea class="form-control" name="question" id="question" placeholder="Masukan Pertanyaan">{{$data->question}}</textarea>
                        </div>

                        <div class="col-lg-6">
                            <label class="required fs-6 fw-bold mb-2">Jawaban</label>
                            <textarea class="form-control" name="answer" id="answer" placeholder="Masukan Jawaban">{{$data->answer}}</textarea>
                        </div>

                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('web.faq.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('web.faq.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>