<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        File Karya
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="main_content('content_list');" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <form id="form_input">
                <div class="card-body pt-0">
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">File Karya</label>
                            <input type="file" name="file" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="min-w-150px mt-10 text-end">
                            <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.admin.karya.update_karya',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Unggah</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>