<div class="row row-eq-height">
    <div class="col-lg-3">
        <div class="card bg-primary card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <h5  class="fw-bolder text-white fs-6 mb-2">Istana Wakil Presiden</h5>
                    <span class="fw-bold text-white fs-5">{{@intval($jumlah_sayembara[1])}} peserta</span>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card bg-success card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <h5  class="fw-bolder text-white fs-6 mb-2">Perkantoran Legislatif</h5>
                    <span class="fw-bold text-white fs-5">{{@intval($jumlah_sayembara[2])}} peserta</span>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card bg-warning card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <h5  class="fw-bolder text-white fs-6 mb-2">Perkantoran Yudikatif</h5>
                    <span class="fw-bold text-white fs-5">{{@intval($jumlah_sayembara[3])}} peserta</span>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card bg-danger card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <h5  class="fw-bolder text-white fs-6 mb-2">Peribadatan</h5>
                    <span class="fw-bold text-white fs-5">{{@intval($jumlah_sayembara[4])}} peserta</span>
                </div>
                
            </div>
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-md-12 mb-4 mt-4">
        <span class="svg-icon svg-icon-2 svg-icon-gray-500">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <path opacity="0.3" d="M5.78001 21.115L3.28001 21.949C3.10897 22.0059 2.92548 22.0141 2.75004 21.9727C2.57461 21.9312 2.41416 21.8418 2.28669 21.7144C2.15923 21.5869 2.06975 21.4264 2.0283 21.251C1.98685 21.0755 1.99507 20.892 2.05201 20.7209L2.886 18.2209L7.22801 13.879L10.128 16.774L5.78001 21.115Z" fill="black"></path>
                <path d="M21.7 8.08899L15.911 2.30005C15.8161 2.2049 15.7033 2.12939 15.5792 2.07788C15.455 2.02637 15.3219 1.99988 15.1875 1.99988C15.0531 1.99988 14.92 2.02637 14.7958 2.07788C14.6717 2.12939 14.5589 2.2049 14.464 2.30005L13.74 3.02295C13.548 3.21498 13.4402 3.4754 13.4402 3.74695C13.4402 4.01849 13.548 4.27892 13.74 4.47095L14.464 5.19397L11.303 8.35498C10.1615 7.80702 8.87825 7.62639 7.62985 7.83789C6.38145 8.04939 5.2293 8.64265 4.332 9.53601C4.14026 9.72817 4.03256 9.98855 4.03256 10.26C4.03256 10.5315 4.14026 10.7918 4.332 10.984L13.016 19.667C13.208 19.859 13.4684 19.9668 13.74 19.9668C14.0115 19.9668 14.272 19.859 14.464 19.667C15.3575 18.77 15.9509 17.618 16.1624 16.3698C16.374 15.1215 16.1932 13.8383 15.645 12.697L18.806 9.53601L19.529 10.26C19.721 10.452 19.9814 10.5598 20.253 10.5598C20.5245 10.5598 20.785 10.452 20.977 10.26L21.7 9.53601C21.7952 9.44108 21.8706 9.32825 21.9221 9.2041C21.9737 9.07995 22.0002 8.94691 22.0002 8.8125C22.0002 8.67809 21.9737 8.54505 21.9221 8.4209C21.8706 8.29675 21.7952 8.18392 21.7 8.08899Z" fill="black"></path>
            </svg>
        </span>
        <span class="text-gray-700 fw-bold fs-3">{{$title_table}}</span>
    </div>
    
    <div class="table-responsive">
        <table class="table table-hover table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
            <thead>
                <tr class="fw-bolder text-muted">
                    <th class="text-center" scope="col">No</th>
                    <th class="text-center" scope="col">Nomor Peserta</th>
                    <th class="">Nama</th>
                    <th class="text-center" scope="col">File Karya</th>
                    <th class="text-center" scope="col">Status</th>
                    <th class="text-center" >keterangan</th>
                    <th class="text-center" >Verifikator</th>
                    <th class="text-center" >No Urut</th>
                    <th class="text-center" scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = (request()->get('page') - 1) * $paging;
                    $i = ($i < 0) ? 0 : $i;
                @endphp
                @foreach ($data_view as $data_s)
                    <tr>
                        <td>
                            @php
                                $i++;
                                echo $i;
                                
                            @endphp
                        </td>
                        <td class="text-center">{{$data_s->no_reg}}</td>
                        <td class="name-person">{{$data_s->name}}</td>
                        <td  class="text-center file-center">
                            @php
                                $data_button = '';
                                $data_edit = '';
                                $list_file_karya = explode(',', $data_s->file_karya);
                                // ---------- START REINDEX ARRAY FOR SORTING -----------//
                                    $reindex_array = array();
                                    foreach ($list_file_karya as $key => $file_karya) {
                                        $state = TRUE;
                                        if(empty($file_karya)){
                                            $data_button = '<span class="badge badge-light-danger fs-7 fw-bold btn-sm">Belum Ada Data</span>'; //set default apabila data kosong
                                            $data_edit = ''; //set default apabila data kosong
                                            $state = FALSE;
                                            break;
                                        }

                                        $real_value = str_replace(array( '[', ']' ), '', $file_karya);
                                        $real_value = explode('::', $real_value);
                                        $file = $real_value[1];
                                        $id_file = $real_value[2];
                                        $urutan = $real_value[0];

                                        $reindex_array[$urutan] = $file;
                                        $reindex_file[$urutan] = $id_file;
                                    
                                    }
                                // ---------- END REINDEX ARRAY FOR SORTING -----------//


                               if($state){
                                   ksort($reindex_array, SORT_NUMERIC); //sort array agar sesuai urutan asc
                                   foreach ($reindex_array as $key => $karya_val) {
                                        $data_button .= '<button type="button" class="btn btn-bg-info text-inverse-info me-2 mb-2 modal-file btn-sm d-flex align-items-center" file="'.asset('storage/'.$karya_val).'" extra-info="Panel '.$key.'"><i class="fa la-eye text-inverse-info"></i> '.$key.'</button>';
                                   }
                               } 
                               if($state){
                                   ksort($reindex_file, SORT_NUMERIC); //sort array agar sesuai urutan asc
                                   foreach ($reindex_file as $key => $idfile_val) {
                                        $data_edit .= '<a href="javascript:;" onclick="load_input(`'.route('web.admin.karya.edit_karya',$idfile_val).'`);" class="btn btn-bg-primary text-inverse-primary me-2 mb-2 btn-sm d-flex align-items-center"><i class="fa la-edit text-inverse-info"></i> '.$key.'</a>';
                                   }
                               } 
                            @endphp
                            <div class="d-flex align-items-center justify-content-center">
                                {!! $data_button !!}
                            </div>
                            <div class="d-flex align-items-center justify-content-center">
                                {!! $data_edit !!}
                            </div>
                           
                        </td>
                        <td  class="text-center">
                            @php
                                $st_verifikasi = '<button type="button" class="btn btn-secondary btn-sm">Belum di Verifikasi</button>';
                                if(strtolower(trim($data_s->status_verifikasi)) == 'lulus verifikasi'){
                                    $st_verifikasi = '<button type="button" class="btn btn-success btn-sm">Lulus Verifikasi</button>';
                                }
                                if(strtolower(trim($data_s->status_verifikasi)) == 'tidak lulus verifikasi'){
                                    $st_verifikasi = '<button type="button" class="btn btn-danger btn-sm">Tidak Lulus Verifikasi</button>';
                                }
                            @endphp

                            {!! $st_verifikasi !!}
                        </td>
                        <td>
                            {{($data_s->ket_verifikasi) ? $data_s->ket_verifikasi : '-'}}
                        </td>
                        <td>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item" style="background:none;">Verifikator : {{($data_s->verifikator) ? $data_s->verifikator : '-'}}</li>
                                <li class="list-group-item" style="background:none;">Tanggal : 
                                    {{($data_s->waktu_verifikasi) ? 
                                        \Carbon\Carbon::parse($data_s->waktu_verifikasi)->format('d/m/Y') : 
                                        '-'
                                    }} 
                                </li>
                            </ul>
                        </td>
                        <td>
                            @if($data_s->status_verifikasi == "Lulus Verifikasi")
                            <a href="javascript:;" onclick="load_input('{{route('web.admin.karya.edit_urut',$data_s->id_karya)}}');" class="btn btn-bg-primary text-inverse-primary me-2 mb-2 btn-sm d-flex align-items-center">
                                <i class="fa la-edit text-inverse-info"></i>
                                {{$data_s->urut ? 'Edit / Ubah No Urut : ' . $data_s->urut : 'Masukkan No Urut'}}
                            </a>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                  Verifikasi
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                  @if (Auth::user()->role == 1)
                                    <li><span class="veri_karya dropdown-item text-hover-inverse-white bg-hover-secondary" url="{{route('web.admin.karya.verifikasi',['jenis'=>$kategori, 'penilaian'=>'reset','id_lulus'=>$data_s->id])}}"><i class="fas fa-sync pe-2"></i>Reset</span></li>
                                  @endif  
                                  
                                  @if (Auth::user()->role == 2)
                                    <li><span class="veri_karya dropdown-item text-hover-inverse-white bg-hover-secondary"  url="{{route('web.panitia.karya.verifikasi',['jenis'=>$kategori, 'penilaian'=>'lulus','id_lulus'=>$data_s->id])}}"><i class="fas fa-check pe-2"></i>Lulus</span></li>
                                    <li><span class="veri_karya dialog-up dropdown-item text-hover-inverse-white bg-hover-secondary"  url="{{route('web.panitia.karya.verifikasi',['jenis'=>$kategori, 'penilaian'=>'tidak_lulus','id_lulus'=>$data_s->id])}}"><i class="fas fa-times pe-2"></i>Tidak Lulus</span></li>
                                  @endif
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-12 mt-5">
        {{$data_view->links('pages.admin.karya.pagination')}}
    </div>
    
</div>

