@section('menu-karya', 'active')

<x-office-layout title="List Data Verifikasi">
    <div id="content_list">
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="col-md-12 mb-10">
                    <div class="card">
                        <div class="card-header card-header-stretch">
                            <!--begin::Title-->
                                <div class="card-title d-flex align-items-center">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                        <span class="svg-icon svg-icon-2hx svg-icon-primary"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3" d="M14 3V21H10V3C10 2.4 10.4 2 11 2H13C13.6 2 14 2.4 14 3ZM7 14H5C4.4 14 4 14.4 4 15V21H8V15C8 14.4 7.6 14 7 14Z" fill="black"/>
                                            <path d="M21 20H20V8C20 7.4 19.6 7 19 7H17C16.4 7 16 7.4 16 8V20H3C2.4 20 2 20.4 2 21C2 21.6 2.4 22 3 22H21C21.6 22 22 21.6 22 21C22 20.4 21.6 20 21 20Z" fill="black"/>
                                            </svg>
                                        </span>
                                    <!--end::Svg Icon-->
                                    <h3 class="fw-bolder m-0 text-gray-800 ps-2">Chart Statistik Sayembara</h3>
                                </div>
                            <!--end::Title-->
                           
                        </div>
                        <div class="card-body">
                            <div id="chart_rekap_jenis" class="chart_div"></div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header card-header-stretch">
                        <!--begin::Title-->
                        <div class="card-title d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                <span class="svg-icon svg-icon-2hx svg-icon-primary"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M14 3V21H10V3C10 2.4 10.4 2 11 2H13C13.6 2 14 2.4 14 3ZM7 14H5C4.4 14 4 14.4 4 15V21H8V15C8 14.4 7.6 14 7 14Z" fill="black"/>
                                    <path d="M21 20H20V8C20 7.4 19.6 7 19 7H17C16.4 7 16 7.4 16 8V20H3C2.4 20 2 20.4 2 21C2 21.6 2.4 22 3 22H21C21.6 22 22 21.6 22 21C22 20.4 21.6 20 21 20Z" fill="black"/>
                                    </svg>
                                </span>
                            <!--end::Svg Icon-->
                            <h3 class="fw-bolder m-0 text-gray-800 ps-2">Statistik Dokumen Karya</h3>
                        </div>
                        <!--end::Title-->
                        <!--begin::Toolbar-->
                        <div class="card-toolbar m-0">
                            <!--begin::Tab nav-->
                            @php
                                $prefix = (Auth::user()->role == 1) ? 'admin' : 'panitia';
                            @endphp
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch fs-6 border-0 fw-bolder" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a  href="{{route('web.'.$prefix.'.karya.kategori', ['kategori' => 'wapres', 'page'=>1])}}" class="nav-link justify-content-center text-active-gray-800 {{(@$kategori == 'wapres') ? 'active':NULL}}" data-bs-toggle="tab" role="tab" >Wapres</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a href="{{route('web.'.$prefix.'.karya.kategori', ['kategori' => 'legislatif', 'page'=>1])}}" class="nav-link justify-content-center text-active-gray-800 {{(@$kategori == 'legislatif') ? 'active':NULL}}" data-bs-toggle="tab" role="tab">Legislatif</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a  href="{{route('web.'.$prefix.'.karya.kategori', ['kategori' => 'yudikatif', 'page'=>1])}}" class="nav-link justify-content-center text-active-gray-800 {{(@$kategori == 'yudikatif') ? 'active':NULL}}" data-bs-toggle="tab" role="tab" >Yudikatif</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a  href="{{route('web.'.$prefix.'.karya.kategori', ['kategori' => 'peribadatan', 'page'=>1])}}" class="nav-link justify-content-center text-active-gray-800 text-hover-gray-800 {{(@$kategori == 'peribadatan') ? 'active':NULL}}" data-bs-toggle="tab" role="tab" >Peribadatan</a>
                                </li>
                            </ul>
                            <!--end::Tab nav-->
                        </div>
                        <!--end::Toolbar-->
                    </div>
                    <div class="card-body">
                        <div id="list_result"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    {{-- ------------START MODAL FOR VIEW DOCUMENT--------------- --}}
        <div class="modal fade" tabindex="-1" id="document_modal" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="title-up" style="flex-direction: column;">
                            <h5 class="modal-title fs-2" id="name_person"></h5>
                            <p class="text-gray-600 m-0 text-md-start fs-4" id="desc_title"></p>
                            <p class="text-gray-600 m-0 text-md-start fs-4" id="extra_tile"></p>
                        </div>
                        
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="body_content" style="height: 70vh">
                        
                    </div>
                    <div class="modal-footer">
                        <div class="navi-file">
                            
                        </div>
                        
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        
                    </div>
                </div>
            </div>
        </div>
    {{-- ------------END MODAL FOR VIEW DOCUMENT--------------- --}}

    {{-- ------------START MODAL FOR KENTERANGAN TIDAK LULUS --------------- --}}
        <div class="modal fade" tabindex="-1" id="reject_modal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="title-up" style="flex-direction: column;">
                            <h5 class="modal-title fs-2" id="name_person2"></h5>
                            <p class="text-gray-600 m-0 text-md-start fs-4">Keterangan Tidak Lulus</p>
                        </div>
                        
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="d-flex flex-column mb-8">
                            <label class="fs-6 fw-bold mb-2">Alasan Tidak Lulus</label>
                            <textarea class="form-control form-control-solid" id="ket_reject" rows="5" name="keterangan" placeholder="Keterangan Tidak Lulus"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="submit_keterangan" class="btn btn-primary">
                            <span class="indicator-label">Submit</span>
                            <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                            </span>
                        </button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    {{-- ------------END MODAL FOR KENTERANGAN TIDAK LULUS --------------- --}}
    @section('custom_js')
        <script>
            let queryString = window.location.search;
            let parameters = new URLSearchParams(queryString);
            let paging = parameters.get('page');
            let current_url = new URL(window.location.href);
            // let jenis = parameters.get('jenis');
            var url = current_url.pathname.split("/"),
                jenis = url[url.length-1];
            url.length = url.length-1;
            // let base_url = url.join('/');

            @if (Auth::guard('web')->user()->role == 2)
                let base_url = "{{route('web.panitia.karya.index')}}";
            @elseif (Auth::guard('web')->user()->role == 1)
                let base_url = "{{route('web.admin.karya.index')}}";
            @endif
            
            var _token = '{{ csrf_token() }}';
            var personPeserta = null;

            var id_modal =  $('#document_modal');
            var modalDoc = new bootstrap.Modal(id_modal, {
                keyboard: false
            });

            var id_modal_reject = $('#reject_modal');
            var modalReject = new bootstrap.Modal(id_modal_reject, {
                keyboard: false
            });

            var iframe = function(src){
                        return '<iframe class="col-md-12" src="'+src+'" title="Document Viewer"  style="height:100%" allowfullscreen></iframe>';
            }
            
            var c_selector_karya_file;
            
            function stateUrlHandling(url){
                var url = new URL(url);
                var path_name = url.pathname;
                var url_get = path_name.split("/"),
                jenis = url_get[url_get.length-1];
                var page = url.searchParams.get("page");
                if(page === null){
                    page = 1;
                }
                window.history.pushState(null, null, url);
                fetch_data(page, jenis);
            }

            function submit_data(url, method = 'GET', returning=function(){}, data = {}){
                var data_post = data;
                loaded(); //antisipasi jika fast click behavior dilakukan
                $.ajax({
                    url: url,
                    method: method,
                    beforeSend: function( xhr ) {
                        loading();
                    },
                    data: data_post,
                    success:function(data)
                    {
                        var url = location.href;
                        stateUrlHandling(url);
                    }
                })
                .done(function() {
                    returning();
                    loaded();
                })
                .fail(function() {
                    // alert( "error" );
                    loaded();

                    Swal.fire({ text: 'gagal dalam mendapatkan data, silahkan refresh browser anda', icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                });

            }

            function fetch_data(page, jenis){
                loaded(); //antisipasi jika fast click behavior dilakukan
                $.ajax({
                    url: base_url+'/'+jenis,
                    method:"GET",
                    beforeSend: function( xhr ) {
                        loading();
                    },
                    data:{page:page},
                    success:function(data)
                    {
                        $('#list_result').html(data);
                    }
                })
                .done(function() {
                    loaded();
                })
                .fail(function() {
                    // alert( "error" );
                    loaded();

                    Swal.fire({ text: 'gagal dalam mendapatkan data, silahkan refresh browser anda', icon: "error", buttonsStyling: !1, confirmButtonText: "Ok, Mengerti!", customClass: { confirmButton: "btn btn-primary" } });
                });
            }
           

            $(document).ready(function(){
                if(paging == null){
                    // load_list(1);
                    paging = 1;
                    if(base_url == current_url){
                        jenis = 'wapres';
                    }
                    // return;
                }
                fetch_data(paging, jenis);

                $(document).on('click', '.page-link', function(event){
                    event.preventDefault(); 
                    var attr = $(this).attr('href');
                    if (typeof attr == 'undefined') {
                       return;
                    }

                    stateUrlHandling(attr);
                });
                $(window).on('popstate', function(event) {
                    var url = location.href;
                    stateUrlHandling(url);
                });

                $('.nav-link').click(function(){
                    var href = $(this).attr('href');
                    stateUrlHandling(href);
                })

                $('#list_result').on('click','.modal-file',function(){
                    const jns_dokumen = [];
                    jns_dokumen[3] = 'Surat Pakta Integritas';
                    jns_dokumen[4] = 'Surat Tanggung Jawab';
                    jns_dokumen[5] = null;
                    
                    var indexTd = $(this).parent().parent().index();
                    var personName = $(this).parent().parent().children('td.name-person').text();
                    var filePath = $(this).attr('file');
                    var extra_title = $(this).attr('extra-info');
                    var disable_prev = null,
                        disable_next = null;

                    c_selector_karya_file = $(this);

                    if(c_selector_karya_file.index() == $(this).parent().children('.modal-file').last().index()){
                        disable_next = 'disabled';
                    }

                    if(c_selector_karya_file.index() == $(this).parent().children('.modal-file').first().index()){
                        disable_prev = 'disabled';
                    }

                    var navi_file = '<button type="button" class="btn btn-warning me-3" id="prev_file" '+disable_prev+'><i class="fas fa-angle-double-left me-1"></i> Sebelumnya</button>'+
                    '<button type="button" class="btn btn-success me-3" id="next_file" '+disable_next+'>Selanjutnya <i class="fas fa-angle-double-right ms-1"></i></button>';

                    id_modal.find('#name_person').text('');
                    
                    if(indexTd != 5){
                        //------ menampilkan nama pada modal kecuali selain kolom karya --------
                        navi_file = null;
                        id_modal.find('#name_person').text(personName); 
                    }
                    id_modal.find('.modal-footer .navi-file').html(navi_file);
                    id_modal.find('#desc_title').text(jns_dokumen[indexTd]);
                    id_modal.find('#extra_tile').text(null);
                    if (typeof extra_title !== 'undefined' && extra_title !== false) {
                       id_modal.find('#extra_tile').text(extra_title);
                    }
                    id_modal.find('#body_content').html(iframe(filePath));
                    modalDoc.show();
                })

                $('#document_modal').on('click','#next_file, #prev_file', function(){
                    var current_id = this.id;
                    var _next = c_selector_karya_file.next();
                    var _prev = c_selector_karya_file.prev();

                    var check_available_next = _next.next();
                    var check_available_prev = _prev.prev();

                    $('#document_modal .navi-file button').removeAttr('disabled');
                    
                    if(!(check_available_next.hasClass('modal-file') && check_available_next.attr('file')) && current_id == 'next_file'){
                        $(this).attr('disabled', 'disabled');
                    }
                    
                    if(!(check_available_prev.hasClass('modal-file') && check_available_prev.attr('file')) && current_id == 'prev_file'){
                        $(this).attr('disabled', 'disabled');
                    }

                    // --------- start untuk next file karya -----------//
                        if(current_id == 'next_file'){
                            var extra_title = _next.attr('extra-info');
                            var filePath = _next.attr('file');

                            if(_next.hasClass('modal-file') && _next.attr('file')){
                                
                                c_selector_karya_file = _next;
                                id_modal.find('#extra_tile').text(extra_title);
                                id_modal.find('#body_content').html(iframe(filePath));

                            }
                        }
                    // --------- end untuk next file karya -----------//

                    // --------- start untuk previous file karya -----------//
                        if(current_id == 'prev_file'){
                            var extra_title = _prev.attr('extra-info');
                            var filePath = _prev.attr('file');

                            if(_prev.hasClass('modal-file') && _prev.attr('file')){
                                
                                c_selector_karya_file = _prev;
                                id_modal.find('#extra_tile').text(extra_title);
                                id_modal.find('#body_content').html(iframe(filePath));

                            }
                        }
                    // --------- end untuk previous file karya -----------//
                })

                $('#list_result').on('click','.veri_karya',function(){

                    var url_send = $(this).attr('url');
                        personPeserta = $(this).parents('tr').children('td.name-person').text();
                    var return_function = function(){
                        Swal.fire('Status Peserta '+personPeserta+' telah berubah', '', 'success');
                    }
                    
                    
                    if($(this).hasClass('dialog-up')){
                        id_modal_reject.find('#name_person2').text(personPeserta);
                        id_modal_reject.find('#submit_keterangan').attr('uri', url_send);
                        modalReject.show();
                        return;
                    }
                    
                    Swal.fire({
                        title: 'Apakah Anda yakin?',
                        text: "Anda akan merubah status verifikasi dari peserta "+personPeserta,
                        icon: 'warning',
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Ya',
                        denyButtonText: 'Tidak',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            submit_data(url_send, 'GET', return_function);
                            
                        } else if (result.isDenied) {
                            Swal.fire('Perubahan Dibatalkan', '', 'info')
                        }
                    })
                    

                })

                $('#submit_keterangan').click(function(){
                    var keterangan_reject = $('#ket_reject').val();
                    var url_send = $(this).attr('uri');

                    var return_function = function(){
                        modalReject.hide();
                        Swal.fire('Status Peserta '+personPeserta+' telah berubah', '', 'success');
                       
                    }
                    var data_post = {'_token' : _token, 'keterangan' : keterangan_reject};
                    submit_data(url_send, 'POST', return_function, data_post);
                })

            });
        </script>
        <script>
            // ----------------- Start Chart untuk Rekap Jenis Peserta ----------------//
                am4core.ready(function() {
                
                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end
        
                // Create chart instance
                var chart = am4core.create("chart_rekap_jenis", am4charts.XYChart);
                // Add data
                chart.data = {!! $data_chart !!};
        
                // Create axes
        
                // chart.colors.step = 2;
                chart.colors.list = [
                    am4core.color("#009EF7"),
                    am4core.color("#50CD89"),
                    am4core.color("#fd7e14"),
                    am4core.color("#FFC700"),
                    
                ];
        
                chart.legend = new am4charts.Legend()
                chart.legend.position = 'bottom'
                chart.legend.paddingBottom = 20
                chart.legend.labels.template.maxWidth = 95
        
                var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
                xAxis.dataFields.category = 'kategori'
                xAxis.renderer.cellStartLocation = 0.1
                xAxis.renderer.cellEndLocation = 0.9
                xAxis.renderer.grid.template.location = 0;
        
                var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
                yAxis.min = 0;
        
                // ---------- Start Function list inside amchart4 ----------//
                    function createSeries(value, name) {
                        var series = chart.series.push(new am4charts.ColumnSeries())
                        series.dataFields.valueY = value
                        series.dataFields.categoryX = 'kategori'
                        series.name = name
        
                        series.events.on("hidden", arrangeColumns);
                        series.events.on("shown", arrangeColumns);
        
                        series.columns.template.events.on("hit", function(ev) {
                            seriesIndex = ev.target.baseSprite.series.indexOf(ev.target.dataItem.component);
                            dataItemIndex = ev.target.dataItem.component.dataItems.indexOf(ev.target.dataItem);
                            console.log(dataItemIndex);
                            // console.log("clicked on ", ev.target.dataItem.component.coba);
                        }, this);
        
                        var bullet = series.bullets.push(new am4charts.LabelBullet())
                        bullet.interactionsEnabled = false
                        bullet.dy = -10;
                        bullet.label.text = '{valueY}'
                        bullet.label.fill = am4core.color('#000');
        
                        // series.columns.template.adapter.add("fill", function(fill, target) {
                        //     chart.color = target.dataItem.dataContext.color;
                        //     return chart.color;
                        // });
        
                        return series;
                    }
        
                    function arrangeColumns() {
        
                        var series = chart.series.getIndex(0);
        
                        var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
                        if (series.dataItems.length > 1) {
                            var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                            var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                            var delta = ((x1 - x0) / chart.series.length) * w;
                            if (am4core.isNumber(delta)) {
                                var middle = chart.series.length / 2;
        
                                var newIndex = 0;
                                chart.series.each(function(series) {
                                    if (!series.isHidden && !series.isHiding) {
                                        series.dummyData = newIndex;
                                        newIndex++;
                                    }
                                    else {
                                        series.dummyData = chart.series.indexOf(series);
                                    }
                                })
                                var visibleCount = newIndex;
                                var newMiddle = visibleCount / 2;
        
                                chart.series.each(function(series) {
                                    var trueIndex = chart.series.indexOf(series);
                                    var newIndex = series.dummyData;
        
                                    var dx = (newIndex - trueIndex + middle - newMiddle) * delta
        
                                    series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                                    series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                                })
                            }
                        }
                    }
                // ---------- End Function list inside amchart4 ----------//
                chart.cursor = new am4charts.XYCursor();
        
                createSeries('jumlah', 'Jumlah Pemilih');
                createSeries('lolos', 'Jumlah Lulus Tahap Selanjutnya');
                createSeries('jumlah_karya', 'Jumlah Karya');
                createSeries('jumlah_lanjut', 'Jumlah Lolos Penilaian');
                // createSeries('tidak_lolos', 'Jumlah Tidak Lulus Verifikasi');
                // createSeries('karya', 'Jumlah Dokumen Karya');
        
            });
            // ----------------- End Chart untuk  Rekap Jenis Peserta ----------------//
        </script>
    @endsection
</x-office-layout >