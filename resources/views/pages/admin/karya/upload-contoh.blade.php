@section('menu-karya', 'active')

<x-office-layout title="List Data Verifikasi">
    <div id="content_list">
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="col-md-12 mb-10">
                    <div class="card">
                        <div class="card-header card-header-stretch">
                            <!--begin::Title-->
                                <div class="card-title d-flex align-items-center">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                        <span class="svg-icon svg-icon-2hx svg-icon-primary"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3" d="M14 3V21H10V3C10 2.4 10.4 2 11 2H13C13.6 2 14 2.4 14 3ZM7 14H5C4.4 14 4 14.4 4 15V21H8V15C8 14.4 7.6 14 7 14Z" fill="black"/>
                                            <path d="M21 20H20V8C20 7.4 19.6 7 19 7H17C16.4 7 16 7.4 16 8V20H3C2.4 20 2 20.4 2 21C2 21.6 2.4 22 3 22H21C21.6 22 22 21.6 22 21C22 20.4 21.6 20 21 20Z" fill="black"/>
                                            </svg>
                                        </span>
                                    <!--end::Svg Icon-->
                                    <h3 class="fw-bolder m-0 text-gray-800 ps-2">Coba Upload</h3>
                                </div>
                            <!--end::Title-->
                           
                        </div>
                        <div class="card-body">
                            <form id="form1" runat="server">
                                <input type="file" name="file" /><input type="button" id="btnUpload" value="Upload" />
                                <div class="progress" style="display: none">
                                    <div class="progress-bar" role="progressbar"></div>
                                </div>
                                <div id="lblMessage" style="color: Green"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    @section('custom_js')
        <script>
            $("body").on("click", "#btnUpload", function () {
                $.ajax({
                    url: '{{route("web.admin.karya.upload_file")}}',
                    type: 'POST',
                    data: new FormData($('form')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (file) {
                        setTimeout(function () {
                            $(".progress").hide();
                            $("#lblMessage").html("<b>" + file.name + "</b> has been uploaded.");
                        }, 1000);
                    },
                    error: function (a) {
                        $("#lblMessage").html(a.responseText);
                    },
                    failure: function (a) {
                        $("#lblMessage").html(a.responseText);
                    },
                    xhr: function () {
                        var fileXhr = $.ajaxSettings.xhr();
                        if (fileXhr.upload) {
                            $(".progress").show();
                            fileXhr.upload.addEventListener("progress", function (e) {
                                if (e.lengthComputable) {
                                    var percentage = Math.ceil(((e.loaded / e.total) * 100));
                                    $('.progress-bar').text(percentage + '%');
                                    $('.progress-bar').width(percentage + '%');
                                    if (percentage == 100) {
                                        $('.progress-bar').text('100%');
                                    }
                                }
                            }, false);
                        }
                        return fileXhr;
                    }
                });
            });
        </script>
    @endsection
</x-office-layout >