<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card">
            <div class="card-header border-0 pt-6">
                <div class="card-title">
                    <h6>
                        @if ($data->id)
                            Ubah
                        @else
                            Tambah
                        @endif
                        data User
                    </h6>
                </div>
                <div class="card-toolbar">
                    <div class="d-flex justify-content-end">
                        <button type="button" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                    </div>
                </div>
            </div>
            <form id="form_input">
                <div class="card-body pt-0">
                    <div class="form-group row mb-5">
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Nama</label>
                            <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="Masukan Nama" value="{{$data->name}}" />
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Username</label>
                            <input type="text" maxlength="20" class="form-control" name="username" id="username" placeholder="Masukan Username" value="{{$data->username}}" />
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Role</label>
                            <select class="form-control" name="role" id="role">
                                <option value="">Pilih Role</option>
                                <option value="1" {{$data->role == 1 ? 'selected' : '' }}>Admin</option>
                                <option value="2" {{$data->role == 2 ? 'selected' : '' }}>Panitia</option>
                                <option value="3" {{$data->role == 3 ? 'selected' : '' }}>Penilai</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">No HP</label>
                            <input type="tel" maxlength="20" class="form-control" name="no_handphone" id="no_handphone" placeholder="Masukan No HP" value="{{$data->phone}}" />
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Masukan Email" value="{{$data->email}}" />
                        </div>
                        <div class="col-lg-4">
                            <label class="required fs-6 fw-bold mb-2">Password</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Masukan Password" />
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="min-w-150px mt-10 text-end">
                            @if ($data->id)
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('web.user.update',$data->id)}}','PATCH');" class="btn btn-sm btn-primary">Simpan</button>
                            @else
                            <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('web.user.store')}}','POST');" class="btn btn-sm btn-primary">Simpan</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    text_only('username');
    number_only('no_handphone');
    format_email('email');
</script>