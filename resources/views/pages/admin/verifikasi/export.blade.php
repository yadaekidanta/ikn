<style>
    .table{
        border: 1px solid black;
    }
</style>
<table class="table">
    <thead>
        <tr>
            <th colspan="4"> Admin</th>
        </tr>
    </thead>
    <thead>
    <tr>
        <th>No.</th>
        <th>No. Tim Pendaftar</th>
        <th>Nama Ketua</th>
        <th>No Hp Ketua</th>
        <th>No Ktp Ketua</th>
        <th>Email Ketua</th>
        <th>Nomor Peserta</th>
        <th>No RKA Ketua</th>
        <th>SKA Ketua</th>
        <th>Tanggal SKA Ketua</th>
        
        <th>Negara Ketua</th>
        <th>Alamat Ketua</th>
        <th>Provinsi Ketua</th>
        <th>Kota Ketua</th>
        <th>Kode Pos Ketua</th>
        

        <th>Nama Perusahaan</th>
        <th>NPWP Perusahaan</th>
        <th>Tgl. SIUJK Perusahaan</th>
        <th>SBU Perusahaan</th>

        <th>Nama Anggota</th>
        <th>No Ktp Anggota</th>
        <th>No Hp Anggota</th>
        <th>Email Anggota</th>
        <th>SKA Anggota</th>
        <th>Tgl. SKA Anggota</th>
        <th>No. RKA Anggota</th>
        <th>Negara Anggota</th>
        <th>Alamat Anggota</th>
        <th>Provinsi Anggota</th>
        <th>Kota Anggota</th>
        <th>Kode Pos Anggota</th>

        <th>IsWapres</th>
        <th>Legislatif</th>
        <th>Yudikatif</th>
        <th>Peribadatan</th>
        <th>Status</th>
        <th>Verifikator</th>
    </tr>
    </thead>
    <tbody>
    @php
        $i=1;
        $rowspan_id = NULL;
        $rowspan = NULL;
        $span_status = FALSE;
        $temp_id = 0;
    @endphp
    @foreach($data as $data_result)
        @php
             
            $hide = TRUE;
            $rowspan = NULL;
            $span_status = FALSE;
            
            if($temp_id != $data_result->id ){
                $temp_id = $data_result->id;
                $span_status = TRUE;
                $hide = FALSE;
            } 
            if($span_status == TRUE){
                $rowspan = ' rowspan='.$data_result->jumlah_anggota;
            }
            if($data_result->jumlah_anggota == 0){
                $rowspan = NULL;
            }

        @endphp
        <tr>
            @if($hide == FALSE)
                <td {{$rowspan}}>
                    @php
                        echo $i;
                        $i++;
                    @endphp
                </td>
                <td {{$rowspan}}>{{$data_result->id}}</td>
                <td {{$rowspan}}>{{ $data_result->name }}</td>
                <td {{$rowspan}}>{{ $data_result->phone }}</td>
                <td {{$rowspan}}>{{ $data_result->ktp_no }}</td>
                <td {{$rowspan}}>{{ $data_result->email }}</td>
                <td {{$rowspan}}>{{ $data_result->no_peserta }}</td>
                <td {{$rowspan}}>{{ $data_result->nrka_no }}</td>
                <td {{$rowspan}}>{{ $data_result->nama_ska_ketua }}</td>
                <td {{$rowspan}}>{{ $data_result->tanggal_ska }}</td>

                <td {{$rowspan}}>{{ $data_result->ketua_negara }}</td>
                <td {{$rowspan}}>{{ $data_result->address }}</td>
                <td {{$rowspan}}>{{ $data_result->ketua_provinsi }}</td>
                <td {{$rowspan}}>{{ $data_result->ketua_kota }}</td>
                <td {{$rowspan}}>{{ $data_result->ketua_kode_pos }}</td>

                <td {{$rowspan}}>{{ $data_result->nama_perusahaan }}</td>
                <td {{$rowspan}}>{{ $data_result->npwp_perusahaan }}</td>
                <td {{$rowspan}}>{{ $data_result->tagl_siujk }}</td>
                <td {{$rowspan}}>{{ $data_result->sbu }}</td>
            @endif

            <td>{{ $data_result->name_anggota }}</td>
            <td>{{ $data_result->ktp_anggota }}</td>
            <td>{{ $data_result->telepon_anggota }}</td>
            <td>{{ $data_result->email_anggota }}</td>
            <td>{{ $data_result->nama_ska_anggota }}</td>
            <td>{{ $data_result->tgl_ska_anggota }}</td>
            <td>{{ $data_result->nrka_anggota }}</td>
            <td>{{ $data_result->negara_anggota }}</td>
            <td>{{ $data_result->alamat_anggota }}</td>
            <td>{{ $data_result->provinsi_anggota }}</td>
            <td>{{ $data_result->kota_anggota }}</td>
            <td>{{ $data_result->anggota_kode_pos }}</td>

            @if($hide == FALSE)
                <td {{$rowspan}}>{{ ($data_result->sayembara_1 > 0) ? '✔' : NULL;}}</td>
                <td {{$rowspan}}>{{ ($data_result->sayembara_2 > 0) ? '✔' : NULL;}}</td>
                <td {{$rowspan}}>{{ ($data_result->sayembara_3 > 0) ? '✔' : NULL;}}</td>
                <td {{$rowspan}}>{{ ($data_result->sayembara_4 > 0) ? '✔' : NULL;}}</td>
            @endif
            <td>{{$data_result->st}}</td>
            <td>{{$data_result->created_by ? \App\Models\User::where('id',$data_result->created_by)->first()->name : '-'}}</td>
        </tr>
    @endforeach
    </tbody>
</table>