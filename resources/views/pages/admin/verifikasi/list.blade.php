<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Statistik Verifikasi Kategori Sayembara</h3>
            </div>
            <div class="card-body">
                <div id="chart_rekap_jenis" class="chart_div"></div>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card bg-warning card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-warning">Pendaftar</a>
                    <span class="fw-bold text-white fs-5">{{$total}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/008-boy-3.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-danger card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-danger">Tidak Lengkap Administrasi</a>
                    <span class="fw-bold text-white fs-5">{{$administrasi}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/009-boy-4.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-success card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-success">Lengkap Administrasi</a>
                    <span class="fw-bold text-white fs-5">{{$lengkap}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/016-boy-7.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="card bg-gray-400 card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-gray-400">Belum Verifikasi</a>
                    <span class="fw-bold text-white fs-5">{{$lengkap - $lulus - $tidak}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/007-boy-2.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-info card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-info">Lulus Verifikasi</a>
                    <span class="fw-bold text-white fs-5">{{$lulus}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/029-boy-11.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-dark card-xl-stretch mb-5 mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-dark">Tidak Lulus Verifikasi</a>
                    <span class="fw-bold text-white fs-5">{{$tidak}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/004-boy-1.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table align-middle table-row-dashed fs-6 gy-5">
        <thead>
            <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                <th class="min-w-30px">No</th>
                <th class="min-w-125px">No Tim Pendaftaran</th>
                <th class="min-w-125px">Nama Ketua</th>
                <th class="min-w-125px">No HP</th>
                <th class="min-w-125px">Email</th>
                <th class="min-w-125px">Perusahaan</th>
                <th class="min-w-30px">Jumlah Anggota</th>
                <th class="min-w-30px">IsWapres</th>
                <th class="min-w-30px">Legislatif</th>
                <th class="min-w-30px">Yudikatif</th>
                <th class="min-w-30px">Peribadatan</th>
                <th class="min-w-125px">Nama Verifikator</th>
                <th class="min-w-125px">Status Verifikasi</th>
                <th class="min-w-125px">No Peserta 1</th>
                <th class="min-w-125px">No Peserta 2</th>
                <th class="min-w-125px">Aksi</th>
            </tr>
        </thead>
        <tbody class="fw-bold text-gray-600">
            @foreach ($collection as $key => $item)
            @php
            $json_data = ($item->jenis_sayembara) ? $item->jenis_sayembara : json_encode(array());
            $pilihan_sayembara = json_decode($json_data);
            @endphp
            <tr>
                <td>
                    {{$key+ $collection->firstItem()}}
                </td>
                <td>
                    {{$item->id}}
                </td>
                <td>
                    {{$item->nama_ketua}}
                </td>
                <td>
                    {{$item->phone}}
                </td>
                <td>
                    {{$item->email}}
                </td>
                <td>
                    {{$item->company ? $item->company->name : ''}}
                </td>
                <td>
                    {{$item->anggota ? $item->anggota->count() : 0}}
                </td>
                <td>
                    @if(in_array(1, $pilihan_sayembara))
                    <span class="svg-icon svg-icon-muted svg-icon-2hx">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M9.89557 13.4982L7.79487 11.2651C7.26967 10.7068 6.38251 10.7068 5.85731 11.2651C5.37559 11.7772 5.37559 12.5757 5.85731 13.0878L9.74989 17.2257C10.1448 17.6455 10.8118 17.6455 11.2066 17.2257L18.1427 9.85252C18.6244 9.34044 18.6244 8.54191 18.1427 8.02984C17.6175 7.47154 16.7303 7.47154 16.2051 8.02984L11.061 13.4982C10.7451 13.834 10.2115 13.834 9.89557 13.4982Z" fill="currentColor"/>
                        </svg>
                    </span>
                    <a href="{{route('web.admin.user-verification.unduh',[$item->id,'sayembara_name' => $link_download_kartu[1]]) }}" class="me-5">
                        <span class="svg-icon svg-icon-muted svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path opacity="0.3" d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z" fill="currentColor"/>
                            <path d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z" fill="currentColor"/>
                            <path opacity="0.3" d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z" fill="currentColor"/>
                            </svg>
                        </span>
                    </a>
                    @endif
                </td>
                <td>
                    @if(in_array(2, $pilihan_sayembara))
                    <span class="svg-icon svg-icon-muted svg-icon-2hx">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M9.89557 13.4982L7.79487 11.2651C7.26967 10.7068 6.38251 10.7068 5.85731 11.2651C5.37559 11.7772 5.37559 12.5757 5.85731 13.0878L9.74989 17.2257C10.1448 17.6455 10.8118 17.6455 11.2066 17.2257L18.1427 9.85252C18.6244 9.34044 18.6244 8.54191 18.1427 8.02984C17.6175 7.47154 16.7303 7.47154 16.2051 8.02984L11.061 13.4982C10.7451 13.834 10.2115 13.834 9.89557 13.4982Z" fill="currentColor"/>
                        </svg>
                    </span>
                    <a href="{{ route('web.admin.user-verification.unduh', [$item->id,'sayembara_name' => $link_download_kartu[2]] ) }}" class="me-5">
                        <span class="svg-icon svg-icon-muted svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path opacity="0.3" d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z" fill="currentColor"/>
                            <path d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z" fill="currentColor"/>
                            <path opacity="0.3" d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z" fill="currentColor"/>
                            </svg>
                        </span>
                    </a>
                    @endif
                </td>
                <td>
                    @if(in_array(3, $pilihan_sayembara))
                    <span class="svg-icon svg-icon-muted svg-icon-2hx">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M9.89557 13.4982L7.79487 11.2651C7.26967 10.7068 6.38251 10.7068 5.85731 11.2651C5.37559 11.7772 5.37559 12.5757 5.85731 13.0878L9.74989 17.2257C10.1448 17.6455 10.8118 17.6455 11.2066 17.2257L18.1427 9.85252C18.6244 9.34044 18.6244 8.54191 18.1427 8.02984C17.6175 7.47154 16.7303 7.47154 16.2051 8.02984L11.061 13.4982C10.7451 13.834 10.2115 13.834 9.89557 13.4982Z" fill="currentColor"/>
                        </svg>
                    </span>
                    <a href="{{ route('web.admin.user-verification.unduh', [$item->id,'sayembara_name' => $link_download_kartu[3]] ) }}" class="me-5">
                        <span class="svg-icon svg-icon-muted svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path opacity="0.3" d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z" fill="currentColor"/>
                            <path d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z" fill="currentColor"/>
                            <path opacity="0.3" d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z" fill="currentColor"/>
                            </svg>
                        </span>
                    </a>
                    @endif
                </td>
                <td>
                    @if(in_array(4, $pilihan_sayembara))
                    <span class="svg-icon svg-icon-muted svg-icon-2hx">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M9.89557 13.4982L7.79487 11.2651C7.26967 10.7068 6.38251 10.7068 5.85731 11.2651C5.37559 11.7772 5.37559 12.5757 5.85731 13.0878L9.74989 17.2257C10.1448 17.6455 10.8118 17.6455 11.2066 17.2257L18.1427 9.85252C18.6244 9.34044 18.6244 8.54191 18.1427 8.02984C17.6175 7.47154 16.7303 7.47154 16.2051 8.02984L11.061 13.4982C10.7451 13.834 10.2115 13.834 9.89557 13.4982Z" fill="currentColor"/>
                        </svg>
                    </span>
                    <a href="{{ route('web.admin.user-verification.unduh', [$item->id,'sayembara_name' => $link_download_kartu[4]] ) }}" class="me-5">
                        <span class="svg-icon svg-icon-muted svg-icon-2hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path opacity="0.3" d="M19 15C20.7 15 22 13.7 22 12C22 10.3 20.7 9 19 9C18.9 9 18.9 9 18.8 9C18.9 8.7 19 8.3 19 8C19 6.3 17.7 5 16 5C15.4 5 14.8 5.2 14.3 5.5C13.4 4 11.8 3 10 3C7.2 3 5 5.2 5 8C5 8.3 5 8.7 5.1 9H5C3.3 9 2 10.3 2 12C2 13.7 3.3 15 5 15H19Z" fill="currentColor"/>
                            <path d="M13 17.4V12C13 11.4 12.6 11 12 11C11.4 11 11 11.4 11 12V17.4H13Z" fill="currentColor"/>
                            <path opacity="0.3" d="M8 17.4H16L12.7 20.7C12.3 21.1 11.7 21.1 11.3 20.7L8 17.4Z" fill="currentColor"/>
                            </svg>
                        </span>
                    </a>
                    @endif
                </td>
                <td>
                    {{$item->id_verif ? \App\Models\User::where('id',$item->id_verif)->first()->name : 'Belum Verifikasi'}}
                </td>
                <td>
                    {{ $item->st ? $item->st : 'Belum Verifikasi' }} <br>
                    Alasan : {{ $item->ket ? $item->ket : '' }}
                </td>
                <td>
                    @php
                    $count = \App\Models\Karya::where('user_id',$item->id)->get()->count();
                    @endphp
                    @if(\App\Models\Karya::where('user_id',$item->id)->get()->count() > 0)
                    @php
                    $first = \App\Models\Karya::where('user_id',$item->id)->first()->no_reg;
                    @endphp
                        @if($first)
                        {{$first}}
                        @else
                        -
                        @endif
                    @endif
                </td>
                <td>
                    @if(\App\Models\Karya::where('user_id',$item->id)->get()->count() > 1)
                    @php
                    $last = \App\Models\Karya::where('user_id',$item->id)->latest('id')->first()->no_reg;
                    @endphp
                        @if($last)
                        {{$last}}
                        @else
                        -
                        @endif
                    @endif
                </td>
                <td>
                    @if($item->id_uv)
                    <a href="javascript:;" onclick="load_input('{{route('web.admin.user-verification.show',[$item->id_uv])}}');" class="btn btn-sm btn-info">Lihat</a>
                    {{-- <a href="javascript:;" onclick="handle_confirm('Apakah anda yakin ingin membatalkan verifikasi data ini ?','Ya','Tidak','PATCH','{{route('web.admin.user-verification.cancel',$item->id_uv)}}');" class="btn btn-sm btn-danger">Hapus</a> --}}
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {{$collection->links('themes.office.pagination')}}
</div>

<script>
    // ----------------- Start Chart untuk Rekap Jenis Peserta ----------------//
        am4core.ready(function() {
    
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end
    
            // Create chart instance
            var chart = am4core.create("chart_rekap_jenis", am4charts.XYChart);
            // Add data
            chart.data = {!! $data_chart !!};
    
            // Create axes
    
            // chart.colors.step = 2;
            chart.colors.list = [
                am4core.color("#009EF7"),
                am4core.color("#50CD89"),
                am4core.color("#F1416C"),
                am4core.color("#FFC700"),
            ];
    
            chart.legend = new am4charts.Legend()
            chart.legend.position = 'bottom'
            chart.legend.paddingBottom = 20
            chart.legend.labels.template.maxWidth = 95
    
            var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
            xAxis.dataFields.category = 'kategori'
            xAxis.renderer.cellStartLocation = 0.1
            xAxis.renderer.cellEndLocation = 0.9
            xAxis.renderer.grid.template.location = 0;
    
            var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
            yAxis.min = 0;
    
            // ---------- Start Function list inside amchart4 ----------//
                function createSeries(value, name) {
                    var series = chart.series.push(new am4charts.ColumnSeries())
                    series.dataFields.valueY = value
                    series.dataFields.categoryX = 'kategori'
                    series.name = name
    
                    series.events.on("hidden", arrangeColumns);
                    series.events.on("shown", arrangeColumns);
    
                    series.columns.template.events.on("hit", function(ev) {
                        seriesIndex = ev.target.baseSprite.series.indexOf(ev.target.dataItem.component);
                        dataItemIndex = ev.target.dataItem.component.dataItems.indexOf(ev.target.dataItem);
                        console.log(dataItemIndex);
                        // console.log("clicked on ", ev.target.dataItem.component.coba);
                    }, this);
    
                    var bullet = series.bullets.push(new am4charts.LabelBullet())
                    bullet.interactionsEnabled = false
                    bullet.dy = -10;
                    bullet.label.text = '{valueY}'
                    bullet.label.fill = am4core.color('#000');
    
                    // series.columns.template.adapter.add("fill", function(fill, target) {
                    //     chart.color = target.dataItem.dataContext.color;
                    //     return chart.color;
                    // });
    
                    return series;
                }
    
                function arrangeColumns() {
    
                    var series = chart.series.getIndex(0);
    
                    var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
                    if (series.dataItems.length > 1) {
                        var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                        var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                        var delta = ((x1 - x0) / chart.series.length) * w;
                        if (am4core.isNumber(delta)) {
                            var middle = chart.series.length / 2;
    
                            var newIndex = 0;
                            chart.series.each(function(series) {
                                if (!series.isHidden && !series.isHiding) {
                                    series.dummyData = newIndex;
                                    newIndex++;
                                }
                                else {
                                    series.dummyData = chart.series.indexOf(series);
                                }
                            })
                            var visibleCount = newIndex;
                            var newMiddle = visibleCount / 2;
    
                            chart.series.each(function(series) {
                                var trueIndex = chart.series.indexOf(series);
                                var newIndex = series.dummyData;
    
                                var dx = (newIndex - trueIndex + middle - newMiddle) * delta
    
                                series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                                series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                            })
                        }
                    }
                }
            // ---------- End Function list inside amchart4 ----------//
            chart.cursor = new am4charts.XYCursor();

            createSeries('jumlah', 'Jumlah Pemilih');
            createSeries('lolos', 'Jumlah Lulus Verifikasi');
            createSeries('tidak_lolos', 'Jumlah Tidak Lulus Verifikasi');
            // createSeries('karya', 'Jumlah Dokumen Karya');
    
        });
    // ----------------- End Chart untuk  Rekap Jenis Peserta ----------------//
</script>