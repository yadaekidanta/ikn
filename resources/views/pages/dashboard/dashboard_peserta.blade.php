<style type="text/css">
    .img-sayembara{
        padding: 0 12%;
    }
    .title-sayembara{
        text-align: center;
        padding: 1em 10px;
        font-size: 18px;
    }
    .title-sayembara.peribadatan{
        padding-bottom: 2.2em;
    }
</style>
<div class="card">
    <div class="card-body" style="padding-top: 3rem">
        @if (!@$with_header)
            <div class="floating-title">
                <h2>Unduh Dokumen Sayembara</h2>
            </div>
            <div class="d-flex flex-column text-end mb-10">
                <span class="text-dark fw-bolder fs-2 fst-italic" style="margin-top: -1em;">28 Maret 2022</span>
            </div>
        @endif
        <div class="row">

            @if (@$with_header)
                <div class="col-lg-12">
                    <div class="pt-lg-10 mb-10">
                        <div class="fw-bold fs-3 text-muted mb-15">Selamat datang, {{Auth::guard('web')->user()->name}}</div>
                        <h1 class="fw-bolder fs-1qx text-gray-800 mb-7">Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara</h1>
                    </div>
                </div>
            @endif
            
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-3">
                        <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download', 1)}}');" id="tombol_download_istana" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                            <img src="{{asset('image/Istana-Wakil-Presiden.png')}}" class="w-100 img-sayembara" alt="">
                            <h3 class="title-sayembara">
                                Unduh <br>
                                Dokumen Sayembara <br>
                                Kompleks <br>
                                Istana Wakil Presiden

                            </h3>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download', 2)}}');" id="tombol_download_istana" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                            <img src="{{asset('image/Legislatif.png')}}" class="w-100 img-sayembara" alt="">
                            <h3 class="title-sayembara">
                                Unduh <br>
                                Dokumen Sayembara <br>
                                Kompleks <br>
                                Perkantoran Legislatif
                            </h3>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download', 3)}}');" id="tombol_download_istana" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                            <img src="{{asset('image/Yudikatif.png')}}" class="w-100 img-sayembara" alt="">
                            <h3 class="title-sayembara">
                                Unduh <br>
                                Dokumen Sayembara <br>
                                Kompleks <br>
                                Perkantoran Yudikatif 
                            </h3>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download', 4)}}');" id="tombol_download_istana" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                            <img src="{{asset('image/Peribadatan.png')}}" class="w-100 img-sayembara" alt="">
                            <h3 class="title-sayembara peribadatan">
                                Unduh <br>
                                Dokumen Sayembara <br>
                                Kompleks Peribadatan
                            </h3>
                        </a>
                    </div>
                    {{-- @if (date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime('2022-03-30 23:59:58')))
                        <div class="col-lg-12 mt-5">
                            <div class="text-center">
                                <a href="{{route('web.profile.index')}}" class="btn btn-lg btn-primary fw-bolder">Daftar / Ubah</a>
                            </div>
                        </div>
                    @endif --}}

                </div>
            </div>
            
        </div>
    </div>
</div>