@if(Auth::guard('web')->user()->role == 1)
    <div class="row gy-5 g-xl-8">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Grafik Pengunjung</h3>
                </div>
                <div class="card-body">
                    <div id="chart_visitor" class="chart_div"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Pengunjung Per Hari ini</h3>
                </div>
                <div class="card-body">
                    <div class="hover-scroll h-400px px-5">
                        <table class="table table-row-dashed table-striped table-row-gray-300 align-middle gs-0 gy-4">
                            <thead>
                                <tr class="fw-bolder text-muted">
                                    <th class="min-w-100px">IP</th>
                                    <th class="min-w-50px">TOTAL</th>
                                    <th class="min-w-140px">TANGGAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($visitor as $item)
                                <tr>
                                    <td>
                                        <span class="fw-bold d-block fs-7">{{$item->client_ip}}</span>
                                    </td>
                                    <td>
                                        <span class="fw-bold d-block fs-7">{{$item->total}}</span>
                                    </td>
                                    <td>
                                        <span class="fw-bold d-block fs-7">{{$item->date->isoFormat('dddd, D MMMM YYYY')}}</span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Pendaftar</h3>
                </div>
                <div class="card-body">
                    <div class="hover-scroll h-400px px-5">
                        <table class="table table-row-dashed table-striped table-row-gray-300 align-middle gs-0 gy-4">
                            <thead>
                                <tr class="fw-bolder text-muted">
                                    <th class="min-w-50px">TOTAL PENDAFTAR</th>
                                    <th class="min-w-140px">TANGGAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pendaftar as $item)
                                <tr>
                                    <td>
                                        <span class="fw-bold d-block fs-7">{{$item->total}}</span>
                                    </td>
                                    <td>
                                        <span class="fw-bold d-block fs-7">{{$item->created_at->isoFormat('dddd, D MMMM YYYY')}}</span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Login Per Hari ini</h3>
                </div>
                <div class="card-body">
                    <div class="hover-scroll h-400px px-5">
                        <table class="table table-row-dashed table-striped table-row-gray-300 align-middle gs-0 gy-4">
                            <thead>
                                <tr class="fw-bolder text-muted">
                                    <th class="min-w-100px">IP</th>
                                    <th class="min-w-50px">TOTAL</th>
                                    <th class="min-w-140px">TANGGAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($login as $item)
                                <tr>
                                    <td>
                                        <span class="fw-bold d-block fs-7">{{$item->client_ip}}</span>
                                    </td>
                                    <td>
                                        <span class="fw-bold d-block fs-7">{{$item->total}}</span>
                                    </td>
                                    <td>
                                        <span class="fw-bold d-block fs-7">{{$item->date->isoFormat('dddd, D MMMM YYYY')}}</span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chart_cpeserta", am4charts.XYChart);

        // Enable chart cursor
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Enable scrollbar
        chart.scrollbarX = new am4core.Scrollbar();

        // Add data
        chart.data = '';

        // Create axes
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.grid.template.location = 0.5;
        dateAxis.dateFormatter.inputDateFormat = "yyyy-MM-dd";
        dateAxis.renderer.minGridDistance = 40;
        dateAxis.tooltipDateFormat = "MMM dd, yyyy";
        dateAxis.dateFormats.setKey("day", "dd");

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        // Create series
        var series = chart.series.push(new am4charts.LineSeries());
        series.tooltipText = "{date_field}\n[bold font-size: 17px]total: {valueY}[/]";
        series.dataFields.valueY = "total";
        series.dataFields.dateX = "date_field";
        series.strokeDasharray = 3;
        series.strokeWidth = 2
        series.strokeOpacity = 0.3;
        series.strokeDasharray = "3,3"

        var bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.strokeWidth = 2;
        bullet.stroke = am4core.color("#fff");
        bullet.setStateOnChildren = true;
        bullet.propertyFields.fillOpacity = "opacity";
        bullet.propertyFields.strokeOpacity = "opacity";

        var hoverState = bullet.states.create("hover");
        hoverState.properties.scale = 1.7;
    });
    am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chart_peserta", am4charts.XYChart);

        // Enable chart cursor
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Enable scrollbar
        chart.scrollbarX = new am4core.Scrollbar();

        // Add data
        chart.data = '';

        // Create axes
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.grid.template.location = 0.5;
        dateAxis.dateFormatter.inputDateFormat = "yyyy-MM-dd";
        dateAxis.renderer.minGridDistance = 40;
        dateAxis.tooltipDateFormat = "MMM dd, yyyy";
        dateAxis.dateFormats.setKey("day", "dd");

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        // Create series
        var series = chart.series.push(new am4charts.LineSeries());
        series.tooltipText = "{date_field}\n[bold font-size: 17px]total: {valueY}[/]";
        series.dataFields.valueY = "total";
        series.dataFields.dateX = "date_field";
        series.strokeDasharray = 3;
        series.strokeWidth = 2
        series.strokeOpacity = 0.3;
        series.strokeDasharray = "3,3"

        var bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.strokeWidth = 2;
        bullet.stroke = am4core.color("#fff");
        bullet.setStateOnChildren = true;
        bullet.propertyFields.fillOpacity = "opacity";
        bullet.propertyFields.strokeOpacity = "opacity";

        var hoverState = bullet.states.create("hover");
        hoverState.properties.scale = 1.7;
    });
    am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chart_visitor", am4charts.XYChart);
        chart.legend = new am4charts.Legend();
        chart.legend.labels.template.text = "[bold {color}]{name}[/]";

        // Enable chart cursor
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.lineX.disabled = true;
        chart.cursor.lineY.disabled = true;

        // Enable scrollbar
        chart.scrollbarX = new am4core.Scrollbar();

        // Add data
        chart.data = {!!$cvisitor!!};

        // Create axes
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.grid.template.location = 0.5;
        dateAxis.dateFormatter.inputDateFormat = "yyyy-MM-dd";
        dateAxis.renderer.minGridDistance = 40;
        dateAxis.tooltipDateFormat = "MMM dd, yyyy";
        dateAxis.dateFormats.setKey("day", "dd");

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        // Create series
        var series = chart.series.push(new am4charts.LineSeries());
        series.tooltipText = "{date_field}\n[bold font-size: 17px]total: {valueY}[/]";
        series.dataFields.valueY = "total_visitor";
        series.dataFields.dateX = "date_field";
        series.stroke = am4core.color("#ff0000"); // red
        series.strokeDasharray = 5;
        series.strokeWidth = 3
        series.strokeOpacity = 0.7;
        
        series.name = "Pengujung";
        



        var series_2 = chart.series.push(new am4charts.LineSeries());
        series_2.tooltipText = "{date_field}\n[bold font-size: 17px]total: {valueY}[/]";
        series_2.dataFields.valueY = "total_peserta";
        series_2.dataFields.dateX = "date_field";
        series_2.stroke = am4core.color("#000000"); // red
        series_2.strokeDasharray = 5;
        series_2.strokeWidth = 3
        series_2.strokeOpacity = 0.7;
        
        series_2.name = "Peserta";

        var bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.strokeWidth = 2;
        bullet.stroke = am4core.color("#fff");
        bullet.setStateOnChildren = true;
        bullet.propertyFields.fillOpacity = "opacity";
        bullet.propertyFields.strokeOpacity = "opacity";

        var bullet_2 = series_2.bullets.push(new am4charts.CircleBullet());
        bullet_2.strokeWidth = 2;
        bullet_2.stroke = am4core.color("#fff");
        bullet_2.setStateOnChildren = true;
        bullet_2.propertyFields.fillOpacity = "opacity";
        bullet_2.propertyFields.strokeOpacity = "opacity";

        var hoverState = bullet.states.create("hover");
        hoverState.properties.scale = 1.7;

        var hoverState = bullet_2.states.create("hover");
        hoverState.properties.scale = 1.7;
    });
    </script>
@elseif(Auth::guard('web')->user()->role == 2)
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pt-lg-10 mb-10">
                        <div class="fw-bold fs-3 text-muted mb-15">Selamat datang, {{Auth::guard('web')->user()->name}}</div>
                        <h1 class="fw-bolder fs-1qx text-gray-800 mb-7">Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    {!! $info_peserta !!}
@endif