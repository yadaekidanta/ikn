<style>html,body { padding: 0; margin:0; }</style>
<div style="font-family:Arial,Helvetica,sans-serif; line-height: 1.5; font-weight: normal; font-size: 15px; color: #2F3044; min-height: 100%; margin:0; padding:0; width:100%; background-color:#edf2f7">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;margin:0 auto; padding:0; max-width:600px">
		<tbody>
			<tr>
				<td align="center" valign="center" style="text-align:center; padding: 40px">
					<a href="javascript:;" rel="noopener" target="_blank">
						<img src="{{asset('logo.jpg')}}" style="height: 45px" alt="logo">
							<tr>
								<td align="left" valign="center">
									<div style="text-align:left; margin: 0 20px; padding: 40px; background-color:#ffffff; border-radius: 6px">
										<!--begin:Email content-->
										<div style="padding-bottom: 30px; font-size: 17px;">
											Dengan Hormat Tim pendaftar <strong>{{$notifiable->id}}</strong>
										</div>
										@if($st == "Lulus Verifikasi")
										<div style="padding-bottom: 30px">
											<strong>Anda telah lulus verifikasi administrasi</strong> Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara.
										</div>
										<div style="padding-bottom: 30px">
											Silahkan akses <a href="sayembaraikn.pu.go.id/auth">sayembaraikn.pu.go.id/auth</a> untuk dapat mengunduh nomor peserta, data dukung dokumen sayembara, dan Undangan Aanwijzing pada halaman dashboard masing-masing peserta.
										</div>
										@else
										<div style="padding-bottom: 30px">
											Mohon maaf, <strong>Anda tidak lulus verifikasi administrasi</strong> Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara.
										</div>
										<div style="padding-bottom: 30px">
											Terima kasih atas partisipasi dalam Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara, semoga sukses selalu.
										</div>
										@endif
										<!--end:Email content-->
										<div style="padding-bottom: 10px">Terima Kasih,
										<br>Panitia Sayembara IKN 2022.
										<br>08119393661
										<tr>
											<td align="center" valign="center" style="font-size: 13px; text-align:center;padding: 20px; color: #6d6e7c;">
												<p>Jl. Pattimura No. 20 Kebayoran Baru Jakarta Selatan 12110.</p>
												<p>Copyright ©
												<a href="javascript:;" rel="noopener" target="_blank">Kementerian Pekerjaan Umum dan Perumahan Rakyat</a>.</p>
											</td>
										</tr></br></div>
									</div>
								</td>
							</tr>
						</img>
					</a>
				</td>
			</tr>
		</tbody>
	</table>
</div>