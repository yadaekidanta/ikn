<div class="row">
    <div class="col-lg-4">
        <div class="card bg-warning card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-warning">Pendaftar</a>
                    <span class="fw-bold text-white fs-5">{{$total}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/008-boy-3.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-danger card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-danger">Tidak Lengkap Administrasi</a>
                    <span class="fw-bold text-white fs-5">{{$administrasi}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/009-boy-4.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-success card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-success">Lengkap Administrasi</a>
                    <span class="fw-bold text-white fs-5">{{$lengkap}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/016-boy-7.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="card bg-gray-400 card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-gray-400">Belum Verifikasi</a>
                    <span class="fw-bold text-white fs-5">{{$lengkap - $lulus - $tidak}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/007-boy-2.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-info card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-info">Lulus Verifikasi</a>
                    <span class="fw-bold text-white fs-5">{{$lulus}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/029-boy-11.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card bg-dark card-xl-stretch mb-5 mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <a href="#" class="fw-bolder text-white fs-6 mb-2 text-hover-dark">Tidak Lulus Verifikasi</a>
                    <span class="fw-bold text-white fs-5">{{$tidak}} peserta</span>
                </div>
                <img src="{{asset('keenthemes/media/svg/avatars/004-boy-1.svg')}}" alt="" class="align-self-end h-100px">
            </div>
        </div>
    </div>
</div>
<table class="table align-middle table-row-dashed fs-6 gy-5">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">No</th>
            <th class="min-w-125px">No Tim Pendaftaran</th>
            <th class="min-w-125px">Proses Verifikasi</th>
            <th class="min-w-125px">Nama Verifikator</th>
            <th class="min-w-125px">Status Verifikasi</th>
            <th class="min-w-125px">Keterangan</th>
            <th class="text-end min-w-70px">Aksi</th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $key => $item)
        <tr>
            <td>
                {{$key+ $collection->firstItem()}}
            </td>
            <td>
                {{$item->id}}
            </td>
            <td>
                @if($item->st == "Belum Verifikasi" OR $item->st == null OR $item->st == "Tidak Lengkap Administrasi")
                    @if($item->id_uv)
                        {{-- <a href="javascript:;" onclick="load_input('{{route('web.user-verification.show',[$item->id_uv])}}');" class="btn btn-sm btn-primary">Buka Data & Proses Verifikasi</a> --}}
                    @else
                        <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin ingin Verifikasi Tim ini ?','Ya','Tidak','GET','{{route('web.user-verification.create',[$item->id])}}');" class="btn btn-sm btn-primary">Buka Data & Proses Verifikasi</a>
                    @endif
                @endif
            </td>
            <td>
                {{$item->id_verif ? \App\Models\User::where('id',$item->id_verif)->first()->name : ''}}
            </td>
            <td>
                {{ $item->st ? $item->st : 'Belum Verifikasi' }}
            </td>
            <td>
                {{ $item->ket ? $item->ket : '' }}
            </td>
            <td>
                @if($item->id_verif ? Auth::guard('web')->user()->id == $item->id_verif : '')
                @if($item->st != "Tidak Lengkap Administrasi")
                {{-- <a href="javascript:;" onclick="load_input('{{route('web.user-verification.edit',[$item->id_uv])}}');" class="btn btn-sm btn-warning me-5">Ubah / Edit</a> --}}
                @endif
                {{-- <a href="javascript:;" onclick="handle_confirm('Apakah anda yakin ingin membatalkan verifikasi data ini ?','Ya','Tidak','PATCH','{{route('web.user-verification.cancel',$item->id_uv)}}');" class="btn btn-sm btn-danger">Hapus</a> --}}
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{$collection->links('themes.office.pagination')}}