<x-office-layout title="List Data Verifikasi">
    <div id="content_list">
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="card">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Statistik Verifikasi Kategori Sayembara</h3>
                            </div>
                            <div class="card-body">
                                <div id="chart_rekap_jenis" class="chart_div"></div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <form id="content_filter">
                        <div class="card-header border-0 pt-6">
                            <div class="card-title">
                                <div class="d-flex align-items-center position-relative my-1">
                                    {{-- <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                        </svg>
                                    </span> --}}
                                    <select class="form-select form-select-solid" name="st" onchange="load_list(1);">
                                        <option value="">Pilih Status</option>
                                        <option value="">Semua</option>
                                        <option value="Belum">Belum</option>
                                        <option value="Lulus">Lulus</option>
                                        <option value="Tidak">Tidak Lulus</option>
                                    </select>
                                </div>
                            </div>
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end">
                                    <a href="{{route('web.admin.user-verification.export')}}" class="btn btn-sm btn-primary">Export Excel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-body pt-0">
                        <div id="list_result"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>
            load_list(1);
        </script>
        <script>
            // ----------------- Start Chart untuk Rekap Jenis Peserta ----------------//
                am4core.ready(function() {
            
                    // Themes begin
                    am4core.useTheme(am4themes_animated);
                    // Themes end
            
                    // Create chart instance
                    var chart = am4core.create("chart_rekap_jenis", am4charts.XYChart);
                    // Add data
                    chart.data = {!! $data_chart !!};
            
                    // Create axes
            
                    // chart.colors.step = 2;
                    chart.colors.list = [
                        am4core.color("#009EF7"),
                        am4core.color("#50CD89"),
                        am4core.color("#F1416C"),
                        am4core.color("#FFC700"),
                    ];
            
                    chart.legend = new am4charts.Legend()
                    chart.legend.position = 'bottom'
                    chart.legend.paddingBottom = 20
                    chart.legend.labels.template.maxWidth = 95
            
                    var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
                    xAxis.dataFields.category = 'kategori'
                    xAxis.renderer.cellStartLocation = 0.1
                    xAxis.renderer.cellEndLocation = 0.9
                    xAxis.renderer.grid.template.location = 0;
            
                    var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
                    yAxis.min = 0;
            
                    // ---------- Start Function list inside amchart4 ----------//
                        function createSeries(value, name) {
                            var series = chart.series.push(new am4charts.ColumnSeries())
                            series.dataFields.valueY = value
                            series.dataFields.categoryX = 'kategori'
                            series.name = name
            
                            series.events.on("hidden", arrangeColumns);
                            series.events.on("shown", arrangeColumns);
            
                            series.columns.template.events.on("hit", function(ev) {
                                seriesIndex = ev.target.baseSprite.series.indexOf(ev.target.dataItem.component);
                                dataItemIndex = ev.target.dataItem.component.dataItems.indexOf(ev.target.dataItem);
                                console.log(dataItemIndex);
                                // console.log("clicked on ", ev.target.dataItem.component.coba);
                            }, this);
            
                            var bullet = series.bullets.push(new am4charts.LabelBullet())
                            bullet.interactionsEnabled = false
                            bullet.dy = -10;
                            bullet.label.text = '{valueY}'
                            bullet.label.fill = am4core.color('#000');
            
                            // series.columns.template.adapter.add("fill", function(fill, target) {
                            //     chart.color = target.dataItem.dataContext.color;
                            //     return chart.color;
                            // });
            
                            return series;
                        }
            
                        function arrangeColumns() {
            
                            var series = chart.series.getIndex(0);
            
                            var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
                            if (series.dataItems.length > 1) {
                                var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                                var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                                var delta = ((x1 - x0) / chart.series.length) * w;
                                if (am4core.isNumber(delta)) {
                                    var middle = chart.series.length / 2;
            
                                    var newIndex = 0;
                                    chart.series.each(function(series) {
                                        if (!series.isHidden && !series.isHiding) {
                                            series.dummyData = newIndex;
                                            newIndex++;
                                        }
                                        else {
                                            series.dummyData = chart.series.indexOf(series);
                                        }
                                    })
                                    var visibleCount = newIndex;
                                    var newMiddle = visibleCount / 2;
            
                                    chart.series.each(function(series) {
                                        var trueIndex = chart.series.indexOf(series);
                                        var newIndex = series.dummyData;
            
                                        var dx = (newIndex - trueIndex + middle - newMiddle) * delta
            
                                        series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                                        series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                                    })
                                }
                            }
                        }
                    // ---------- End Function list inside amchart4 ----------//
                    chart.cursor = new am4charts.XYCursor();
        
                    createSeries('jumlah', 'Jumlah Pemilih');
                    createSeries('lolos', 'Jumlah Lulus Verifikasi');
                    createSeries('tidak_lolos', 'Jumlah Tidak Lulus Verifikasi');
                    // createSeries('karya', 'Jumlah Dokumen Karya');
            
                });
            // ----------------- End Chart untuk  Rekap Jenis Peserta ----------------//
        </script>
    @endsection
</x-office-layout >