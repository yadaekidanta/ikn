<style type="text/css">
    ul.ck_image {
  list-style-type: none;
}

ul.ck_image li {
  display: inline-block;
  width: 24%;
}

ul.ck_image input[type="checkbox"][id^="cb"] {
  display: none;
}

ul.ck_image label {
  border: 1px solid #fff;
  padding: 10px;
  display: block;
  position: relative;
  margin: 10px;
  cursor: pointer;
}

ul.ck_image label:before {
  /*background-color: white;*/
  color: white;
  /*content: " ";*/
  display: block;
  border-radius: 50%;
  border: 1px solid grey;
  position: absolute;
  top: -5px;
  left: -5px;
  width: 25px;
  height: 25px;
  text-align: center;
  line-height: 28px;
  transition-duration: 0.4s;
  /*transform: scale(0);*/
}

ul.ck_image label img {
  /*height: 100px;*/
  width: 100%;
  transition-duration: 0.2s;
  transform-origin: 50% 50%;
}

:checked + label {
  border-color: #ddd;
}

:checked + label:before {
  content: "✓";
  background-color: grey;
  transform: scale(1);
}

:checked + label img {
  transform: scale(0.9);
  box-shadow: 0 0 5px #333;
  z-index: -1;
}
</style>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="stepper stepper-pills" id="verif_step">
            <div class="card" style="background-color:#cddbf0;">
                <div class="card-body">
                    <div class="stepper-nav flex-center flex-wrap mb-10">
                        <div class="stepper-item mx-2 my-4 current" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">1</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">
                                    Profil
                                </h3>
                                <div class="stepper-desc">
                                    Ketua Tim
                                </div>
                            </div>
                        </div>
                        <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">2</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">
                                    Profil
                                </h3>
                
                                <div class="stepper-desc">
                                    Badan Usaha
                                </div>
                            </div>
                        </div>
                        <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">3</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">
                                    Profil
                                </h3>
                
                                <div class="stepper-desc">
                                    Anggota SKA
                                </div>
                            </div>
                        </div>
                        <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">4</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">
                                    Profil
                                </h3>
                
                                <div class="stepper-desc">
                                    Anggota Non SKA
                                </div>
                            </div>
                        </div>
                        <div class="stepper-item mx-2 my-4" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">5</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">
                                    Profil
                                </h3>
                
                                <div class="stepper-desc">
                                    Jenis Sayembara
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card" style="background-color:#e0e0e0;">
                <input type="hidden" id="id_verif" value="{{$verif->id}}">
                <div class="card-body">
                    <form class="form w-lg-500px mx-auto" novalidate="novalidate" id="form_input">
                        <div class="mb-5">
                            <div class="flex-column current" data-kt-stepper-element="content">
                                <div class="fv-row">
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">Nama Lengkap</label>
                                        <div class="col-lg-8 fv-row">
                                            <input type="text" name="nama_lengkap" readonly class="form-control form-control-lg form-control-solid" placeholder="Nama Lengkap" value="{{$data->name}}" />
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">
                                            <span class="">No. KTP</span>
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="No. KTP harus valid"></i>
                                        </label>
                                        <div class="col-lg-5">
                                            <input type="tel" maxlength="16" readonly id="no_ktp" name="no_ktp" class="form-control form-control-lg form-control-solid" placeholder="No. KTP" value="{{$data->ktp_no}}" />
                                        </div>
                                        <div class="col-lg-3 fv-row">
                                            <a href="{{asset('storage/' .$data->ktp)}}" target="_blank" class="btn btn-primary mar-top-1">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">
                                            <span class="">No. NPWP</span>
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="No. NPWP harus valid"></i>
                                        </label>
                                        <div class="col-lg-5 fv-row">
                                            <input type="tel" maxlength="20" readonly id="no_npwp" name="no_npwp" class="form-control form-control-lg form-control-solid" placeholder="No. NPWP" value="{{$data->npwp_no}}" />
                                        </div>
                                        <div class="col-lg-3 fv-row">
                                            <a href="{{asset('storage/' .$data->npwp)}}" target="_blank" class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">
                                            <span class="">SKA</span>
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="SKA harus jernih"></i>
                                        </label>
                                        <div class="col-lg-5">
                                            <input type="text" readonly name="tanggal_ska" id="tanggal_ska" class="form-control form-control-lg form-control-solid" placeholder="SKA Sub" value="{{$data->ska_sub_id ? $data->sub->name : ''}}" />
                                            <input type="text" readonly name="tanggal_ska" id="tanggal_ska" class="form-control form-control-lg form-control-solid" placeholder="Masa Berakhir Berlaku SKA" value="{{$data->tanggal_ska}}" />
                                        </div>
                                        <div class="col-lg-3 fv-row">
                                            <a href="{{asset('storage/' .$data->ska)}}" target="_blank" class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">
                                            <span class="">Surat Ijazah Pendidikan Terakhir</span>
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Ijazah harus jernih"></i>
                                        </label>
                                        <div class="col-lg-8 fv-row">
                                            <a href="{{asset('storage/' .$data->ijazah)}}" target="_blank" class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-column" data-kt-stepper-element="content">
                                <div class="fv-row">
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">Nama Perusahaan</label>
                                        <div class="col-lg-8 fv-row">
                                            <input type="text" name="nama_perusahaan" readonly class="form-control form-control-lg form-control-solid" placeholder="Nama Perusahaan" value="{{$company ? $company->name : 'Belum Diketahui'}}" />
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">
                                            <span class="">Akta Pendirian</span>
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Akta Pendirian harus valid"></i>
                                        </label>
                                        @if($company)
                                        @if($company->akta)
                                        <div class="col-lg-8 fv-row">
                                            <a href="{{asset('storage/' .$company->akta)}}" download class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">
                                            <span class="">Akta Pendirian (Perubahan)</span>
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Akta Pendirian Perubahan harus valid"></i>
                                        </label>
                                        @if($company)
                                        @if($company->akta_perubahan)
                                        <div class="col-lg-8 fv-row">
                                            <a href="{{asset('storage/' .$company->akta_perubahan)}}" download class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">
                                            <span class="">SIUJK</span>
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="SIUJK harus valid"></i>
                                        </label>
                                        @if($company)
                                        @if($company->siujk)
                                        <div class="col-lg-8 fv-row">
                                            <a href="{{asset('storage/' .$company->siujk)}}" download class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">SBU</label>
                                        @if($company)
                                        <div class="col-lg-8 fv-row">
                                            <input type="text" name="nama_perusahaan" readonly class="form-control form-control-lg form-control-solid" placeholder="" value="{{$company->sbu ? $company->sbup->name : ''}}" />
                                        </div>
                                        @endif
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">No SBU</label>
                                        <div class="col-lg-8 fv-row">
                                            <input type="text" name="nama_perusahaan" readonly class="form-control form-control-lg form-control-solid" placeholder="" value="{{$company ? $company->sbu_no : ''}}" />
                                            @if($company)
                                            @if($company->sbu_file)
                                            <div class="col-lg-8 fv-row">
                                                <a href="{{asset('storage/' .$company->sbu_file)}}" download class="btn btn-primary">
                                                    Lihat File
                                                    <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                <rect x="0" y="0" width="24" height="24"/>
                                                                <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                                <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                                <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                            </g>
                                                        </svg>
                                                    </span>
                                                </a>
                                            </div>
                                            @endif
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">
                                            <span class="">NPWP</span>
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="NPWP harus valid"></i>
                                        </label>
                                        @if($company)
                                        <div class="col-lg-5">
                                            <input type="tel" maxlength="20" readonly id="no_npwp" name="no_npwp" class="form-control form-control-lg form-control-solid" placeholder="No. NPWP" value="{{$company ? $company->npwp_no : ''}}" />
                                        </div>
                                        @if($company->npwp)
                                        <div class="col-lg-3 fv-row">
                                            <a href="{{asset('storage/' .$company->npwp)}}" download class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6">
                                            <span class="">Surat Perjanjian Dukungan Badan Usaha</span>
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Surat Dukungan Perusahaan harus valid"></i>
                                        </label>
                                        @if($company)
                                        @if($company->file_dukungan_perusahaan)
                                        <div class="col-lg-8 fv-row">
                                            <a href="{{asset('storage/' .$company->file_dukungan_perusahaan)}}" download class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="flex-column" data-kt-stepper-element="content">
                                <div class="table-responsive">
                                    @if($anggota->count() < 1)
                                    <div class="alert alert-primary d-flex align-items-center p-5 mb-10">
                                        <span class="svg-icon svg-icon-2hx svg-icon-primary me-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor"/>
                                                <rect x="11" y="17" width="7" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor"/>
                                                <rect x="11" y="9" width="2" height="2" rx="1" transform="rotate(-90 11 9)" fill="currentColor"/>
                                            </svg>
                                        </span>
                                        <div class="d-flex flex-column">
                                            <h4 class="mb-1 text-primary">Belum ada anggota wajib SKA</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                    @else
                                    @foreach ($anggota as $key => $item)
                                    @if($key < 4)
                                    <h4 class="mb-1 text-primary">Anggota {{ $item->qualification->name }}</h4>
                                    <hr>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">Nama Lengkap</label>
                                        <div class="col-lg-8 fv-row">
                                            <input type="text" name="nama_perusahaan" readonly class="form-control form-control-lg form-control-solid" placeholder="" value="{{ $item->name }}" />
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">SKA Sub</label>
                                        <div class="col-lg-8 fv-row">
                                            <input type="text" name="nama_perusahaan" readonly class="form-control form-control-lg form-control-solid" placeholder="" value="{{$item->ska_sub_id ? $item->sub->name : ''}}" />
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">Masa Berakhir Berlaku SKA</label>
                                        <div class="col-lg-8 fv-row">
                                            <input type="text" name="nama_perusahaan" readonly class="form-control form-control-lg form-control-solid" placeholder="" value="{{$item->tanggal_ska}}" />
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">File SKA</label>
                                        <div class="col-lg-8 fv-row">
                                            <a href="{{asset('storage/' .$item->ska)}}" target="_blank" class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">File Ijazah</label>
                                        <div class="col-lg-8 fv-row">
                                            <a href="{{asset('storage/' .$item->ijazah)}}" target="_blank" class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    @endif
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="flex-column" data-kt-stepper-element="content">
                                <div class="table-responsive">
                                    @if($anggota->count() < 5)
                                    <div class="alert alert-primary d-flex align-items-center p-5 mb-10">
                                        <span class="svg-icon svg-icon-2hx svg-icon-primary me-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor"/>
                                                <rect x="11" y="17" width="7" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor"/>
                                                <rect x="11" y="9" width="2" height="2" rx="1" transform="rotate(-90 11 9)" fill="currentColor"/>
                                            </svg>
                                        </span>
                                        <div class="d-flex flex-column">
                                            <h4 class="mb-1 text-primary">Belum ada anggota Non SKA</h4>
                                            <span></span>
                                        </div>
                                    </div>
                                    @else
                                    @foreach ($anggota as $key => $item)
                                    @if($key > 3)
                                    <h4 class="mb-1 text-primary">Anggota</h4>
                                    <hr>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">Nama Lengkap</label>
                                        <div class="col-lg-8 fv-row">
                                            <input type="text" name="nama_perusahaan" readonly class="form-control form-control-lg form-control-solid" placeholder="" value="{{ $item->name }}" />
                                        </div>
                                    </div>
                                    <div class="row mb-6">
                                        <label class="col-lg-4 col-form-label fw-bold fs-6 ">File Ijazah</label>
                                        <div class="col-lg-8 fv-row">
                                            <a href="{{asset('storage/' .$item->ijazah)}}" target="_blank" class="btn btn-primary">
                                                Lihat File
                                                <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"/>
                                                            <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                            <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    @endif
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="flex-column" data-kt-stepper-element="content">
                                @if($verif->ket != "" || $verif->ket != null)
                                <div class="alert alert-primary d-flex align-items-center p-5 mb-10">
                                    <span class="svg-icon svg-icon-2hx svg-icon-primary me-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor"/>
                                            <rect x="11" y="17" width="7" height="2" rx="1" transform="rotate(-90 11 17)" fill="currentColor"/>
                                            <rect x="11" y="9" width="2" height="2" rx="1" transform="rotate(-90 11 9)" fill="currentColor"/>
                                        </svg>
                                    </span>
                                    <div class="d-flex flex-column">
                                        <h4 class="mb-1 text-primary">Tidak Lulus Verifikasi</h4>
                                        <span>{{$verif->ket}}</span>
                                    </div>
                                </div>
                                @endif
                                @php
                                $json_data = (@$data->jenis_sayembara) ? $data->jenis_sayembara : json_encode(array());
                                $pilihan_sayembara = json_decode($json_data);
                                @endphp
                                <div class="fv-row" style="width: 100%">
                                    <div class="col-lg-12">
                                        <h3>Sayembara yang diikuti :</h3>
                                        <div class="row">
                                            <ul class="ck_image">
                                              <li><input type="checkbox" id="cb1" disabled class="check_limit" name="kategori[]"  value="1" {{(in_array(1, $pilihan_sayembara) ? 'checked' : NULL)}}/>
                                                <label for="cb1"><img src="{{asset('image/Istana-Wakil-Presiden.png')}}" /></label>
                                                <h5 style="text-align: center;">
                                                    Sayembara <br>
                                                    Kompleks <br>
                                                    Istana Wakil Presiden
                                                </h5>
                                              </li>
                                              <li><input type="checkbox" id="cb2" disabled class="check_limit" name="kategori[]" value="2" {{(in_array(2, $pilihan_sayembara) ? 'checked' : NULL)}}/>
                                                <label for="cb2"><img src="{{asset('image/Legislatif.png')}}" /></label>
                                                <h5 style="text-align: center;">
                                                    Sayembara <br>
                                                    Kompleks <br>
                                                    Perkantoran Legislatif
                                                </h5>
                                              </li>
                                              <li><input type="checkbox" id="cb3" disabled class="check_limit" name="kategori[]" value="3" {{(in_array(3, $pilihan_sayembara) ? 'checked' : NULL)}}/>
                                                <label for="cb3"><img src="{{asset('image/Yudikatif.png')}}" /></label>
                                                <h5 style="text-align: center;">
                                                    Sayembara <br>
                                                    Kompleks <br>
                                                    Perkantoran Yudikatif
                                                </h5>
                                              </li>
                                              <li><input type="checkbox" id="cb4" disabled class="check_limit" name="kategori[]" value="4" {{(in_array(4, $pilihan_sayembara) ? 'checked' : NULL)}}/>
                                                <label for="cb4"><img src="{{asset('image/Peribadatan.png')}}" /></label>
                                                <h5 style="text-align: center;">
                                                    Sayembara <br>
                                                    Kompleks <br> Peribadatan
                                                    <br> &nbsp;
                                                </h5>
                                              </li>
                                            </ul>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-stack">
                            <div class="me-2">
                                @if($verif->st == "Lulus Verifikasi" || $verif->st == "Tidak Lulus Verifikasi" || $verif->ket != "" || $verif->ket != null || $verif->st == "Tidak Lengkap Administrasi")
                                <button type="button" onclick="load_list(1);" class="btn btn-danger me-5">
                                    Tutup
                                </button>
                                @else
                                <button type="button" onclick="handle_confirm('Apakah anda yakin ingin membatalkan verifikasi data ini ?','Ya','Tidak','PATCH','{{route('web.user-verification.cancel',$verif->id)}}');" class="btn btn-danger me-5">
                                    Batalkan
                                </button>
                                @endif
                            </div>
                            <div class="me-2">
                                <button type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous">
                                    Kembali
                                </button>
                            </div>
                            <div>
                                @if($data->jenis_sayembara != "[]")
                                <button type="button" class="btn btn-warning" data-kt-stepper-action="submit" onclick="handle_confirm_input('Apakah anda yakin ingin tidak meluluskan verifikasi data ini ?','Ya','Tidak','PATCH','{{route('web.user-verification.nverify',$verif->id)}}');">
                                    <span class="indicator-label">
                                        Tidak Lulus
                                    </span>
                                    <span class="indicator-progress">
                                        Harap Tunggu... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </span>
                                </button>
                                <button type="button" class="btn btn-success" data-kt-stepper-action="submit" onclick="handle_confirm('Apakah anda yakin ingin verifikasi data ini ?','Ya','Tidak','PATCH','{{route('web.user-verification.verify',$verif->id)}}');">
                                    <span class="indicator-label">
                                        Lulus
                                    </span>
                                    <span class="indicator-progress">
                                        Harap Tunggu... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </span>
                                </button>
                                @endif
                                <button type="button" class="btn btn-primary" data-kt-stepper-action="next">
                                    Selanjutnya
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>
            stepper('verif_step');
        </script>
    </div>
</div>