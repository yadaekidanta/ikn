<div class="row row-eq-height">
    <div class="col-lg-3">
        <div class="card bg-primary card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <h5  class="fw-bolder text-white fs-6 mb-2">Istana Wakil Presiden</h5>
                    <span class="fw-bold text-white fs-5">{{@intval($jumlah_sayembara[1])}} peserta</span>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card bg-success card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <h5  class="fw-bolder text-white fs-6 mb-2">Perkantoran Legislatif</h5>
                    <span class="fw-bold text-white fs-5">{{@intval($jumlah_sayembara[2])}} peserta</span>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card bg-warning card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <h5  class="fw-bolder text-white fs-6 mb-2">Perkantoran Yudikatif</h5>
                    <span class="fw-bold text-white fs-5">{{@intval($jumlah_sayembara[3])}} peserta</span>
                </div>
                
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card bg-danger card-xl-stretch mb-xl-8">
            <div class="card-body d-flex align-items-center pt-3 pb-0">
                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                    <h5  class="fw-bolder text-white fs-6 mb-2">Peribadatan</h5>
                    <span class="fw-bold text-white fs-5">{{@intval($jumlah_sayembara[4])}} peserta</span>
                </div>
                
            </div>
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-md-12 mb-4 mt-4">
        <span class="svg-icon svg-icon-2 svg-icon-gray-500">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <path opacity="0.3" d="M5.78001 21.115L3.28001 21.949C3.10897 22.0059 2.92548 22.0141 2.75004 21.9727C2.57461 21.9312 2.41416 21.8418 2.28669 21.7144C2.15923 21.5869 2.06975 21.4264 2.0283 21.251C1.98685 21.0755 1.99507 20.892 2.05201 20.7209L2.886 18.2209L7.22801 13.879L10.128 16.774L5.78001 21.115Z" fill="black"></path>
                <path d="M21.7 8.08899L15.911 2.30005C15.8161 2.2049 15.7033 2.12939 15.5792 2.07788C15.455 2.02637 15.3219 1.99988 15.1875 1.99988C15.0531 1.99988 14.92 2.02637 14.7958 2.07788C14.6717 2.12939 14.5589 2.2049 14.464 2.30005L13.74 3.02295C13.548 3.21498 13.4402 3.4754 13.4402 3.74695C13.4402 4.01849 13.548 4.27892 13.74 4.47095L14.464 5.19397L11.303 8.35498C10.1615 7.80702 8.87825 7.62639 7.62985 7.83789C6.38145 8.04939 5.2293 8.64265 4.332 9.53601C4.14026 9.72817 4.03256 9.98855 4.03256 10.26C4.03256 10.5315 4.14026 10.7918 4.332 10.984L13.016 19.667C13.208 19.859 13.4684 19.9668 13.74 19.9668C14.0115 19.9668 14.272 19.859 14.464 19.667C15.3575 18.77 15.9509 17.618 16.1624 16.3698C16.374 15.1215 16.1932 13.8383 15.645 12.697L18.806 9.53601L19.529 10.26C19.721 10.452 19.9814 10.5598 20.253 10.5598C20.5245 10.5598 20.785 10.452 20.977 10.26L21.7 9.53601C21.7952 9.44108 21.8706 9.32825 21.9221 9.2041C21.9737 9.07995 22.0002 8.94691 22.0002 8.8125C22.0002 8.67809 21.9737 8.54505 21.9221 8.4209C21.8706 8.29675 21.7952 8.18392 21.7 8.08899Z" fill="black"></path>
            </svg>
        </span>
        <span class="text-gray-700 fw-bold fs-3">{{$title_table}}</span>
    </div>
    
    <div class="table-responsive">
        <table class="table table-hover table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
            <thead>
                <tr class="fw-bolder text-muted">
                    <th class="text-center" scope="col">No</th>
                    <th class="text-center" >No Urut</th>
                    <th class="text-center" scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $i = (request()->get('page') - 1) * $paging;
                    $i = ($i < 0) ? 0 : $i;
                @endphp
                @foreach ($data_view as $data_s)
                    <tr>
                        <td>
                            @php
                                $i++;
                                echo $i;
                                
                            @endphp
                        </td>
                        <td>
                            {{$data_s->urut}}
                        </td>
                        <td class="text-center">
                            <a href="javascript:;" onclick="load_input('{{route('web.penilai.karya.show',$data_s->id_karya)}}');" class="btn btn-bg-primary text-inverse-primary me-2 mb-2 btn-sm align-items-center">
                                <i class="fa la-view text-inverse-info"></i>
                                Lihat Karya
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-12 mt-5">
        {{$data_view->links('pages.penilai.karya.pagination')}}
    </div>
    
</div>

