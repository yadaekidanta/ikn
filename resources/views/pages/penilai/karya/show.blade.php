@section('custom_css')
    <style>
        @media (min-width: 768px){
            .seven-cols .col-md-1,
            .seven-cols .col-sm-1,
            .seven-cols .col-lg-1  {
                width: 100%;
                *width: 100%;
            }
        }


        @media (min-width: 992px) {
            .seven-cols .col-md-1,
            .seven-cols .col-sm-1,
            .seven-cols .col-lg-1 {
                width: 14.285714285714285714285714285714%;
                *width: 14.285714285714285714285714285714%;
            }
        }


        @media (min-width: 1200px) {
            .seven-cols .col-md-1,
            .seven-cols .col-sm-1,
            .seven-cols .col-lg-1 {
                width: 14.285714285714285714285714285714%;
                *width: 14.285714285714285714285714285714%;
            }
        }
    </style>
@endsection
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header card-header-stretch">
                        <!--begin::Title-->
                        <div class="card-title d-flex align-items-center">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                <span class="svg-icon svg-icon-2hx svg-icon-primary d-none"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M14 3V21H10V3C10 2.4 10.4 2 11 2H13C13.6 2 14 2.4 14 3ZM7 14H5C4.4 14 4 14.4 4 15V21H8V15C8 14.4 7.6 14 7 14Z" fill="black"/>
                                    <path d="M21 20H20V8C20 7.4 19.6 7 19 7H17C16.4 7 16 7.4 16 8V20H3C2.4 20 2 20.4 2 21C2 21.6 2.4 22 3 22H21C21.6 22 22 21.6 22 21C22 20.4 21.6 20 21 20Z" fill="black"/>
                                    </svg>
                                </span>
                            <!--end::Svg Icon-->
                            <h3 class="fw-bolder m-0 text-gray-800 ps-2 d-none">File Karya {{$karya->urut}}</h3>
                        </div>
                        <div class="card-toolbar m-0">
                            <ul class="nav nav-tabs nav-line-tabs nav-stretch fs-6 border-0 fw-bolder" role="tablist">
                            @if(\App\Models\Karya::where('jenis_sayembara',$karya->jenis_sayembara)->where('urut','<',$karya->urut)->get()->count() > 0)
                            @php
                            $b = \App\Models\Karya::where('jenis_sayembara',$karya->jenis_sayembara)->where('urut','<',$karya->urut)->latest('urut')->first();
                            @endphp
                            <li class="nav-item" role="presentation">
                                <a href="javascript:;" onclick="load_input('{{route('web.penilai.karya.show',$b->id)}}');" class="nav-link justify-content-center text-active-gray-800" data-bs-toggle="tab" role="tab" >Sebelumnya</a>
                            </li>
                            @endif
                            <span class="mt-8">
                                No Urut : {{$karya->urut}}
                            </span>
                            @if(\App\Models\Karya::where('jenis_sayembara',$karya->jenis_sayembara)->where('urut','>',$karya->urut)->get()->count() > 0)
                            @php
                            $n = \App\Models\Karya::where('jenis_sayembara',$karya->jenis_sayembara)->where('urut','>',$karya->urut)->first();
                            @endphp
                            <a href="javascript:;" onclick="load_input('{{route('web.penilai.karya.show',$n->id)}}');" class="nav-link justify-content-center text-active-gray-800" data-bs-toggle="tab" role="tab" >Selanjutnya</a>
                            @endif
                            </ul>
                        </div>
                        <!--end::Title-->
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            </div>
            <div class="col-12">
            </div>
        </div>
        <div class="row seven-cols">
            @foreach($karya->karya_file as $file)
            <div class="card col-lg-1">
                <div class="card-header card-header-stretch">
                    <!--begin::Title-->
                    <div class="card-title d-flex align-items-center">
                        <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                            <span class="svg-icon svg-icon-2hx svg-icon-primary"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M14 3V21H10V3C10 2.4 10.4 2 11 2H13C13.6 2 14 2.4 14 3ZM7 14H5C4.4 14 4 14.4 4 15V21H8V15C8 14.4 7.6 14 7 14Z" fill="black"/>
                                <path d="M21 20H20V8C20 7.4 19.6 7 19 7H17C16.4 7 16 7.4 16 8V20H3C2.4 20 2 20.4 2 21C2 21.6 2.4 22 3 22H21C21.6 22 22 21.6 22 21C22 20.4 21.6 20 21 20Z" fill="black"/>
                                </svg>
                            </span>
                        <!--end::Svg Icon-->
                        <h3 class="fw-bolder m-0 text-gray-800 ps-2">File Karya {{$karya->urut}}</h3>
                    </div>
                    <!--end::Title-->
                </div>
                <div class="card-body">
                    <iframe class="col-md-12" src="'{{$file->file}}'" title="Document Viewer"  style="height:100%" allowfullscreen></iframe>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>