@php
$end = date('2022-04-08');
$last = \Carbon\Carbon::createFromTimestamp($last);
if($user->role == 4)
{
    if($user->user_id == 0){
        $role = 'Ketua';
    }else{
        $role = 'Anggota';
    }
    if(!$user->verified_at){
        $color = 'info';
        $status = 'Belum Verifikasi';
    }else{
        $color = 'success';
        $status = 'Sudah Verifikasi';
    }
}
$col_num = 0;
$id_array = array();
foreach($collection as $col_item){
    if($col_num >= 4){
        break;
    }
    $id_array[] = $col_item->id;
    $col_num++;
}
@endphp
<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">
                    <a href="javascript:;" class="text-muted text-hover-primary">Profil</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">Anggota Tim</li>
            </ul>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card mb-5 mb-xl-5">
            <div class="card-body pt-9 pb-0">
                <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                    <div class="me-7 mb-4">
                        <div class="symbol symbol-100px symbol-lg-100px symbol-fixed position-relative">
                            <img src="{{asset('logo.jpg')}}" alt="image" />
                            <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
                        </div>
                    </div>
                    <div class="flex-grow-1">
                        <div class="d-flex justify-content-between align-items-start flex-wrap">
                            <div class="d-flex flex-column">
                                <div class="d-flex align-items-center">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-1 fw-bolder me-1">Tim Nomor Pendaftaran {{$user->id}}</a>
                                </div>
                            </div>
                            <div class="d-flex">
                                <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Tanggal Mulai Daftar {{$user->created_at->isoFormat('dddd, DD MMMM YYYY')}}</a>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap flex-stack">
                            <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                                <div class="d-flex justify-content-between w-100 mt-auto mb-2 d-none">
                                    <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil Ketua</span>
                                    <span class="fw-bolder fs-6">{{$complete}}%</span>
                                </div>
                                <div class="h-5px mx-3 w-100 bg-light d-none">
                                    <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$complete}}%;" aria-valuenow="{{$complete}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="d-flex flex-wrap">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1" style="font-color:#e2304a;">Waktu terakhir Login {{\Carbon\Carbon::parse($last)->diffForHumans()}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap flex-stack">
                            <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                                <div class="d-flex justify-content-between w-100 mt-auto mb-2 d-none">
                                    <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil Perusahaan</span>
                                    <span class="fw-bolder fs-6">{{$completep}}%</span>
                                </div>
                                <div class="h-5px mx-3 w-100 bg-light d-none">
                                    <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$completep}}%;" aria-valuenow="{{$completep}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="d-flex flex-wrap">
                                    <a href="javascript:;" class="text-hover-primary fs-5 fw-bolder me-1" style="color:#e2304a;">Batas Waktu Pendaftaran {{\Carbon\Carbon::parse($end)->isoFormat('dddd, DD MMMM YYYY')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_create_account_stepper">
            <div class="card d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px w-xxl-400px me-9" style="height:25%;background-color:#cddbf0;">
                <div class="card-body px-6 px-lg-10 px-xxl-15 py-20">
                    <div class="stepper-nav">
                        <div class="stepper-item completed" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">1</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">Profil</h3>
                                <div class="stepper-desc fw-bold">Ketua Tim</div>
                            </div>
                        </div>
                        <div class="stepper-item completed" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">2</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">Profil</h3>
                                <div class="stepper-desc fw-bold">Perusahaan</div>
                            </div>
                        </div>
                        <div class="stepper-item current" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">3</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">Profil</h3>
                                <div class="stepper-desc fw-bold">Anggota</div>
                            </div>
                        </div>
                        <div class="stepper-item" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">4</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">Kategori Sayembara</h3>
                                <div class="stepper-desc fw-bold">Pilih Kategori Sayembara Anda</div>
                            </div>
                        </div>
                        <div class="stepper-item" data-kt-stepper-element="nav">
                            <div class="stepper-line w-40px"></div>
                            <div class="stepper-icon w-40px h-40px">
                                <i class="stepper-check fas fa-check"></i>
                                <span class="stepper-number">5</span>
                            </div>
                            <div class="stepper-label">
                                <h3 class="stepper-title">Selesai</h3>
                                <div class="stepper-desc fw-bold">Kirim data Anda untuk verifikasi</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card d-flex flex-row-fluid" style="background-color:#e0e0e0;">
                <div class="card-header cursor-pointer card-header-stretch">
                    <div class="card-title m-0">
                        <h3 class="fw-bolder m-0">{{$data->id ? 'Perbarui' : 'Tambah'}} Anggota</h3>
                    </div>
                    <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-danger align-self-center">
                        <span class="svg-icon svg-icon-4 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="black" />
                                <path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="black" />
                            </svg>
                        </span>
                        Kembali
                    </a>
                </div>
                <form id="form_input">
                    <div class="card-body py-20 w-100 w-xl-700px px-9 flex-center">
                        <div class="current" data-kt-stepper-element="content">
                            <div class="fv-row">
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6 required">Nama Lengkap</label>
                                    <div class="col-lg-8 fv-row">
                                        <input type="text" name="nama_lengkap" class="form-control form-control-lg form-control-solid" placeholder="Nama Lengkap" value="{{$data->name}}" />
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6 {{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}}">No. Handphone</label>
                                    <div class="col-lg-8 fv-row">
                                        <input type="tel" maxlength="20" id="no_handphone" name="no_handphone" class="form-control form-control-lg form-control-solid" placeholder="No. Handphone" value="{{$data->phone}}" />
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6 {{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}}">Email</label>
                                    <div class="col-lg-8 fv-row">
                                        <input type="email" name="email" class="form-control form-control-lg form-control-solid" placeholder="Email" value="{{$data->email}}" />
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">
                                        <span class="required">No. KTP / Passport</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="No. KTP harus valid"></i>
                                    </label>
                                    <div class="col-lg-8 fv-row">
                                        <input type="tel" maxlength="16" id="no_ktp" name="no_ktp" class="form-control form-control-lg form-control-solid" placeholder="No. KTP" value="{{$data->ktp_no}}" />
                                        
                                        <input type="file" id="fa_01" name="ktp" class="form-control form-control-lg form-control-solid {{($data->ktp) ? 'custom-file':NULL;}}" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="fa_01" class="label-file btn btn-primary">Ubah File</label>
                                             
                                        @if ($data->ktp)
                                        <a href="{{asset('storage/' .$data->ktp)}}" download class="btn btn-primary">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        @endif

                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 2MB</div>
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">
                                        <span class="{{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}}">No. NPWP</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="No. NPWP harus valid"></i>
                                    </label>
                                    <div class="col-lg-8 fv-row">
                                        <input type="tel" maxlength="20" id="no_npwp" name="no_npwp" class="form-control form-control-lg form-control-solid" placeholder="No. NPWP" value="{{$data->npwp_no}}" />

                                        <input type="file" id="fa_02" name="npwp" class="form-control form-control-lg form-control-solid {{($data->npwp) ? 'custom-file':NULL;}}" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="fa_02" class="label-file btn btn-primary">Ubah File</label>

                                     
                                        @if ($data->npwp)
                                        <a href="{{asset('storage/' .$data->npwp)}}" download class="btn btn-primary">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        @endif

                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 2MB</div>
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">
                                        <span class="{{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}}">No. NRKA / No. STRA</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="No. NRKA / No. STRA harus valid"></i>
                                    </label>
                                    <div class="col-lg-8 fv-row">
                                        <input type="tel" maxlength="20" id="no_nrka" name="no_nrka" class="form-control form-control-lg form-control-solid" placeholder="No. NRKA / No. STRA" value="{{$data->nrka_no}}" />
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label {{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}} fw-bold fs-6">SKA Sub</label>
                                    <div class="col-lg-8 fv-row">
                                        <div class="d-flex">
                                            @foreach($sub as $item)
                                            <div class="form-check form-check-custom form-check-solid me-10">
                                                <input class="form-check-input" type="radio" value="{{$item->id}}" name="sub" id="sub" {{$data->ska_sub_id == $item->id ? 'checked' : ''}}/>
                                                <label class="form-check-label" for="sub">
                                                    {{$item->name}}
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">
                                        <span class="{{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}}">SKA</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="SKA harus jernih"></i>
                                    </label>
                                    <div class="col-lg-8 fv-row">

                                        <input type="file" id="fa_03" name="ska" class="form-control form-control-lg form-control-solid {{($data->ska) ? 'custom-file':NULL;}}" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="fa_03" class="label-file btn btn-primary">Ubah File</label>

                                        @if ($data->ska)
                                        <a href="{{asset('storage/' .$data->ska)}}" download class="btn btn-primary">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        @endif
                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 2MB</div>
                                        
                                        @if($collection->count() < 4 || in_array($data->id, $id_array))
                                        <select class="form-select form-select-solid" name="kualifikasi" id="kualifikasi">
                                            <option value="">Pilih SKA Kualifikasi</option>
                                            @foreach($kualifikasi as $item)
                                                <option value="{{$item->id}}" {{$data->ska_kualifikasi_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @else
                                        <input type="text" id="ska_kualifikasi" name="ska_kualifikasi" class="form-control form-control-lg form-control-solid" placeholder="SKA Kualifikasi" value="{{$data->ska_kualifikasi}}" />
                                        @endif
                                        <input type="text" name="tanggal_ska" id="tanggal_ska" class="form-control form-control-lg form-control-solid" placeholder="Masa Berakhir Berlaku SKA" value="{{$data->tanggal_ska}}" />
                                        
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label fw-bold fs-6">
                                        <span class="required">Surat Ijazah Pendidikan Terakhir</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Ijazah harus jernih"></i>
                                    </label>
                                    <div class="col-lg-8 fv-row">

                                        <input type="file" id="fa_04" name="ijazah" class="form-control form-control-lg form-control-solid {{($data->ijazah) ? 'custom-file':NULL;}}" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="fa_04" class="label-file btn btn-primary">Ubah File</label>
 
                                        @if ($data->ijazah)
                                        <a href="{{asset('storage/' .$data->ijazah)}}" download class="btn btn-primary">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        @endif

                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 2MB</div>
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label {{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}} fw-bold fs-6">Alamat</label>
                                    <div class="col-lg-8 fv-row">
                                        <textarea name="alamat" class="form-control form-control-lg form-control-solid" placeholder="Alamat">{{$data->address}}</textarea>
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label {{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}} fw-bold fs-6">Kode Pos</label>
                                    <div class="col-lg-8 fv-row">
                                        <input type="tel" id="kode_pos" name="kode_pos" maxlength="5" class="form-control form-control-lg form-control-solid" placeholder="Kode Pos" value="{{$data->postcode}}" />
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label {{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}} fw-bold fs-6">Provinsi</label>
                                    <div class="col-lg-8 fv-row">
                                        <select class="form-select form-select-solid" name="provinsi" id="provinsi">
                                            <option>Pilih Provinsi</option>
                                            @foreach($province as $item)
                                                <option value="{{$item->id}}" {{$data->province_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-6">
                                    <label class="col-lg-4 col-form-label {{$collection->count() < 4 || in_array($data->id, $id_array) ? 'required' : ''}} fw-bold fs-6">Kota</label>
                                    <div class="col-lg-8 fv-row">
                                        <select class="form-select form-select-solid" name="kota" id="kota">
                                            <option>Harap pilih Provinsi dahulu</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end py-6 px-9">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-danger me-10">
                            <span class="svg-icon svg-icon-4 me-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="black" />
                                    <path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="black" />
                                </svg>
                            </span>
                            Kembali
                        </a>
                        @if($data->id)
                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.anggota.update',$data->id)}}','PATCH');" class="btn btn-primary">
                            Simpan
                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
                                    <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
                                </svg>
                            </span>
                        </button>
                        @else
                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.anggota.store')}}','POST');" class="btn btn-primary">
                            Simpan
                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
                                    <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
                                </svg>
                            </span>
                        </button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('keenthemes/plugins/custom/fslightbox/fslightbox.bundle.js')}}"></script>
<script>
    obj_startdatenow('tanggal_ska');
    number_only('no_handphone');
    number_only('no_ktp');
    number_only('no_npwp');
    number_only('no_nrka');
    npwp_format("no_npwp");
    number_only('kode_pos');
    obj_select('provinsi','Pilih Provinsi');
    obj_select('kota','Pilih Kota');
    @if($data->province_id)
    $('#provinsi').val('{{$data->province_id}}');
    setTimeout(function(){ 
        $('#provinsi').trigger('change');
        setTimeout(function(){ 
            $('#kota').val('{{$data->city_id}}');
            $('#kota').trigger('change');
        }, 1200);
    }, 500);
    @endif
    $("#provinsi").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.get_city')}}",
            data: {prov : $("#provinsi").val()},
            success: function(response){
                $("#kota").html(response);
            }
        });
    });
</script>