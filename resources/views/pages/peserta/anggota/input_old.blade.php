@php
$end = date('2022-03-28');
$last = \Carbon\Carbon::createFromTimestamp($last);
if($user->role == 4)
{
    if($user->user_id == 0){
        $role = 'Ketua';
    }else{
        $role = 'Anggota';
    }
    if(!$user->verified_at){
        $color = 'info';
        $status = 'Belum Verifikasi';
    }else{
        $color = 'success';
        $status = 'Sudah Verifikasi';
    }
}
@endphp
<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Halaman Profil</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">
                    <a href="javascript:;" class="text-muted text-hover-primary">Anggota Tim</a>
                </li>
                {{-- <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">Overview</li> --}}
            </ul>
        </div>
        <div class="d-flex align-items-center gap-2 gap-lg-3 d-none">
            <div class="m-0">
                <a href="#" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                    <span class="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="black" />
                        </svg>
                    </span>
                    Filter
                </a>
                <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_6220edcb36682">
                    <div class="px-7 py-5">
                        <div class="fs-5 text-dark fw-bolder">Filter Options</div>
                    </div>
                    <div class="separator border-gray-200"></div>
                    <div class="px-7 py-5">
                        <div class="mb-10">
                            <label class="form-label fw-bold">Status:</label>
                            <div>
                                <select class="form-select form-select-solid" data-kt-select2="true" data-placeholder="Select option" data-dropdown-parent="#kt_menu_6220edcb36682" data-allow-clear="true">
                                    <option></option>
                                    <option value="1">Approved</option>
                                    <option value="2">Pending</option>
                                    <option value="2">In Process</option>
                                    <option value="2">Rejected</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-10">
                            <label class="form-label fw-bold">Member Type:</label>
                            <div class="d-flex">
                                <label class="form-check form-check-sm form-check-custom form-check-solid me-5">
                                    <input class="form-check-input" type="checkbox" value="1" />
                                    <span class="form-check-label">Author</span>
                                </label>
                                <label class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="2" checked="checked" />
                                    <span class="form-check-label">Customer</span>
                                </label>
                            </div>
                        </div>
                        <div class="mb-10">
                            <label class="form-label fw-bold">Notifications:</label>
                            <div class="form-check form-switch form-switch-sm form-check-custom form-check-solid">
                                <input class="form-check-input" type="checkbox" value="" name="notifications" checked="checked" />
                                <label class="form-check-label">Enabled</label>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true">Reset</button>
                            <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card mb-5 mb-xl-10">
            <div class="card-body pt-9 pb-0">
                <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                    <div class="me-7 mb-4">
                        <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
                            <img src="{{asset('logo.jpg')}}" alt="image" />
                            <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
                        </div>
                    </div>
                    <div class="flex-grow-1">
                        <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                            <div class="d-flex flex-column">
                                <div class="d-flex align-items-center">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-1 fw-bolder me-1">Tim No Urut {{$user->id}}</a>
                                </div>
                            </div>
                            <div class="d-flex">
                                <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Tanggal Daftar {{$user->created_at->isoFormat('dddd, DD MMMM YYYY')}}</a>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap flex-stack">
                            <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
                            </div>
                            @if($user->is_unduh == 0)
                            <div class="d-flex">
                                <div class="d-flex flex-wrap">
                                    <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download')}}');" id="tombol_download" class="btn btn-flex btn-danger px-6">
                                        <span class="svg-icon svg-icon-2x">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                                    <path d="M5.74714567,13.0425758 C4.09410362,11.9740356 3,10.1147886 3,8 C3,4.6862915 5.6862915,2 9,2 C11.7957591,2 14.1449096,3.91215918 14.8109738,6.5 L17.25,6.5 C19.3210678,6.5 21,8.17893219 21,10.25 C21,12.3210678 19.3210678,14 17.25,14 L8.25,14 C7.28817895,14 6.41093178,13.6378962 5.74714567,13.0425758 Z" fill="#000000" opacity="0.3"/>
                                                    <path d="M11.1288761,15.7336977 L11.1288761,17.6901712 L9.12120481,17.6901712 C8.84506244,17.6901712 8.62120481,17.9140288 8.62120481,18.1901712 L8.62120481,19.2134699 C8.62120481,19.4896123 8.84506244,19.7134699 9.12120481,19.7134699 L11.1288761,19.7134699 L11.1288761,21.6699434 C11.1288761,21.9460858 11.3527337,22.1699434 11.6288761,22.1699434 C11.7471877,22.1699434 11.8616664,22.1279896 11.951961,22.0515402 L15.4576222,19.0834174 C15.6683723,18.9049825 15.6945689,18.5894857 15.5161341,18.3787356 C15.4982803,18.3576485 15.4787093,18.3380775 15.4576222,18.3202237 L11.951961,15.3521009 C11.7412109,15.173666 11.4257142,15.1998627 11.2472793,15.4106128 C11.1708299,15.5009075 11.1288761,15.6153861 11.1288761,15.7336977 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.959697, 18.661508) rotate(-270.000000) translate(-11.959697, -18.661508) "/>
                                                </g>
                                            </svg>
                                        </span>
                                        <span class="d-flex flex-column align-items-start ms-2">
                                            <span class="fs-3 fw-bolder">Unduh</span>
                                            <span class="fs-7">File KAK</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            @endif
                            <div class="d-flex">
                                <div class="d-flex flex-wrap">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Last Login {{\Carbon\Carbon::parse($last)->diffForHumans()}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap flex-stack">
                            <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
                                <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                    <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil</span>
                                    <span class="fw-bolder fs-6">{{$user->calculate_profile($user)}}%</span>
                                </div>
                                <div class="h-5px mx-3 w-100 bg-light mb-3">
                                    <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$user->calculate_profile($user)}}%;" aria-valuenow="{{$user->calculate_profile($user)}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="d-flex flex-wrap">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Sisa Waktu {{\Carbon\Carbon::parse($end)->diffForHumans()}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-5 mb-xl-10">
            <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
                <div class="card-title m-0">
                    <h3 class="fw-bolder m-0">{{$data->id ? 'Perbarui' : 'Tambah'}} Anggota</h3>
                </div>
            </div>
            <div id="kt_account_settings_profile_details" class="collapse show">
                <form id="form_input" class="form">
                    <div class="card-body border-top p-9">
                        <div class="row mb-6 d-none">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">Full Name</label>
                            <div class="col-lg-8">
                                <div class="row">
                                    <div class="col-lg-6 fv-row">
                                        <input type="text" name="fname" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="First name" value="Max" />
                                    </div>
                                    <div class="col-lg-6 fv-row">
                                        <input type="text" name="lname" class="form-control form-control-lg form-control-solid" placeholder="Last name" value="Smith" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label fw-bold fs-6 required">Nama Lengkap</label>
                            <div class="col-lg-8 fv-row">
                                <input type="text" name="nama_lengkap" class="form-control form-control-lg form-control-solid" placeholder="Nama Lengkap" value="{{$data->name}}" />
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label fw-bold fs-6 required">No. Handphone</label>
                            <div class="col-lg-8 fv-row">
                                <input type="tel" maxlength="20" id="no_handphone" name="no_handphone" class="form-control form-control-lg form-control-solid" placeholder="No. Handphone" value="{{$data->phone}}" />
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label fw-bold fs-6 required">Email</label>
                            <div class="col-lg-8 fv-row">
                                <input type="email" name="email" class="form-control form-control-lg form-control-solid" placeholder="Email" value="{{$data->email}}" />
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">
                                <span class="required">No. KTP</span>
                                <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="No. KTP harus valid"></i>
                            </label>
                            <div class="col-lg-8 fv-row">
                                <input type="tel" maxlength="16" id="no_ktp" name="no_ktp" class="form-control form-control-lg form-control-solid" placeholder="No. KTP" value="{{$data->ktp_no}}" />
                                <input type="file" name="ktp" class="form-control form-control-lg form-control-solid" accept="application/pdf,image/jpeg,image/jpg,image/png" />
                                @if ($data->ktp)
                                <a class="d-block overlay" data-fslightbox="lightbox-basic" href="{{asset('storage/' .$data->ktp)}}">
                                    <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('storage/' .$data->ktp)}}')"></div>
                                    <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                        <i class="bi bi-eye-fill text-white fs-3x"></i>
                                    </div>
                                </a>
                                @endif
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">
                                <span class="required">No. NPWP</span>
                                <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="No. NPWP harus valid"></i>
                            </label>
                            <div class="col-lg-8 fv-row">
                                <input type="tel" maxlength="20" id="no_npwp" name="no_npwp" class="form-control form-control-lg form-control-solid" placeholder="No. NPWP" value="{{$data->npwp_no}}" />
                                <input type="file" name="npwp" class="form-control form-control-lg form-control-solid" accept="application/pdf,image/jpeg,image/jpg,image/png" />
                                @if ($data->npwp)
                                <a class="d-block overlay" data-fslightbox="lightbox-basic" href="{{asset('storage/' .$data->npwp)}}">
                                    <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('storage/' .$data->npwp)}}')"></div>
                                    <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                        <i class="bi bi-eye-fill text-white fs-3x"></i>
                                    </div>
                                </a>
                                @endif
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">
                                <span class="required">No. NRKA / No. STRA</span>
                                <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="No. NRKA / No. STRA harus valid"></i>
                            </label>
                            <div class="col-lg-8 fv-row">
                                <input type="tel" maxlength="20" id="no_nrka" name="no_nrka" class="form-control form-control-lg form-control-solid" placeholder="No. NRKA / No. STRA" value="{{$data->nrka_no}}" />
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label  fw-bold fs-6">SKA Sub</label>
                            <div class="col-lg-8 fv-row">
                                <div class="d-flex">
                                    @foreach($sub as $item)
                                    <div class="form-check form-check-custom form-check-solid me-10">
                                        <input class="form-check-input" type="radio" value="{{$item->id}}" name="sub" id="sub" {{$data->ska_sub_id == $item->id ? 'checked' : ''}}/>
                                        <label class="form-check-label" for="sub">
                                            {{$item->name}}
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label fw-bold fs-6">
                                <span class="required">SKA</span>
                                <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="SKA harus jernih"></i>
                            </label>
                            <div class="col-lg-8 fv-row">
                                <input type="file" name="ska" class="form-control form-control-lg form-control-solid" accept="application/pdf,image/jpeg,image/jpg,image/png" />
                                <input type="text" name="tanggal_ska" id="tanggal_ska" class="form-control form-control-lg form-control-solid" placeholder="Masa Berakhir Berlaku SKA" />
                                @if ($data->ska)
                                <a class="d-block overlay" data-fslightbox="lightbox-basic" href="{{asset('storage/' .$data->ska)}}">
                                    <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('storage/' .$data->ska)}}')"></div>
                                    <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                        <i class="bi bi-eye-fill text-white fs-3x"></i>
                                    </div>
                                </a>
                                @endif
                            </div>
                        </div>
                        <div class="row mb-6 d-none">
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">SKA Kualifikasi</label>
                            <div class="col-lg-8 fv-row">
                                <select class="form-select form-select-solid" name="kualifikasi" id="kualifikasi">
                                    <option>Pilih SKA Kualifikasi</option>
                                    @foreach($kualifikasi as $item)
                                        <option value="{{$item->id}}" {{$data->ska_kualifikasi_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Alamat</label>
                            <div class="col-lg-8 fv-row">
                                <textarea name="alamat" class="form-control form-control-lg form-control-solid" placeholder="Alamat">{{$data->address}}</textarea>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Kode Pos</label>
                            <div class="col-lg-8 fv-row">
                                <input type="tel" id="kode_pos" name="kode_pos" maxlength="5" class="form-control form-control-lg form-control-solid" placeholder="Kode Pos" value="{{$data->postcode}}" />
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Provinsi</label>
                            <div class="col-lg-8 fv-row">
                                <select class="form-select form-select-solid" name="provinsi" id="provinsi">
                                    <option>Pilih Provinsi</option>
                                    @foreach($province as $item)
                                        <option value="{{$item->id}}" {{$data->province_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-6">
                            <label class="col-lg-4 col-form-label required fw-bold fs-6">Kota</label>
                            <div class="col-lg-8 fv-row">
                                <select class="form-select form-select-solid" name="kota" id="kota">
                                    <option>Harap pilih Provinsi dahulu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-end py-6 px-9">
                        <button type="button" onclick="load_list(1);" class="btn btn-light btn-active-light-primary me-2">Batalkan</button>
                        @if($data->id)
                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.anggota.update',$data->id)}}','PATCH');" class="btn btn-primary">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.anggota.store')}}','POST');" class="btn btn-primary">Simpan</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('keenthemes/plugins/custom/fslightbox/fslightbox.bundle.js')}}"></script>
<script>
    obj_date('tanggal_ska');
    number_only('no_handphone');
    number_only('no_ktp');
    number_only('no_npwp');
    number_only('no_nrka');
    npwp_format("no_npwp");
    number_only('kode_pos');
    obj_select('provinsi','Pilih Provinsi');
    obj_select('kota','Pilih Kota');
    @if($data->province_id)
    $('#provinsi').val('{{$data->province_id}}');
    setTimeout(function(){ 
        $('#provinsi').trigger('change');
        setTimeout(function(){ 
            $('#kota').val('{{$data->city_id}}');
            $('#kota').trigger('change');
        }, 1200);
    }, 500);
    @endif
    $("#provinsi").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('web.get_city')}}",
            data: {prov : $("#provinsi").val()},
            success: function(response){
                $("#kota").html(response);
            }
        });
    });
</script>