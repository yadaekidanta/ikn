@php
$end = date('2022-04-08');
$last = $last->created_at;
$total = 0;
// if($user->role == 4)
// {
//     if($data->user_id == 0){
//         $role = 'Ketua';
//     }else{
//         $role = 'Anggota';
//     }
//     if(!$data->verified_at){
//         $color = 'warning';
//         $status = 'Belum Verifikasi';
//     }else{
//         $color = 'success';
//         $status = 'Sudah Verifikasi';
//     }
// }
@endphp
<div class="card mb-5 mb-xl-5">
    <div class="card-body pt-9 pb-0">
        <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
            <div class="me-7 mb-4">
                <div class="symbol symbol-100px symbol-lg-100px symbol-fixed position-relative">
                    <img src="{{asset('logo.jpg')}}" alt="image" />
                    <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
                </div>
            </div>
            <div class="flex-grow-1">
                <div class="d-flex justify-content-between align-items-start flex-wrap">
                    <div class="d-flex flex-column">
                        <div class="d-flex align-items-center">
                            <a href="javascript:;" class="text-gray-900 text-hover-primary fs-1 fw-bolder me-1">Tim Nomor Pendaftaran {{$user->id}}</a>
                        </div>
                    </div>
                    <div class="d-flex">
                        <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Tanggal Mulai Daftar {{$user->created_at->isoFormat('dddd, DD MMMM YYYY')}}</a>
                    </div>
                </div>
                <div class="d-flex flex-wrap flex-stack">
                    <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                        <div class="d-flex justify-content-between w-100 mt-auto mb-2 d-none">
                            <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil Ketua</span>
                            <span class="fw-bolder fs-6">{{$complete}}%</span>
                        </div>
                        <div class="h-5px mx-3 w-100 bg-light d-none">
                            <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$complete}}%;" aria-valuenow="{{$complete}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="d-flex flex-wrap">
                            <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1" style="font-color:#e2304a;">Waktu terakhir Login {{\Carbon\Carbon::parse($last)->diffForHumans()}}</a>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-wrap flex-stack">
                    <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                        <div class="d-flex justify-content-between w-100 mt-auto mb-2 d-none">
                            <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil Perusahaan</span>
                            <span class="fw-bolder fs-6">{{$completep}}%</span>
                        </div>
                        <div class="h-5px mx-3 w-100 bg-light d-none">
                            <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$completep}}%;" aria-valuenow="{{$completep}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="d-flex flex-wrap">
                            <a href="javascript:;" class="text-hover-primary fs-5 fw-bolder me-1" style="color:#e2304a;">Batas Waktu Pendaftaran {{\Carbon\Carbon::parse($end)->isoFormat('dddd, DD MMMM YYYY')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_create_account_stepper">
    <div class="card d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px w-xxl-400px me-9" style="height:25%;background-color:#cddbf0;">
        <div class="card-body px-6 px-lg-10 px-xxl-15 py-20">
            <div class="stepper-nav">
                <div class="stepper-item completed" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">1</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Profil</h3>
                        <div class="stepper-desc fw-bold">Ketua Tim</div>
                    </div>
                </div>
                <div class="stepper-item completed" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">2</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Profil</h3>
                        <div class="stepper-desc fw-bold">Perusahaan</div>
                    </div>
                </div>
                <div class="stepper-item current" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">3</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Profil</h3>
                        <div class="stepper-desc fw-bold">Anggota</div>
                    </div>
                </div>
                <div class="stepper-item" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">4</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Kategori Sayembara</h3>
                        <div class="stepper-desc fw-bold">Pilih Kategori Sayembara Anda</div>
                    </div>
                </div>
                <div class="stepper-item" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">5</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Selesai</h3>
                        <div class="stepper-desc fw-bold">Kirim data Anda untuk verifikasi</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card d-flex flex-row-fluid" style="background-color:#e0e0e0;">
                <div class="card-header cursor-pointer card-header-stretch">
                    @if($data->count() < 4)
                    <div class="card-title m-0">
                        <a href="javascript:;" onclick="load_input('{{route('web.anggota.create')}}');" class="btn btn-sm btn-primary align-self-center" style="float:right;">Tambah Anggota</a>
                    </div>
                    @endif
                    {{-- <br> --}}
                    <div class="card-toolbar">
                        {{-- <ul class="nav nav-tabs nav-line-tabs nav-stretch fs-6 border-0">
                            @foreach ($data as $key => $item)
                            <li class="nav-item">
                                <a class="nav-link {{$key == 0 ? 'active' : ''}}" data-bs-toggle="tab" style="color:#e2304a" href="#anggota_{{$item->id}}">Anggota {{++$key}}</a>
                            </li>
                            @endforeach
                        </ul> --}}
                    </div>
                </div>
                <div class="card-body py-20 w-100 w-xl-700px px-9 flex-center">
                    <div class="current" data-kt-stepper-element="content">
                        <div class="fv-row mb-5">
                            <div class="col-lg-12">
                                <h1 class="d-flex text-dark fw-bolder fs-5 align-items-center" style="margin-top:-10%;">Anggota Wajib SKA</h1>
                                <div class="table-responsive">
                                    <table class="table align-middle table-row-dashed fs-6 gy-5">
                                        <tbody class="fw-bold text-gray-600">
                                            @foreach ($data as $key => $item)
                                            @php
                                            if($key < 4){
                                                $total += $item->calculate_anggota($item);
                                            }
                                            @endphp
                                            @if($key < 4)
                                            <tr>
                                                <td>
                                                    {{ $item->ska_kualifikasi_id > 0 ? $item->qualification->name : '-' }}
                                                </td>
                                                <td>
                                                    {{ $item->name }}
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
                                                        <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                                            <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil</span>
                                                            <span class="fw-bolder fs-6">{{$item->calculate_anggota($item)}}%</span>
                                                        </div>
                                                        <div class="h-5px mx-3 w-100 bg-light mb-3">
                                                            <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$item->calculate_anggota($item)}}%;" aria-valuenow="{{$item->calculate_anggota($item)}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-end">
                                                    <a href="javascript:;" onclick="load_input('{{route('web.anggota.edit',$item->id)}}');" class="btn btn-sm btn-info">Lengkapi / Ubah Profil</a>
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card d-flex flex-row-fluid" style="background-color:#e0e0e0;">
                <div class="card-header cursor-pointer card-header-stretch">
                    @if($data->count() > 3 && $data->count() < 9)
                    <div class="card-title m-0">
                        <a href="javascript:;" onclick="load_input('{{route('web.anggota.create')}}');" class="btn btn-sm btn-primary align-self-center" style="float:right;">Tambah Anggota</a>
                    </div>
                    @endif
                    {{-- <br> --}}
                    <div class="card-toolbar">
                        {{-- <ul class="nav nav-tabs nav-line-tabs nav-stretch fs-6 border-0">
                            @foreach ($data as $key => $item)
                            <li class="nav-item">
                                <a class="nav-link {{$key == 0 ? 'active' : ''}}" data-bs-toggle="tab" style="color:#e2304a" href="#anggota_{{$item->id}}">Anggota {{++$key}}</a>
                            </li>
                            @endforeach
                        </ul> --}}
                    </div>
                </div>
                <div class="card-body py-20 w-100 w-xl-700px px-9 flex-center">
                    <div class="current" data-kt-stepper-element="content">
                        <div class="fv-row mb-5">
                            <div class="col-lg-12">
                                <h1 class="d-flex text-dark fw-bolder fs-5 align-items-center" style="margin-top:-10%;">Anggota Tidak Wajib SKA</h1>
                                <div class="table-responsive">
                                    <table class="table align-middle table-row-dashed fs-6 gy-5">
                                        <tbody class="fw-bold text-gray-600">
                                            @foreach ($data as $key => $item)
                                            @if($key > 3)
                                            <tr>
                                                <td>
                                                    {{ $item->ska_kualifikasi_id > 0 ? $item->qualification->name : '-' }}
                                                </td>
                                                <td>
                                                    {{ $item->name }}
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
                                                        <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                                            <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil</span>
                                                            <span class="fw-bolder fs-6">{{$item->calculate_anggota_non($item)}}%</span>
                                                        </div>
                                                        <div class="h-5px mx-3 w-100 bg-light mb-3">
                                                            <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$item->calculate_anggota_non($item)}}%;" aria-valuenow="{{$item->calculate_anggota_non($item)}}" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-end">
                                                    <a href="javascript:;" onclick="load_input('{{route('web.anggota.edit',$item->id)}}');" class="btn btn-sm btn-info">Lengkapi / Ubah Profil</a>
                                                </td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <a href="{{route('web.perusahaan.index')}}" class="btn btn-danger me-10">
                        <span class="svg-icon svg-icon-4 me-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="black" />
                                <path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="black" />
                            </svg>
                        </span>
                        Kembali
                    </a>
                    @if($data->count() > 3)
                    @if($total >= 400)
                    <a href="{{route('web.kategori-sayembara.index')}}" class="btn btn-primary">
                        Selanjutnya
                        <span class="svg-icon svg-icon-4 ms-1 me-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
                                <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
                            </svg>
                        </span>
                    </a>
                    @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    number_only('no_npwp');
    npwp_format("no_npwp");
</script>
<script src="{{asset('keenthemes/plugins/custom/fslightbox/fslightbox.bundle.js')}}"></script>