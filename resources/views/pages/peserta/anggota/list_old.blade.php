@php
$end = date('2022-03-28');
$last = $last->created_at;
if($data->role == 4)
{
    if($data->user_id == 0){
        $role = 'Ketua';
    }else{
        $role = 'Anggota';
    }
    if(!$data->verified_at){
        $color = 'warning';
        $status = 'Belum Verifikasi';
    }else{
        $color = 'success';
        $status = 'Sudah Verifikasi';
    }
}
@endphp
<div class="card mb-5 mb-xl-5">
    <div class="card-body pt-9 pb-0">
        <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
            <div class="me-7 mb-4">
                <div class="symbol symbol-100px symbol-lg-100px symbol-fixed position-relative">
                    <img src="{{asset('logo.jpg')}}" alt="image" />
                    <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
                </div>
            </div>
            <div class="flex-grow-1">
                <div class="d-flex justify-content-between align-items-start flex-wrap">
                    <div class="d-flex flex-column">
                        <div class="d-flex align-items-center">
                            <a href="javascript:;" class="text-gray-900 text-hover-primary fs-1 fw-bolder me-1">Tim No Urut {{$data->id}}</a>
                        </div>
                    </div>
                    <div class="d-flex">
                        <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Tanggal Daftar {{$data->created_at->isoFormat('dddd, DD MMMM YYYY')}}</a>
                    </div>
                </div>
                <div class="d-flex flex-wrap flex-stack">
                    <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                    </div>
                    @if($data->is_unduh == 0)
                    <div class="d-flex">
                        <div class="d-flex flex-wrap">
                            <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download')}}');" id="tombol_download" class="btn btn-flex btn-danger px-6">
                                <span class="svg-icon svg-icon-2x">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"/>
                                            <path d="M5.74714567,13.0425758 C4.09410362,11.9740356 3,10.1147886 3,8 C3,4.6862915 5.6862915,2 9,2 C11.7957591,2 14.1449096,3.91215918 14.8109738,6.5 L17.25,6.5 C19.3210678,6.5 21,8.17893219 21,10.25 C21,12.3210678 19.3210678,14 17.25,14 L8.25,14 C7.28817895,14 6.41093178,13.6378962 5.74714567,13.0425758 Z" fill="#000000" opacity="0.3"/>
                                            <path d="M11.1288761,15.7336977 L11.1288761,17.6901712 L9.12120481,17.6901712 C8.84506244,17.6901712 8.62120481,17.9140288 8.62120481,18.1901712 L8.62120481,19.2134699 C8.62120481,19.4896123 8.84506244,19.7134699 9.12120481,19.7134699 L11.1288761,19.7134699 L11.1288761,21.6699434 C11.1288761,21.9460858 11.3527337,22.1699434 11.6288761,22.1699434 C11.7471877,22.1699434 11.8616664,22.1279896 11.951961,22.0515402 L15.4576222,19.0834174 C15.6683723,18.9049825 15.6945689,18.5894857 15.5161341,18.3787356 C15.4982803,18.3576485 15.4787093,18.3380775 15.4576222,18.3202237 L11.951961,15.3521009 C11.7412109,15.173666 11.4257142,15.1998627 11.2472793,15.4106128 C11.1708299,15.5009075 11.1288761,15.6153861 11.1288761,15.7336977 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.959697, 18.661508) rotate(-270.000000) translate(-11.959697, -18.661508) "/>
                                        </g>
                                    </svg>
                                </span>
                                <span class="d-flex flex-column align-items-start ms-2">
                                    <span class="fs-3 fw-bolder">Unduh</span>
                                    <span class="fs-7">File KAK</span>
                                </span>
                            </a>
                        </div>
                    </div>
                    @endif
                    <div class="d-flex">
                        <div class="d-flex flex-wrap">
                            <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Last Login {{\Carbon\Carbon::parse($last)->diffForHumans()}}</a>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-wrap flex-stack">
                    <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                        <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                            <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil</span>
                            <span class="fw-bolder fs-6">{{$complete}}%</span>
                        </div>
                        <div class="h-5px mx-3 w-100 bg-light">
                            <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$complete}}%;" aria-valuenow="{{$complete}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="d-flex flex-wrap">
                            <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Sisa Waktu {{\Carbon\Carbon::parse($end)->diffForHumans()}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header cursor-pointer card-header-stretch">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">Anggota Tim</h3>
        </div>
        @if($collection->count() > 1)
        <div class="card-toolbar">
            <ul class="nav nav-tabs nav-line-tabs nav-stretch fs-6 border-0">
                @foreach ($collection as $key => $item)
                <li class="nav-item">
                    <a class="nav-link {{$key == 0 ? 'active' : ''}}" data-bs-toggle="tab" href="#anggota_{{$item->id}}">Anggota {{++$key}}</a>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
        @if($data->is_unduh == 1)
            @if($collection->count() < 10)
            <a href="javascript:;" onclick="load_input('{{route('web.anggota.create')}}');" class="btn btn-sm btn-primary align-self-center">Tambah Anggota</a>
            @endif
        @endif
    </div>
</div>
<div class="card mb-5 mb-xl-5">
    <div class="card-body p-9">
        <div class="tab-content" id="tab_anggota">
            @foreach ($collection as $key => $item)
            <div class="tab-pane fade {{$key == 0 ? 'show active' : ''}}" id="anggota_{{$item->id}}" role="tabpanel">
                <div class="row mb-7">
                    <div class="col-lg-6">
                        <div class="row mb-3">
                            <label class="col-lg-4 fw-bold">Nama Lengkap</label>
                            <div class="col-lg-8">
                                <span class="fw-bolder fs-6 text-gray-800">{{$item->name}}</span>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-lg-4 fw-bold">No. Handphone</label>
                            <div class="col-lg-8 fv-row">
                                <span class="fw-bold text-gray-800 fs-6">{{$item->phone}}</span>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-lg-4 fw-bold">Email</label>
                            <div class="col-lg-8 fv-row">
                                <span class="fw-bold text-gray-800 fs-6">{{$item->email}}</span>
                            </div>
                        </div>
                        <div class="row d-none">
                            <label class="col-lg-4 fw-bold">Email
                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Email terverifikasi"></i></label>
                            <div class="col-lg-8 d-flex align-items-center">
                                <span class="fw-bolder fs-6 text-gray-800 me-2"></span>
                                <span class="badge badge-success">Terverifikasi</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="d-flex align-items-center w-200px w-sm-300px flex-column mt-3">
                                    <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                        <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil</span>
                                        <span class="fw-bolder fs-6">{{$item->calculate_anggota($item)}}%</span>
                                    </div>
                                    <div class="h-5px mx-3 w-100 bg-light mb-3">
                                        <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$item->calculate_anggota($item)}}%;" aria-valuenow="{{$item->calculate_anggota($item)}}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <a href="javascript:;">
                                    @if($item->verified_at)
                                    <span class="svg-icon svg-icon-1 svg-icon-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                            <path d="M10.0813 3.7242C10.8849 2.16438 13.1151 2.16438 13.9187 3.7242V3.7242C14.4016 4.66147 15.4909 5.1127 16.4951 4.79139V4.79139C18.1663 4.25668 19.7433 5.83365 19.2086 7.50485V7.50485C18.8873 8.50905 19.3385 9.59842 20.2758 10.0813V10.0813C21.8356 10.8849 21.8356 13.1151 20.2758 13.9187V13.9187C19.3385 14.4016 18.8873 15.491 19.2086 16.4951V16.4951C19.7433 18.1663 18.1663 19.7433 16.4951 19.2086V19.2086C15.491 18.8873 14.4016 19.3385 13.9187 20.2758V20.2758C13.1151 21.8356 10.8849 21.8356 10.0813 20.2758V20.2758C9.59842 19.3385 8.50905 18.8873 7.50485 19.2086V19.2086C5.83365 19.7433 4.25668 18.1663 4.79139 16.4951V16.4951C5.1127 15.491 4.66147 14.4016 3.7242 13.9187V13.9187C2.16438 13.1151 2.16438 10.8849 3.7242 10.0813V10.0813C4.66147 9.59842 5.1127 8.50905 4.79139 7.50485V7.50485C4.25668 5.83365 5.83365 4.25668 7.50485 4.79139V4.79139C8.50905 5.1127 9.59842 4.66147 10.0813 3.7242V3.7242Z" fill="#00A3FF" />
                                            <path class="permanent" d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z" fill="white" />
                                        </svg>
                                    </span>
                                    @else
                                    <span class="svg-icon svg-icon-1 svg-icon-warning">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#ffc700">
                                                    <rect x="0" y="7" width="16" height="2" rx="1"/>
                                                    <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"/>
                                                </g>
                                                <path class="permanent" d="M14.8563 9.1903C15.0606 8.94984 15.3771 8.9385 15.6175 9.14289C15.858 9.34728 15.8229 9.66433 15.6185 9.9048L11.863 14.6558C11.6554 14.9001 11.2876 14.9258 11.048 14.7128L8.47656 12.4271C8.24068 12.2174 8.21944 11.8563 8.42911 11.6204C8.63877 11.3845 8.99996 11.3633 9.23583 11.5729L11.3706 13.4705L14.8563 9.1903Z" fill="white" />
                                            </g>
                                        </svg>
                                    </span>
                                    @endif
                                </a>
                                <a href="javascript:;" class="btn btn-sm btn-light-{{$color}} fw-bolder ms-2 fs-8 py-1 px-3">{{$status}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="row">
                            @if($data->is_unduh == 1)
                            <div class="col-12 mb-3">
                                <a href="javascript:;" onclick="load_input('{{route('web.anggota.edit',$item->id)}}');" class="btn btn-sm btn-outline btn-outline-dashed btn-outline-warning btn-active-light-warning">Lengkapi / Ubah Profil</a>
                            </div>
                            <div class="col-12">
                                <a href="javascript:;" onclick="load_input('{{route('web.anggota.show',$item->id)}}');" class="btn btn-sm btn-outline btn-outline-dashed btn-outline-primary btn-active-light-primary">Lihat</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed p-6 d-none">
            <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                    <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                    <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                </svg>
            </span>
            <div class="d-flex flex-stack flex-grow-1">
                <div class="fw-bold">
                    <h4 class="text-gray-900 fw-bolder">Perhatian!</h4>
                    <div class="fs-6">Harap masukkan list anggota &amp; dokumen Anda dan tim.
                    <a class="fw-bolder" href="javascript:;">Tambah Dokumen</a>.</div>
                </div>
            </div>
        </div>
    </div>
</div>