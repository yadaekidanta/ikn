@php
    $end = date('2022-04-08');
    $last = $last->created_at;
    $title = "\'Apakah Anda Yakin ? <br> Proses ini tidak akan bisa dikembalikan\'";
    $confirm_title = "\'Iya\'";
    $deny_title = "\'Batal\'";
    $method = "\'POST\'";

    $fdate = date('Y-m-d');
    $tdate = date('2022-04-16');
    $datetime1 = new DateTime($fdate);
    $datetime2 = new DateTime($tdate);
    $interval = $datetime1->diff($datetime2);
    $days = $interval->format('%a');
@endphp
@if($days < 0)
    <style type="text/css">
        ul.ck_image {
    list-style-type: none;
    }

    ul.ck_image li {
    display: inline-block;
    width: 24%;
    }

    ul.ck_image input[type="checkbox"][id^="cb"] {
    display: none;
    }

    ul.ck_image label {
    border: 1px solid #fff;
    padding: 10px;
    display: block;
    position: relative;
    margin: 10px;
    cursor: pointer;
    }

    ul.ck_image label:before {
    /*background-color: white;*/
    color: white;
    /*content: " ";*/
    display: block;
    border-radius: 50%;
    border: 1px solid grey;
    position: absolute;
    top: -5px;
    left: -5px;
    width: 25px;
    height: 25px;
    text-align: center;
    line-height: 28px;
    transition-duration: 0.4s;
    /*transform: scale(0);*/
    }

    ul.ck_image label img {
    /*height: 100px;*/
    width: 100%;
    transition-duration: 0.2s;
    transform-origin: 50% 50%;
    }

    :checked + label {
    border-color: #ddd;
    }

    :checked + label:before {
    content: "✓";
    background-color: grey;
    transform: scale(1);
    }

    :checked + label img {
    transform: scale(0.9);
    box-shadow: 0 0 5px #333;
    z-index: -1;
    }
    table tr td{
        border: 2px solid black;
    }
    </style>
    <div class="card mb-5 mb-xl-5">
        <div class="card-body pt-9 pb-0">
            <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                <div class="me-7 mb-4">
                    <div class="symbol symbol-100px symbol-lg-100px symbol-fixed position-relative">
                        <img src="{{asset('logo.jpg')}}" alt="image" />
                        <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
                    </div>
                </div>
                <div class="flex-grow-1">
                    <div class="d-flex justify-content-between align-items-start flex-wrap">
                        <div class="d-flex flex-column">
                            <div class="d-flex align-items-center">
                                <a href="javascript:;" class="text-gray-900 text-hover-primary fs-1 fw-bolder me-1">Tim Nomor Pendaftaran {{$user->id}}</a>
                            </div>
                        </div>
                        <div class="d-flex">
                            <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Tanggal Mulai Daftar {{$user->created_at->isoFormat('dddd, DD MMMM YYYY')}}</a>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap flex-stack">
                        <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                        </div>
                        <div class="d-flex">
                            <div class="d-flex flex-wrap">
                                <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1" style="font-color:#e2304a;">Waktu terakhir Login {{\Carbon\Carbon::parse($last)->diffForHumans()}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap flex-stack">
                        <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                            <div class="d-flex justify-content-between w-100 mt-auto mb-2 d-none">
                                <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil</span>
                                <span class="fw-bolder fs-6">{{$complete}}%</span>
                            </div>
                            <div class="h-5px mx-3 w-100 bg-light d-none">
                                <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$complete}}%;" aria-valuenow="{{$complete}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="d-flex flex-wrap">
                                <a href="javascript:;" class="text-hover-primary fs-5 fw-bolder me-1" style="color:#e2304a;">Batas Waktu Pendaftaran {{\Carbon\Carbon::parse($end)->isoFormat('dddd, DD MMMM YYYY')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_create_account_stepper">
        <div class="card d-flex flex-row-fluid" style="background-color:#e0e0e0;">
            <form id="form_input" action="javascript:;">
                <div class="card-body py-20 w-100 px-9 flex-center ">
                    <div class="current" data-kt-stepper-element="content">
                        <div class="fv-row" style="width: 100%">
                            <div class="col-lg-12">
                                <div class="row">

                                    <p style="text-align: left;">
                                        Lulus
                                    </p>
                                    <div class="col-md-12">
                                        <p>Hore</p>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Terimakasih</p>
                                    </div>
                                
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            
            </form>
        </div>
    </div>
@else
    <style type="text/css">
        ul.ck_image {
    list-style-type: none;
    }

    ul.ck_image li {
    display: inline-block;
    width: 24%;
    }

    ul.ck_image input[type="checkbox"][id^="cb"] {
    display: none;
    }

    ul.ck_image label {
    border: 1px solid #fff;
    padding: 10px;
    display: block;
    position: relative;
    margin: 10px;
    cursor: pointer;
    }

    ul.ck_image label:before {
    /*background-color: white;*/
    color: white;
    /*content: " ";*/
    display: block;
    border-radius: 50%;
    border: 1px solid grey;
    position: absolute;
    top: -5px;
    left: -5px;
    width: 25px;
    height: 25px;
    text-align: center;
    line-height: 28px;
    transition-duration: 0.4s;
    /*transform: scale(0);*/
    }

    ul.ck_image label img {
    /*height: 100px;*/
    width: 100%;
    transition-duration: 0.2s;
    transform-origin: 50% 50%;
    }

    :checked + label {
    border-color: #ddd;
    }

    :checked + label:before {
    content: "✓";
    background-color: grey;
    transform: scale(1);
    }

    :checked + label img {
    transform: scale(0.9);
    box-shadow: 0 0 5px #333;
    z-index: -1;
    }
    table tr td{
        border: 2px solid black;
    }
    </style>
    @if (@$with_header)
        <div class="card mb-5 mb-xl-5">
            <div class="card-body pt-9 pb-0">
                <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                    <div class="me-7 mb-4">
                        <div class="symbol symbol-100px symbol-lg-100px symbol-fixed position-relative">
                            <img src="{{asset('logo.jpg')}}" alt="image" />
                            <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
                        </div>
                    </div>
                    <div class="flex-grow-1">
                        <div class="d-flex justify-content-between align-items-start flex-wrap">
                            <div class="d-flex flex-column">
                                <div class="d-flex align-items-center">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-1 fw-bolder me-1">Tim Nomor Pendaftaran {{$user->id}}</a>
                                </div>
                            </div>
                            <div class="d-flex">
                                <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Tanggal Mulai Daftar {{$user->created_at->isoFormat('dddd, DD MMMM YYYY')}}</a>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap flex-stack">
                            <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                            </div>
                            <div class="d-flex">
                                <div class="d-flex flex-wrap">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1" style="font-color:#e2304a;">Waktu terakhir Login {{\Carbon\Carbon::parse($last)->diffForHumans()}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap flex-stack">
                            <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                                <div class="d-flex justify-content-between w-100 mt-auto mb-2 d-none">
                                    <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil</span>
                                    <span class="fw-bolder fs-6">{{$complete}}%</span>
                                </div>
                                <div class="h-5px mx-3 w-100 bg-light d-none">
                                    <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$complete}}%;" aria-valuenow="{{$complete}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="d-flex flex-wrap">
                                    <a href="javascript:;" class="text-hover-primary fs-5 fw-bolder me-1" style="color:#e2304a;">Batas Waktu Pendaftaran {{\Carbon\Carbon::parse($end)->isoFormat('dddd, DD MMMM YYYY')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    

    <div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_create_account_stepper">
        
        
        {{-- <div class="card d-flex flex-row-fluid" style="background: #ffc107;"> --}}
        <div class="card d-flex flex-row-fluid">
            <form id="form_input" action="javascript:;">
                <div class="card-body py-20 w-100 px-9 flex-center ">
                    <div class="current" data-kt-stepper-element="content">
                        <div class="fv-row" style="width: 100%">

                            @if (!@$with_header)
                                <div class="floating-title">
                                    <h2>Ringkasan Pendaftaran</h2>
                                </div>
                                <div class="d-flex flex-column text-end mb-10">
                                    <span class="text-dark fw-bolder fs-2 fst-italic" style="margin-top: -3em;">8 April 2022</span>
                                </div>
                            @endif

                            <div class="col-lg-12">
                                <div class="row">
                                    
                                    <p class="fs-3" style="text-align: left;">
                                        Saat ini Anda tercatat terdaftar sebagai peserta pada Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara,
                                        Dengan rincian sebagai berikut : 
                                    </p>
                                    <div class="col-md-12">
                                        <table class="table table-striped gy-7 gs-7">
                                            <tbody>
                                                <tr>
                                                    <td class="col-md-3 bold fw-bolder fs-3">Ketua</td>
                                                    <td style="width: 15px;">:</td>
                                                    <td class="fw-bolder fs-3">{{$user->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3 bold fw-bolder fs-3">Badan Usaha Pendukung</td>
                                                    <td style="width: 15px;">:</td>
                                                    <td class="fw-bolder fs-3">{{@$data_perusahaan[0]['name']}}</td>
                                                </tr>
                                                <tr style="vertical-align: baseline;">
                                                    <td class="col-md-3 bold fw-bolder fs-3">Kelompok</td>
                                                    <td style="width: 15px;">:</td>
                                                    <td class="fw-bolder fs-3" >
                                                        <ol>
                                                        @foreach ($anggota_group as $anggota)
                                                            <li style="padding-left: 10px;"><span>{{ $anggota->name }}</span></li>
                                                        @endforeach

                                                        </ol>
                                                    </td>
                                                </tr>
                                                <tr style="vertical-align: baseline;">
                                                    <td class="col-md-3 bold fw-bolder fs-3">Pilihan Sayembara</td>
                                                    <td style="width: 15px;">:</td>
                                                    <td class="fw-bolder fs-3" >
                                                        <ol>
                                                            @foreach($sayembara_pilihan as $sayembara_list)
                                                                <li style="padding-left: 10px;"><span>{{$sayembara_list}}</span></li>
                                                            @endforeach
                                                        </ol>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <br>
                                        <p class="fs-3">Data yang telah Anda masukkan, telah tersimpan pada sistem. Kami akan tindak lanjuti untuk proses verifikasi administrasi</p>
                                    </div>
                                    <div class="col-md-12">
                                        <p class="fs-3">Terimakasih</p>
                                    </div>
                                
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
            
            </form>
        </div>
    </div>
@endif