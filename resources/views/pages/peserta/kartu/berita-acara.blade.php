<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="floating-title">
                    <h2>BERITA ACARA AANWIJZING DAN ADDENDUM DOKUMEN SAYEMBARA</h2>
                </div>
                <div class="d-flex flex-column text-end mb-10">
                    <span class="text-dark fw-bolder fs-2 fst-italic" style="margin-top: -1em;">28 April 2022</span>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 aanwizjing-berita mt-9">
                            <h3>BERITA ACARA AANWIJZING</h3>
                            <p class="start-up-message" style="text-align: justify">
                                Silakan unduh berita acara aanwijzing disini 
                            </p>
                            <div class="col-md-12 text-center mt-10">
                                <a href="{{ URL::signedRoute('web.StepUp.download_berita',['jenis'=>'berita'])}}" class="btn btn-lg btn-primary" >Unduh</a>
                            </div>
                        </div>
                       

                        <div class="col-lg-12 aanwijzing-dok-sayembara mt-10">
                            <h3>ADDENDUM DOKUMEN SAYEMBARA</h3>
                            <p class="start-up-message" style="text-align: justify">
                                Silakan unduh addendum dokumen sayembara di sini
                            </p>
                            <div class="row">
                                <div class="col-lg-3">
                                    <a href="{{ URL::signedRoute('web.StepUp.download_berita',['jenis'=>'wapres'])}}" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                                        <img src="{{asset('image/Istana-Wakil-Presiden.png')}}" class="w-100 img-sayembara" alt="">
                                        <h3 class="title-sayembara">
                                            Unduh Addendum<br>
                                            Dokumen Sayembara <br>
                                            Kompleks <br>
                                            Istana Wakil Presiden
            
                                        </h3>
                                    </a>
                                </div>
                                <div class="col-lg-3">
                                    <a href="{{ URL::signedRoute('web.StepUp.download_berita',['jenis'=>'legislatif'])}}"  class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                                        <img src="{{asset('image/Legislatif.png')}}" class="w-100 img-sayembara" alt="">
                                        <h3 class="title-sayembara">
                                            Unduh Addendum <br>
                                            Dokumen Sayembara <br>
                                            Kompleks <br>
                                            Perkantoran Legislatif
                                        </h3>
                                    </a>
                                </div>
                                <div class="col-lg-3">
                                    <a href="{{ URL::signedRoute('web.StepUp.download_berita',['jenis'=>'yudikatif'])}}"  class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                                        <img src="{{asset('image/Yudikatif.png')}}" class="w-100 img-sayembara" alt="">
                                        <h3 class="title-sayembara">
                                            Unduh Addendum <br>
                                            Dokumen Sayembara <br>
                                            Kompleks <br>
                                            Perkantoran Yudikatif 
                                        </h3>
                                    </a>
                                </div>
                                <div class="col-lg-3">
                                    <a href="{{ URL::signedRoute('web.StepUp.download_berita',['jenis'=>'peribadatan'])}}" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                                        <img src="{{asset('image/Peribadatan.png')}}" class="w-100 img-sayembara" alt="">
                                        <h3 class="title-sayembara peribadatan">
                                            Unduh Addendum <br>
                                            Dokumen Sayembara <br>
                                            Kompleks Peribadatan
                                        </h3>
                                    </a>
                                </div>
                                {{-- @if (date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime('2022-03-30 23:59:58')))
                                    <div class="col-lg-12 mt-5">
                                        <div class="text-center">
                                            <a href="{{route('web.profile.index')}}" class="btn btn-lg btn-primary fw-bolder">Daftar / Ubah</a>
                                        </div>
                                    </div>
                                @endif --}}
            
                            </div>
                        </div>
                    </div>
                

                </div>
            </div>
            
        </div>
    </div>
</div>
<br>
<br>