<style type="text/css">
    .img-sayembara{
        padding: 0 12%;
    }
    .title-sayembara{
        text-align: center;
        padding: 1em 10px;
        font-size: 18px;
    }
    .title-sayembara.peribadatan{
        padding-bottom: 2.2em;
    }
    .start-up-message{
        font-size: 1.3em;
    }
    .floating-title{
        position: absolute;
        top: -16px;
        padding: 5px 15px;
        border: 1px solid #ccc;
        background: white;
        border-radius: 5px;
    }
    .floating-title h2{
        margin: 0;
    }
    .button-aanwijzing{
        padding: 5px 10%;
    }
</style>

<div class="card">
    <div class="card-body">
        <div class="fw-bold fs-3 text-muted mb-2">Selamat datang, Pendaftar {{Auth::guard('web')->user()->id}}</div>
        <h1 class="fw-bolder fs-1qx text-gray-800">Sayembara Konsep Perancangan Kawasan dan Bangunan Gedung di Ibu Kota Nusantara</h1>
        <br>
        <h1>Waktu pemasukkan karya tinggal <span id="waktu" style="color:red;"></span></h1>
        <h2>Klik ini untuk menggunggah karya.
            <a href="{{route('web.karya.index')}}">Link</a>
        </h2>
    </div>
</div>
<br>
<br>
<div class="card mb-10">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="floating-title">
                    <h2>Format Kop Panel Karya</h2>
                </div>
                <div class="d-flex flex-column text-end mb-10">
                    <span class="text-dark fw-bolder fs-2 fst-italic" style="margin-top: -1em;">19 Mei 2022</span>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-3">
                                <a href="{{asset('panel/iswapres.pdf')}}" target="_blank" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                                    <img src="{{asset('image/Istana-Wakil-Presiden.png')}}" class="w-100 img-sayembara" alt="">
                                    <h3 class="title-sayembara">
                                        Unduh <br>
                                        Format Panel <br>
                                        Kompleks <br>
                                        Istana Wakil Presiden
        
                                    </h3>
                                </a>
                            </div>
                            <div class="col-lg-3">
                                <a href="{{asset('panel/legislatif.pdf')}}" target="_blank" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                                    <img src="{{asset('image/Legislatif.png')}}" class="w-100 img-sayembara" alt="">
                                    <h3 class="title-sayembara">
                                        Unduh <br>
                                        Format Panel <br>
                                        Kompleks <br>
                                        Perkantoran Legislatif
                                    </h3>
                                </a>
                            </div>
                            <div class="col-lg-3">
                                <a href="{{asset('panel/yudikatif.pdf')}}" target="_blank" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                                    <img src="{{asset('image/Yudikatif.png')}}" class="w-100 img-sayembara" alt="">
                                    <h3 class="title-sayembara">
                                        Unduh <br>
                                        Format Panel <br>
                                        Kompleks <br>
                                        Perkantoran Yudikatif 
                                    </h3>
                                </a>
                            </div>
                            <div class="col-lg-3">
                                <a href="{{asset('panel/peribadatan.pdf')}}" target="_blank" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                                    <img src="{{asset('image/Peribadatan.png')}}" class="w-100 img-sayembara" alt="">
                                    <h3 class="title-sayembara peribadatan">
                                        Unduh <br>
                                        Format Panel <br>
                                        Kompleks Peribadatan
                                    </h3>
                                </a>
                            </div>
                            {{-- @if (date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime('2022-03-30 23:59:58')))
                                <div class="col-lg-12 mt-5">
                                    <div class="text-center">
                                        <a href="{{route('web.profile.index')}}" class="btn btn-lg btn-primary fw-bolder">Daftar / Ubah</a>
                                    </div>
                                </div>
                            @endif --}}
        
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

{{-- Start Hanya bisa dilihat untuk yang lulus verifikasi --}}
    {!! $video_lapangan !!}
{{-- End Hanya bisa dilihat untuk yang lulus verifikasi --}}

{{-- Start Hanya bisa dilihat untuk yang lulus verifikasi --}}
    {!! $berita_acara !!}
{{-- End Hanya bisa dilihat untuk yang lulus verifikasi --}}

{{-- Start Hanya bisa dilihat untuk yang lulus verifikasi --}}
    {!! $materi_aanwijzing !!}
{{-- End Hanya bisa dilihat untuk yang lulus verifikasi --}}

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="floating-title">
                    <h2>Penyampaian Hasil Verifikasi</h2>
                </div>
                <div class="d-flex flex-column text-end mb-10">
                    <span class="text-dark fw-bolder fs-2 fst-italic" style="margin-top: -1em;">18 April 2022</span>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-12">
                        <p class="start-up-message">
                            Peserta dengan nomor Tim <b>{{$user_detail->id}}</b> dinyatakan <b>“{{(@$user_detail->verifikator->st) ? $user_detail->verifikator->st : 'Tidak Lulus Verifikasi'}}”</b>. <br>
                            
                        </p>
                        @if (strtolower(@$user_detail->verifikator->st) != 'lulus verifikasi')
                            <p class="start-up-message">Terimakasih atas partisipasinya dan sudah menjadi bagian dalam sayembara untuk Negeri Indonesia</p>
                        @endif
                        {{-- apabila lulus verifikasi bisa download --}}
                            @if (strtolower(@$user_detail->verifikator->st) == 'lulus verifikasi')
                                <p class="start-up-message">Silahkan unduh undangan aanwijzing.</p>
                                <div class="col-md-12 ">
                                    <div class="button-aanwijzing">
                                        <div class="row">
                                            <a href="{{asset('dokumen/Aanwijzing.pdf')}}" class="btn btn-lg btn-primary fw-bolder col-md-12">Undangan Aanwijzing Online</a>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <a href="{{asset('dokumen/Aanwijzing-lapangan.pdf')}}" class="btn btn-lg btn-primary fw-bolder col-md-12">Undangan Aanwijzing Lapangan</a>
                                        </div>
                                        
                                    </div>
                                    <br>
                                    <div class="table-responsive mt-5">
                                        <p class="start-up-message">
                                            Anda dapat mengunduh kartu peserta dan data dukung (dalam format Autocad) sebagai bahan pembuatan karya.
                                        </p>
                                        
                                        
                                        <table class="table table-striped gy-7 gs-7">
                                            <thead>
                                                <tr class="fw-bold fs-6 text-gray-800 border-bottom border-gray-200">
                                                    @php
                                                        $array_jenis_sayembara = json_decode($user_detail->jenis_sayembara);
                                                    @endphp
                                                    <th></th>
                                                    @foreach ( $array_jenis_sayembara as $jns_sayembara)
                                                        <th class="text-center fs-3">{{$kategori_list[$jns_sayembara - 1]}}</th>
                                                    @endforeach
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="fw-bold fs-3">Kartu Peserta</td>
                                                    @foreach ( $array_jenis_sayembara as $jns_sayembara)    
                                                        <td class="text-center">
                                                            <a href="{{ route('web.kartu.kartu_peserta', ['sayembara_name' => $link_download_kartu[$jns_sayembara]] ) }}" class="btn btn-lg btn-primary fw-bolder">Unduh</a>
                                                        </td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td class="fw-bold fs-3">Data Dukung</td>
                                                    @foreach ( $array_jenis_sayembara as $jns_sayembara)    
                                                        <td class="text-center"> 
                                                            <a href="{{ asset('dokumen/data_dukung/'.$data_dukung[$jns_sayembara]) }}" class="btn btn-lg btn-primary fw-bolder">Unduh</a>
                                                        </td>
                                                    @endforeach
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif
                        {{-- apabila lulus verifikasi bisa download --}}

                    </div>
                  

                </div>
            </div>
            
        </div>
    </div>
</div>
<script>
    
</script>
<br>
<br>
{!! $view_info !!}

<br>
<br>
{!! $kak_peserta !!}