<x-office-layout title="Selamat Datang">
    <div id="content_list">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Dashboard</h1>
                    <span class="h-20px border-gray-300 border-start mx-4 d-none"></span>
                    <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1 d-none">
                        <li class="breadcrumb-item text-muted">
                            <a href="../../demo1/dist/index.html" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-300 w-5px h-2px"></span>
                        </li>
                        <li class="breadcrumb-item text-muted">Utilities</li>
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-300 w-5px h-2px"></span>
                        </li>
                        <li class="breadcrumb-item text-muted">Wizards</li>
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-300 w-5px h-2px"></span>
                        </li>
                        <li class="breadcrumb-item text-dark">Vertical</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div id="list_result"></div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @if ($user_detail->verifikator->agree == 1)
        <div class="modal fade" tabindex="-1" id="document_modal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="title-up" style="flex-direction: column;">
                            <h5 class="modal-title fs-2"> File Pendukung Tambahan</h5>
                        </div>
                        
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="body_content">
                        File data pendukung tambahan hanya dapat diunduh satu kali, apakah anda yakin untuk menerima data pendukung tambahan?

                        <div class="group-dw-btn text-center mt-9">
                            <button class="btn btn-primary fw-bolder col-md-2 me-4" id="download_once">Ya</button>
                            <button class="btn btn-danger fw-bolder col-md-2" data-bs-dismiss="modal">Tidak</button>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    
    @section('custom_js')
        <script>
            load_list(1);
            var link = null; 
            $(document).ready(function(){ 
                $('#list_result').on('click','#check_agreement', function(){
                    if($(this).is(':checked') && link === null){
                        $('#btn_dowload').removeAttr('disabled');
                        return;
                    }

                    $('#btn_dowload').attr('disabled','disabled');
                })
                

                $('#list_result').on('click','#btn_dowload', function(){
                    
                    var action_btn = $(this);
                    if($('#check_agreement').is(':checked')){
                        $(action_btn).attr('disabled', 'disabled');
                        $(action_btn).html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Loading...');

                        $.get( "{{route('web.StepUp.agreement')}}", function(data) {
                            // console.log(data);
                            // $(action_btn).html('File Sudah Diunduh');
                            link = data;
                        })
                        .done(function(){
                            $(action_btn).html('Unduh');
                            $(action_btn).removeAttr('disabled');
                            if(link !== null){
                                window.location = link;
                                return;
                            }
                        })
                        .fail(function(xhr, status, error) {
                            // alert( "error" );
                            var errorMessage = xhr.status + ': ' + xhr.statusText
                            // console.log(errorMessage);
                            Swal.fire('Download File Limit', '', 'error');
                            $(action_btn).html('File Sudah Diunduh Sebelumnya');
                        })

                        
                    }
                
                })
            })
            var countDownDate = new Date("Jun 1, 2022 23:59:59").getTime();

            // Update the count down every 1 second
            var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="waktu"
            document.getElementById("waktu").innerHTML = days + " hari " + hours + " jam "
            + minutes + " menit " + seconds + " detik ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("waktu").innerHTML = "EXPIRED";
            }
            }, 1000);
        </script>
    @endsection
</x-office-layout>