@if (strtolower(Auth::user()->verifikator->st) == 'lulus verifikasi')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="floating-title">
                        <h2>Materi Aanwijzing</h2>
                    </div>
                    <div class="d-flex flex-column text-end mb-10">
                        <span class="text-dark fw-bolder fs-2 fst-italic" style="margin-top: -1em;">22 April 2022</span>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="start-up-message" style="text-align: justify">
                                Materi Aanwijzing yang telah disampaikan ke peserta secara online merupakan bahan dari hasil Penyusunan Urban Design Development dan Rencana Pengembangan Kawasan yang telah disusun oleh Kementerian PUPR. Bahan dibagikan sebagai informasi penyusunan karya. Peserta sayembara dilarang menggunakan materi ini selain untuk penyusunan Karya Sayembara Konsep Perancangan Kawasan dan Bangunan Gedung di Ibu Kota Nusantara. 
                            </p>
                            <p class="start-up-message" style="text-align: justify">
                                Bila dikemudian hari diketahui bahwa materi ini dipergunakan tidak sebagaimana mestinya dan disebarkan tanpa izin dari Kementerian PUPR sebagai Instansi yang berwenang, maka Peserta akan digugurkan saat mengikuti proses sayembara. 
                            </p>
                            <label class="form-check form-check-custom form-check-solid mt-10">
                                <input id="check_agreement" class="form-check-input h-20px w-20px" type="checkbox" name="agreement[]" value="agree">
                                <span class="form-check-label fw-bold text-gray-600">Dengan ini peserta sayembara menyetujui dengan pernyataan di atas</span>
                            </label>
                            <div class="col-md-12 text-center mt-10">
                                <button class="btn btn-lg btn-primary" id="btn_dowload" type="button" disabled>Unduh</button>
                            </div>
                        </div>
                    

                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <br>
    <br>
@endif