<div class="card mb-10">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="floating-title">
                    <h2>VIDEO DRONE LOKASI SAYEMBARA</h2>
                </div>
                <div class="d-flex flex-column text-end mb-10">
                    <span class="text-dark fw-bolder fs-2 fst-italic" style="margin-top: -1em;">28 April 2022</span>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 aanwizjing-berita mt-9">
                            <p class="start-up-message" style="text-align: justify">
                                Klik <a href="https://bit.ly/dronelokasisayembara">Link</a> ini untuk mengunduh video drone lokasi sayembara
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<div class="card mb-10">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="floating-title">
                    <h2>DOKUMENTASI DAN VIDEO KUNJUNGAN LAPANGAN</h2>
                </div>
                <div class="d-flex flex-column text-end mb-10">
                    <span class="text-dark fw-bolder fs-2 fst-italic" style="margin-top: -1em;">28 April 2022</span>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 aanwizjing-berita mt-9">
                            <p class="start-up-message" style="text-align: justify">
                                Klik <a href="https://bit.ly/dokumentasikunlapsayembara">Link</a> ini untuk mengunduh dokumentasi dan video kunjungan lapangan
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
