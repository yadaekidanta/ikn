<table class="table align-middle table-row-dashed fs-6 gy-5">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">No</th>
            <th class="min-w-125px">No Peserta</th>
            <th class="min-w-125px">Jenis Sayembara</th>
            <th class="min-w-125px">Surat Pakta Integritas</th>
            <th class="min-w-125px">Surat Tanggung Jawab</th>
            {{-- <th class="min-w-125px">Tanggal Unggah</th> --}}
            {{-- <th class="min-w-125px">Status</th> --}}
            <th class="text-end min-w-70px">Aksi</th>
        </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach ($collection as $key => $item)
        @php
        if($item->jenis_sayembara == 1){
            $jenis_sayembara = 'Istana Wakil Presiden';
        }
        elseif($item->jenis_sayembara == 2){
            $jenis_sayembara = 'Legislatif';
        }
        elseif($item->jenis_sayembara == 3){
            $jenis_sayembara = 'Yudikatif';
        }
        else{
            $jenis_sayembara = 'Peribadatan';
        }
        @endphp
        <tr>
            <td>
                {{$key+ $collection->firstItem()}}
            </td>
            <td>
                {{$item->no_reg}}
            </td>
            <td>
                {{$jenis_sayembara}}
            </td>
            <td>
                @if ($item->surat_pakta_integritas)
                <a href="{{asset('storage/' .$item->surat_pakta_integritas)}}" target="_blank" class="btn btn-sm btn-primary">
                    Lihat
                    <span class="svg-icon svg-icon-4 ms-1 me-0">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                            </g>
                        </svg>
                    </span>
                </a>
                <a href="javascript:;" onclick="load_input('{{route('web.karya.edit',[$item->id])}}');" class="btn btn-sm btn-warning">Unggah</a>
                @else
                <a href="javascript:;" onclick="load_input('{{route('web.karya.edit',[$item->id])}}');" class="btn btn-sm btn-warning">Unggah</a>
                @endif
            </td>
            <td>
                @if ($item->surat_tanggung_jawab)
                <a href="{{asset('storage/' .$item->surat_tanggung_jawab)}}" target="_blank" class="btn btn-sm btn-primary">
                    Lihat
                    <span class="svg-icon svg-icon-4 ms-1 me-0">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"/>
                                <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                            </g>
                        </svg>
                    </span>
                </a>
                <a href="javascript:;" onclick="load_input('{{route('web.karya.edits',[$item->id])}}');" class="btn btn-sm btn-warning">Unggah</a>
                @else
                <a href="javascript:;" onclick="load_input('{{route('web.karya.edits',[$item->id])}}');" class="btn btn-sm btn-warning">Unggah</a>
                @endif
            </td>
            {{-- <td>
                $item->created_at ? $item->created_at->isoFormat('dddd, DD MMMM YYYY') : ''
            </td> --}}
            <td>
                <a href="javascript:;" onclick="load_input('{{route('web.karya.show',[$item->id])}}');" class="btn btn-sm btn-info me-5">Karya</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>