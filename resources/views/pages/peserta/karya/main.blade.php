<x-office-layout title="Karya">
    <style>
        .floating-title{
            position: absolute;
            top: -16px;
            padding: 5px 15px;
            border: 1px solid #ccc;
            background: white;
            border-radius: 5px;
        }
        .floating-title h2{
            margin: 0;
            display:block;
        }
        .start-up-message{
            display:block;
            font-size: 1.3em;
        }
    </style>
    <div id="content_list">
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="card" style="margin-bottom:3%;">
                    <div class="card-body">
                        <div class="fw-bold fs-3 text-muted mb-2">Selamat datang, Pendaftar {{Auth::guard('web')->user()->id}}</div>
                        <h1 class="fw-bolder fs-1qx text-gray-800">Sayembara Konsep Perancangan Kawasan dan Bangunan Gedung di Ibu Kota Nusantara</h1>
                        <br>
                        <h1>Waktu pemasukkan karya tinggal <span id="waktu" style="color:red;"></span></h1>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body pt-0">
                        <div class="floating-title">
                            <h2>Penyusunan Karya</h2>
                        </div>
                        <p class="start-up-message" style="margin-top:3%;">
                            Waktu penyusunan Karya 40 hari (23 April- 1 Juni 2022). Untuk mengunggah karya silakan klik tombol sesuai dengan kategori sayembara.
                        </p>
                        <div id="list_result"></div>
                        <p class="start-up-message" style="margin-top:3%;">
                            Catatan: <br>
                            Anda masih dapat mengubah file yang diunggah sampai batas waktu pengumpulan karya ditutup pada 1 Juni 2022 pukul 23.59. Apabila ingin mengubah file unggahan, tekan “pilih file” pada file karya yang diubah.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="content_input"></div>
    @section('custom_js')
        <script>
            load_list(1);
            var countDownDate = new Date("Jun 1, 2022 23:59:59").getTime();

            // Update the count down every 1 second
            var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="waktu"
            document.getElementById("waktu").innerHTML = days + " hari " + hours + " jam "
            + minutes + " menit " + seconds + " detik ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("waktu").innerHTML = "EXPIRED";
            }
            }, 1000);
        </script>
    @endsection
</x-office-layout >