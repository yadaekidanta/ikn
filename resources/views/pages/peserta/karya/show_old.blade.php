@php
if($karya->jenis_sayembara == 1){
    $jenis_sayembara = 'Istana Wakil Presiden';
}
elseif($karya->jenis_sayembara == 2){
    $jenis_sayembara = 'Legislatif';
}
elseif($karya->jenis_sayembara == 3){
    $jenis_sayembara = 'Yudikatif';
}
else{
    $jenis_sayembara = 'Peribadatan';
}
@endphp
<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">
                    <a href="javascript:;" class="text-muted text-hover-primary">Karya</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">Unggah Karya</li>
            </ul>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card mb-5 mb-xl-5">
            <div class="card-body pt-9 pb-0">
                <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                    <div class="flex-grow-1">
                        <div class="d-flex justify-content-between align-items-start flex-wrap">
                            <div class="d-flex flex-column">
                                <div class="d-flex align-items-center">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-1 fw-bolder me-1">No Peserta {{$karya->no_reg}}</a>
                                </div>
                            </div>
                            <div class="d-flex">
                                <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Jenis Sayembara : {{$jenis_sayembara}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-5 mb-xl-5" style="background-color:#e0e0e0;">
            <div class="card-header card-header-stretch">
                {{-- <h3 class="card-title">Title</h3> --}}
                <div class="card-toolbar">
                    <ul class="nav nav-tabs nav-line-tabs nav-stretch fs-6 border-0">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#panel_1">Panel 1</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" data-bs-toggle="tab" href="#panel_2">Panel 2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" data-bs-toggle="tab" href="#panel_3">Panel 3</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" data-bs-toggle="tab" href="#panel_4">Panel 4</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" data-bs-toggle="tab" href="#panel_5">Panel 5</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" data-bs-toggle="tab" href="#panel_6">Panel 6</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-dark" data-bs-toggle="tab" href="#panel_7">Panel 7</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-body pt-9 pb-0">
                <div class="tab-content" id="panelContent">
                    <form class="form" id="form_input" novalidate="novalidate">
                        <input id="urut" name="urut" type="hidden">
                        <input id="id_karya" name="id_karya" type="hidden" value="{{$karya->id}}">
                        <div class="tab-pane fade show active" id="panel_1" role="tabpanel">
                        </div>
                        <div class="tab-pane fade" id="panel_2" role="tabpanel">
                        </div>
                        <div class="tab-pane fade" id="panel_3" role="tabpanel">
                        </div>
                        <div class="tab-pane fade" id="panel_4" role="tabpanel">
                        </div>
                        <div class="tab-pane fade" id="panel_5" role="tabpanel">
                        </div>
                        <div class="tab-pane fade" id="panel_6" role="tabpanel">
                        </div>
                        <div class="tab-pane fade" id="panel_7" role="tabpanel">
                        </div>
                        <button id="tombol_simpan" class="btn btn-primary" data-kt-stepper-action="next">
                            Unggah & Lanjut
                        </button>
                    </form>
                </div>
                <div class="stepper stepper-pills stepper-column d-flex flex-column flex-lg-row" id="step_karya">
                    <div class="d-flex flex-row-auto w-100 w-lg-300px">
                        <div class="stepper-nav flex-cente">
                            <div class="stepper-item me-5 current" data-kt-stepper-element="nav">
                                <div class="stepper-line w-40px"></div>
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">1</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">
                                        File Karya 1
                                    </h3>
                                    <div class="stepper-desc">
                                        Upload Karya
                                    </div>
                                </div>
                            </div>
                            <div class="stepper-item me-5" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <div class="stepper-line w-40px"></div>
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">2</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">
                                        File Karya 2
                                    </h3>
                                    <div class="stepper-desc">
                                        Upload Karya
                                    </div>
                                </div>
                            </div>
                            <div class="stepper-item me-5" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <div class="stepper-line w-40px"></div>
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">3</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">
                                        File Karya 3
                                    </h3>
                                    <div class="stepper-desc">
                                        Upload Karya
                                    </div>
                                </div>
                            </div>
                            <div class="stepper-item me-5" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <div class="stepper-line w-40px"></div>
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">4</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">
                                        File Karya 4
                                    </h3>
                                    <div class="stepper-desc">
                                        Upload Karya
                                    </div>
                                </div>
                            </div>
                            <div class="stepper-item me-5" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <div class="stepper-line w-40px"></div>
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">5</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">
                                        File Karya 5
                                    </h3>
                                    <div class="stepper-desc">
                                        Upload Karya
                                    </div>
                                </div>
                            </div>
                            <div class="stepper-item me-5" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <div class="stepper-line w-40px"></div>
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">6</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">
                                        File Karya 6
                                    </h3>
                                    <div class="stepper-desc">
                                        Upload Karya
                                    </div>
                                </div>
                            </div>
                            <div class="stepper-item me-5" data-kt-stepper-element="nav" data-kt-stepper-action="step">
                                <div class="stepper-line w-40px"></div>
                                <div class="stepper-icon w-40px h-40px">
                                    <i class="stepper-check fas fa-check"></i>
                                    <span class="stepper-number">7</span>
                                </div>
                                <div class="stepper-label">
                                    <h3 class="stepper-title">
                                        File Karya 7
                                    </h3>
                                    <div class="stepper-desc">
                                        Upload Karya
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="flex-row-fluid">
                        <form class="form w-lg-500px mx-auto" id="form_input" novalidate="novalidate">
                            <input id="id_file" name="id_file" type="hidden">
                            <input id="urut" name="urut" type="hidden">
                            <input id="tipe" name="tipe" type="hidden">
                            <input id="id_karya" name="id_karya" type="hidden" value="{{$karya->id}}">
                            <div class="mb-10">
                                <div class="flex-column current" data-kt-stepper-element="content">
                                    <div class="fv-row mb-10" style="margin-top:30%;">
                                        <input type="file" id="f01" name="karya1" class="form-control form-control-lg form-control-solid" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="f01" class="label-file btn btn-primary">Ubah File</label>
                                        <a id="file1" href="" target="_blank" class="btn btn-primary" style="display:none;">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 20MB</div>
                                    </div>
                                </div>
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <div class="fv-row mb-10" style="margin-top:30%;">
                                        <input type="file" id="f02" name="karya2" class="form-control form-control-lg form-control-solid" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="f02" class="label-file btn btn-primary">Ubah File</label>
                                        <a id="file2" href="" target="_blank" class="btn btn-primary" style="display:none;">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 20MB</div>
                                    </div>
                                </div>
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <div class="fv-row mb-10" style="margin-top:30%;">
                                        <input type="file" id="f03" name="karya3" class="form-control form-control-lg form-control-solid" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="f03" class="label-file btn btn-primary">Ubah File</label>
                                        <a id="file3" href="" target="_blank" class="btn btn-primary" style="display:none;">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 20MB</div>
                                    </div>
                                </div>
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <div class="fv-row mb-10" style="margin-top:30%;">
                                        <input type="file" id="f04" name="karya4" class="form-control form-control-lg form-control-solid" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="f04" class="label-file btn btn-primary">Ubah File</label>
                                        <a id="file4" href="" target="_blank" class="btn btn-primary" style="display:none;">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 20MB</div>
                                    </div>
                                </div>
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <div class="fv-row mb-10" style="margin-top:30%;">
                                        <input type="file" id="f05" name="karya5" class="form-control form-control-lg form-control-solid" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="f05" class="label-file btn btn-primary">Ubah File</label>
                                        <a id="file5" href="" target="_blank" class="btn btn-primary" style="display:none;">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 20MB</div>
                                    </div>
                                </div>
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <div class="fv-row mb-10" style="margin-top:30%;">
                                        <input type="file" id="f06" name="karya6" class="form-control form-control-lg form-control-solid" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="f06" class="label-file btn btn-primary">Ubah File</label>
                                        <a id="file6" href="" target="_blank" class="btn btn-primary" style="display:none;">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 20MB</div>
                                    </div>
                                </div>
                                <div class="flex-column" data-kt-stepper-element="content">
                                    <div class="fv-row mb-10" style="margin-top:30%;">
                                        <input type="file" id="f07" name="karya7" class="form-control form-control-lg form-control-solid" placeholder="Ubah File" accept="application/pdf" />
                                        <label for="f07" class="label-file btn btn-primary">Ubah File</label>
                                        <a id="file6" href="" target="_blank" class="btn btn-primary" style="display:none;">
                                            Unduh File Sebelumnya
                                            <span class="svg-icon svg-icon-4 ms-1 me-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M2,13 C2,12.5 2.5,12 3,12 C3.5,12 4,12.5 4,13 C4,13.3333333 4,15 4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 C2,15 2,13.3333333 2,13 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                        <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 8.000000) rotate(-180.000000) translate(-12.000000, -8.000000) " x="11" y="1" width="2" height="14" rx="1"/>
                                                        <path d="M7.70710678,15.7071068 C7.31658249,16.0976311 6.68341751,16.0976311 6.29289322,15.7071068 C5.90236893,15.3165825 5.90236893,14.6834175 6.29289322,14.2928932 L11.2928932,9.29289322 C11.6689749,8.91681153 12.2736364,8.90091039 12.6689647,9.25670585 L17.6689647,13.7567059 C18.0794748,14.1261649 18.1127532,14.7584547 17.7432941,15.1689647 C17.3738351,15.5794748 16.7415453,15.6127532 16.3310353,15.2432941 L12.0362375,11.3779761 L7.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000004, 12.499999) rotate(-180.000000) translate(-12.000004, -12.499999) "/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </a>
                                        <div class="form-text" style="color:#e2304a">jenis file yg diperbolehkan : PDF dengan ukuran max 20MB</div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-stack">
                                <div class="me-2">
                                    <a href="javascript:;" onclick="load_list(1);" class="btn btn-danger me-10">
                                        <span class="svg-icon svg-icon-4 me-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="black" />
                                                <path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="black" />
                                            </svg>
                                        </span>
                                        Tutup
                                    </a>
                                    <button onclick="load_edit($('#id_file').val());" type="button" class="btn btn-light btn-active-light-primary" data-kt-stepper-action="previous">
                                        Kembali
                                    </button>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-primary" data-kt-stepper-action="submit">
                                        <span class="indicator-label">
                                            Selesai
                                        </span>
                                        <span class="indicator-progress">
                                            Harap Tunggu... <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                        </span>
                                    </button>
                
                                    <button id="tombol_simpan" class="btn btn-primary" data-kt-stepper-action="next">
                                        Unggah & Lanjut
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    step_karya('step_karya');
    function load_edit(id){
        $("#id_file").val(id);
    }
</script>