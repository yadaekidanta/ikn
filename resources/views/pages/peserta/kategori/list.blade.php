@php
$end = date('2022-04-08');
$last = $last->created_at;
$json_data = (@$user->jenis_sayembara) ? $user->jenis_sayembara : json_encode(array());
$pilihan_sayembara = json_decode($json_data);
@endphp
<style type="text/css">
    ul.ck_image {
  list-style-type: none;
}

ul.ck_image li {
  display: inline-block;
  width: 24%;
}

ul.ck_image input[type="checkbox"][id^="cb"] {
  display: none;
}

ul.ck_image label {
  border: 1px solid #fff;
  padding: 10px;
  display: block;
  position: relative;
  margin: 10px;
  cursor: pointer;
}

ul.ck_image label:before {
  /*background-color: white;*/
  color: white;
  /*content: " ";*/
  display: block;
  border-radius: 50%;
  border: 1px solid grey;
  position: absolute;
  top: -5px;
  left: -5px;
  width: 25px;
  height: 25px;
  text-align: center;
  line-height: 28px;
  transition-duration: 0.4s;
  /*transform: scale(0);*/
}

ul.ck_image label img {
  /*height: 100px;*/
  width: 100%;
  transition-duration: 0.2s;
  transform-origin: 50% 50%;
}

:checked + label {
  border-color: #ddd;
}

:checked + label:before {
  content: "✓";
  background-color: grey;
  transform: scale(1);
}

:checked + label img {
  transform: scale(0.9);
  box-shadow: 0 0 5px #333;
  z-index: -1;
}
</style>
<div class="card mb-5 mb-xl-5">
    <div class="card-body pt-9 pb-0">
        <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
            <div class="me-7 mb-4">
                <div class="symbol symbol-100px symbol-lg-100px symbol-fixed position-relative">
                    <img src="{{asset('logo.jpg')}}" alt="image" />
                    <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
                </div>
            </div>
            <div class="flex-grow-1">
                <div class="d-flex justify-content-between align-items-start flex-wrap">
                    <div class="d-flex flex-column">
                        <div class="d-flex align-items-center">
                            <a href="javascript:;" class="text-gray-900 text-hover-primary fs-1 fw-bolder me-1">Tim Nomor Pendaftaran {{$user->id}}</a>
                        </div>
                    </div>
                    <div class="d-flex">
                        <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Tanggal Mulai Daftar {{$user->created_at->isoFormat('dddd, DD MMMM YYYY')}}</a>
                    </div>
                </div>
                <div class="d-flex flex-wrap flex-stack">
                    <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                    </div>
                    <div class="d-flex">
                        <div class="d-flex flex-wrap">
                            <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1" style="font-color:#e2304a;">Waktu terakhir Login {{\Carbon\Carbon::parse($last)->diffForHumans()}}</a>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-wrap flex-stack">
                    <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                        <div class="d-flex justify-content-between w-100 mt-auto mb-2 d-none">
                            <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil</span>
                            <span class="fw-bolder fs-6">{{$complete}}%</span>
                        </div>
                        <div class="h-5px mx-3 w-100 bg-light d-none">
                            <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$complete}}%;" aria-valuenow="{{$complete}}" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="d-flex flex-wrap">
                            <a href="javascript:;" class="text-hover-primary fs-5 fw-bolder me-1" style="color:#e2304a;">Batas Waktu Pendaftaran {{\Carbon\Carbon::parse($end)->isoFormat('dddd, DD MMMM YYYY')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_create_account_stepper">
    <div class="card d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px w-xxl-400px me-9" style="height:25%;background-color:#cddbf0;">
        <div class="card-body px-6 px-lg-10 px-xxl-15 py-20">
            <div class="stepper-nav">
                <div class="stepper-item completed" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">1</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Profil</h3>
                        <div class="stepper-desc fw-bold">Ketua Tim</div>
                    </div>
                </div>
                <div class="stepper-item completed" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">2</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Profil</h3>
                        <div class="stepper-desc fw-bold">Perusahaan</div>
                    </div>
                </div>
                <div class="stepper-item completed" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">3</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Profil</h3>
                        <div class="stepper-desc fw-bold">Anggota</div>
                    </div>
                </div>
                <div class="stepper-item current" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">4</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Kategori Sayembara</h3>
                        <div class="stepper-desc fw-bold">Pilih Kategori Sayembara Anda</div>
                    </div>
                </div>
                <div class="stepper-item" data-kt-stepper-element="nav">
                    <div class="stepper-line w-40px"></div>
                    <div class="stepper-icon w-40px h-40px">
                        <i class="stepper-check fas fa-check"></i>
                        <span class="stepper-number">5</span>
                    </div>
                    <div class="stepper-label">
                        <h3 class="stepper-title">Selesai</h3>
                        <div class="stepper-desc fw-bold">Kirim data Anda untuk verifikasi</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card d-flex flex-row-fluid" style="background-color:#e0e0e0;">
        <form id="form_input">
            <div class="card-body py-20 w-100 px-9 flex-center ">
                <div class="current" data-kt-stepper-element="content">
                    <div class="fv-row" style="width: 100%">
                        <div class="col-lg-12">
                            <h3>Silahkan pilih kategori yang akan diikuti :</h3>
                            <div class="row">
                                <ul class="ck_image">
                                  <li><input type="checkbox" id="cb1" class="check_limit" name="kategori[]"  value="1" {{(in_array(1, $pilihan_sayembara) ? 'checked' : NULL)}}/>
                                    <label for="cb1"><img src="{{asset('image/Istana-Wakil-Presiden.png')}}" /></label>
                                    <h5 style="text-align: center;">
                                        Sayembara <br>
                                        Kompleks <br>
                                        Istana Wakil Presiden
                                    </h5>
                                  </li>
                                  <li><input type="checkbox" id="cb2" class="check_limit" name="kategori[]" value="2" {{(in_array(2, $pilihan_sayembara) ? 'checked' : NULL)}}/>
                                    <label for="cb2"><img src="{{asset('image/Legislatif.png')}}" /></label>
                                    <h5 style="text-align: center;">
                                        Sayembara <br>
                                        Kompleks <br>
                                        Perkantoran Legislatif
                                    </h5>
                                  </li>
                                  <li><input type="checkbox" id="cb3" class="check_limit" name="kategori[]" value="3" {{(in_array(3, $pilihan_sayembara) ? 'checked' : NULL)}}/>
                                    <label for="cb3"><img src="{{asset('image/Yudikatif.png')}}" /></label>
                                    <h5 style="text-align: center;">
                                        Sayembara <br>
                                        Kompleks <br>
                                        Perkantoran Yudikatif
                                    </h5>
                                  </li>
                                  <li><input type="checkbox" id="cb4" class="check_limit" name="kategori[]" value="4" {{(in_array(4, $pilihan_sayembara) ? 'checked' : NULL)}}/>
                                    <label for="cb4"><img src="{{asset('image/Peribadatan.png')}}" /></label>
                                    <h5 style="text-align: center;">
                                        Sayembara <br>
                                        Kompleks <br> Peribadatan
                                    </h5>
                                  </li>
                                </ul>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end py-6 px-9">
                <a href="{{route('web.anggota.index')}}" class="btn btn-danger me-10">
                    <span class="svg-icon svg-icon-4 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="black" />
                            <path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="black" />
                        </svg>
                    </span>
                    Kembali
                </a>
                
                <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('web.kategori-sayembara.store')}}','POST');" class="btn btn-success">
                    Simpan
                </button>
              
            </div>
        </form>
    </div>
</div>
