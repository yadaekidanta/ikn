@php
$end = date('2022-03-28');
$last = \Carbon\Carbon::createFromTimestamp($last);
if($data->role == 4)
{
    if($data->user_id == 0){
        $role = 'Ketua';
    }else{
        $role = 'Anggota';
    }
    if(!$data->verified_at){
        $color = 'info';
        $status = 'Belum Verifikasi';
    }else{
        $color = 'success';
        $status = 'Sudah Verifikasi';
    }
}
@endphp
<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Halaman Profil</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">
                    <a href="javascript:;" class="text-muted text-hover-primary">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-muted">Akun</li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">Overview</li>
            </ul>
        </div>
        <div class="d-flex align-items-center gap-2 gap-lg-3 d-none">
            <div class="m-0">
                <a href="#" class="btn btn-sm btn-flex btn-light btn-active-primary fw-bolder" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                    <span class="svg-icon svg-icon-5 svg-icon-gray-500 me-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M19.0759 3H4.72777C3.95892 3 3.47768 3.83148 3.86067 4.49814L8.56967 12.6949C9.17923 13.7559 9.5 14.9582 9.5 16.1819V19.5072C9.5 20.2189 10.2223 20.7028 10.8805 20.432L13.8805 19.1977C14.2553 19.0435 14.5 18.6783 14.5 18.273V13.8372C14.5 12.8089 14.8171 11.8056 15.408 10.964L19.8943 4.57465C20.3596 3.912 19.8856 3 19.0759 3Z" fill="black" />
                        </svg>
                    </span>
                    Filter
                </a>
                <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_6220edcb36682">
                    <div class="px-7 py-5">
                        <div class="fs-5 text-dark fw-bolder">Filter Options</div>
                    </div>
                    <div class="separator border-gray-200"></div>
                    <div class="px-7 py-5">
                        <div class="mb-10">
                            <label class="form-label fw-bold">Status:</label>
                            <div>
                                <select class="form-select form-select-solid" data-kt-select2="true" data-placeholder="Select option" data-dropdown-parent="#kt_menu_6220edcb36682" data-allow-clear="true">
                                    <option></option>
                                    <option value="1">Approved</option>
                                    <option value="2">Pending</option>
                                    <option value="2">In Process</option>
                                    <option value="2">Rejected</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-10">
                            <label class="form-label fw-bold">Member Type:</label>
                            <div class="d-flex">
                                <label class="form-check form-check-sm form-check-custom form-check-solid me-5">
                                    <input class="form-check-input" type="checkbox" value="1" />
                                    <span class="form-check-label">Author</span>
                                </label>
                                <label class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="2" checked="checked" />
                                    <span class="form-check-label">Customer</span>
                                </label>
                            </div>
                        </div>
                        <div class="mb-10">
                            <label class="form-label fw-bold">Notifications:</label>
                            <div class="form-check form-switch form-switch-sm form-check-custom form-check-solid">
                                <input class="form-check-input" type="checkbox" value="" name="notifications" checked="checked" />
                                <label class="form-check-label">Enabled</label>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2" data-kt-menu-dismiss="true">Reset</button>
                            <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true">Apply</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        <div class="card mb-5 mb-xl-5">
            <div class="card-body pt-9 pb-0">
                <div class="d-flex flex-wrap flex-sm-nowrap mb-3">
                    <div class="me-7 mb-4">
                        <div class="symbol symbol-100px symbol-lg-100px symbol-fixed position-relative">
                            <img src="{{asset('logo.jpg')}}" alt="image" />
                            <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-white h-20px w-20px"></div>
                        </div>
                    </div>
                    <div class="flex-grow-1">
                        <div class="d-flex justify-content-between align-items-start flex-wrap">
                            <div class="d-flex flex-column">
                                <div class="d-flex align-items-center">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-1 fw-bolder me-1">Tim No Urut {{$data->id}}</a>
                                </div>
                            </div>
                            <div class="d-flex">
                                <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Tanggal Mulai Daftar {{$data->created_at->isoFormat('dddd, DD MMMM YYYY')}}</a>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap flex-stack">
                            <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                            </div>
                            <div class="d-flex">
                                <div class="d-flex flex-wrap">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1" style="font-color:#e2304a;">Waktu terakhir Login {{\Carbon\Carbon::parse($last)->diffForHumans()}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap flex-stack">
                            <div class="d-flex align-items-center w-200px w-sm-300px flex-column">
                                <div class="d-flex justify-content-between w-100 mt-auto mb-2">
                                    <span class="fw-bold fs-6 text-gray-400">Kelengkapan Profil</span>
                                    <span class="fw-bolder fs-6">{{$complete}}%</span>
                                </div>
                                <div class="h-5px mx-3 w-100 bg-light">
                                    <div class="bg-success rounded h-5px" role="progressbar" style="width: {{$complete}}%;" aria-valuenow="{{$complete}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="d-flex flex-wrap">
                                    <a href="javascript:;" class="text-gray-900 text-hover-primary fs-5 fw-bolder me-1">Sisa Waktu {{\Carbon\Carbon::parse($end)->diffForHumans()}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
            <div class="card-header cursor-pointer">
                <div class="card-title m-0">
                    <h3 class="fw-bolder m-0">Detil Perusahaan</h3>
                </div>
                <a href="javascript:;" onclick="load_list(1);" class="btn btn-primary align-self-center">Kembali</a>
            </div>
            <div class="card-body p-9">
                <div class="row mb-7">
                    <label class="col-lg-4 fw-bold">Nama Perusahaan</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{{$data->name}}</span>
                    </div>
                </div>
                <div class="row mb-7">
                    <label class="col-lg-4 fw-bold">Alamat</label>
                    <div class="col-lg-8 fv-row">
                        <span class="fw-bold text-gray-800 fs-6">{{$data->address}}</span>
                    </div>
                </div>
                <div class="row mb-7">
                    <label class="col-lg-4 fw-bold">No. NPWP</label>
                    <div class="col-lg-8 fv-row">
                        <span class="fw-bold text-gray-800 fs-6">{{$data->npwp_no}}</span>
                        @if ($data->npwp)
                        <a class="d-block overlay" data-fslightbox="lightbox-basic" href="{{asset('storage/' .$data->npwp)}}">
                            <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('storage/' .$data->npwp)}}')"></div>
                            <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                <i class="bi bi-eye-fill text-white fs-3x"></i>
                            </div>
                        </a>
                        @endif
                    </div>
                </div>
                <div class="row mb-7">
                    <label class="col-lg-4 fw-bold">Akta Pendirian</label>
                    <div class="col-lg-8 fv-row">
                        @if ($data->akta)
                        <a class="d-block overlay" data-fslightbox="lightbox-basic" href="{{asset('storage/' .$data->akta)}}">
                            <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('storage/' .$data->akta)}}')"></div>
                            <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                <i class="bi bi-eye-fill text-white fs-3x"></i>
                            </div>
                        </a>
                        @endif
                    </div>
                </div>
                <div class="row mb-7">
                    <label class="col-lg-4 fw-bold">Akta Pendirian (Perubahan)</label>
                    <div class="col-lg-8 fv-row">
                        @if ($data->akta_perubahan)
                        <a class="d-block overlay" data-fslightbox="lightbox-basic" href="{{asset('storage/' .$data->akta_perubahan)}}">
                            <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('storage/' .$data->akta_perubahan)}}')"></div>
                            <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                <i class="bi bi-eye-fill text-white fs-3x"></i>
                            </div>
                        </a>
                        @endif
                    </div>
                </div>
                <div class="row mb-7">
                    <label class="col-lg-4 fw-bold">SIUJK</label>
                    <div class="col-lg-8 fv-row">
                        @if ($data->siujk)
                        <a class="d-block overlay" data-fslightbox="lightbox-basic" href="{{asset('storage/' .$data->siujk)}}">
                            <div class="overlay-wrapper bgi-no-repeat bgi-position-center bgi-size-cover card-rounded min-h-175px" style="background-image:url('{{asset('storage/' .$data->siujk)}}')"></div>
                            <div class="overlay-layer card-rounded bg-dark bg-opacity-25 shadow">
                                <i class="bi bi-eye-fill text-white fs-3x"></i>
                            </div>
                        </a>
                        @endif
                    </div>
                </div>
                <div class="row mb-7">
                    <label class="col-lg-4 fw-bold">SBU</label>
                    <div class="col-lg-8 fv-row">
                        <span class="fw-bold text-gray-800 fs-6">{{$data->sbu}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('keenthemes/plugins/custom/fslightbox/fslightbox.bundle.js')}}"></script>