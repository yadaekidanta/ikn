@php
$end = date('2022-04-08');
$last = $last->created_at;
if($data->role == 4)
{
    if($data->user_id == 0){
        $role = 'Ketua';
    }else{
        $role = 'Anggota';
    }
    if(!$data->verified_at){
        $color = 'info';
        $status = 'Belum Verifikasi';
    }else{
        $color = 'success';
        $status = 'Sudah Verifikasi';
    }
}
@endphp
<div class="toolbar" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara</h1>
            <span class="h-20px border-gray-300 border-start mx-4"></span>
            <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                <li class="breadcrumb-item text-muted">
                    <a href="../../demo1/dist/index.html" class="text-muted text-hover-primary">Profil</a>
                </li>
                <li class="breadcrumb-item">
                    <span class="bullet bg-gray-300 w-5px h-2px"></span>
                </li>
                <li class="breadcrumb-item text-dark">Ketua</li>
            </ul>
        </div>
    </div>
</div>
<div class="post d-flex flex-column-fluid" id="kt_post">
    <div id="kt_content_container" class="container-xxl">
        
    </div>
</div>