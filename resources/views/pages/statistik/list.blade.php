@if(Auth::guard('web')->user()->role == 1)
<div class="row gy-5 g-xl-8">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Statistik Kelengkapan Dokumen</h3>
                <a href="{{route('web.statistik.export-sayembara')}}" style="justify-content: center;align-items: center; display: flex;"><button class="btn btn-sm btn-primary"> Download Rekapitulasi</button></a>
            </div>
            <div class="card-body">
                <div id="chart_statistik" class="chart_div"></div>
            </div>
        </div>
    </div>
    <br>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Statistik Jenis Sayembara</h3>
            </div>
            <div class="card-body">
                <div id="chart_sayembara" class="chart_div"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Statistik Ketua Per Provinsi</h3>
            </div>
            <div class="card-body">
                <div id="chart_provinsi" class="chart_div"></div>
            </div>
        </div>
    </div>

</div>
<script>
// ----------------- Start Chart untuk Statistik Peserta ----------------//
    am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chart_statistik", am4charts.XYChart);
        // Add data
        chart.data = {!! $total_array !!};

        // Create axes

        chart.colors.step = 2;

        // chart.legend = new am4charts.Legend()
        // chart.legend.position = 'bottom'
        // chart.legend.paddingBottom = 20
        // chart.legend.labels.template.maxWidth = 95

        var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
        xAxis.dataFields.category = 'kategori'
        xAxis.renderer.cellStartLocation = 0.1
        xAxis.renderer.cellEndLocation = 0.9
        xAxis.renderer.grid.template.location = 0;

        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.min = 0;

        // ---------- Start Function list inside amchart4 ----------//
            function createSeries(value, name) {
                var series = chart.series.push(new am4charts.ColumnSeries())
                series.dataFields.valueY = value
                series.dataFields.categoryX = 'kategori'
                series.name = name

                series.events.on("hidden", arrangeColumns);
                series.events.on("shown", arrangeColumns);

                series.columns.template.events.on("hit", function(ev) {
                    seriesIndex = ev.target.baseSprite.series.indexOf(ev.target.dataItem.component);
                    dataItemIndex = ev.target.dataItem.component.dataItems.indexOf(ev.target.dataItem);
                    console.log(dataItemIndex);
                    // console.log("clicked on ", ev.target.dataItem.component.coba);
                }, this);

                var bullet = series.bullets.push(new am4charts.LabelBullet())
                bullet.interactionsEnabled = false
                bullet.dy = -10;
                bullet.label.text = '{valueY}'
                bullet.label.fill = am4core.color('#000');

                // series.columns.template.adapter.add("fill", function(fill, target) {
                //     chart.color = target.dataItem.dataContext.color;
                //     return chart.color;
                // });

                return series;
            }

            function arrangeColumns() {

                var series = chart.series.getIndex(0);

                var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
                if (series.dataItems.length > 1) {
                    var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                    var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                    var delta = ((x1 - x0) / chart.series.length) * w;
                    if (am4core.isNumber(delta)) {
                        var middle = chart.series.length / 2;

                        var newIndex = 0;
                        chart.series.each(function(series) {
                            if (!series.isHidden && !series.isHiding) {
                                series.dummyData = newIndex;
                                newIndex++;
                            }
                            else {
                                series.dummyData = chart.series.indexOf(series);
                            }
                        })
                        var visibleCount = newIndex;
                        var newMiddle = visibleCount / 2;

                        chart.series.each(function(series) {
                            var trueIndex = chart.series.indexOf(series);
                            var newIndex = series.dummyData;

                            var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                            series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                            series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                        })
                    }
                }
            }
        // ---------- End Function list inside amchart4 ----------//

        createSeries('jumlah1', 'Kelengkapan');
      
        chart.cursor = new am4charts.XYCursor();

       

    });
// ----------------- End Chart untuk Statistik Peserta ----------------//

// ----------------- Start Chart untuk jenis Sayembara ----------------//
    am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chart_sayembara", am4charts.XYChart);
        // chart.legend = new am4charts.Legend();
        // chart.legend.labels.template.text = "Series: [bold {color}]{name}[/]";
        // chart.legend.useDefaultMarker = true;

        // Add data
        chart.data = {!! $array_jns !!};

        // Create axes

        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.dataFields.category = "jenis";
        categoryAxis.renderer.minGridDistance = 60;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        let series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "jenis";
        series.dataFields.valueY = "jumlah";
        series.columns.template.strokeWidth = 0;
        series.tooltipText = "{valueY.value}";

        var bullet = series.bullets.push(new am4charts.LabelBullet());
        bullet.label.text = "{valueY}";
        bullet.label.verticalCenter = "bottom";
        bullet.label.dy = 0;
        bullet.label.fontSize = 14;
        

        // console.log(series);
        chart.cursor = new am4charts.XYCursor();

        series.columns.template.adapter.add("fill", function(fill, target) {
            chart.color = target.dataItem.dataContext.color;
            return chart.color;
        });

       

    }); // end am4core.ready()
// ----------------- End Chart untuk jenis Sayembara ----------------//

// ----------------- Start Chart untuk statistik per provinsi ----------------//
    am4core.ready(function() {

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chart_provinsi", am4charts.XYChart);
        // chart.legend = new am4charts.Legend();
        // chart.legend.labels.template.text = "Series: [bold {color}]{name}[/]";
        // chart.legend.useDefaultMarker = true;

        // Add data
        chart.data = {!! $ketua_prov !!};

        // Create axes

        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.dataFields.category = "provinsi";
        categoryAxis.renderer.minGridDistance = 0;
        categoryAxis.renderer.labels.template.rotation = 270;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.dx = -20;


        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

        let series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryX = "provinsi";
        series.dataFields.valueY = "jumlah";
        series.columns.template.strokeWidth = 0;
        series.tooltipText = "{valueY.value}";

        var bullet = series.bullets.push(new am4charts.LabelBullet());
        bullet.label.text = "{valueY}";
        bullet.label.verticalCenter = "bottom";
        bullet.label.dy = 0;
        bullet.label.fontSize = 14;


        // console.log(series);
        chart.cursor = new am4charts.XYCursor();

        // series.columns.template.adapter.add("fill", function(fill, target) {
        //     chart.color = target.dataItem.dataContext.color;
        //     return chart.color;
        // });



    }); // end am4core.ready()
// ----------------- End Chart untuk statistik per provinsi ----------------//
</script>
@elseif(Auth::guard('web')->user()->role == 2)
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="pt-lg-10 mb-10">
                    <div class="fw-bold fs-3 text-muted mb-15">Selamat datang, {{Auth::guard('web')->user()->name}}</div>
                    <h1 class="fw-bolder fs-1qx text-gray-800 mb-7">Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@else
<style type="text/css">
    .img-sayembara{
        padding: 0 12%;
    }
    .title-sayembara{
        text-align: center;
        padding: 1em 10px;
        font-size: 18px;
    }
    .title-sayembara.peribadatan{
        padding-bottom: 2.2em;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="pt-lg-10 mb-10">
                    <div class="fw-bold fs-3 text-muted mb-15">Selamat datang, {{Auth::guard('web')->user()->name}}</div>
                    <h1 class="fw-bolder fs-1qx text-gray-800 mb-7">Sayembara Konsep Perancangan Kawasan dan Bangunan di Ibu Kota Nusantara</h1>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-3">
                        <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download', 1)}}');" id="tombol_download_istana" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                            <img src="{{asset('image/Istana-Wakil-Presiden.png')}}" class="w-100 img-sayembara" alt="">
                            <h3 class="title-sayembara">
                                Unduh <br>
                                Dokumen Sayembara <br>
                                Kompleks <br>
                                Istana Wakil Presiden

                            </h3>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download', 2)}}');" id="tombol_download_istana" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                            <img src="{{asset('image/Legislatif.png')}}" class="w-100 img-sayembara" alt="">
                            <h3 class="title-sayembara">
                                Unduh <br>
                                Dokumen Sayembara <br>
                                Kompleks <br>
                                Perkantoran Legislatif
                            </h3>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download', 3)}}');" id="tombol_download_istana" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                            <img src="{{asset('image/Yudikatif.png')}}" class="w-100 img-sayembara" alt="">
                            <h3 class="title-sayembara">
                                Unduh <br>
                                Dokumen Sayembara <br>
                                Kompleks <br>
                                Perkantoran Yudikatif 
                            </h3>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="javascript:;" onclick="handle_download('#tombol_download','{{route('web.download', 4)}}');" id="tombol_download_istana" class="card hoverable card-stretch mb-xl-8" style="background-color: transparent;">
                            <img src="{{asset('image/Peribadatan.png')}}" class="w-100 img-sayembara" alt="">
                            <h3 class="title-sayembara peribadatan">
                                Unduh <br>
                                Dokumen Sayembara <br>
                                Kompleks Peribadatan
                            </h3>
                        </a>
                    </div>
                    @if (date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime('2022-03-30 23:59:58')))
                        <div class="col-lg-12 mt-5">
                            <div class="text-center">
                                <a href="{{route('web.profile.index')}}" class="btn btn-lg btn-primary fw-bolder">Daftar / Ubah</a>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
            
        </div>
    </div>
</div>
@endif
