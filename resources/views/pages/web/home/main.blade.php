<x-web-layout title="Selamat Datang">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row mb-5">
                    <div class="col-lg-6">
                        <img src="{{asset('latar-belakang.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Sejak tahun 2019, Kementerian PUPR telah memulai proses perencanaan IKN mengundang keterlibatan masyarakat umum dalam proses perwujudan IKN yang baru melalui Sayembara Gagasan Desain Kawasan Ibu Kota Negara. Sebagai tindak lanjut hasil sayembara, dilakukan proses Urban Design dengan kebutuhan populasi serta fungsi-fungsi di IKN, serta berkoordinasi dengan Kementerian/ Lembaga terkait penyusunan perencanaan spasial IKN.
                        </p>
                        <p>
                            Untuk mengundang keterlibatan masyarakat Indonesia dalam menyampaikan ide gagasannya, maka diselenggarakan Sayembara Kawasan dan Bangunan Gedung yang berlokasi di Kawasan Inti Pusat Pemerintahan (KIPP - IKN) pada empat kompleks yaitu:
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <img alt="100%x180" src="{{asset('istana-wapres.png')}}" style="width:10%;float:left;">
                        <p>
                            <span style="font-size:60px;" class="dropcap">1</span>
                            <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                            <span style="">Kompleks Istana Wakil Presiden</span>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <img alt="100%x180" src="{{asset('legistatif.png')}}" style="width:10%;float:left;">
                        <p>
                            <span style="font-size:60px;" class="dropcap">2</span>
                            <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                            <span style="">Kompleks Perkantoran Legislatif</span>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <img alt="100%x180" src="{{asset('yudikatif.png')}}" style="width:10%;float:left;">
                        <p>
                            <span style="font-size:60px;" class="dropcap">3</span>
                            <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                            <span style="">Kompleks Perkantoran Yudikatif</span>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <img alt="100%x180" src="{{asset('peribadatan.png')}}" style="width:10%;float:left;">
                        <p>
                            <span style="font-size:60px;" class="dropcap">4</span>
                            <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                            <span style="">Kompleks Peribadatan</span>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="section-syarat">
                    <div class="col-lg-6">
                        <img src="{{asset('persyaratan-umum.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Persyaratan Peserta</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        <ol style="margin-left:3%;">
                            <li>Warga Negara Indonesia (WNI) dan Non-WNI yang bekerjasama dengan Badan Usaha Konsultansi Konstruksi yang memenuhi syarat sesuai peraturan perundangan yang berlaku;</li>
                            <li>Peserta merupakan kelompok yang memiliki keahlian dalam perancangan arsitektur. Ketua kelompok harus WNI, dan memiliki minimum Kompetensi SKA Arsitek Madya/STRA Madya;</li>
                            <li>Anggota kelompok minimal berjumlah 5 (lima) orang dan maksimum 10 (sepuluh) orang termasuk ketua. Setiap orang hanya dapat  tergabung dalam 1 (satu) kelompok. Setiap Peserta Sayembara dapat mengikuti paling banyak 2 (dua) sayembara Konsep Perancangan Kawasan dan Bangunan Gedung di Ibu Kota Nusantara;</li>
                            <li>Minimal 4 (empat) anggota kelompok memiliki kompetensi SKA sebagai berikut :
                                <ol class="alpha-list">
                                    <li>SKA Arsitek Ahli Muda / STRA  Madya</li>
                                    <li>SKA Ahli Teknik Bangunan Gedung minimal Muda</li>
                                    <li>SKA Ahli Teknik Mekanikal/ Ahli Teknik Tenaga Listrik minimal Muda</li>
                                    <li>SKA Ahli Arsitektur Lansekap minimal Muda</li>
                                </ol>
                               <!--  1. SKA Arsitek Ahli Muda / STRA  Madya
                                <br>
                                2. SKA Ahli Teknik Bangunan Gedung minimal Muda
                                <br>
                                3. SKA Ahli Teknik Mekanikal/ Ahli Teknik Tenaga Listrik minimal Muda
                                <br>
                                4. SKA Ahli Arsitektur Lansekap minimal Muda -->
                            </li>
                            <li>Peserta sayembara dapat berkolaborasi dengan disiplin ilmu lainnya seperti, Ahli Geoteknik, Ahli Desain Interior, Teknik Lingkungan, dan/atau ahli terkait lainnya, serta  masyarakat umum lainnya.;</li>
                            <li>Selama pelaksanaan sayembara tidak diperkenankan adanya pergantian personil maupun badan usaha.</li>
                        </ol>
                    </div>
                </div>
                <div class="row mt-5" id="section-tata">
                    <div class="col-lg-6">
                        <img src="{{asset('tata-cara.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Tata Cara Pendaftaran</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        <ol style="margin-left:3%;">
                            <li>Pendaftaran dilakukan secara online melalui website pada menu “Masuk/Daftar”</li>
                            <li>Calon peserta dapat mendapatkan akun dengan mendaftarkan email.</li>
                            <li>Setelah mendaftar, peserta akan mendapatkan email tanggapan berisi User ID dan password.</li>
                            <li>Peserta dapat login/ masuk ke akun yang sudah terdaftar dengan menggunakan User ID dan password yang diperoleh dan mengunduh Dokumen Sayembara berupa Kerangka Acuan Kerja (KAK) dan mengakses form pendaftaran.</li>
                            <li>Peserta mengisi form pendaftaran dan mengunggah dokumen dengan tata cara: <br>
                                <ol class='alpha-list'>
                                    <li>Mengisi data peserta dengan benar dan lengkap serta mengunggah scan identitas (KTP/SIM/Paspor), NPWP, dan SKA/STRA. Ijazah pendidikan terakhir</li>
                                    <li>Mengisi data perusahaan konsultansi konstruksi dengan benar dan lengkap serta mengunggah Akta pendirian perusahaan, SIUJK, SBU (AR 101/102), NPWP Perusahaan.</li>
                                    <li>Mengunggah Surat Perjanjian Dukungan Badan Usaha.</li>
                                </ol>
                               <!--  a. Mengisi data peserta dengan benar dan lengkap serta mengunggah scan identitas (KTP/SIM/Paspor), NPWP, dan SKA/STRA. Ijazah pendidikan terakhir.<br>
                                b. Mengisi data perusahaan konsultansi konstruksi dengan benar dan lengkap serta mengunggah Akta pendirian perusahaan, SIUJK, SBU (AR 101/102), NPWP Perusahaan.<br>
                                c. Mengunggah Surat Perjanjian Dukungan Badan Usaha. -->                                
                            </li>
                            <li>Peserta memilih maksimal 2 (dua) jenis sayembara.</li>
                            <li>Peserta yang lolos verifikasi administrasi akan mendapatkan kartu nomor peserta sayembara</li>
                        </ol>
                    </div>
                </div>
                <div class="row mt-5" id="section-jadwal">
                    <div class="col-lg-6">
                        <img src="{{asset('jadwal-pelaksanaan.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Jadwal Pelaksanaan</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        <p>
                            <table width="100%">
                                <thead>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">28 Mar - 8 April 2022</td>
                                        <td class="col-md-6">Pendaftaran Sayembara</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">9 - 18 April 2022</td>
                                        <td class="col-md-6">Verifikasi Administrasi Calon Peserta Sayembara</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">19 April 2022</td>
                                        <td class="col-md-6">Aanwijzing dan Sosialisasi Urban design development KIPP-IKN</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">21 - 22 April 2022</td>
                                        <td class="col-md-6">Aanwijzing Lapangan</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">23 April - 1 Juni 2022</td>
                                        <td class="col-md-6">Penyusunan Karya Desain</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">1 Juni 2022</td>
                                        <td class="col-md-6">Batas Akhir Pemasukan Karya Desain</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">2 - 5 Juni 2022</td>
                                        <td class="col-md-6">Evaluasi Dokumen Karya</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">6 - 10 Juni 2022</td>
                                        <td class="col-md-6">Penjurian Tahap 1</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">13 Juni 2022</td>
                                        <td class="col-md-6">Pengumuman Penjuran Tahap I</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">20 - 22 Juni 2022</td>
                                        <td class="col-md-6">Presentasi Nominator & Penjurian Tahap 2</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">23 Juni 2022</td>
                                        <td class="col-md-6">Proses Penetapan Pemenang</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">24 Juni 2022</td>
                                        <td class="col-md-6">Penetapan Pemenang</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;width:30%;">27 Juni 2022</td>
                                        <td class="col-md-6">Penyerahan Hadiah</td>
                                    </tr>
                                </thead>
                            </table>
                            <!-- * Jadwal dapat berubah sewaktu-waktu -->
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="section-apresiasi">
                    <div class="col-lg-6">
                        <img src="{{asset('penghargaan.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Penghargaan</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        <p>3 (tiga) karya terbaik akan diberikan penghargaan kepada masing-masing kategori sayembara, sebagai berikut:</p>
                        <table width="100%">
                                <thead>
                                    <tr>
                                        <td class="col-md-4" style="font-weight: bold;"><img width="20" src="{{asset('penghargaan-mini.png')}}" style="margin-right: 10px;"> Pemenang I</td>
                                        <td class="col-md-8">Hadiah Uang sebesar Rp. 500.000.000,-</td>
                                    </tr>
                                    <tr>
                                    <td class="col-md-4" style="font-weight: bold;"><img width="20" src="{{asset('penghargaan-mini.png')}}" style="margin-right: 10px;">Pemenang II</td>
                                        <td class="col-md-8">Hadiah Uang sebesar Rp. 250.000.000,-</td>
                                    </tr>
                                    <tr>
                                    <td class="col-md-4" style="font-weight: bold;"><img width="20" src="{{asset('penghargaan-mini.png')}}" style="margin-right: 10px;">Pemenang III</td>
                                        <td class="col-md-8">Hadiah Uang sebesar Rp. 100.000.000,-</td>
                                    </tr>
                                </thead>
                        </table>
                    </div>
                </div>
                <div class="row mt-5" id="section-tim">
                    <div class="col-lg-6">
                        <img src="{{asset('tim-juri.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Tim Juri</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        <div class="accordion clearfix" data-state="closed">
                            <div class="accordion-header">
                                <div class="accordion-title">
                                    <span style="font-size:55px;color:#1c3144;" class="dropcap">1</span>
                                    <span style="display: block;margin-bottom:-2%;font-size:20px; color:#808285;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                                    <span style="font-size:20px; color:#808285;">Kompleks Istana Wakil Presiden</span>
                                </div>
                            </div>
                            <div class="accordion-content" style="color:#1c3144">
                                <ol style="margin-left:10%;">
                                    <li>Prof. Ar. Gunawan Tjahjono, M.Arch., Ph.D,IAI</li>
                                    <li>Ir. Adjar Prayudi, MCM, MCE</li>
                                    <li>Ar. Karnaya, MAUD, IAI, AA</li>
                                    <li>Ar. Georgius Budi Yulianto,  M.T., IAI, AA</li>
                                    <li>Prof. Dr. Ir. Wiendu Nuryanti, M.Arch., Ph.D</li>
                                    <li>Ar. Jatmika Adi Suryabrata,M.Sc, Ph.D, IAI</li>
                                    <li>Sibarani Sofian, S.T., MUDD, LEED AP</li>
                                    <li>Guntur Iman Nefianto, SE., S.H., M.H</li>
                                    <li>Dr. Ir. Wicaksono Sarosa</li>
                                </ol>
                            </div>
                            <div class="accordion-header">
                                <div class="accordion-title">
                                    <span style="font-size:55px;color:#1c3144;" class="dropcap">2</span>
                                    <span style="display: block;margin-bottom:-2%;font-size:20px; color:#808285;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                                    <span style="font-size:20px; color:#808285;">Kompleks Perkantoran Lembaga Legislatif</span>
                                </div>
                            </div>
                            <div class="accordion-content" style="color:#1c3144">
                                <ol style="margin-left:10%;">
                                    <li>Ar. Andy Siswanto, M. Arch., M.Sc., Ph.D., IAI</li>
                                    <li>Dr.Ir. Andreas Suhono, M.Sc</li>
                                    <li>Ar. Didi Haryadi, IAI, AA</li>
                                    <li>Ar. Endy Subijono, IAI, AA</li>
                                    <li>Prof. Dr. H. Masjaya, M.Si</li>
                                    <li>Ir. Rana Yusuf Nasir, IPM, GP</li>
                                    <li>Ardzuna Sinaga, S.T., B.Sc</li>
                                    <li>Dr. Ma'ruf Cahyono, S.H., M.H.</li>
                                    <li>Dr. H. Farid Wadjdy, M.Pd</li>
                                </ol>
                            </div>
                            <div class="accordion-header">
                                <div class="accordion-title">
                                    <span style="font-size:55px;color:#1c3144;" class="dropcap">3</span>
                                    <span style="display: block;margin-bottom:-2%;font-size:20px; color:#808285;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                                    <span style="font-size:20px; color:#808285;">Kompleks Perkantoran Lembaga Yudikatif</span>
                                </div>
                            </div>
                            <div class="accordion-content" style="color:#1c3144">
                                <ol style="margin-left:10%;">
                                    <li> Ar. Gregorius Antar Awal, IAI, AA</li>
                                    <li>Ir. Djoko Muryanto M.Sc</li>
                                    <li>Ar. Bambang Eryudhawan, IAI</li>
                                    <li>Dr. Ar. Woerjantari Kartidjo Soedarsono, M.T., IAI</li>
                                    <li>Ir. Budi Faisal, MAUD, MLA, Ph.D.</li>
                                    <li>Ir. Jimmy Siswanto Juwana, MSAE</li>
                                    <li>Rahman Andra Wijaya S.T., M.T</li>
                                    <li>Prof. Dr. M. Guntur Hamzah, S.H., M.H</li>
                                    <li>Billy Mambrasar, ST., B.Sc., MBA., M.Sc</li>
                            </div>
                            <div class="accordion-header">
                                <div class="accordion-title">
                                    <span style="font-size:55px;color:#1c3144;" class="dropcap">4</span>
                                    <span style="display: block;margin-bottom:-2%;font-size:20px; color:#808285;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                                    <span style="font-size:20px; color:#808285;">Kompleks Peribadatan</span>
                                </div>
                            </div>
                            <div class="accordion-content" style="color:#1c3144">
                                <ol style="margin-left:10%;">
                                    <li>Ar. H. Munichy B Edrees, M.Arch., IAI, AA</li>
                                    <li>Dr. Ir. Lana Winayanti, MCP</li>
                                    <li>Ar. Sonny Sutanto, M.Arch, IAI</li>
                                    <li>Ar. Panogu Silaban, IAI, AA</li>
                                    <li>Prof. Dr. Ir. Erni Setyowati, MT</li>
                                    <li>Ir. Bintang Agus Nugroho, GP</li>
                                    <li>Ar. Vincentius Hermawan, S.T., MMSt., IAI</li>
                                    <li>Drs. Mohammad Farid Wadjdi, M.M</li>
                                    <li>Ar. Dr. Jonny Wongso, M.T., IAI</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="row mt-5" class="d-none">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="mb-4">
                                <div class="card-body">
                                    <p class="card-title">
                                        <span style="font-size:55px;color:#1c3144;" class="dropcap">1</span>
                                        <span style="font-size:9px; color:#808285;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                                        <span style="font-size:9px; color:#808285;">Kompleks Istana Wakil Presiden</span>
                                    </p>
                                    <p class="card-text" style="color:#1c3144">
                                        Prof. Ar. Gunawan Tjahjono, M.Arch., Ph.D<br>
                                        Ir. Adjar Prayudi, MCM, MSc <br>
                                        Ir. Karnaya. MAUD <br>
                                        Georgius Budi Yulianto, S.T, M.T. <br>
                                        Prof. Dr. Ir. Wiendu Nuryanti, M.Arch., Ph.D. <br>
                                        Ir . Jatmika Adi Suryabrata, M.Sc ., Ph.D <br>
                                        Sibarani Sofian S.T., MUDD <br>
                                        Guntur Iman Nefianto, SE., S.H., M.H <br>
                                        Dr. Ir. Wicaksono Sarosa <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="mb-4">
                                <div class="card-body">
                                    <p class="card-title">
                                        <span style="font-size:55px;color:#1c3144;" class="dropcap">2</span>
                                        <span style="font-size:9px; color:#808285;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                                        <span style="font-size:9px; color:#808285;">Kompleks Perkantoran Lembaga Legislatif</span>
                                    </p>
                                    <p class="card-text" style="color:#1c3144">
                                    Ir. Andy Siswanto, M. Arch., Ph.D<br>
                                    Dr.Ir. Andreas Suhono, M.Sc<br>
                                    Ir. Didi Haryadi <br>
                                    Ir. Endy Subijono <br>
                                    Prof. Dr. H. Masjaya, M.Si<br>
                                    Ir. Rana Yusuf Nasir, IPM, GP <br>
                                    Ardzuna Sinaga, S.T., B.Sc <br>
                                    Dr. Ma'ruf Cahyono, S.H., M.H. <br>
                                    Dr. H. Farid Wadjdy, M.Pd <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="mb-4">
                                <div class="card-body">
                                    <p class="card-title">
                                        <span style="font-size:55px;color:#1c3144;" class="dropcap">3</span>
                                        <span style="font-size:9px; color:#808285;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                                        <span style="font-size:9px; color:#808285;">Kompleks Perkantoran Lembaga Yudikatif</span>    
                                    </p>
                                    <p class="card-text" style="color:#1c3144">
                                    Ir. Gregorius Antar Awal <br>
                                    Ir. Djoko Muryanto M.Sc <br>
                                    Ir. Bambang Eryudhawan <br>
                                    Dr. Ir. Woerjantari Kartidjo Soedarsono, M.T. <br>
                                    Ir. Budi Faisal, M.A.UD, MLA, Ph.D <br>
                                    Ir. Jimmy Siswanto Juwana, MSAE <br>
                                    Rahman Andra Wijaya S.T., M.T <br>
                                    Prof. Dr. M. Guntur Hamzah, S.H., M.H <br>
                                    Billy Mambrasar, ST., B.Sc., MBA., M.Sc<br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="mb-4">
                                <div class="card-body">
                                    <p class="card-title">
                                        <span style="font-size:55px;color:#1c3144;" class="dropcap">4</span>
                                        <span style="font-size:9px; color:#808285;">Sayembara Konsep Perancangan Kawasan dan Bangunan</span>
                                        <span style="font-size:9px; color:#808285;">Kompleks Peribadatan</span>   
                                    </p>
                                   <p class="card-text" style="color:#1c3144">
                                    Ir. Munichy B. Edrees, IAI <br>
                                    Dr. Ir. Lana Winayanti, MCP <br>
                                    Ir. Sonny Sutanto, M.Arch <br>
                                    Ir. Panogu Silaban, IAI <br>
                                    Prof. Dr. Ir. Erni Setyowati, MT <br>
                                    Ir. Bintang Nugroho <br>
                                    Vincentius Hermawan S.T., MMSt <br>
                                    Mohammad Farid Wadjdi <br>
                                    Dr. Jonny Wongso, S.T, M.T <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5" id="section-faq">
                    <div class="col-lg-6">
                        <img src="{{asset('faq.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Frequenty Asked Question (FAQ)</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        @foreach($kategori as $ktgr)
                        <h4  style="font-size:20px; color:#961a2d;">{{$ktgr->name}} <small>({{$ktgr->faq->count()}})</small></h4>
                        <div class="accordion accordion-border clearfix" data-state="closed">
                            @foreach($ktgr->faq as $faq)
                            <div class="accordion-header">
                                <div class="accordion-icon">
                                    <i class="accordion-closed icon-ok-circle"></i>
                                    <i class="accordion-open icon-remove-circle"></i>
                                </div>
                                <div class="accordion-title" style="font-size:16px; color:#1c3144;">
                                    {{$faq->question}}
                                </div>
                            </div>
                            <div class="accordion-content" style="font-size:16px; color:#808285;">{{$faq->answer}}</div>
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                </div><br>
                <div id="row mt-5">
                    <div class="text-center">
                        <h1 style="color:#1c3144;">Kontak Panitia Sayembara</h1>
                        <p>Kantor Satgas Pembangunan Infrastruktur Ibu Kota Negara
                            <br>
                            Gedung Utama Kementerian Pekerjaan Umum dan Perumahan Rakyat lantai dasar
                            <br>
                            Jl. Pattimura No.20, Kebayoran Baru, Jakarta Selatan, Jakarta 12110
                            <br>
                            Info Whatsapp: (+62) 8119393661
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>