<x-web-layout title="Selamat Datang">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row mb-5">
                    <div class="col-lg-6">
                        <img src="{{asset('latar-belakang.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <p>
                            Sejak tahun 2019, Kementerian PUPR telah memulai proses perencanaan IKN mengundang keterlibatan masyarakat umum dalam proses perwujudan IKN yang baru melalui Sayembara Gagasan Desain Kawasan Ibu Kota Negara. Sebagai tindak lanjut hasil sayembara, dilakukan proses Urban Design dengan kebutuhan populasi serta fungsi-fungsi di IKN, serta berkoordinasi dengan Kementerian/ Lembaga terkait penyusunan perencanaan spasial IKN.
                        </p>
                        <p>
                            Untuk mengundang keterlibatan masyarakat Indonesia dalam menyampaikan ide gagasannya, maka diselenggarakan Sayembara Kawasan dan Bangunan Gedung Ikonik yang berlokasi di Kawasan Inti Pusat Pemerintahan (KIPP - IKN) pada empat kompleks yaitu:
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <img alt="100%x180" src="{{asset('istana-wapres.png')}}" style="width:10%;float:left;">
                        <p>
                            <span style="font-size:60px;" class="dropcap">1</span>
                            <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                            <span style="font-size:20px;">Kompleks Istana Wakil Presiden</span>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <img alt="100%x180" src="{{asset('legistatif.png')}}" style="width:10%;float:left;">
                        <p>
                            <span style="font-size:60px;" class="dropcap">2</span>
                            <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                            <span style="font-size:20px;">Kompleks Perkantoran Lembaga Legislatif</span>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <img alt="100%x180" src="{{asset('yudikatif.png')}}" style="width:10%;float:left;">
                        <p>
                            <span style="font-size:60px;" class="dropcap">3</span>
                            <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                            <span style="font-size:20px;">Kompleks Perkantoran Lembaga Yudikatif</span>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <img alt="100%x180" src="{{asset('peribadatan.png')}}" style="width:10%;float:left;">
                        <p>
                            <span style="font-size:60px;" class="dropcap">4</span>
                            <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                            <span style="font-size:20px;">Kompleks Peribadatan</span>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="section-syarat">
                    <div class="col-lg-6">
                        <img src="{{asset('persyaratan-umum.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Persyaratan Umum</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        <ol>
                            <li>Sayembara ini bersifat terbuka bagi masyarakat umum, mahasiswa, arsitek, perancang kota, serta perencana wilayah dan kota secara perseorangan atau kelompok;</li>
                            <li>Peserta sayembara merupakan Warga Negara Indonesia (WNI) dan non-WNI</li>
                            <li>Peserta sayembara dapat mengatasnamakan perseorangan atau kelompok yang berjumlah maksimum 10 (sepuluh) orang, dengan peserta perseorangan, dan ketua kelompok bagi kelompok, harus WNI, yang memiliki SKA Madya arsitektur atau perencanaan kota yang masih berlaku;</li>
                            <li>Karya Peserta bukan merupakan hasil plagiasi atau hasil karya milik orang lain;</li>
                            <li>Seluruh materi sayembara menjadi milik penyelenggara;</li>
                            <li>Keputusan penjurian/penetapan pemenang tidak dapat diganggu gugat.</li>
                        </ol>
                    </div>
                </div>
                <div class="row mt-5" id="section-tata">
                    <div class="col-lg-6">
                        <img src="{{asset('tata-cara.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Tata Cara Pendaftaran</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        <ol>
                            <li>Pendaftaran dilakukan secara online melalui website pada menu “Masuk/Daftar”</li>
                            <li>Calon peserta dapat mendapatkan akun dengan mendaftarkan email.</li>
                            <li>Setelah mendaftar, peserta akan mendapatkan email tanggapan berisi User ID dan password.</li>
                            <li>Peserta dapat login/ masuk ke akun yang sudah terdaftar dengan menggunakan User ID dan password yang diperoleh dan mengunduh Kerangka Acuan Kerja (KAK) dan mengakses form pendaftaran. </li>
                            <li>Peserta mengisi informasi data peserta dengan benar dan lengkap serta mengunggah scan identitas (KTP/SIM/Paspor), NPWP, dan SKA/STRA.</li>
                            <li>Peserta memilih maksimal 2 (dua) jenis sayembara.</li>
                            <li>Setelah mendaftar dan terverifikasi, peserta mendapatkan Kartu Peserta Sayembara.</li>
                        </ol>
                    </div>
                </div>
                <div class="row mt-5" id="section-jadwal">
                    <div class="col-lg-6">
                        <img src="{{asset('jadwal-pelaksanaan.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Jadwal Pelaksanaan</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        <p>
                            <table width="100%">
                                <thead>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">14 – 28 Maret 2022</td>
                                        <td class="col-md-6">Pengumuman dan Pendaftaran Sayembara</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">24 Maret 2022</td>
                                        <td class="col-md-6">Aanwijzing</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">28 Maret</td>
                                        <td class="col-md-6">Aanwizjing Lapangan</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">28 Maret – 6 Mei 2022</td>
                                        <td class="col-md-6">Penyusunan Karya</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">6 Mei 2022</td>
                                        <td class="col-md-6">Batas Akhir Pemasukan Karya </td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">6 – 11 Mei 2022</td>
                                        <td class="col-md-6">Evaluasi Persyaratan Administrasi</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">13 – 18 Mei 2022</td>
                                        <td class="col-md-6">Penjurian Tahap 1</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">20 Mei 2022</td>
                                        <td class="col-md-6">Pengumuman Penjuran Tahap I </td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">23 Mei 2022</td>
                                        <td class="col-md-6">Presentasi Nominator dan Penjurian Tahap 2</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">27 Mei 2022</td>
                                        <td class="col-md-6">Proses Penetapan Pemenang</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">3 Juni 2022</td>
                                        <td class="col-md-6">Pengumuman Hasil Penjurian</td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-6" style="font-weight: bold;">6 Juni 2022</td>
                                        <td class="col-md-6">Pemberian Hadiah</td>
                                    </tr>
                                </thead>
                            </table>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="section-apresiasi">
                    <div class="col-lg-6">
                        <img src="{{asset('penghargaan.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading-block">
                            <h1>Penghargaan</h1>
                        </div>
                        <p>3 (tiga) karya terbaik akan diberikan penghargaan kepada masing-masing kategori sayembara, sebagai berikut:</p>
                        <table width="100%">
                                <thead>
                                    <tr>
                                        <td class="col-md-4" style="font-weight: bold;"><img width="20" src="{{asset('penghargaan-mini.png')}}" style="margin-right: 10px;"> Pemenang I</td>
                                        <td class="col-md-8">Hadiah Uang sebesar Rp. 500.000.000,-</td>
                                    </tr>
                                    <tr>
                                    <td class="col-md-4" style="font-weight: bold;"><img width="20" src="{{asset('penghargaan-mini.png')}}" style="margin-right: 10px;">Pemenang II</td>
                                        <td class="col-md-8">Hadiah Uang sebesar Rp. 250.000.000,-</td>
                                    </tr>
                                    <tr>
                                    <td class="col-md-4" style="font-weight: bold;"><img width="20" src="{{asset('penghargaan-mini.png')}}" style="margin-right: 10px;">Pemenang III</td>
                                        <td class="col-md-8">Hadiah Uang sebesar Rp. 100.000.000,-</td>
                                    </tr>
                                </thead>
                        </table>
                    </div>
                </div>
                <div class="row mt-5" id="section-tim">
                    <div class="col-lg-6">
                        <img src="{{asset('tim-juri.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Tim Juri</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        <div class="accordion clearfix" data-state="closed">
                            <div class="accordion-header">
                                <div class="accordion-title">
                                    <span class="dropcap">1</span>
                                    <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                                    <span style="font-size:20px;">Kompleks Istana Wakil Presiden</span>
                                </div>
                            </div>
                            <div class="accordion-content">
                                Prof. Ir. Gunawan Tjahjono, M.Arch., Ph.D <br>
                                Ir. Adjar Prayudi, MCM, MSc <br>
                                Karnaya, IAI <br>
                                Georgius Budi Yulianto, IAI <br>
                                Prof. Dr. Ir. Wiendu Nuryanti, M.Arch., Ph.D. <br>
                                Ir . Jatmika Adi Suryabrata, M.Sc ., Ph.D <br>
                                Sofian Sibarani <br>
                                Guntur Iman Ne anto (Setwapres) <br>
                                Wicaksono Saroso <br>
                            </div>
                            <div class="accordion-header">
                                <div class="accordion-title">
                                    <span class="dropcap">2</span>
                                    <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                                    <span style="font-size:20px;">Kompleks Perkantoran Lembaga Legislatif</span>
                                </div>
                            </div>
                            <div class="accordion-content">
                                Andy Siswanto <br>
                                Andreas Suhono <br>
                                Didi Haryadi <br>
                                Ir. Endy Subijono, IAI <br>
                                Prof. Dr. H. Masjaya, M.Si Rektor Unmul <br>
                                Ir. Rana Yusuf Nasir, IPM, GP <br>
                                Ardzuna Sinaga <br>
                                TBN Legislatif <br>
                                Dr. H. Farid Wajdi <br>
                            </div>
                            <div class="accordion-header">
                                <div class="accordion-title">
                                    <span class="dropcap">3</span>
                                    <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                                    <span style="font-size:20px;">Kompleks Perkantoran Lembaga Yudikatif</span>
                                </div>
                            </div>
                            <div class="accordion-content">
                                Yori Antar, IAI <br>
                                Djoko Muryanto <br>
                                Bambang Eryudhawan, IAI <br>
                                Dr. Ir. Woerjantari Kartidjo Soedarsono, MT, IAI <br>
                                Ir. Budi Faisal, M.A.UD, MLA, Ph.D <br>
                                Ir. Jimmy Juwana Siswanto <br>
                                Rahman Andra Wijaya,ST,MT <br>
                                TBN Yudikatif <br>
                                Billy Mambrasar <br>
                            </div>
                            <div class="accordion-header">
                                <div class="accordion-title">
                                    <span class="dropcap">4</span>
                                    <span style="display: block;margin-bottom:-2%;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                                    <span style="font-size:20px;">Kompleks Peribadatan</span>
                                </div>
                            </div>
                            <div class="accordion-content">
                                Ir. Munichy B. Edrees, IAI <br>
                                Lana Winayanti <br>
                                Sonny Sutanto, IAI <br>
                                Panogu Silaban, IAI <br>
                                Prof. Dr. Ir. Erni Setyowati, MT <br>
                                Bintang Nugroho <br>
                                Vincentius Hermawan, IAI <br>
                                Plt. Kapus Konghuchu Kementerian Agama <br>
                                Dr. Jonny Wongso, S.T, M.T <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="row mt-5">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="mb-4">
                                <div class="card-body">
                                    <p class="card-title">
                                        <span style="font-size:55px;color:#1c3144;" class="dropcap">1</span>
                                        <span style="font-size:9px; color:#808285;font-family: Myriad-Pro;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                                        <span style="font-size:13px; color:#808285;font-family: Myriad-Pro;">Kompleks Istana Wakil Presiden</span>
                                    </p>
                                    <p class="card-text" style="color:#1c3144">Prof. Ir. Gunawan Tjahjono, M.Arch., Ph.D<br>
                                        Ir. Adjar Prayudi, MCM, MSc <br>
                                        Karnaya, IAI <br>
                                        Georgius Budi Yulianto, IAI <br>
                                        Prof. Dr. Ir. Wiendu Nuryanti, M.Arch., Ph.D. <br>
                                        Ir . Jatmika Adi Suryabrata, M.Sc ., Ph.D <br>
                                        Sofian Sibarani <br>
                                        Guntur Iman Ne anto (Setwapres) <br>
                                        Wicaksono Saroso <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="mb-4">
                                <div class="card-body">
                                    <p class="card-title">
                                        <span style="font-size:55px;color:#1c3144;" class="dropcap">2</span>
                                        <span style="font-size:9px; color:#808285;font-family: Myriad-Pro;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                                        <span style="font-size:13px; color:#808285;font-family: Myriad-Pro;">Kompleks Perkantoran Lembaga Legislatif</span>
                                    </p>
                                    <p class="card-text" style="color:#1c3144">Yori Antar, IAI <br>
                                    Djoko Muryanto <br>
                                    Bambang Eryudhawan, IAI <br>
                                    Dr. Ir. Woerjantari Kartidjo Soedarsono, MT, IAI <br>
                                    Ir. Budi Faisal, M.A.UD, MLA, Ph.D <br>
                                    Ir. Jimmy Juwana Siswanto <br>
                                    Rahman Andra Wijaya,ST,MT <br>
                                    TBN Yudikatif <br>
                                    Billy Mambrasar <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="mb-4">
                                <div class="card-body">
                                    <p class="card-title">
                                        <span style="font-size:55px;color:#1c3144;" class="dropcap">3</span>
                                        <span style="font-size:9px; color:#808285;font-family: Myriad-Pro;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                                        <span style="font-size:13px; color:#808285;font-family: Myriad-Pro;">Kompleks Perkantoran Lembaga Yudikatif</span>    
                                    </p>
                                    <p class="card-text" style="color:#1c3144"> Ir. Munichy B. Edrees, IAI <br>
                                    Lana Winayanti <br>
                                    Sonny Sutanto, IAI <br>
                                    Panogu Silaban, IAI <br>
                                    Prof. Dr. Ir. Erni Setyowati, MT <br>
                                    Bintang Nugroho <br>
                                    Vincentius Hermawan, IAI <br>
                                    Plt. Kapus Konghuchu Kementerian Agama <br>
                                    Dr. Jonny Wongso, S.T, M.T <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="mb-4">
                                <div class="card-body">
                                    <p class="card-title">
                                        <span style="font-size:55px;color:#1c3144;" class="dropcap">4</span>
                                        <span style="font-size:9px; color:#808285;font-family: Myriad-Pro;">Sayembara Konsep Perancangan Bangunan dan Kawasan</span>
                                        <span style="font-size:13px; color:#808285;font-family: Myriad-Pro;">Kompleks Peribadatan</span>   
                                    </p>
                                   <p class="card-text" style="color:#1c3144">Prof. Ir. Gunawan Tjahjono, M.Arch., Ph.D<br>
                                        Ir. Adjar Prayudi, MCM, MSc <br>
                                        Karnaya, IAI <br>
                                        Georgius Budi Yulianto, IAI <br>
                                        Prof. Dr. Ir. Wiendu Nuryanti, M.Arch., Ph.D. <br>
                                        Ir . Jatmika Adi Suryabrata, M.Sc ., Ph.D <br>
                                        Sofian Sibarani <br>
                                        Guntur Iman Ne anto (Setwapres) <br>
                                        Wicaksono Saroso <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5" id="section-faq">
                    <div class="col-lg-6">
                        <img src="{{asset('faq.png')}}" class="w-100">
                    </div>
                    <div class="col-lg-6">
                        <div class="heading">
                            <h1>Frequenty Asked Question (FAQ)</h1>
                        </div>
                        <div class="heading-block">
                            <h1></h1>
                        </div>
                        @foreach ($kategori as $item)
                        <h4  style="font-size:20px; color:#961a2d;font-family: Myriad-Pro;">{{$item->name}} <small>({{$item->faq->count()}})</small></h4>
                        <div class="accordion accordion-border clearfix" data-state="closed">
                            @foreach($item->faq as $faq)
                            <div class="accordion-header">
                                <div class="accordion-icon">
                                    <i class="accordion-closed icon-ok-circle"></i>
                                    <i class="accordion-open icon-remove-circle"></i>
                                </div>
                                <div class="accordion-title" style="font-size:16px; color:#1c3144;font-family: Myriad-Pro;">
                                    {{$faq->question}}
                                </div>
                            </div>
                            <div class="accordion-content" style="font-size:16px; color:#808285;font-family: Myriad-Pro;">{{$faq->answer}}</div>
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                </div><br>
                <div id="row mt-5">
                    <div class="text-center">
                        <h1 style="color:#1c3144;">Panitia Sayembara</h1>
                        <p>Sekretariat Satgas Perencanaan Pembangunan Infrastruktur Ibu Kota Negara
                            <br>
                            Gedung Utama Kementrian Pekerjaan Umum dan Perumahan Rakyat Lantai Dasar
                            <br>
                            Jl.Pattimura No.20, Kebayoran Baru, Jakarta 12110
                            <br>
                            Info Whatsapp:(+62)812-8844-6036,(+62)812-9252-0738
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>