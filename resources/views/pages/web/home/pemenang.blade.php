<x-web-layout title="Pemenang">
    <section id="content">
        <div class="content-wrap">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class=" mb-6 text-center">Selamat Kepada Pemenang Sayembara IKN 2022</h3>
                        <div class="row">
                            <div class="col-lg-3">
                                <a href="{{asset('dokumen/1280-Hasil-Sayembara-Istana-Wakil-Presiden.pdf')}}" id="tombol_download_istana" class="card hoverable card-stretch border-0 mb-xl-8 " style="background-color: transparent;">
                                    <img src="{{asset('image/Istana-Wakil-Presiden.png')}}" class="w-100 img-sayembara p-4" alt="">
                                    <h5 class="title-sayembara text-center ">
                                        Unduh <br>
                                        penetapan pemenang kompleks<br>
                                        Istana Wakil Presiden
        
                                    </h5>
                                </a>
                            </div>
                            <div class="col-lg-3">
                                <a href="{{asset('dokumen/1280.1-Hasil-Sayembara-Kompleks-Perkantoran-Legislatif.pdf')}}" id="tombol_download_istana" class="card hoverable card-stretch border-0 mb-xl-8" style="background-color: transparent;">
                                    <img src="{{asset('image/Legislatif.png')}}" class="w-100 img-sayembara p-4" alt="">
                                    <h5 class="title-sayembara text-center ">
                                        Unduh <br>
                                        penetapan pemenang kompleks<br>
                                        Perkantoran Legislatif
                                    </h5>
                                </a>
                            </div>
                            <div class="col-lg-3">
                                <a href="{{asset('dokumen/1280.2-Hasil-Sayembara-Kompleks-Perkantoran-yudikatif.pdf')}}" id="tombol_download_istana" class="card hoverable card-stretch border-0 mb-xl-8" style="background-color: transparent;">
                                    <img src="{{asset('image/Yudikatif.png')}}" class="w-100 img-sayembara p-4" alt="">
                                    <h5 class="title-sayembara text-center ">
                                        Unduh <br>
                                        penetapan pemenang kompleks<br>
                                        Perkantoran Yudikatif 
                                    </h5>
                                </a>
                            </div>
                            <div class="col-lg-3">
                                <a href="{{asset('dokumen/1280.3-Hasil-Sayembara-Kompleks-Peribadatan.pdf')}}" id="tombol_download_istana" class="card hoverable card-stretch border-0 mb-xl-8" style="background-color: transparent;">
                                    <img src="{{asset('image/Peribadatan.png')}}" class="w-100 img-sayembara p-4" alt="">
                                    <h5 class="title-sayembara text-center  peribadatan">
                                        Unduh <br>
                                        penetapan pemenang kompleks<br>
                                        Kompleks Peribadatan
                                    </h5>
                                </a>
                            </div>
                            {{-- @if (date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime('2022-03-30 23:59:58')))
                                <div class="col-lg-12 mt-5">
                                    <div class="text-center">
                                        <a href="{{route('web.profile.index')}}" class="btn btn-lg btn-primary fw-bolder">Daftar / Ubah</a>
                                    </div>
                                </div>
                            @endif --}}
        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-web-layout>