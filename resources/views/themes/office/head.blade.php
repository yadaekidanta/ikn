<head>
    <title>{{config('app.name') . ': ' .$title ?? config('app.name')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta charset="utf-8" />
    <meta name="description" content="{{config('app.name') . ': ' .$title ?? config('app.name')}}" />
    <meta name="keywords" content="{{config('app.name') . ': ' .$title ?? config('app.name')}}" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
    <meta property="og:url" content="https://keenthemes.com/metronic" />
    <meta property="og:site_name" content="Keenthemes | Metronic" />
    <link rel="canonical" href="https://preview.keenthemes.com/metronic8" />
    <link rel="shortcut icon" href="{{asset('logo.jpg')}}" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href="{{asset('keenthemes/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('keenthemes/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{asset('keenthemes/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('keenthemes/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('amcharts4/core.js')}}"></script>
    <script src="{{asset('amcharts4/charts.js')}}"></script>
    <script src="{{asset('amcharts4/themes/animated.js')}}"></script>
    <!--end::Global Stylesheets Bundle-->
    <style>
        .chart_div {
            width: 100%;
            height: 500px;
        }
        .blockui .blockui-overlay{
			position: fixed;
		}
        /* ---------- start style untuk tombol upload --------- */
            .label-file{
                display: none;
            }
            
            .custom-file[type=file] + .label-file{
                display: inline-flex;
                flex-direction: column;
                border-radius: 6px;
            }
            .label-file.ellipsis{
                display: block !important;
                max-width: 250px !important;
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                margin-bottom: 4px !important;
            }

            .custom-file[type=file] {
                position: absolute;
                filter: alpha(opacity=0);
                opacity: 0;
                width: 100px;
            }
            .custom-file[type=file] + label {
                border: 1px solid #CCC;
                border-radius: 3px;
                text-align: left;
                padding: 10px;
                
                margin: 0;
                left: 0;
                position: relative;
                text-align: center;
            }
            .custom-file[type=file] + label:not(.ellipsis){
                width: 150px;
            }
    
        /* ---------- end style untuk tombol upload --------- */
    </style>
    @yield('custom_css')
</head>