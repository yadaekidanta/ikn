<header id="header" class="full-header transparent-header" data-sticky-class="not-dark" style="background-color:#FFFFFF;">
    <div id="header-wrap" style="background-color:#FFFFFF;">
        <div class="container" style="background-color:#FFFFFF;">
            <div class="header-row" style="background-color:#FFFFFF;">
                <div id="logo" class="me-lg-10">
                    <a href="javascript:;" class="standard-logo" data-dark-logo="{{asset('head.png')}}" style="width:100%;height:70px;">
                        <img src="{{asset('head.png')}}" alt="Kementerian Pekerjaan Umum dan Perumahan Rakyat">
                    </a>
                    <a href="javascript:;" class="retina-logo" data-dark-logo="{{asset('head.png')}}" style="width:100%;height:70px;">
                        <img src="{{asset('head.png')}}" alt="Kementerian Pekerjaan Umum dan Perumahan Rakyat">
                    </a>
                </div>
                <div class="header-misc">
                    {{-- <div id="top-search" class="header-misc-icon d-none">
                        <a href="#" id="top-search-trigger"><i class="icon-line-search"></i><i class="icon-line-cross"></i></a>
                    </div> --}}
                    <div id="top-cart" class="header-misc-icon d-none">
                        <a href="#" id="top-cart-trigger"><i class="icon-line-bag"></i><span class="top-cart-number">5</span></a>
                        <div class="top-cart-content">
                            <div class="top-cart-title">
                                <h4>Shopping Cart</h4>
                            </div>
                            <div class="top-cart-items">
                                <div class="top-cart-item">
                                    <div class="top-cart-item-image">
                                        <a href="#"><img src="images/shop/small/1.jpg" alt="Blue Round-Neck Tshirt" /></a>
                                    </div>
                                    <div class="top-cart-item-desc">
                                        <div class="top-cart-item-desc-title">
                                            <a href="#">Blue Round-Neck Tshirt with a Button</a>
                                            <span class="top-cart-item-price d-block">$19.99</span>
                                        </div>
                                        <div class="top-cart-item-quantity">x 2</div>
                                    </div>
                                </div>
                                <div class="top-cart-item">
                                    <div class="top-cart-item-image">
                                        <a href="#"><img src="images/shop/small/6.jpg" alt="Light Blue Denim Dress" /></a>
                                    </div>
                                    <div class="top-cart-item-desc">
                                        <div class="top-cart-item-desc-title">
                                            <a href="#">Light Blue Denim Dress</a>
                                            <span class="top-cart-item-price d-block">$24.99</span>
                                        </div>
                                        <div class="top-cart-item-quantity">x 3</div>
                                    </div>
                                </div>
                            </div>
                            <div class="top-cart-action">
                                <span class="top-checkout-price">$114.95</span>
                                <a href="#" class="button button-3d button-small m-0">View Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="primary-menu-trigger" style="float:left;">
                    <svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
                </div>
                <nav class="primary-menu">
                    <ul class="one-page-menu menu-container" data-easing="easeInOutExpo" data-speed="1250" data-offset="65">
                        <li class="menu-item">
                            <a href="#" class="menu-link" data-href="#section-syarat"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px; font-color:#231f20;letter-spacing: 0px;text-transform: capitalize;">Persyaratan Peserta</div></a>
                        </li>
                        <li class="menu-item">
                            <a href="#" class="menu-link" data-href="#section-tata"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px; font-color:#231f20;letter-spacing: 0px;text-transform: capitalize;">Tata Cara Pendaftaran</div></a>
                        </li>
                        <li class="menu-item">
                            <a href="#" class="menu-link" data-href="#section-jadwal"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;letter-spacing: 0px;text-transform: capitalize;">Jadwal Pelaksanaan</div></a>
                        </li>
                        <li class="menu-item">
                            <a href="#" class="menu-link" data-href="#section-apresiasi"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;letter-spacing: 0px;text-transform: capitalize;">Penghargaan</div></a>
                        </li>
                        <li class="menu-item">
                            <a href="#" class="menu-link" data-href="#section-tim"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;letter-spacing: 0px;text-transform: capitalize;">Tim Juri</div></a>
                        </li>
                        <li class="menu-item">
                            <a href="#" class="menu-link" data-href="#section-faq"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;letter-spacing: 0px;text-transform: capitalize;">FAQ</div></a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="{{route('web.auth.index')}}">
                                <div class="menu-text button button-mini button-border button-rounded button-red" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;letter-spacing: 0px;text-transform: capitalize;">Masuk / Daftar</div>
                            </a>
                        </li>
                    </ul>
                    <ul class="menu-container d-none">
                        <li class="menu-item">
                            <a class="menu-link" href="index.html"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;">Unduh Panduan Singkat</div></a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="index.html"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;">Syarat dan Ketentuan</div></a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="index.html"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;">Tata Cara Pendaftaran</div></a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="index.html"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;">Jadwal</div></a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="index.html"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;">Penghargaan</div></a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="index.html"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;">Tim Juri</div></a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="index.html"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;">FAQ</div></a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="{{route('web.auth.index')}}"><div class="menu-text" style="font-family: 'Lato', sans-serif; font-size:15px;font-color:#231f20;">Masuk / Daftar</div></a>
                        </li>
                    </ul>
                </nav>
                <form class="top-search-form" action="search.html" method="get">
                    <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter.." autocomplete="off">
                </form>
            </div>
        </div>
    </div>
    <div class="header-wrap-clone"></div>
</header>