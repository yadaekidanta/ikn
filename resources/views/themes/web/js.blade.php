<script src="{{asset('semicolon/js/jquery.js')}}"></script>
<script src="{{asset('semicolon/js/plugins.min.js')}}"></script>

<!-- Footer Scripts
============================================= -->
<script src="{{asset('semicolon/js/functions.js')}}"></script>
<script>
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
	window.setTimeout("waktu()", 500);
 
	function waktu() {
		var waktu = new Date();
		setTimeout("waktu()", 500);
        document.getElementById('clock').innerHTML = checkTime(waktu.getHours()) + ":" + checkTime(waktu.getMinutes()) + ":" + checkTime(waktu.getSeconds()) + " WIB";
	}
</script>
<script type='text/javascript'>
    var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    var thisDay = date.getDay(),
        thisDay = myDays[thisDay];
    var yy = date.getYear();
    var year = (yy < 1000) ? yy + 1900 : yy;
    document.getElementById("waktu").innerHTML = thisDay + ', ' + day + ' ' + months[month] + ' ' + year;
    // document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
</script>