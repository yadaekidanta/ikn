<!DOCTYPE html>
<html dir="ltr" lang="en-US">
@include('themes.web.head')

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		<div id="top-bar" class="transparent-topbar" style="background-color:#282465;">
			<div class="container clearfix">

				<div class="row justify-content-between">
					<div class="col-12 col-md-auto">

						<!-- Top Links
						============================================= -->
						<div class="top-links">
							<ul class="top-links-container">
								<li class="top-links-item">
									<a href="javascript:;">
										<span class="text-white" id="waktu"></span>
									</a>
								</li>
								<li class="top-links-item">
									<a href="javascript:;">
										<span class="text-white" id="clock"></span>
									</a>
								</li>
								{{-- <li class="top-links-item"><a href="demo-seo-faqs.html">FAQs</a></li>
								<li class="top-links-item"><a href="demo-seo-contact.html">Contact</a></li>
								<li class="top-links-item"><a href="#"><img src="demos/seo/images/flags/eng.png" alt="Lang">Eng</a>
									<ul class="top-links-sub-menu">
										<li class="top-links-item"><a href="#"><img src="demos/seo/images/flags/fre.png" alt="Lang">French</a></li>
										<li class="top-links-item"><a href="#"><img src="demos/seo/images/flags/ara.png" alt="Lang">Arabic</a></li>
										<li class="top-links-item"><a href="#"><img src="demos/seo/images/flags/tha.png" alt="Lang">Thai</a></li>
									</ul>
								</li> --}}
							</ul>
						</div><!-- .top-links end -->

					</div>
					
					<div class="col-12 col-md-auto dark d-none">
						<!-- Top Social
						============================================= -->
						<ul id="top-social">
							<li><a href="https://facebook.com/semicolonweb" class="si-facebook" target="_blank"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
							<li><a href="https://twitter.com/__semicolon" class="si-twitter" target="_blank"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
							<li><a href="https://youtube.com/semicolonweb" class="si-youtube" target="_blank"><span class="ts-icon"><i class="icon-youtube"></i></span><span class="ts-text">Youtube</span></a></li>
							<li><a href="https://instagram.com/semicolonweb" class="si-instagram" target="_blank"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
							<li><a href="tel:+10.11.85412542" class="si-call"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+10.11.85412542</span></a></li>
							<li><a href="mailto:info@canvas.com" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">info@canvas.com</span></a></li>
						</ul><!-- #top-social end -->

					</div>
				</div>

			</div>
		</div>
		<!-- Header
		============================================= -->
		@include('themes.web.header')
		<!-- #header end -->
		<img src="{{asset('banner.png')}}" class="img-fluid w-100">
		@php
			$model = \App\Models\RunningText::first();
		@endphp
        @if($model)
		<marquee style="background-color:#282465;width:100%;height:29px;">
			<h1 class="text-white" style="font-size: 18px;font-weight: initial;">{!! $model->name !!}</h1>
		</marquee>
        @endif
		{{-- include('themes.web.slider') --}}

		<!-- Content
		============================================= -->
        {{$slot}}
		<!-- #content end -->

		<!-- Footer
		============================================= -->
		@include('themes.web.footer')
        <!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- JavaScripts
	============================================= -->
	@include('themes.web.js')
	@yield('custom_js')
</body>
</html>