<section id="slider" class="slider-element slider-parallax swiper_wrapper min-vh-50">
    <div class="slider-inner">

        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                <div class="swiper-slide dark">
                    <div class="container">
                        <div class="slider-caption slider-caption-center">
                            <h2 data-animate="fadeInUp"></h2>
                            <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200"></p>
                        </div>
                    </div>
                    <div class="swiper-slide-bg" style="background-image: url('{{asset('banner.png')}}');background-repeat:no-repeat;background-size:contain;background-position:center;"></div>
                </div>
            </div>
        </div>
    </div>
</section>