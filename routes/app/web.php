<?php

use app\Models\User;
use App\Http\Controllers\Admin\Karya;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\RegionalController;
use App\Http\Controllers\Web\AuthController;
use App\Http\Controllers\Web\MainController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Web\KaryaController;
use App\Http\Controllers\Penilai\KaryaController AS PenilaiKaryaController;
use App\Http\Controllers\Admin\TeamController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\KetuaController;
use App\Http\Controllers\Web\AnggotaController;
use App\Http\Controllers\Web\ProfileController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Web\FinishingController;
use App\Http\Controllers\Web\KaryaFileController;
use App\Http\Controllers\Web\PerusahaanController;
use App\Http\Controllers\Admin\FaqCategoryController;
use App\Http\Controllers\Admin\RunningTextController;
use App\Http\Controllers\Web\KategoriSayembaraController;
use App\Http\Controllers\Panitia\UserVerificationController;
use App\Http\Controllers\Admin\UserVerificationController as AdminVerificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['domain' => ''], function() {
    Route::prefix('')->name('web.')->group(function(){
        Route::get('',[MainController::class, 'index'])->name('index');
        Route::get('pemenang',[MainController::class, 'pemenang'])->name('pemenang');
        Route::get('reload-captcha-login', [MainController::class, 'captcha_login'])->name('captcha-login');
        Route::get('reload-captcha-register', [MainController::class, 'captcha_register'])->name('captcha-register');
        Route::prefix('auth')->name('auth.')->group(function(){
            Route::get('',[AuthController::class, 'index'])->name('index');
            Route::post('login',[AuthController::class, 'do_login'])->name('login');
            Route::post('register',[AuthController::class, 'do_register'])->name('register');
            Route::get('verify/{auth:email}',[AuthController::class, 'do_verify'])->name('verify');
        });
        Route::middleware(['auth'])->group(function(){
            Route::bind('id_lulus', function($id){
                $data =  User::findOrFail($id)->verifikator()
                                ->whereRaw('LOWER(TRIM(st)) = ?', ['lulus verifikasi'])
                                ->first();
                return (empty($data)) ? abort('404') : $data->user_id;
            });

            Route::prefix('penilai')->name('penilai.')->group(function(){
                Route::prefix('karya')->name('karya.')->group(function(){
                    Route::get('', [PenilaiKaryaController::class, 'index'])->name('index');
                    Route::get('{karya}/show', [PenilaiKaryaController::class, 'show'])->name('show');
                    Route::get('/{kategori}', [PenilaiKaryaController::class, 'index'])->where('kategori', 'wapres|yudikatif|legislatif|peribadatan')->name('kategori');
                    Route::match(['get', 'post'],'/verifikasi/{jenis}/{penilaian}/{id_lulus}', [PenilaiKaryaController::class, 'verifikasi'])
                    ->where('jenis', 'wapres|yudikatif|legislatif|peribadatan')
                    ->where('penilaian', 'reset|lulus|tidak_lulus')
                    ->name('verifikasi');
                });
            });
            Route::prefix('admin')->name('admin.')->group(function(){
                Route::get('user-verification/{userVerification}/{sayembara_name}/unduh',[AdminVerificationController::class, 'unduh'])->name('user-verification.unduh');
                Route::get('user-verification/export',[AdminVerificationController::class, 'export_pdf'])->name('user-verification.export');
                Route::get('user-verification/email',[AdminVerificationController::class, 'send_mail'])->name('user-verification.email');
                Route::resource('user-verification', AdminVerificationController::class);
                Route::patch('user-verification/{userVerification}/cancel',[AdminVerificationController::class, 'cancel'])->name('user-verification.cancel');
                Route::get('user-verification/{userVerification}/show',[AdminVerificationController::class, 'show'])->name('user-verification.show');
                
                Route::middleware(['admin'])->group(function(){
                    Route::prefix('karya')->name('karya.')->group(function(){
                        Route::get('{fileTransaksi}/edit-karya',[Karya::class, 'edit_karya'])->name('edit_karya');
                        Route::patch('{fileTransaksi}/update-karya',[Karya::class, 'update_karya'])->name('update_karya');
                        Route::get('{transaksi}/edit-urut',[Karya::class, 'edit_urut'])->name('edit_urut');
                        Route::patch('{transaksi}/update-urut',[Karya::class, 'update_urut'])->name('update_urut');
                        Route::get('', [Karya::class, 'index'])->name('index');
                        Route::get('/{kategori}', [Karya::class, 'index'])->where('kategori', 'wapres|yudikatif|legislatif|peribadatan')->name('kategori');
                        Route::match(['get', 'post'],'/verifikasi/{jenis}/{penilaian}/{id_lulus}', [Karya::class, 'verifikasi'])
                        ->where('jenis', 'wapres|yudikatif|legislatif|peribadatan')
                        ->where('penilaian', 'reset|lulus|tidak_lulus')
                        ->name('verifikasi');
                    });
                });
            });
            
            Route::middleware(['panitia'])->group(function(){
                Route::prefix('panitia')->name('panitia.')->group(function(){

                    Route::prefix('karya')->name('karya.')->group(function(){
                        Route::get('', [Karya::class, 'index'])->name('index');
                        Route::get('/{kategori}', [Karya::class, 'index'])->where('kategori', 'wapres|yudikatif|legislatif|peribadatan')->name('kategori');
                        Route::match(['get', 'post'],'/verifikasi/{jenis}/{penilaian}/{id_lulus}', [Karya::class, 'verifikasi'])
                        ->where('jenis', 'wapres|yudikatif|legislatif|peribadatan')
                        ->where('penilaian', 'reset|lulus|tidak_lulus')
                        ->name('verifikasi');
                    });
                });
            });

            Route::resource('user-verification', UserVerificationController::class);
            Route::get('user-verification/{user}/create',[UserVerificationController::class, 'create'])->name('user-verification.create');
            Route::get('user-verification/{userVerification}/show',[UserVerificationController::class, 'show'])->name('user-verification.show');
            Route::patch('user-verification/{userVerification}/update',[UserVerificationController::class, 'update'])->name('user-verification.update');
            Route::patch('user-verification/{userVerification}/verify',[UserVerificationController::class, 'verify'])->name('user-verification.verify');
            Route::patch('user-verification/{userVerification}/nverify',[UserVerificationController::class, 'nverify'])->name('user-verification.nverify');
            Route::patch('user-verification/{userVerification}/cancel',[UserVerificationController::class, 'cancel'])->name('user-verification.cancel');
            Route::resource('faq-category', FaqCategoryController::class);
            Route::resource('faq', FaqController::class);
            Route::resource('running-text', RunningTextController::class);
            Route::resource('user', UserController::class);
            // Route::redirect('/','dashboard');
            Route::get('download/{id}',[MainController::class, 'download'])
            ->name('download')
            ->where(['id' => '[0-9]+']);
            
            Route::get('karya/{karya}/edits',[KaryaController::class, 'edits'])->name('karya.edits');
            Route::resource('karya', KaryaController::class);
            Route::post('karya-file/ubah',[KaryaFileController::class, 'ubah'])->name('karya-file.ubah');
            Route::get('karya-file/get_file',[KaryaFileController::class, 'get_file'])->name('karya-file.get_file');
            Route::get('karya-file/check_step',[KaryaFileController::class, 'check_step'])->name('karya-file.check_step');
            Route::resource('karya-file', KaryaFileController::class);
            // Route::get('finishing',[FinishingController::class, 'index'])->name('finishing');
            // Route::get('kategori-sayembara',[KategoriSayembaraController::class, 'index'])->name('kategori-sayembara.index');
            Route::get('dashboard',[MainController::class, 'dashboard'])->name('dashboard');
            Route::get('info',[MainController::class, 'information'])->name('info');
            Route::prefix('statistik')->name('statistik.')->group(function(){
                Route::get('',[MainController::class, 'statistik'])->name('index');
                Route::get('export-sayembara',[MainController::class, 'export_sayembara'])->name('export-sayembara');
            });

            Route::prefix('StepUp')->name('StepUp.')->group(function(){
                Route::get('', [MainController::class, 'StepUp'])->name('index');
                Route::get('agreement', [MainController::class, 'agreement'])->name('agreement');
                Route::get('agreement/download/{user}', [MainController::class, 'ZipDownload'])->name('zip-download');

                Route::get('download-berita/{jenis}', [MainController::class, 'download_berita'])->where('jenis', 'berita|wapres|yudikatif|legislatif|peribadatan')->name('download_berita');
            });
            // Route::get('StepUp',[MainController::class, 'StepUp'])->name('StepUp');
            Route::prefix('kartu-peserta')->name('kartu.')->group(function(){
                Route::get('/{sayembara_name}',[MainController::class, 'kartu_peserta'])->name('kartu_peserta');
            });
            

            // Route::get('statistik',[MainController::class, 'statistik'])->name('statistik');
            // Route::get('statistik/download',[MainController::class, 'statistik'])->name('statistik.expo');

            // Route::get('finishing',[FinishingController::class, 'index'])->name('finishing');
            
            
            Route::resource('ketua', KetuaController::class);
            Route::get('export-ketua',[KetuaController::class, 'export'])->name('ketua.export');
            Route::resource('company', CompanyController::class);
            Route::resource('team', TeamController::class);
            Route::get('dashboard-visitor',[MainController::class, 'dashboard_visitor'])->name('dashboard-visitor');
            /*
            Route::prefix('profile')->name('profile.')->group(function(){
                Route::get('',[ProfileController::class, 'index'])->name('index');
                Route::get('show',[ProfileController::class, 'show'])->name('show');
                Route::get('edit',[ProfileController::class, 'edit'])->name('edit');
                Route::patch('update',[ProfileController::class, 'update'])->name('update');
            });
            Route::prefix('kategori-sayembara')->name('kategori-sayembara.')->group(function(){
                Route::get('',[KategoriSayembaraController::class, 'index'])->name('index');
                // Route::get('create',[KategoriSayembaraController::class, 'create'])->name('create');
                Route::post('store',[KategoriSayembaraController::class, 'store'])->name('store');
            });

            Route::prefix('perusahaan')->name('perusahaan.')->group(function(){
                Route::get('',[PerusahaanController::class, 'index'])->name('index');
                Route::get('create',[PerusahaanController::class, 'create'])->name('create');
                Route::post('store',[PerusahaanController::class, 'store'])->name('store');
                Route::get('{perusahaan}/show',[PerusahaanController::class, 'show'])->name('show');
                Route::get('{perusahaan}/edit',[PerusahaanController::class, 'edit'])->name('edit');
                Route::patch('{perusahaan}/update',[PerusahaanController::class, 'update'])->name('update');
            });
            Route::prefix('anggota')->name('anggota.')->group(function(){
                Route::get('',[AnggotaController::class, 'index'])->name('index');
                Route::get('create',[AnggotaController::class, 'create'])->name('create');
                Route::post('store',[AnggotaController::class, 'store'])->name('store');
                Route::get('{anggota}/show',[AnggotaController::class, 'show'])->name('show');
                Route::get('{anggota}/edit',[AnggotaController::class, 'edit'])->name('edit');
                Route::patch('{anggota}/update',[AnggotaController::class, 'update'])->name('update');
            });

            Route::prefix('finishing')->name('finishing.')->group(function(){
                Route::get('',[FinishingController::class, 'index'])->name('index');
                // Route::get('create',[FinishingController::class, 'create'])->name('create');
                Route::get('ending',[FinishingController::class, 'end_request'])->name('ending');
            });
            */
            Route::get('logout',[AuthController::class, 'do_logout'])->name('auth.logout');
        });
        Route::post('city/get', [RegionalController::class, 'get_city'])->name('get_city');
    });
    Route::get('verification',[AuthController::class, 'verification'])->name('verification.notice');
    Route::get('view-clear', function(){
        Artisan::call('view:clear');
        return response()->json([
            'alert' => 'success',
            'message' => 'View Clear!'
        ]);
    })->name('view.clear');
    Route::get('migrate', function(){
        Artisan::call('migrate');
        return response()->json([
            'alert' => 'success',
            'message' => 'DB Migrate!'
        ]);
    })->name('db.migrate');
    Route::get('storage-link', function(){
        Artisan::call('storage:link');
        return response()->json([
            'alert' => 'success',
            'message' => 'Storage Linked!'
        ]);
    })->name('storage.link');
    Route::get('db-seed', function(){
        Artisan::call('db:seed');
        return response()->json([
            'alert' => 'success',
            'message' => 'DB Seed!'
        ]);
    })->name('db.seed');
});